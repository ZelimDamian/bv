#include "TetMesh.h"

using namespace bv;
using namespace bv::tethex;

void TetMesh::updateIndices()
{
	indices.clear();
	indices.reserve(hedra.size() * 12);

	for (int i = 0; i < hedra.size(); ++i)
	{
		Tet& t = hedra[i];

		indices.push_back(t.a);
		indices.push_back(t.b);
		indices.push_back(t.c);

		indices.push_back(t.a);
		indices.push_back(t.c);
		indices.push_back(t.d);

		indices.push_back(t.c);
		indices.push_back(t.b);
		indices.push_back(t.d);

		indices.push_back(t.b);
		indices.push_back(t.a);
		indices.push_back(t.d);
	}
}

void TetMesh::updateValences()
{
	nodeIndices.resize(hedra.size() * 4);

	for (int i = 0; i < hedra.size(); ++i)
	{
		nodeIndices[i * 4 + 0] = hedra[i].a;
		nodeIndices[i * 4 + 1] = hedra[i].b;
		nodeIndices[i * 4 + 2] = hedra[i].c;
		nodeIndices[i * 4 + 3] = hedra[i].d;
	}

	valences.resize(vertices.size());
	valenceIndices.resize(hedra.size() * 4);

	for (int i = 0; i < hedra.size(); ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			const int tetNodeId = i * 4 + j;
			const int node = nodeIndices[tetNodeId];
			valenceIndices[tetNodeId] = valences[node]++;
		}
	}
}

// copy the vertex positions to the surface
void TetMesh::updateSurfaceVertices()
{
    auto& surfaceMesh = surface.ref();

	if (surfaceMesh.vertices.size() != vertices.size())
	{
		surfaceMesh.vertices.resize(vertices.size());
	}
    
    for (int i = 0; i < vertices.size(); ++i)
    {
		const glm::vec3& pos = vertices[i].get<TetMesh::TVertex::Pos>();
		const glm::vec3& nrm = vertices[i].get<TetMesh::TVertex::Nrm>();
	    const glm::vec4& col = vertices[i].get<TetMesh::TVertex::Col>();
		surfaceMesh.vertices[i].get<ColorMesh::TVertex::Pos>() = pos;
		surfaceMesh.vertices[i].get<ColorMesh::TVertex::Nrm>() = nrm;
//	    surfaceMesh.vertices[i].get<ColorMesh::TVertex::Col>() = col;
    }
}

void TetMesh::updatePositions()
{
	Meshable::updatePositions();
	surface->positions = positions;
}

