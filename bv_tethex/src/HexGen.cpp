#include "HexGen.h"

using namespace bv;
using namespace bv::tethex;

HexMesh::Id HexGen::cube(glm::vec3 size, glm::ivec3 dims)
{
	const int WIDTH = dims.x;
	const int HEIGHT = dims.y;
	const int DEPTH = dims.z;
	const int SIZE = WIDTH * HEIGHT * DEPTH;
	const int WIDTHDEPTH = WIDTH * DEPTH;
	const int TOTALSIZE = WIDTHDEPTH * HEIGHT;

	HexMesh::Id id = HexMesh::create();
	auto mesh = HexMesh::get(id);

	// the direction of the main axis of the beam
	glm::vec3 dirx (1.0f, 0.0f, 0.0f);
	glm::vec3 diry (0.0f, 1.0f, 0.0f);
	glm::vec3 dirz (0.0f, 0.0f, 1.0f);

	float elementWidth =  size.x / (dims.x - 1);
	float elementHeight = size.y / (dims.y - 1);
	float elementDepth = size.z / (dims.z - 1);

	// allocate some space
	mesh->vertices.reserve(TOTALSIZE);

	// center offset
	glm::vec3 offset = -size / 2.0f;

	// fill the position buffer
	for (size_t i = 0; i < HEIGHT; i ++)
	{
		for (size_t j = 0; j < DEPTH; j++)
		{
			for (size_t k = 0; k < WIDTH; k++)
			{
				const glm::vec3 point = offset +
						dirx * elementWidth * (float)k +
						dirz * elementDepth * (float)j +
						diry * elementHeight * (float)i;

				mesh->vertex(point);
			}
		}
	}

	// create hexhedral elements comprising the beam
	for (int i = 0; i < HEIGHT - 1; i++)
	{
		for (int j = 0; j < DEPTH - 1; j++)
		{
			for (int k = 0; k < WIDTH - 1; k++)
			{
				// create a finite element with indices
				int indices[8] = {
						(i + 0) * WIDTHDEPTH + (j + 0) * WIDTH + k,
						(i + 0) * WIDTHDEPTH + (j + 0) * WIDTH + k + 1,
						(i + 1) * WIDTHDEPTH + (j + 0) * WIDTH + k + 1,
						(i + 1) * WIDTHDEPTH + (j + 0) * WIDTH + k,
						(i + 0) * WIDTHDEPTH + (j + 1) * WIDTH + k,
						(i + 0) * WIDTHDEPTH + (j + 1) * WIDTH + k + 1,
						(i + 1) * WIDTHDEPTH + (j + 1) * WIDTH + k + 1,
						(i + 1) * WIDTHDEPTH + (j + 1) * WIDTH + k
				};

				mesh->hedra.emplace_back(indices);
			}
		}
	}

	return id;
}


