#include "TetraGen.h"

#include <tetgen.h>
#include <TetHexMeshUtils.h>

#define MIN_VOLUME 0.00000000f
#define MAX_ASPECT 10.00000000f

using namespace bv;
using namespace bv::tethex;

TetMesh::Id TetraGen::generate(Mesh::Id meshId, float maxVolume, float minRatio, bool nobisect)
{
    tetgenio in;
    tetgenio::facet *f;
    tetgenio::polygon *p;

    // All indices start from 0.
    in.firstnumber = 0;

    auto mesh = Mesh::get(meshId);

    in.numberofpoints = mesh->vertices.size();
    in.pointlist = new REAL[in.numberofpoints * 3];

    for (int j = 0; j < in.numberofpoints; ++j)
    {
        in.pointlist[j * 3 + 0] = mesh->vertices[j].get<0>()[0];
        in.pointlist[j * 3 + 1] = mesh->vertices[j].get<0>()[1];
        in.pointlist[j * 3 + 2] = mesh->vertices[j].get<0>()[2];
    }

    in.numberoffacets = mesh->triangles.size();
    in.facetlist = new tetgenio::facet[in.numberoffacets];
    in.facetmarkerlist = new int[in.numberoffacets];

    for (int l = 0; l < in.numberoffacets; ++l)
    {
        in.facetmarkerlist[l] = 0;

        f = &in.facetlist[l];
        f->numberofholes = 0;
        f->holelist = NULL;
        f->numberofpolygons = 1;
        f->polygonlist = new tetgenio::polygon[f->numberofpolygons];

        for (int i = 0; i < f->numberofpolygons; ++i)
        {
            p = &f->polygonlist[i];
            p->numberofvertices = 3;
            p->vertexlist = new int[p->numberofvertices];
            p->vertexlist[0] = mesh->triangles[l].vi[0];
            p->vertexlist[1] = mesh->triangles[l].vi[1];
            p->vertexlist[2] = mesh->triangles[l].vi[2];
        }
    }

    tetgenbehavior beh;
    beh.plc = 1;
    beh.quiet = true;
    beh.quality = 1;
	beh.nobisect = nobisect;
    beh.minratio = minRatio;
    beh.fixedvolume = 1;
    beh.maxvolume = maxVolume;
	beh.neighout = 2; // wants > 1 FSR

    // output for the generated mesh
    tetgenio out;

    // do the actual mesh generation
    tetrahedralize(&beh, &in, &out);

    // create a new tetrahedral mesh to store the generated result
    auto tetmesh = TetMesh::get(TetMesh::create());

    // allocate the required memeory for vertices
    tetmesh->resize(out.numberofpoints);

    // copy the generated vertex data into the newly created mesh
    for (int j = 0; j < out.numberofpoints; ++j)
    {
        tetmesh->vertices[j].get<TetMesh::TVertex::Pos>()[0] = (float) out.pointlist[j * 3 + 0];
        tetmesh->vertices[j].get<TetMesh::TVertex::Pos>()[1] = (float) out.pointlist[j * 3 + 1];
        tetmesh->vertices[j].get<TetMesh::TVertex::Pos>()[2] = (float) out.pointlist[j * 3 + 2];
    }

	tetmesh->updatePositions();

    // allocate the required memory for the tetraheral indices
//    tetmesh->hedra.resize(out.numberoftetrahedra);

    // copy all the indices into the new mesh
    for (int k = 0; k < out.numberoftetrahedra; ++k)
    {
	    auto tet = Tet {
			    out.tetrahedronlist[k * 4 + 0],
			    out.tetrahedronlist[k * 4 + 1],
			    out.tetrahedronlist[k * 4 + 2],
			    out.tetrahedronlist[k * 4 + 3]
	    };

	    float aspect = TetMeshUtils::aspect(tetmesh->id(), tet);

//	    float volume = TetMeshUtils::volume(tetmesh->id(), tet);
//	    if(volume > MIN_VOLUME)
	    if(aspect < MAX_ASPECT)
	    {
		    tetmesh->hedra.push_back(tet);
	    }

//	    tetmesh->hedra[k].indices[0] =   out.tetrahedronlist[k * 4 + 0];
//        tetmesh->hedra[k].indices[1] = out.tetrahedronlist[k * 4 + 1];
//        tetmesh->hedra[k].indices[2] = out.tetrahedronlist[k * 4 + 2];
//        tetmesh->hedra[k].indices[3] = out.tetrahedronlist[k * 4 + 3];
    }


    // create the surface mesh
    tetmesh->updateSurfaceVertices();

    // create all the surface triangles on the surface
    for (int i = 0; i < out.numberoftrifaces; ++i)
    {
        tetmesh->surface->tri(out.trifacelist[i * 3 + 0], out.trifacelist[i * 3 + 1], out.trifacelist[i * 3 + 2]);
    }

//    tetmesh->surfaceTetMapping.resize(tetmesh->surface->triangles.size());
//
//    // create the mapping between surface and volume
//    for (int i = 0; i < tetmesh->surface->triangles.size(); ++i)
//    {
//        int tet1 = out.adjtetlist[i*2 + 0];
//        int tet2 = out.adjtetlist[i*2 + 1];
//
//        // take the tet index
//        if(tet1 < 0)
//        {
//            tetmesh->surfaceTetMapping[i] = tet2;
//        }
//        else
//        {
//            tetmesh->surfaceTetMapping[i] = tet1;
//        }
//    }

    // generate indicies that are used for rendering
    tetmesh->updateIndices();
	tetmesh->updateValences();
	tetmesh->surface->updatePositions();
	tetmesh->surface->updateIndices();
	tetmesh->surface->setupConnections();
    tetmesh->surface->updateAllNormals();

    return tetmesh->id();
}
