#include "HexMesh.h"

using namespace bv;
using namespace bv::tethex;

void HexMesh::updateIndices()
{
	// cube wireframe indices
	const int cubeInd[24] = {
		0,1,1,2,2,3,3,0,
		3,7,2,6,0,4,1,5,
		4,5,5,6,6,7,7,4
	};

	indices.reserve(24 * hedra.size());

	for (int i = 0; i < hedra.size(); ++i)
	{
		for (int j = 0; j < 24; ++j)
		{
			const Hex& hex = hedra[i];
			indices.push_back(hex.indices[cubeInd[j]]);
		}
	}
}

void HexMesh::updateSurfaceVertices()
{
	// copy the vertex positions to the surface
	surface->vertices.clear();
	surface->vertices.reserve(vertices.size());

	for (int i = 0; i < vertices.size(); ++i)
	{
		surface->vertices.emplace_back(vertices[i].get<TVertex::Pos>());
	}
}

