#pragma once

#include "FileReader.h"
#include "HexMesh.h"
#include <sstream>

namespace bv
{
	namespace tethex
	{
		class LBIELoader
		{
		public:
			static HexMesh::Id hex(const std::string& filename)
			{
				// read full file and stream
				std::istringstream contents { FileReader::read("models/" + filename + ".raw") };

				size_t nodeCount, elCount;
				// buffer
				std::string line;

				// read first line with meta
				std::getline(contents, line);

				// read number of nodes
				c_scanf(line.c_str(), "%i %i", &nodeCount, &elCount);

				using namespace tethex;

				auto mesh = HexMesh::create();

				mesh->m_positions.resize(nodeCount);

				for (int i = 0; i < nodeCount; ++i)
				{
					// read node data
					std::getline(contents, line);
					auto& pos = mesh->m_positions[i];
					// parse node data
					c_scanf(line.c_str(), "%f %f %f", &pos.x, &pos.y, &pos.z);
				}

				mesh->updateVertices();

				mesh->hedra.resize(elCount);

				for (int i = 0; i < elCount; ++i)
				{
					// read node data
					std::getline(contents, line);

					int inds[8];

					// parse el data
					c_scanf(line.c_str(), "%i %i %i %i %i %i %i %i",
					        &inds[0], &inds[1], &inds[2], &inds[3], &inds[4], &inds[5], &inds[6], &inds[7]);

					mesh->hedra[i] = Hex { inds };
				}

				return mesh;
			}
		};
	}
}