#pragma once

#include <stdint.h>

namespace bv
{
    namespace tethex
    {
        class Tetrahedron
        {
        public:
            Tetrahedron() { }

            Tetrahedron(const int (&inds)[4]):
                a(inds[0]), b(inds[1]), c(inds[2]), d(inds[3]) { }

            Tetrahedron(int a, int b, int c, int d):
                a(a), b(b), c(c), d(d) { }

            union {
                struct { int a, b, c, d; };
                int indices[4];
            };
        };

        using Tet = Tetrahedron;
    }
}