#pragma once

#include "Tet.h"
#include "Store.h"
#include "RenderMesh.h"
#include "Meshable.h"

namespace bv
{
    namespace tethex
    {
        class TetMesh: public Storable<TetMesh>, public Meshable<PosNormColVertex>
        {
        public:

            TetMesh(TetMesh::Id id): StorableBase ( id )
			{
				surface = Mesh::create();
			}

            void updateIndices();
			// udpates all the vertex information of the surface mesh with the one from this polyhedral mesh
			void updateSurfaceVertices();

	        // update valence data
	        void updateValences();

	        void updatePositions();

            void setPositions(const VectorVec3& positions)
            {
                this->positions = positions;
                surface->positions = positions;
            }

            std::vector<Tet> hedra;
            // [0] containts index of tetrahedron adjesent to first surface triangle and so on
            std::vector<int> surfaceTetMapping;
            Mesh::Id surface;

	        std::vector<int> nodeIndices;
	        std::vector<int> valences;
	        std::vector<int> valenceIndices;

            rendering::RenderMesh::Id renderMeshId;
        };
    }
}