#pragma once

#include "Common.h"
#include "HexMesh.h"

namespace bv
{
	namespace tethex
	{
		class HexGen
		{
		public:
			static HexMesh::Id cube(glm::vec3 size, glm::ivec3 dims);
		};
	}
}

