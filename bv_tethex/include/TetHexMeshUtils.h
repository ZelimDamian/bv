#pragma once

#include "TetMesh.h"

namespace bv
{
    namespace tethex
    {
        struct TetMeshUtils
        {
            struct ShortestEdgeResult
            {
                float length;
                int elIdx;
            };

	        static float longest(tethex::TetMesh::Id mesh, int idx)
	        {
		        return longest(mesh, mesh->hedra[idx]);
	        }

	        static float longest(tethex::TetMesh::Id mesh, const Tet& tet)
	        {
		        float longest = FLT_MIN;

		        for (int j = 0; j < 4; ++j)
		        {
			        auto pos1 = mesh->positions[tet.indices[j]];

			        for (int k = j + 1; k < 4; ++k)
			        {
				        auto pos2 = mesh->positions[tet.indices[k]];

				        float length = glm::length(pos1 - pos2);

				        if (length > longest)
				        {
					        longest = length;
				        }
			        }
		        }

		        return longest;
	        }

	        static float aspect(tethex::TetMesh::Id mesh, int idx)
	        {
		        return aspect(mesh, mesh->hedra[idx]);
	        }

	        static float aspect(tethex::TetMesh::Id mesh, const Tet& tet)
	        {
		        auto v = volume(mesh, tet);
		        auto l = longest(mesh, tet);

		        return std::pow(l, 3.0f) / v;
	        }

            static float shortest(tethex::TetMesh::Id mesh, int idx)
            {
	            return shortest(mesh, mesh->hedra[idx]);
            }

	        static float shortest(tethex::TetMesh::Id mesh, const Tet& tet)
            {
                float shortest = FLT_MAX;

                for (int j = 0; j < 4; ++j)
                {
                    auto pos1 = mesh->positions[tet.indices[j]];

                    for (int k = j + 1; k < 4; ++k)
                    {
                        auto pos2 = mesh->positions[tet.indices[k]];

                        float length = glm::length(pos1 - pos2);

						if (length < shortest)
                        {
                            shortest = length;
                        }
                    }
                }

                return shortest;
            }

            inline static float det3(const glm::mat3& mat)
            {
                const float* A = &mat[0][0];

                return A[0] * A[4] * A[8] - A[0] * A[5] * A[7] - A[3] * A[1] * A[8]
                       + A[3] * A[2] * A[7] + A[6] * A[1] * A[5] - A[6] * A[2] * A[4];
            }

            static float volume(const tethex::TetMesh::Id mesh, int idx)
            {
	            const auto tet = mesh->hedra[idx];
	            return volume(mesh, tet);
            }

	        static float volume(const tethex::TetMesh::Id mesh, const Tet& tet)
            {
                auto o = mesh->positions[tet.a];

                auto a = mesh->positions[tet.b] - o;
                auto b = mesh->positions[tet.c] - o;
                auto c = mesh->positions[tet.d] - o;

                auto bc = glm::cross(b, c);

				return glm::abs(glm::dot(a, bc));
            }

            static ShortestEdgeResult findShortest(tethex::TetMesh::Id mesh)
            {
                float shortest = FLT_MAX;
                int idx = -1;

                for (int i = 0; i < mesh->hedra.size(); ++i)
                {
                    float distance = TetMeshUtils::shortest(mesh, i);

                    if(distance < shortest)
                    {
                        shortest = distance;
                        idx = i;
                    }
                }

                return { shortest, idx };
            }


	        static glm::vec3 center(TetMesh::Id handle, int i)
	        {
				return center(handle, handle->hedra[i]);
	        }

	        static glm::vec3 center(TetMesh::Id handle, const Tet& tet)
	        {
		        const auto& p1 = handle->positions[tet.a];
		        const auto& p2 = handle->positions[tet.b];
		        const auto& p3 = handle->positions[tet.c];
		        const auto& p4 = handle->positions[tet.d];

		        return (p1 + p2 + p3 + p4) / 4.0f;
	        }


        };

    }
}
