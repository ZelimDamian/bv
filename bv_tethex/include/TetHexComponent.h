#include "Component.h"
#include "RenderingSystem.h"
#include "Shader.h"
#include "TetMesh.h"
#include "TetraGen.h"
#include "MeshFactory.h"

namespace bv
{
    namespace tethex
    {
        class TetHexComponent : public Component<TetHexComponent>
        {
        public:
	        TetHexComponent(Id id, Entity::Id entity) : ComponentBase(id, entity) { }

	        static void init()
	        {
		        using namespace rendering;

		        Mesh::Id mesh = MeshFactory::cube(1.0f, 1.0f, 1.0f);
		        TetMesh::Id generated = TetraGen::generate(mesh);
		        RenderMesh::Id generatedRenderHandle = RenderMesh::create<TetMesh>(generated);

		        static auto& renderer = RenderingSystem::instance();

		        Shader::Id colorsShader = Shader::require("colors");
		        Uniform::Id uColor = Uniform::require(colorsShader, "uColor", UniformType::Vec4);

				renderer.onrender([=]
				{
					renderer.reset(Culling::NO, RenderMode::LINE_STRIP);

				    glm::vec3 translation = glm::vec3(0.0f, 0.0f, 0.0f);
				    glm::mat4 xform = glm::translate(glm::mat4(), translation);
				    renderer.setTransform(xform);

				    Shader::use(colorsShader);
				    Uniform::set(uColor, glm::vec4(1.0f, 1.0f, 1.0f, 0.0001f));

				    renderer.reset();
				    renderer.render(generatedRenderHandle);

				    Uniform::set(uColor, glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));

				    renderer.reset(Culling::NO, RenderMode::LINE_STRIP);
				    renderer.render(generatedRenderHandle);

				    Uniform::set(uColor, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
					renderer.reset(Culling::NO, RenderMode::POINTS);
					renderer.render(generatedRenderHandle);
				});
	        }

	        static std::string title() { return "TetHex Component"; }
        };
    }
}