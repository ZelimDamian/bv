#pragma once

#include <Component.h>
#include <Sphere.h>
#include <ColorLerper.h>

#include "TetMesh.h"
#include "TetHexMeshUtils.h"

namespace bv
{
	namespace tethex
	{
		class TetMeshQualityComponent: public Component<TetMeshQualityComponent>
		{
		public:

			enum class Mode
			{
				Aspect,
				Valence,
				EdgeLength,
				Volume
			};

			TetMeshQualityComponent(Id id, Entity::Id entity, TetMesh::Id mesh): ComponentBase(id, entity), m_mesh(mesh)
			{}

			static void init()
			{
				subscribeToUpdates();

				auto renderer = RenderingSystem::instance();

				auto onrender = [&, renderer]
				{
					using namespace rendering;

					for (int idx = 0; idx < components().size(); ++idx)
					{
						auto& comp = components(idx);

						if(!comp.m_active)
						{
							continue;
						}

						auto mesh = components(idx).m_mesh;

						std::vector<float> vertQualities(mesh->vertices.size());

						std::vector<float> tetQualities;
						tetQualities.reserve(mesh->vertices.size());

						float max = comp.m_max;
						float min = comp.m_min;

						if(comp.m_mode == Mode::Aspect)
						{
							tetQualities.resize(mesh->hedra.size());

							for (int i = 0; i < mesh->hedra.size(); ++i)
							{
								tetQualities[i] = TetMeshUtils::aspect(mesh, i);
							}

							max = *std::max_element(tetQualities.begin(), tetQualities.end());
							min = *std::min_element(tetQualities.begin(), tetQualities.end());

							// fill with large values
							for (int i = 0; i < vertQualities.size(); ++i)
							{
								vertQualities[i] = 0.0f;
							}
						}
						else if(comp.m_mode == Mode::Valence)
						{
							for (int i = 0; i < mesh->vertices.size(); ++i)
							{
								vertQualities[i] = mesh->valences[i];
							}

							max = *std::max_element(vertQualities.begin(), vertQualities.end());
							min = *std::min_element(vertQualities.begin(), vertQualities.end());
						}
						else if(comp.m_mode == Mode::EdgeLength)
						{
							std::vector<float> lengths(mesh->hedra.size());

							for (int i = 0; i < lengths.size(); ++i)
							{
								lengths[i] = tethex::TetMeshUtils::shortest(mesh, i);
							}

							max = *std::max_element(lengths.begin(), lengths.end());
							min = *std::min_element(lengths.begin(), lengths.end());

							// collect all values
							for (int i = 0; i < mesh->hedra.size(); ++i)
							{
								for (int j = 0; j < 4; ++j)
								{
									int nodeIndex = mesh->nodeIndices[i * 4 + j];

									vertQualities[nodeIndex] = glm::max(vertQualities[nodeIndex], lengths[i]);
								}
							}
						}
						else if(comp.m_mode == Mode::Volume)
						{
							tetQualities.resize(mesh->hedra.size());

							for (int i = 0; i < mesh->hedra.size(); ++i)
							{
								tetQualities[i] = TetMeshUtils::volume(mesh, i);
							}

							max = *std::max_element(tetQualities.begin(), tetQualities.end());
							min = *std::min_element(tetQualities.begin(), tetQualities.end());

							// fill with large values
							for (int i = 0; i < vertQualities.size(); ++i)
							{
								vertQualities[i] = 0.0f;
							}

//							// collect all values
//							for (int i = 0; i < mesh->hedra.size(); ++i)
//							{
//								for (int j = 0; j < 4; ++j)
//								{
//									int nodeIndex = mesh->nodeIndices[i * 4 + j];
//
//									vertQualities[nodeIndex] = glm::min(vertQualities[nodeIndex], volumes[i]);
//								}
//							}
						}

						if(comp.m_updateExtrema)
						{
							comp.m_max = max;
							comp.m_min = min;
						}

						if(max != min)
						{
							for (int i = 0; i < mesh->vertices.size(); ++i)
							{
								const auto value = vertQualities[i];
								const glm::vec4 color = comp.sample(value);

								mesh->vertices[i].set<TetMesh::TVertex::Col>(color);
							}
						}


						// if we have tet quality data
						if(!tetQualities.empty())
						{
							comp.m_tets->positions.resize(tetQualities.size());

							for (int i = 0; i < mesh->hedra.size(); ++i)
							{
								auto center = TetMeshUtils::center(mesh, i);
								if(!comp.m_filter || (tetQualities[i] <= comp.m_max && tetQualities[i] >= comp.m_min))
								{
									comp.m_tets->positions[i] = center;
								}
								else
								{
									comp.m_tets->positions[i] = { };
								}
							}

							comp.m_tets->updateVertices();

							for (int i = 0; i < tetQualities.size(); i++)
							{
								const auto value = tetQualities[i];
								const auto color = comp.sample(value);

								comp.m_tets->vertices[i].set<TetMesh::TVertex::Col>(color);
							}

							// render the mesh
							renderer->reset(Culling::NO, RenderMode::POINTS);
							Shader::use("diffuse_colors");

							const glm::mat4& mat = comp.m_entity->transform();
							renderer->transform(mat);

							renderer->submit(RenderMesh::create(comp.m_tets));
						}
						else
						{
							///////////////////////////////////////////////////////////////////////////////////////
							/// update render mesh
							///////////////////////////////////////////////////////////////////////////////////////

							auto renderMesh = RenderMesh::create(mesh);

							// render the mesh
							renderer->reset(Culling::NO, RenderMode::LINES);
							Shader::use("diffuse_colors");

							const glm::mat4& mat = comp.m_entity->transform();
							renderer->transform(mat);
							renderer->submit(renderMesh);
						}
					}
				};

				renderer->onrender(onrender);
			}

			void update() { }

			void attachGUI()
			{
				auto panel = this->gui();

				panel->check("Auto update", [this] { m_updateExtrema = !m_updateExtrema; }, [this] { return m_updateExtrema; });
				panel->check("Filter", [this] { m_filter = !m_filter; }, [this] { return m_filter; });
				panel->bind<float>("Max", [this] { return m_max; }, [this] (float value) { m_max = value; });
				panel->bind<float>("Min", [this] { return m_min; }, [this] (float value) { m_min = value; });

				std::vector<std::string> modes = { "Aspect", "Valence", "EdgeLength", "Volume" };
				panel->combo("Mode", modes, [this] (int index) { m_mode = Mode(index); });
			}

		private:
			TetMesh::Id m_mesh;
			Mesh::Id m_tets { Mesh::create() };

			float m_min, m_max;
			bool m_updateExtrema { true };
			bool m_filter { false };

			ColorLerper m_colorLerper { glm::vec4 { 0.0f, 0.0f, 1.0f, 1.0f },
			                            glm::vec4 { 0.0f, 1.0f, 1.0f, 1.0f },
			                            glm::vec4 { 0.0f, 1.0f, 0.0f, 1.0f },
			                            glm::vec4 { 1.0f, 1.0f, 0.0f, 1.0f },
			                            glm::vec4 { 1.0f, 0.0f, 0.0f, 1.0f } };

			Mode m_mode;

			const glm::vec4 sample(const float value)
			{
				const auto absValue = glm::length(value);
				const auto clampedValue = std::min(m_max, glm::max(m_min, absValue));
				const auto maxMagnitude = m_max - m_min;
				const auto factor = (clampedValue - m_min) / maxMagnitude;

				const glm::vec4 color = m_colorLerper.sample(factor);
				return color;
			}
		};
	}
}