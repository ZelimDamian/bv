#pragma once

#include <stdint.h>

namespace bv
{
	namespace tethex
	{
		class Hexahedron
		{
		public:
			Hexahedron() {}
			Hexahedron(int a, int b, int c, int d, int e, int f, int g, int h):
					a(a), b(b), c(c), d(d), e(e), f(f), g(g), h(h) {}

			// construct a hex from an array of indices
			explicit Hexahedron(int indices[8])
			{
				for (int i = 0; i < 8; ++i)
				{
					this->indices[i] = indices[i];
				}
			}

			union
			{
				struct { int a, b, c, d, e, f, g, h; };
				int indices[8];
			};
		};

		// an alias for a hexahedron
		using Hex = Hexahedron;
	}
}