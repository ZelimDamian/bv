#pragma once

#include "Common.h"
#include "TetMesh.h"

namespace bv
{
	namespace tethex
	{
		class TetraGen
		{
		public:
			static TetMesh::Id generate(Mesh::Id meshId, float maxVolume = 0.01f, float minRatio = 1.414, bool nobisect = true);
		};
	}
}
