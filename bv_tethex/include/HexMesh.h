#pragma once

#include "Hex.h"
#include "Meshable.h"
#include "RenderMesh.h"
#include "Store.h"

namespace bv
{
	namespace tethex
	{
		class HexMesh: public Meshable<PosColorVertex>, public Storable<HexMesh>
		{
		public:
            using TVertex = PosColorVertex;

			HexMesh(Id id): StorableBase(id)
			{
				surface = Mesh::create();
			}

			void updateIndices();
            void updateSurfaceVertices();

			void setPositions(const VectorVec3& positions)
			{
				this->positions = positions;
				surface->positions = positions;
			}

			std::vector<Hex> hedra;
			Mesh::Id surface;

			rendering::RenderMesh::Id renderMeshId;
		};
	}
}