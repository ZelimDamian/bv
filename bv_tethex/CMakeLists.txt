SETUP(bv_tethex)

SET(SOURCE_FILES
    include/Tet.h
    include/TetHexComponent.h
    src/Tet.cpp
    include/TetMesh.h
    src/TetMesh.cpp
    include/TetraGen.h
    src/TetraGen.cpp
    include/Hex.h
    include/HexGen.h
    src/HexGen.cpp
    include/HexMesh.h
    src/HexMesh.cpp
)

ADD_HEADER(TetHexMeshUtils)
ADD_HEADER(TetMeshQualityComponent)

# try to compile the TetGen files as well
# need to make sure that there are no entry points
# by using the lib version
ADD_DEFINITIONS(-DTETLIBRARY)
SET(INCLUDE_DIRS ${INCLUDE_DIRS} ext/tetgen)
SET(SOURCE_FILES ${SOURCE_FILES} ext/tetgen/predicates.cxx ext/tetgen/tetgen.cxx)

LINK(core)

BOOTSTRAP(${target})
