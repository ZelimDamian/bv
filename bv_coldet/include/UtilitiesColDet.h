#pragma once

#include "Mesh.h"
#include "Entity.h"
#include "Collider.h"
#include "Octree.h"
#include "Bitree.h"
#include "MeshContact.h"

#include "MeshUtilities.h"

#include <numeric>

namespace bv
{
    namespace coldet
    {
        struct Utilities
        {
            static Mesh::Id colliderMesh(Entity::Id entity)
            {
                // get the second collider
                auto collider = Collider::find(entity);

                if(!collider)
                {
                    Log::writeln("Missing collider");
					return Mesh::Id::invalid();
                }

                if (auto* concept = dynamic_cast<const Collider::ShapeModel<Octree>*>(collider->concept()))
                {
                    return concept->shape().mesh();
				}
				else if (auto* concept = dynamic_cast<const Collider::ShapeModel<Bitree>*>(collider->concept()))
				{
					return concept->shape().mesh();
				}

                return Mesh::Id::invalid();
            }

	        template <ContactSide S>
	        static std::vector<glm::vec3> collectNormals(const MeshContact *meshContact)
	        {
		        std::vector<int> faceIndices = meshContact->uniqueIndicesSided<S>();
		        return MeshUtilities::collectNormals(faceIndices, meshContact->mesh<S>());
	        }
        };
	}
}
