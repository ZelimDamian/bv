#pragma once

#include "Contact.h"

namespace bv
{
    namespace coldet
    {
        struct Hit
        {
	        Hit(int idx, float t) : idx(idx), t(t) {}
            Hit(int idx, float t, float u, float v) : idx(idx), t(t), u(u), v(v) { }
	        int idx;
	        float t;
            float u, v; // barycentric coordinates
        };

        // tri index and ray t parameter of where the hit occured
        using HitCollection = std::vector<Hit>;

        // describes the contact between a ray and mesh
        class RayMeshContact: public Contact
        {
        public:
            RayMeshContact(const Ray& ray, Mesh::Id mesh,
                        const glm::mat4& transform):
                    m_mesh(mesh), m_ray(ray),
                    m_transform(transform) { }

            const Mesh::Id &mesh() const { return m_mesh; }
            Ray ray() { return m_ray; }

            void addHit(int triIndex, float t, float u, float v)
            {
                m_pairs.emplace_back(triIndex, t, u, v);
            }

            void addHit(int triIndex, float t)
            {
                m_pairs.emplace_back(triIndex, t);
            }

            void sort()
            {
                // sort ascending on hit t param
                std::sort(m_pairs.begin(), m_pairs.end(), [] (const Hit& hit1, const Hit& hit2){
                   return hit1.t < hit2.t;
                });
            }

	        const HitCollection& pairs() const { return m_pairs; }

            virtual bool isFound() const { return !m_pairs.empty(); }

        private:
            Ray m_ray;
            Mesh::Id m_mesh;

            glm::mat4 m_transform;

            HitCollection m_pairs;
        };
    }
}
