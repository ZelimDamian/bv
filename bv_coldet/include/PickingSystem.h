#pragma once

#include "System.h"

#include "SelectionSystem.h"
#include "Collider.h"

#include "MouseSystem.h"
#include "KeyboardSystem.h"
#include "CameraSystem.h"

#include "CollisionMethodRayOctree.h"
#include "RayMeshContact.h"
#include "RenderMeshComponent.h"

namespace bv
{
    namespace coldet
    {
        class PickingSystem: public SystemSingleton<PickingSystem>
        {
        public:

	        struct Hit
	        {
		        Hit(Entity::Id entity, Ray ray, float t): entity(entity), ray(ray), t(t) { }
		        Entity::Id entity;
		        Ray ray;
		        float t;
	        };

            void init()
            {
                auto mouse = MouseSystem::instance();

                auto onclick = [this]
                {
					auto hits = raycast();

	                SelectionSystem::instance()->clear();

	                if(!hits.empty())
	                {
		                // pick the closest hit as the selected entity
		                SelectionSystem::instance()->add(hits[0].entity);
	                }
                };

                mouse->onright(onclick);

	            auto onupdate = [this]
	            {
		            // add colliders to all entity for picking
		            for (auto &&entity : Entity::handles())
		            {
			            // only if there is an attached mesh
			            if(auto rmc = RenderMeshComponent::find(entity))
			            {
				            auto collider = entity->require<Collider>();
				            // if there isn't a shape already
				            if(!collider->concept())
				            {
					            // create an octree around the mesh
					            collider->createShape<Octree>(rmc->mesh());
					            collider->deactivate();
				            }
			            }
		            }
	            };

	            UpdateSystem::instance()->onupdate(onupdate);
            }

	        const std::vector<Hit>& raycast()
	        {
		        m_cache.clear();

		        auto ray = CameraSystem::instance()->ray(MouseSystem::instance()->pos());

		        auto& colliders = Collider::components();

		        for (int i = 0; i < colliders.size(); ++i)
		        {
			        auto rmc = colliders[i].entity()->component<RenderMeshComponent>();

			        if(rmc && !rmc->isActive())
			        {
				        continue;
			        }

			        if (auto concept = colliders[i].concept<Octree>())
			        {
				        CollisionMethodRayOctree method (ray, concept->shape());
				        method.mat2(colliders[i].entity()->transform());

				        auto contact = method();
				        auto rayContact = static_cast<RayMeshContact*>(contact.get());

				        if(rayContact->isFound())
				        {
					        rayContact->sort();

					        m_cache.emplace_back(colliders[i].entity(), ray, rayContact->pairs()[0].t);
				        }
			        }
		        }

				bv::sort(m_cache, [] (const Hit& hit1, const Hit& hit2) { return hit1.t < hit2.t; });

		        return m_cache;
	        }

	        const std::vector<Hit>& cache() const { return m_cache; }

        private:
	        std::vector<Hit> m_cache;
        };
    }
}