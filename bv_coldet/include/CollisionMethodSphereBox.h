#pragma once
#include "CollisionMethod.h"
#include "Sphere.h"
#include "Box.h"

namespace bv
{
    namespace coldet
    {
        using CollisionMethodSphereBox = CollisionMethodTemplate<Sphere, Box>;
    }
}
