#pragma once
#include "Collider.h"

class ColliderBox : public Collider
{
public:
	ColliderBox();
	~ColliderBox();

	void initialize();
};

typedef PTR(ColliderBox) ColliderBoxPtr;