#pragma once
#include "Singleton.h"
#include "CollisionMethod.h"
#include "Collider.h"

namespace bv
{
	namespace coldet
	{
		class CollisionMethodProvider: public Singleton<CollisionMethodProvider>
		{
		public:
			CollisionMethodPtr get(const Collider::ShapeConcept* concept1, const Collider::ShapeConcept* concept2);
			CollisionMethodPtr get(Entity::Id entity1, Entity::Id entity2);
		};
	}
}
