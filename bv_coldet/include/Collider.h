#pragma once
#include "Component.h"
#include "ColliderLayers.h"

namespace bv
{
    namespace coldet
    {
        class Collider: public Component<Collider>
        {
        public:
            // type erasure as in http://www.cplusplus.com/articles/oz18T05o/
            class ShapeConcept
            {
            public:
                virtual ~ShapeConcept() {}
            };

            template <class T>
            struct ShapeModel: public ShapeConcept
            {
            public:
                template<typename ... Args>
                ShapeModel(const Args&... args): m_shape(T(args...)) {}
				ShapeModel(const T& shape) : m_shape(shape) {}
				ShapeModel() {}

                T& shape() { return m_shape; }
                const T& shape() const { return m_shape; }

            private:
                T m_shape;
            };

        public:
            Collider(Id id, Entity::Id entity);

            // need custom copy constructor because of unique_ptr
//            Collider(const Collider& other): Collider(other.m_id, other.m_entity)
//            {
//                m_shapeConcept = std::move(other.m_shapeConcept);
//            }

            template <class Shape>
            Shape& shape(const Shape& extShape)
            {
                auto* pShapeModel = new ShapeModel<Shape>(extShape);
                m_shapeConcept = std::shared_ptr<ShapeModel<Shape>>(pShapeModel);
                return pShapeModel->shape();
            }

            template <class Shape>
            Shape& requireShape()
            {
                auto* pShapeModel = new ShapeModel<Shape>();
                m_shapeConcept = std::shared_ptr<ShapeModel<Shape>>(pShapeModel);
                return pShapeModel->shape();
            }

            template <class TShape, typename ... Args>
            TShape& createShape(const Args& ... args)
            {
                auto* pShapeModel = new ShapeModel<TShape>(args...);
                m_shapeConcept = std::shared_ptr<ShapeModel<TShape>>(pShapeModel);
                return pShapeModel->shape();
            }

		    template <class TShape>
            const ShapeModel<TShape>* concept() const { return dynamic_cast<ShapeModel<TShape>*>(m_shapeConcept.get()); }

            template <class TShape>
            ShapeModel<TShape>* concept() { return dynamic_cast<ShapeModel<TShape>*>(m_shapeConcept.get()); }

		    const ShapeConcept* concept() const { return m_shapeConcept.get(); }

	        bool isDebugRendered() const { return m_debugRendered; }

	        void attachGUI()
	        {
		        auto panel = ComponentBase::gui();

		        panel->button("DebugRender", [&]
		        {
			       m_debugRendered = !m_debugRendered;
		        });

                panel->bind("Layer", m_layer, 0, ColliderLayers::MaxLayers - 1);
	        }

            int layer() const { return m_layer; }
            void layer(int layer) { m_layer = layer; }

        private:
			std::shared_ptr<ShapeConcept> m_shapeConcept;

			bool m_debugRendered { false };
            int m_layer { 0 };
        };

        using ColliderPtr = Collider*;
    }
}