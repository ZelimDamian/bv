#pragma once
#include "CollisionMethod.h"
#include "Octree.h"

namespace bv
{
    namespace coldet
    {
        using CollisionMethodOctree = CollisionMethodTemplate<Octree, Octree>;
    }
}

