#pragma once
#include "Common.h"

#include "Octree.h"
#include "Bitree.h"
#include "Contact.h"
#include "Intersection.coldet.h"

namespace bv
{
	namespace coldet
	{
		class CollisionMethod
		{
		public:
			virtual void find() = 0;

			glm::mat4 mat1() { return m_mat1; }
			glm::mat4 mat2() { return m_mat2; }

			void mat1(glm::mat4 mat) { m_mat1 = mat; updateLocalize(); }
			void mat2(glm::mat4 mat) { m_mat2 = mat; updateLocalize(); }

			// std::move contact out of method
            ContactPtr moveContact() { return std::move(m_contact); }
			// sugar for find and moveContact
			ContactPtr operator()() { this->find(); return moveContact(); }

			void debugRenderFirst(bool debugRenderFirst) { m_debugRenderFirst = debugRenderFirst; }
			void debugRenderSecond(bool debugRenderSecond) { m_debugRenderSecond = debugRenderSecond; }

			// iteration counter for profiling
			int counter() const { return m_counter; }

		protected:
			void updateLocalize() { m_localizeMatrix = glm::inverse(m_mat1) * m_mat2; }

			ContactPtr m_contact;

			glm::mat4 m_mat1;
			glm::mat4 m_mat2;
			glm::mat4 m_localizeMatrix;

			bool m_debugRenderFirst { false };
			bool m_debugRenderSecond { false };

			// for profiling
			int m_counter { 0 };
		};

		// T and S represent the shapes whereas R and U represent the sub-components as in T=Octree vs S=Octree and contacts of type Contact<R=Face, S=Face>
		template<class T, class S>
		class CollisionMethodTemplate : public CollisionMethod
		{
		public:
			CollisionMethodTemplate(const T& first, const S& second): m_first(&first), m_second(&second) { }

			void first(PTR(T) first) { m_first = first; }
			void first(T& first) { m_first = &first; }
			const PTR(T) first() const { return m_first; }
			void second(PTR(S) second) { m_second = second; }
			void second(S& second) { m_second = &second; }
			const PTR(S) second() const { return m_second; }

			// defined in corresponding .cpp files
			void find();

		protected:
			const PTR(T) m_first;
			const PTR(S) m_second;

			// to display a debug mesh for the BVH
			static Mesh::Id s_debugMesh;

			void recurse(const T&, const S&);/* {} */
            void init();
		};

		template<class T, class S>
		Mesh::Id CollisionMethodTemplate<T, S>::s_debugMesh;

		typedef std::unique_ptr<CollisionMethod> CollisionMethodPtr;
	}
}
