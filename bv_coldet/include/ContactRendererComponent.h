#pragma once

#include "Component.h"

namespace bv
{
    namespace coldet
    {
        class ContactRendererComponent: public Component<ContactRendererComponent>
        {
        public:
            ContactRendererComponent(Id id, Entity::Id entity): ComponentBase(id, entity) { }

            void initialize();

        private:
            void render();
        };
    }
}