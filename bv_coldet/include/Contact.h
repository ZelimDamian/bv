#pragma once

#include "Common.h"
#include "Mesh.h"
#include "Collider.h"

namespace bv
{
    namespace coldet
    {
        enum class ContactSide
        {
            First = 0,
            Second,
	        None
        };

//        template <class TContact>
        class Contact
        {
        public:
            Contact() {}
            Contact(const glm::vec3& point): m_point(point) {}
            virtual ~Contact() {}

            const glm::vec3 &point() const { return m_point; }
            void point(const glm::vec3 &point) { m_point = point; }

            const glm::vec3 &normal() const { return m_normal; }
            void normal(const glm::vec3 &normal) { m_normal = normal; }

            const glm::vec3 &velocity() const { return m_velocity; }
            void velocity(const glm::vec3 &velocity) { m_velocity = velocity; }

            float area() const { return m_area; }
            void area(float area) { m_area = area; }

	        virtual bool isFound() const { return false; }

	        Collider::Id master() const { return m_master; }
	        void master(Collider::Id master) { m_master = master; }

	        Collider::Id slave() const { return m_slave; }
	        void slave(Collider::Id slave) { m_slave = slave; }

        protected:
            glm::vec3 m_point;
            glm::vec3 m_normal;
            glm::vec3 m_velocity;

            float m_area;

	        Collider::Id m_master;
	        Collider::Id m_slave;
        };

        using ContactPtr = std::unique_ptr<Contact>;
        using ContactCollection = std::vector<ContactPtr>;

        class TriangleContact: public Contact //<TriangleContact>
        {
        public:
            TriangleContact(Tri tri1, Tri tri2): m_tri1(tri1), m_tri2(tri2)
            { }

            const Tri &tri1() const { return m_tri1; }
            void tri1(const Tri &tri1) { m_tri1 = tri1; }
            const Tri &tri2() const { return m_tri2; }
            void tri2(const Tri &tri2) { m_tri2 = tri2; }

        private:
            Tri m_tri1;
            Tri m_tri2;
        };
    }
}