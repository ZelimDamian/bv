#pragma once

#include "System.h"
#include "Entity.h"
#include "Contact.h"
#include "Collider.h"
#include "CollisionMethod.h"
#include <unordered_map>

namespace bv
{
    namespace coldet
    {
	    template<class TValue>
        struct PairHasher
        {
	        using Key = pair_key<TValue>;

            inline size_t operator ()(const Key& key) const
            {
                std::hash<int> hashInt;
                return hashInt((int)key.first) * 31 ^ hashInt((int)key.second);
            }
        };

	    template <class TKey, class TValue>
	    using hashed_map = std::unordered_map<pair_key<TKey>, TValue, PairHasher<TKey>>;

        using ContactMap = hashed_map<Collider::Id, ContactPtr>;
	    using MethodMap = hashed_map<Collider::Id, CollisionMethodPtr>;

	    using ColliderKey = ContactMap::key_type;

	    struct ContactQueryResult
	    {
		    const Contact* contact;
		    ContactSide side;
	    };

        class CollisionSystem: public SystemSingleton<CollisionSystem>
        {
        public:

            void init() override;

	        // perform collision detection between all entity pairs with attached collider
            void process();

	        // find all entities in scene intersected by ray
            std::vector<Entity::Id> pick(Ray ray);

	        // return list of all contacts for a given collider
	        std::vector<ContactQueryResult> contacts(Collider::Id) const;

	        // get contact between two colliders
			ContactQueryResult contact(const Collider::Id collider1, const Collider::Id collider2) const;

	        // get contact between two colliders from the previous frame
	        const Contact* oldContact(const Collider::Id collider1, const Collider::Id collider2);

	        // find colliders for a contact
			ColliderKey colliders(const Contact* contact);

        private:
	        ContactMap  m_contacts;
	        ContactMap  m_oldContacts;

            ContactMap m_contactsInternal;
            ContactMap m_oldContactsInternal;

	        MethodMap   m_methods;
        };
    }
}