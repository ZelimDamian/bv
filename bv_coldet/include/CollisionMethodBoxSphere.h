#pragma once

#include "Common.h"
#include "CollisionMethod.h"
#include "Sphere.h"

namespace bv
{
    namespace coldet
    {
        using CollisionMethodBoxSphere = CollisionMethodTemplate<Box, Sphere>;
    }
}

