#pragma once
#include "Collider.h"
#include "Sphere.h"

class ColliderSphere :
	public Collider
{
public:
	ColliderSphere();
	~ColliderSphere();

	void initialize();
};

typedef PTR(ColliderSphere) ColliderSpherePtr;