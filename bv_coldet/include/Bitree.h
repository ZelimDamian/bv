#pragma once

#include "Common.h"
#include "CollisionShape.h"
#include "Tri.h"
#include "Box.h"
#include "Mesh.h"

namespace bv
{
	namespace coldet
	{
		class Bitree;
		typedef Bitree* BitreePtr;

		class Bitree
		{
		public:
			Bitree(Mesh::Id mesh);

			bool isLeaf() const { return m_left == NULL && m_right == NULL; }

			void distribute();

			const Box& box() const { return m_box; }
			void box(Box box) { m_box = box; }

			int level() const { return m_level; }

            const std::vector<Tri>& faces() const { return m_faces; }
            Tri faces(int index) const { return m_faces[index]; }

            Mesh::Id mesh() const { return m_mesh; }

			BitreePtr left() const { return m_left; }
			BitreePtr right() const { return m_right; }

            void refit();

			float errorMetric(Box& box);


		protected:
			Bitree(Bitree* parent);

			Bitree* m_parent;
			BitreePtr m_left;
			BitreePtr m_right;

			Box m_box;

			std::vector<Tri> m_faces;
			Mesh::Id m_mesh;

			int m_minTriCount;
			int m_targetTriCount;
			int m_level;
			static const int MAX_LEVEL = 8;
			static const int MIN_TRI_COUNT = 10;
			static const int TARGET_COUNT = 15;
			static float MIN_ERROR;
		};
	}
}


