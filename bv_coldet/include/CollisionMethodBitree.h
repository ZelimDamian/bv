#pragma once
#include "CollisionMethod.h"
#include "Bitree.h"

namespace bv
{
    namespace coldet
    {
        using CollisionMethodBitree = CollisionMethodTemplate<Bitree, Bitree>;
    }
}

