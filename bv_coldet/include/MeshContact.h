#pragma once

#include "Contact.h"

namespace bv
{
    namespace coldet
    {
        using IdxPair= std::pair<int, int>;
	    using IdxPairCollection = std::vector<IdxPair>;

        class MeshContact: public Contact
        {
        public:
            MeshContact(Mesh::Id mesh1, Mesh::Id mesh2): m_mesh1(mesh1), m_mesh2(mesh2) { }

            const Mesh::Id &mesh1() const { return m_mesh1; }
            void mesh1(const Mesh::Id &mesh1) { m_mesh1 = mesh1; }
            const Mesh::Id &mesh2() const { return m_mesh2; }
            void mesh2(const Mesh::Id &mesh2) { m_mesh2 = mesh2; }

            const Mesh::Id mesh(ContactSide side) const
            {
                if(side == ContactSide::First)
                {
                    return m_mesh1;
                }
                else
                {
                    return m_mesh2;
                }
            }

	        template <ContactSide SIDE>
            const Mesh::Id mesh() const
            {
                return mesh(SIDE);
            }

            const IdxPairCollection & pairs() const { return m_pairs; }
	        IdxPairCollection & pairs() { return m_pairs; }
//	        const IdxPairCollection & potentials() const { return m_potentials; }

            std::vector<int> uniqueIndicesFirst() const
            {
                return uniqueIndicesSided<ContactSide::First >();
            }

            std::vector<int> uniqueIndicesSecond() const
            {
                return uniqueIndicesSided<ContactSide::Second >();
            }

            std::vector<int> uniqueIndicesSided(ContactSide side) const
            {
                if(side == ContactSide::First)
                {
                    return uniqueIndicesSided<ContactSide::First>();
                }
                else
                {
                    return uniqueIndicesSided<ContactSide::Second>();
                }
            }

            template<ContactSide SIDE>
            std::vector<int> uniqueIndicesSided() const
            {
                std::vector<int> unique(m_pairs.size());

                for (int i = 0; i < m_pairs.size(); ++i)
                {
                    unique[i] = std::get<(int)SIDE>(m_pairs[i]);
                }

                bv::filterUnique(unique);

                return unique;
            }

	        void addPair(IdxPair pair)
	        {
		        addPair(pair.first, pair.second);
	        }

            void addPair(int first, int second)
            {
                m_pairs.emplace_back(first, second);
            }

            bool isFound() const { return !m_pairs.empty(); }

	        // recalculate the penetration values
	        void updateInfo() const;

		    struct Info
		    {
			    std::vector<int> slaveVertIndices;
			    std::vector<glm::vec3> slaveVertPositions;
                std::vector<glm::vec3> slaveContactNormals;

			    std::vector<float> penetrations;
                std::vector<glm::vec3> masterContactNormals;
			    glm::vec3 masterContactNormal;

			    // cache
			    std::vector<int> penetratingIndices;
		    };

		    using InfoPtr = std::unique_ptr<Info>;

	        const Info* info() const
	        {
		        if(!m_info)
		        {
			        updateInfo();
		        }
		        return m_info.get();
	        }

        private:
            Mesh::Id m_mesh1;
            Mesh::Id m_mesh2;

            IdxPairCollection m_pairs;

			bool m_penetrationsReady;

			mutable InfoPtr m_info;
        };
    }
}