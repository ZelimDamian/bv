#pragma once
#include "Plane.h"
#include "Box.h"
#include "Sphere.h"
#include "Intersection.h"
#include "Octree.h"

namespace bv
{
//    namespace coldet
    //{
		namespace Intersection
		{
			// obb vs obb
			bool test(const Box& box1, const Box& box2, const glm::mat4& mat);

			// sphere vs sphere transformed
			bool test(const Sphere& sphere1, const Sphere& sphere2, const glm::mat4& mat);

			// box vs triangle transformed
			bool test(const Box& box1, const glm::vec3& vertex1,
						const glm::vec3& vertex2, const glm::vec3& vertex3, const glm::mat4& mat);
			// box vs triangle
			bool test(const Box& box1, const glm::vec3& vertex1,
						const glm::vec3& vertex2, const glm::vec3& vertex3);
			// box vs triangle
			bool test(const Box& box1, const std::array<glm::vec3, 3>& vertices);

			// triangle vs triangle transformed
			bool test(const std::array<glm::vec3, 3>&, const std::array<glm::vec3, 3>&, const glm::mat4& mat);

			// triangle vs triangle
			bool test(const glm::vec3&, const glm::vec3&, const glm::vec3&,
						const glm::vec3&, const glm::vec3&, const glm::vec3&);

			// box vs sphere transformed
			bool test(const Box& box, const Sphere& sphere, const glm::mat4& matrix);

			// box vs sphere
			bool test(const Box&, const Ray&);

			// box sphere with t output
			bool test(const Box&, const Ray&, float&);
			// box sphere with t output with transformation
			bool test(const Box&, const Ray&, const glm::mat4& mat, float&);

			// plane vs ray with t output
			bool test(const Plane& plane, const Ray& ray, float& t);

			// ray vs triangle
			bool test(const Ray& ray, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3, float& t);
			bool test(const Ray& ray, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3, float& t, float& u, float& v);
			// ray vs triangle
			bool test(const Ray& ray, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3);

			// ray vs triangle transformed
			bool test(const Ray& ray, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3,
						const glm::mat4& mat, float& t);

			// test ray against mesh in octree
			bool test(const Ray& ray, const coldet::Octree& octree);

			// test transformed ray agains mesh in octree
			bool test(const Ray& ray, const coldet::Octree& octree, const glm::mat4& mat);
		};
    //}
}