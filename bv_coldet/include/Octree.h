#pragma once

#include "Common.h"
#include "Mesh.h"
#include "Box.h"
#include "Tri.h"
#include "CollisionShape.h"

namespace bv
{
    namespace coldet
    {
        class Octree;
        typedef PTR(Octree) OctreePtr;

        class Octree// : public CollisionShape
        {
        public:
	        Octree(Mesh::Id id);
	        Octree(Octree* parent, Box box);

	        void encloseFaces();
            void distribute();
	        void refit();

	        bool needsRefit();

            const Box& box() const { return m_box; }
            void box(Box box) {  m_box = box; }

            int level() { return m_level; }

            std::vector<Tri>& faces() { return m_faces; }
            const std::vector<Tri>& faces() const { return m_faces; }

            inline Tri face(size_t i) const { return m_faces[i]; }

//            inline const Octree* const* children() const { return &m_children[0]; }
            inline const Octree* children(size_t index) const { return &m_children[index]; }

            bool isLeaf() const { return m_children.empty(); }

//            BufferVec3* positions() { return m_pPositions; }
            const VectorVec3& positions() const { return m_mesh->positions; }

			Mesh::Id mesh() const { return m_mesh; }

	        void updateNumLeafs();

	        unsigned int numLeafs() const;

        private:

            static const int MAX_LEVEL = 7;
            static const int MAX_FACES = 10;

            void split();

            Octree* m_parent;

			std::vector<Octree> m_children;

            std::vector<Tri> m_faces;
            Mesh::Id m_mesh;

            int m_level;
	        int m_leafs; // number of leafs under this node

            Box m_box;
        };
    }
}
