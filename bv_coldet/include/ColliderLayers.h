#pragma once
#include "Singleton.h"

namespace bv
{
    namespace coldet
    {
        class ColliderLayers: public Singleton<ColliderLayers>
        {
        public:
            const static int MaxLayers = 32;

	        ColliderLayers()
	        {
		        // by default same layer colliders should check
		        for (int i = 0; i < MaxLayers; ++i)
		        {
			        set(i, i, true);
		        }
	        }

            void link(int layer1, int layer2) { set(layer1, layer2, true); }
	        void unlink(int layer1, int layer2) { set(layer1, layer2, false); }

            bool check(int layer1, int layer2)
            {
	            // some same-layer collisions are unnecessary
//                if(layer1 == layer2) return true;
                return get(layer1, layer2);
            }

        private:
            bool m_matrix[MaxLayers * MaxLayers];

            void set(int layer1, int layer2, bool value)
            {
                m_matrix[layer1 * MaxLayers + layer2] = value;
                m_matrix[layer2 * MaxLayers + layer1] = value;
            }

            bool get(int layer1, int layer2)
            {
                assert(
                    m_matrix[layer1 * MaxLayers + layer2] ==
                    m_matrix[layer2 * MaxLayers + layer1]
                );

                return m_matrix[layer2 * MaxLayers + layer1];
            }
        };
    }
}