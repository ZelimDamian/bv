#pragma once
#include "CollisionMethod.h"

namespace bv
{
    namespace coldet
    {
        using CollisionMethodRayOctree = CollisionMethodTemplate<Ray, Octree>;
    }
}
