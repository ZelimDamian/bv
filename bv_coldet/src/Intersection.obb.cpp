#include "Intersection.coldet.h"

#include "MathLib.h"

using namespace bv;
//using namespace coldet;

#define EPSILON 0.000001f

static const glm::vec3 EPSILON_VEC = glm::vec3(EPSILON);

bool Intersection::test(const Box &box1, const Box &box2, const glm::mat4 &localization)
{
    glm::vec3 hA = box1.halfsize();
    glm::vec3 hB = box2.halfsize();

    glm::vec3 cA = box1.center();
    glm::vec3 cB = box2.center();

    glm::vec3 mB_T = Math::transform(cB, localization) - cA;

	// Transform the extents of B
	glm::vec3 bX = glm::vec3(localization[0]) + EPSILON_VEC;	// x-axis of box B
	glm::vec3 bY = glm::vec3(localization[1]) + EPSILON_VEC;    // y-axis of box B
	glm::vec3 bZ = glm::vec3(localization[2]) + EPSILON_VEC;	// z-axis of box B

    glm::vec3 mB_TA = glm::vec3(glm::abs(mB_T.x), glm::abs(mB_T.y), glm::abs(mB_T.z));

    glm::vec3 hx_B = bX * hB.x;   // x extent of box B
    glm::vec3 hy_B = bY * hB.y;   // y extent of box B
    glm::vec3 hz_B = bZ * hB.z;   // z extent of box B

    // Check for separation along the axes of box A
    if (mB_TA.x >= hA.x + glm::abs(hx_B.x) + glm::abs(hy_B.x) + glm::abs(hz_B.x))
        return false;

    if (mB_TA.y >= hA.y + glm::abs(hx_B.y) + glm::abs(hy_B.y) + glm::abs(hz_B.y))
        return false;

    if (mB_TA.z >= hA.z + glm::abs(hx_B.z) + glm::abs(hy_B.z) + glm::abs(hz_B.z))
        return false;

    // Check for separation along the axes box B, hx_B/hy_B/hz_B
    if (glm::abs(glm::dot(mB_T, bX)) >= glm::abs(hA.x * bX.x) + glm::abs(hA.y * bX.y) + glm::abs(hA.z * bX.z) + hB.x)
        return false;

    if (glm::abs(glm::dot(mB_T, bY)) >= glm::abs(hA.x * bY.x) + glm::abs(hA.y * bY.y) + glm::abs(hA.z * bY.z) + hB.y)
        return false;

    if (glm::abs(glm::dot(mB_T, bZ)) >= glm::abs(hA.x * bZ.x) + glm::abs(hA.y * bZ.y) + glm::abs(hA.z * bZ.z) + hB.z)
        return false;

    glm::vec3 axis;
    //
    //a.x ^ b.x = (1,0,0) ^ bX
    axis = glm::vec3(0, -bX.z, bX.y);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.y * axis.y) + glm::abs(hA.z * axis.z)
                                          + glm::abs(glm::dot(axis, hy_B)) + glm::abs(glm::dot(axis, hz_B)))
        return false;

    // a.x ^ b.y = (1,0,0) ^ bY
    axis = glm::vec3(0, -bY.z, bY.y);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.y * axis.y) + glm::abs(hA.z * axis.z)
                                          + glm::abs(glm::dot(axis, hz_B)) + glm::abs(glm::dot(axis, hx_B)))
        return false;

    // a.x ^ b.z = (1,0,0) ^ bZ
    axis = glm::vec3(0, -bZ.z, bZ.y);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.y * axis.y) + glm::abs(hA.z * axis.z)
                                          + glm::abs(glm::dot(axis, hx_B)) + glm::abs(glm::dot(axis, hy_B)))
        return false;

    // a.y ^ b.x = (0,1,0) ^ bX
    axis = glm::vec3(bX.z, 0, -bX.x);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.z * axis.z) + glm::abs(hA.x * axis.x)
                                          + glm::abs(glm::dot(axis,hy_B)) + glm::abs(glm::dot(axis, hz_B)))
        return false;

    // a.y ^ b.y = (0,1,0) ^ bY
    axis = glm::vec3(bY.z, 0, -bY.x);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.z * axis.z) + glm::abs(hA.x * axis.x)
                                          + glm::abs(glm::dot(axis, hz_B)) + glm::abs(glm::dot(axis, hx_B)))
        return false;

    // a.y ^ b.z = (0,1,0) ^ bZ
    axis = glm::vec3(bZ.z, 0, -bZ.x);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.z * axis.z) + glm::abs(hA.x * axis.x)
                                          + glm::abs(glm::dot(axis, hx_B)) + glm::abs(glm::dot(axis, hy_B)))
        return false;

    // a.z ^ b.x = (0,0,1) ^ bX
    axis = glm::vec3(-bX.y, bX.x, 0);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.x * axis.x) + glm::abs(hA.y * axis.y)
                                          + glm::abs(glm::dot(axis, hy_B)) + glm::abs(glm::dot(axis, hz_B)))
        return false;

    // a.z ^ b.y = (0,0,1) ^ bY
    axis = glm::vec3(-bY.y, bY.x, 0);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.x * axis.x) + glm::abs(hA.y * axis.y)
                                          + glm::abs(glm::dot(axis, hz_B)) + glm::abs(glm::dot(axis, hx_B)))
        return false;

    // a.z ^ b.z = (0,0,1) ^ bZ
    axis = glm::vec3(-bZ.y, bZ.x, 0);
    if (glm::abs(glm::dot(mB_T, axis)) >= glm::abs(hA.x * axis.x) + glm::abs(hA.y * axis.y)
                                          + glm::abs(glm::dot(axis, hx_B)) + glm::abs(glm::dot(axis, hy_B)))
        return false;

    return true;
}
// https://github.com/idmillington/cyclone-physics/blob/master/src/collide_fine.cpp
//// This preprocessor definition is only used as a convenience
//// in the boxAndBox intersection  method.
//#define TEST_OVERLAP(axis) overlapOnAxis(one, two, (axis), toCentre)
//
//bool boxAndBox(
//	const Box &one,
//	const Box &two
//	)
//{
//	// Find the vector between the two centres
//	glm::vec3 toCentre = two.getAxis(3) - one.getAxis(3);
//
//	return (
//		// Check on box one's axes first
//		TEST_OVERLAP(one.getAxis(0)) &&
//		TEST_OVERLAP(one.getAxis(1)) &&
//		TEST_OVERLAP(one.getAxis(2)) &&
//
//		// And on two's
//		TEST_OVERLAP(two.getAxis(0)) &&
//		TEST_OVERLAP(two.getAxis(1)) &&
//		TEST_OVERLAP(two.getAxis(2)) &&
//
//		// Now on the cross products
//		TEST_OVERLAP(one.getAxis(0) % two.getAxis(0)) &&
//		TEST_OVERLAP(one.getAxis(0) % two.getAxis(1)) &&
//		TEST_OVERLAP(one.getAxis(0) % two.getAxis(2)) &&
//		TEST_OVERLAP(one.getAxis(1) % two.getAxis(0)) &&
//		TEST_OVERLAP(one.getAxis(1) % two.getAxis(1)) &&
//		TEST_OVERLAP(one.getAxis(1) % two.getAxis(2)) &&
//		TEST_OVERLAP(one.getAxis(2) % two.getAxis(0)) &&
//		TEST_OVERLAP(one.getAxis(2) % two.getAxis(1)) &&
//		TEST_OVERLAP(one.getAxis(2) % two.getAxis(2))
//		);
//}
//#undef TEST_OVERLAP