#include "CollisionMethodSphereBox.h"
#include "Intersection.coldet.h"

using namespace bv;
using namespace coldet;

namespace bv
{
	namespace coldet
	{
		template<>
		void CollisionMethodSphereBox::find()
		{
            if (Intersection::test(*m_second, *m_first, glm::inverse(m_localizeMatrix)))
			{
				m_contact = ContactPtr(new Contact);
			}
		}
	}
}

