#include "Intersection.Coldet.h"

using namespace bv;
using namespace coldet;

bool Intersection::test(const Ray &ray, const coldet::Octree &octree, const glm::mat4& mat)
{
    return test(Ray::transform(ray, mat), octree);
}

void recurse(const Ray& ray, const coldet::Octree* octree, float& t)
{
    // if we already hit a triangle we shall terminate
    if(t > 0.0f)
    {
        return;
    }

    // check that we hit the bounding box
    if(Intersection::test(octree->box(), ray))
    {
        // if the box is a leaf
        if(octree->isLeaf())
        {
            size_t face_count = octree->faces().size();

            if (face_count > 0)
            {
                auto& positions = octree->mesh()->positions;

                for (size_t j = 0; j < face_count; j++)
                {
                    const Tri face = octree->face(j);

                    std::array<glm::vec3, 3> vertices = face.vertices(positions);

                    // perform the actual test
                    if(Intersection::test(ray, vertices[0], vertices[1], vertices[2], t))
                    {
                        // exit if a hit happened
                        return;
                    }
                }
            }
        }
    }
    else
    {
        for (int i = 0; i < 8; ++i)
        {
            recurse(ray, octree->children(i), t);
        }
    }
}

bool Intersection::test(const Ray &ray, const coldet::Octree &octree)
{
    float t = -1.0f;
    recurse(ray, &octree, t);
    return t > 0.0f;
}
