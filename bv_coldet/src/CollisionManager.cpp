#include "CollisionManager.h"
#include "Entity.h"
#include "SceneManager.h"
#include "CollisionResponseManager.h"
#include "Intersection.h"
#include "CollisionShape.h"
#include "CollisionMethod.h"
#include "CollisionMethodProvider.h"

CollisionManager* CollisionManager::s_instance = new CollisionManager();

CollisionManager* CollisionManager::instance()
{
	if(s_instance == NULL)
	{ 
		s_instance = new CollisionManager(); 
	}
	return s_instance;
}

CollisionManager::CollisionManager(void)
{
}


CollisionManager::~CollisionManager(void)
{
}

void CollisionManager::initialize()
{

}

void CollisionManager::update()
{
	EntityCollection entities = SceneManager::instance()->current()->entities();

	for (size_t i = 0; i < entities.size(); i++)
	{
		for (size_t j = i + 1; j < entities.size(); j++)
		{
			EntityPtr entity1 = entities[i];
			EntityPtr entity2 = entities[j];

			ColliderPtr collider1 = entity1->collider();
			ColliderPtr collider2 = entity2->collider();

			if (collider1 && collider2)
			{
				CollisionShapePtr shape1 = collider1->shape();
				CollisionShapePtr shape2 = collider2->shape();

				CollisionMethodPtr method = CollisionMethodProvider::instance()->get(shape1, shape2);
				
				method->mat1(entity1->matrix());
				method->mat2(entity2->matrix());

				const ContactCollection& contacts = method->find();
				//CollisionResponseManager::instance()->add(contacts);

				if (contacts.size() > 0)
				{
					collider1->notify(collider2, contacts);
					collider2->notify(collider1, contacts);
				}

				// In case we are dealing with raw pointers the method needs to be destroyed
				DESTROY(method);
			}
		}
	}
}

