#include "CollisionSystem.h"
#include "Intersection.coldet.h"
#include "Bounding.h"
#include "CollisionMethod.h"
#include <CollisionMethodProvider.h>
#include <MouseSystem.h>
#include <CameraSystem.h>
#include "ColliderLayers.h"
#include "Profiler.h"

using namespace bv;
using namespace coldet;

void CollisionSystem::init()
{
    auto worker = [this]
    {
        auto tock = Profiler::instance()->tick("Collision system update");
        process();
        tock();
    };

    UpdateSystem::instance()->onupdate(worker);

    auto onsync = [this]
    {
        m_contacts = std::move(m_contactsInternal);
        m_oldContacts = std::move(m_oldContactsInternal);

        m_oldContactsInternal = ContactMap();
        m_contactsInternal = ContactMap();
    };

    UpdateSystem::instance()->onsync(onsync);
}

void CollisionSystem::process()
{
    // store old contacts
	m_contactsInternal.swap(m_oldContactsInternal);

	// clear current contact list
	m_contactsInternal.clear();

    auto& colliders = Collider::components();

    for (int i = 0; i < colliders.size(); ++i)
    {
        // get the first collider
        Collider& collider1 = colliders[i];

        // check if its active
        if(!collider1.isActive())
        {
            continue;
        }

        // extract the shape concept
        auto* concept1 = collider1.concept();

        for (int j = i + 1; j < colliders.size(); ++j)
        {
            // get the second collider
            Collider& collider2 = colliders[j];

            // check if its active
            if(!collider2.isActive())
            {
                continue;
            }

            // check that the layers are to be checked
            if(!ColliderLayers::instance()->check(collider1.layer(), collider2.layer()))
            {
                continue;
            }

            // get the concept of the collision shape
            auto* concept2 = collider2.concept();

	        // key used to index into cache
	        auto key = ColliderKey {collider1.id(), collider2.id()};

			// find the appropriate collision detection method based on the shapes
            auto found = m_methods.find(key);

	        CollisionMethod* method;

	        if(found == m_methods.end()) // there is no method in cache
	        {
		        auto newMethod = CollisionMethodProvider::instance()->get(concept1, concept2);

		        // make sure there is an appropriate method available
		        if(!newMethod)
		        {
			        continue;
		        }

		        m_methods[key] = std::move(newMethod);
		        method = m_methods[key].get();
	        }
	        else
	        {
		        method = found->second.get();
	        }

            method->debugRenderFirst(collider1.isDebugRendered());
            method->debugRenderSecond(collider2.isDebugRendered());

            // specify the transformation of the first shape
            method->mat1(collider1.entity()->transform());
            // specify the transformation of the second shape
            method->mat2(collider2.entity()->transform());

			// perform the actual collision detection
            method->find();

            // retreive the contact
            ContactPtr contact = method->moveContact();

			// check if the contact exists
            if(contact && contact->isFound())
            {
	            contact->slave(collider1.id());
	            contact->master(collider2.id());

				Log::debug("Collison detected - %s vs %s",
					collider1.entity()->name().c_str(),
					collider2.entity()->name().c_str());

                // save the contact between the two collider instances
                m_contactsInternal[key] = std::move(contact);
            }
        }
    }
}

std::vector<Entity::Id> CollisionSystem::pick(Ray ray)
{
    std::vector<Entity::Id> picked;

    for (size_t i = 0; i < Entity::count(); i++)
    {
        Entity::Id entityId = Entity::Id(i);
        Box box = Bounding::box(entityId);

        float t;
        if(Intersection::test(box, ray, glm::inverse(entityId->transform()), t))
        {
            picked.push_back(entityId);
        }
    }

    return picked;
}

ColliderKey CollisionSystem::colliders(const Contact* contact)
{
	using ContactMapValue = ContactMap::value_type;

	auto predicate = [contact](const ContactMapValue& val)
	{
		return val.second.get() == contact;
	};

	auto found = std::find_if(m_contacts.begin(), m_contacts.end(), predicate);

	if (found != m_contacts.end())
	{
		return found->first;
	}

	return ColliderKey{};
}

std::vector<ContactQueryResult> CollisionSystem::contacts(Collider::Id collider) const
{
	std::vector<ContactQueryResult> contacts;

	for (auto &&cont : m_contacts)
	{
		if(cont.second->isFound())
		{
			ContactQueryResult result;
			result.contact = cont.second.get();

			// check the order
			if(cont.first.first == collider)
			{
				result.side = ContactSide::First;
				contacts.push_back(result);
			}
			else if(cont.first.second == collider)
			{
				result.side = ContactSide::Second;
				contacts.push_back(result);
			}
		}
	}

	return contacts;
}

// get contact between two colliders
ContactQueryResult CollisionSystem::contact(const Collider::Id collider1, const Collider::Id collider2) const
{
	auto key = ColliderKey{ collider1, collider2 };
	auto found = m_contacts.find(key);
	auto side = ContactSide::First;

	if (found == m_contacts.end())
	{
		auto reversedKey = ColliderKey{ collider2, collider1 };
		found = m_contacts.find(reversedKey);
		side = ContactSide::Second;

		if (found == m_contacts.end())
		{
			return { nullptr, ContactSide::None };
		}
	}

	return { found->second.get(), side};
}

const Contact* CollisionSystem::oldContact(const Collider::Id collider1, const Collider::Id collider2)
{
	auto key = ColliderKey{ collider1, collider2 };
	auto found = m_oldContacts.find(key);

	return found != m_oldContacts.end() ? found->second.get() : nullptr;
}