#include "CollisionMethod.h"

namespace bv
{
    namespace coldet
    {
        template<>
        void CollisionMethodTemplate<Sphere, Sphere>::find()
        {
            if (Intersection::test(*m_first, *m_second, m_localizeMatrix))
            {
                m_contact = ContactPtr(new Contact());
            }
        };
    }
}

