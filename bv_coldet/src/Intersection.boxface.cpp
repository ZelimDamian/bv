#include "Intersection.coldet.h"

using namespace bv;

bool Intersection::test(const Box &box1, const std::array<glm::vec3, 3>& vertices)
{
    return test(box1, vertices[0], vertices[1], vertices[2]);
}

bool Intersection::test(const Box &box,
                        const glm::vec3 &vertex1,
                        const glm::vec3 &vertex2,
                        const glm::vec3 &vertex3 )
{
    Box faceBox;

    faceBox.enclose(vertex1);
    faceBox.enclose(vertex2);
    faceBox.enclose(vertex3);

    return Intersection::test(box, faceBox);

    //return false;
}

bool Intersection::test(const Box &box,
                        const glm::vec3 &vertex1,
                        const glm::vec3 &vertex2,
                        const glm::vec3 &vertex3,
                        const glm::mat4 &mat)
{
    Box faceBox;

    faceBox.enclose(glm::vec3(mat * glm::vec4(vertex1, 1.0f)));
    faceBox.enclose(glm::vec3(mat * glm::vec4(vertex2, 1.0f)));
    faceBox.enclose(glm::vec3(mat * glm::vec4(vertex3, 1.0f)));

    return Intersection::test(box, faceBox);

    //return false;
}
