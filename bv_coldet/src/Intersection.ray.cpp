#include "Intersection.coldet.h"

using namespace bv;

bool Intersection::test(const Box& box, const Ray& ray)
{
    float t;
    return test(box, ray, t);
}

bool Intersection::test(const Box& box, const Ray& ray, const glm::mat4& mat, float& t)
{
    return test(box, Ray::transform(ray, mat), t);
}

//bool Intersection::test(const Box& box, const Ray& ray, float& param)
//{
//    glm::vec3 rayOrigin = ray.pos();
//    glm::vec3 rayDirection = ray.dir();
//    glm::vec3 boxMin = box.min();
//    glm::vec3 boxMax = box.max();
//
//    //first test if start in box
//    if (rayOrigin.x >= boxMin.x
//        && rayOrigin.x <= boxMax.x
//        && rayOrigin.y >= boxMin.y
//        && rayOrigin.y <= boxMax.y
//        && rayOrigin.z >= boxMin.z
//        && rayOrigin.z <= boxMax.z)
//    {
//        param = 0.0f;// here we consider cube is full and rayOrigin is in cube so intersect at rayOrigine
//        return true;
//    }
//
//    //Second we check each face
//    glm::vec3 maxT = glm::vec3(-1.0f, -1.0f, -1.0f);
//    //Vector3 minT = new Vector3(-1.0f);
//    //calcul intersection with each faces
//    if (rayOrigin.x < boxMin.x && rayDirection.x != 0.0f)
//        maxT.x = (boxMin.x - rayOrigin.x) / rayDirection.x;
//    else if (rayOrigin.x > boxMax.x && rayDirection.x != 0.0f)
//        maxT.x = (boxMax.x - rayOrigin.x) / rayDirection.x;
//    if (rayOrigin.y < boxMin.y && rayDirection.y != 0.0f)
//        maxT.y = (boxMin.y - rayOrigin.y) / rayDirection.y;
//    else if (rayOrigin.y > boxMax.y && rayDirection.y != 0.0f)
//        maxT.y = (boxMax.y - rayOrigin.y) / rayDirection.y;
//    if (rayOrigin.z < boxMin.z && rayDirection.z != 0.0f)
//        maxT.z = (boxMin.z - rayOrigin.z) / rayDirection.z;
//    else if (rayOrigin.z > boxMax.z && rayDirection.z != 0.0f)
//        maxT.z = (boxMax.z - rayOrigin.z) / rayDirection.z;
//
//    //get the maximum maxT
//    if (maxT.x > maxT.y && maxT.x > maxT.z)
//    {
//        if (maxT.x < 0.0f)
//            return false;// ray go on opposite of face
//        //coordonate of hit point of face of cube
//        float coord = rayOrigin.z + maxT.x * rayDirection.z;
//        // if hit point coord ( intersect face with ray) is out of other plane coord it miss
//        if (coord < boxMin.z || coord > boxMax.z)
//            return false;
//        coord = rayOrigin.y + maxT.x * rayDirection.y;
//        if (coord < boxMin.y || coord > boxMax.y)
//            return false;
//        param = maxT.x;
//        return true;
//    }
//    if (maxT.y > maxT.x && maxT.y > maxT.z)
//    {
//        if (maxT.y < 0.0f)
//            return false;// ray go on opposite of face
//        //coordonate of hit point of face of cube
//        float coord = rayOrigin.z + maxT.y * rayDirection.z;
//        // if hit point coord ( intersect face with ray) is out of other plane coord it miss
//        if (coord < boxMin.z || coord > boxMax.z)
//            return false;
//        coord = rayOrigin.x + maxT.y * rayDirection.x;
//        if (coord < boxMin.x || coord > boxMax.x)
//            return false;
//        param = maxT.y;
//        return true;
//    }
//    else //Z
//    {
//        if (maxT.z < 0.0f)
//            return false;// ray go on opposite of face
//        //coordonate of hit point of face of cube
//        float coord = rayOrigin.x + maxT.z * rayDirection.x;
//        // if hit point coord ( intersect face with ray) is out of other plane coord it miss
//        if (coord < boxMin.x || coord > boxMax.x)
//            return false;
//        coord = rayOrigin.y + maxT.z * rayDirection.y;
//        if (coord < boxMin.y || coord > boxMax.y)
//            return false;
//        param = maxT.z;
//        return true;
//    }
//}

// Optimized method
bool Intersection::test(const Box& box, const Ray &r, float& tmin)
{
	float tmax, tymin, tymax, tzmin, tzmax;
	tmin =  (box[r.sign[0]].x - r.pos().x) * r.inv_direction.x;
	tmax =  (box[1-r.sign[0]].x - r.pos().x) * r.inv_direction.x;
	tymin = (box[r.sign[1]].y - r.pos().y) * r.inv_direction.y;
	tymax = (box[1-r.sign[1]].y - r.pos().y) * r.inv_direction.y;
	if ( (tmin > tymax) || (tymin > tmax) )
		return false;
	if (tymin > tmin)
		tmin = tymin;
	if (tymax < tmax)
		tmax = tymax;
	tzmin = (box[r.sign[2]].z - r.pos().z) * r.inv_direction.z;
	tzmax = (box[1-r.sign[2]].z - r.pos().z) * r.inv_direction.z;
	if ( (tmin > tzmax) || (tzmin > tmax) )
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	return ( (tmin < FLT_MAX) && (tmax > 0.0f) );
}

bool Intersection::test(const Plane& plane, const Ray& ray, float& t)
{
    float numerator = glm::dot(plane.normal(), plane.pos() - ray.pos());
    float denominator = glm::dot(plane.normal(), ray.dir());

    if (denominator != 0)
    {
        t = numerator / denominator;
    }

    return true;
}

bool Intersection::test(const Ray& ray, const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3,
                        const glm::mat4& mat, float& t)
{
    return test(Ray::transform(ray, mat), v1, v2, v3, t);
}

bool Intersection::test(const Ray &ray, const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec3 &v3)
{
	float t;
	return test(ray, v1, v2, v3, t);
}

bool Intersection::test(const Ray &ray, const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec3 &v3, float& t)
{
    float u, v;
    return test(ray, v1, v2, v3, t, u, v);
}

bool Intersection::test(const Ray &ray, const glm::vec3 &v1, const glm::vec3 &v2, const glm::vec3 &v3, float& t, float& u, float& v)
{
    glm::vec3 e1, e2;  //Edge1, Edge2
    glm::vec3 P, Q, T;
    float det, inv_det;

    //Find vectors for two edges sharing V1
    e1 = v2 - v1;
    e2 = v3 - v1;

    //Begin calculating determinant - also used to calculate u parameter
    P = glm::cross(ray.dir(), e2);

    //if determinant is near zero, ray lies in plane of triangle
    det = glm::dot(e1, P);

    //NOT CULLING
    if(det > -FLT_EPSILON && det < FLT_EPSILON) return false;
    inv_det = 1.f / det;

    //calculate distance from V1 to ray origin
    T = ray.pos() - v1;

    //Calculate u parameter and test bound
    u = glm::dot(T, P) * inv_det;

    //The intersection lies outside of the triangle
    if(u < 0.f || u > 1.f) return false;

    //Prepare to test v parameter
    Q = glm::cross(T, e1);

    //Calculate V parameter and test bound
    v = glm::dot(ray.dir(), Q) * inv_det;
    //The intersection lies outside of the triangle
    if(v < 0.f || u + v  > 1.f) return false;

    t = glm::dot(e2, Q) * inv_det;

    // No hit, no win
    return t > FLT_EPSILON;
}
