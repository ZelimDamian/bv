#include <thread>
#include "CollisionMethodOctree.h"

#include "Intersection.coldet.h"
#include "MeshContact.h"
#include "DebugMesh.h"

#include "Parallel.h"

using namespace bv;
using namespace coldet;

namespace bv
{
    namespace coldet
    {

        template<>
        void CollisionMethodOctree::init()
        {
        }

        template<>
        void CollisionMethodOctree::recurse(const Octree& octree1, const Octree& octree2)
        {
	        const size_t oct1_count = octree1.faces().size();
	        const size_t oct2_count = octree2.faces().size();

	        if (oct1_count != 0 && oct2_count != 0 && Intersection::test(octree1.box(), octree2.box(), m_localizeMatrix))
            {
				if (octree1.isLeaf() && octree2.isLeaf())
				{
					if (m_debugRenderFirst || m_debugRenderSecond)
					{
						auto debug = DebugMesh::instance();

						if (m_debugRenderFirst)
						{
							debug->box(octree1.box(), m_mat1);
						}

						if (m_debugRenderSecond)
						{
							debug->box(octree2.box(), m_mat2);
						}
					}

	                const auto mesh1 = m_first->mesh();
	                const auto mesh2 = m_second->mesh();

					for (size_t i = 0; i < oct2_count; i++)
					{
						const Tri face2 = octree2.face(i);

						for (size_t j = 0; j < oct1_count; j++)
						{
							const Tri face1 = octree1.face(j);

							m_counter++;

							const auto& positions1 = mesh1->positions;
							const auto& positions2 = mesh2->positions;

//                            if(Intersection::test(face1.vertices(positions1), face2.vertices(positions2), m_localizeMatrix))
							{
								((MeshContact*)m_contact.get())->addPair(face1.index, face2.index);
							}
						}
					}
                }
                else if (octree1.isLeaf())
                {
                    for (size_t i = 0; i < 8; i++)
                    {
                        //OctreePtr child = ;
                        recurse(octree1, *octree2.children(i));
                    }
                }
                else if (octree2.isLeaf())
                {
                    for (size_t i = 0; i < 8; i++)
                    {
                        //OctreePtr child = ;
                        recurse(*octree1.children(i), octree2);
                    }
                }
                else
                {
                    for (size_t i = 0; i < 8; i++)
                    {
                        for (size_t j = 0; j < 8; j++)
                        {
                            //OctreePtr child1 = ;
                            //OctreePtr child2 = ;
                            recurse(*octree1.children(i), *octree2.children(j));
                        }
                    }
                }
            }
        }

        template<>
        void CollisionMethodOctree::find()
        {
            if (m_first && m_second)
            {
                // create an empty contact
	            auto contact = new MeshContact(m_first->mesh(), m_second->mesh());
	            m_contact = ContactPtr(contact);

                // run the recursive tree traversal to find pairs
                CollisionMethodOctree::recurse(*m_first, *m_second);

	            auto& pairs = contact->pairs();
	            
	            auto mesh1 = m_first->mesh();
	            auto mesh2 = m_second->mesh();


				if (pairs.size() > 0)
				{
					// the mast to be used to store the testing results
					std::vector<int> mask(pairs.size(), 0);

					auto tester = [=, &mask](size_t idx)
					{
						const auto& positions1 = mesh1->positions;
						const auto& positions2 = mesh2->positions;

						const auto pair = pairs[idx];
						mask[idx] = Intersection::test(mesh1->tri(pair.first).vertices(positions1),
							mesh2->tri(pair.second).vertices(positions2),
							m_localizeMatrix);
					};

					// run testing in parallel
					Parallel::foreach(pairs, tester);

					bv::filter(pairs, [&](const IdxPair& pair)
					{
						// find index of the pair in question
						size_t idx = &pair - &pairs[0];
						// use mask value as filter predicate
						return mask[idx];
					});
				}
            }
        }
    }
}