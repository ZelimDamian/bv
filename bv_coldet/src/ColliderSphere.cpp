#include "ColliderSphere.h"
#include "Sphere.h"
#include "Entity.h"

ColliderSphere::ColliderSphere()
{
}


ColliderSphere::~ColliderSphere()
{
}

void ColliderSphere::initialize()
{
	m_shape = PTR(CollisionShape)(new Sphere(glm::vec3(), 1.0f));
}
