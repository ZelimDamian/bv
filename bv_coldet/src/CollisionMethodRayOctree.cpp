#include "CollisionMethodRayOctree.h"
#include "RayMeshContact.h"

#include "DebugMesh.h"

namespace bv
{
    namespace coldet
    {
        template<>
        void CollisionMethodRayOctree::recurse(
                const Ray& ray,
                const Octree& octree)
        {
            // check that we hit the bounding box
			if (Intersection::test(octree.box(), ray))
			{
				// if the box is a leaf
				if (octree.isLeaf())
				{
//					const BufferVec3& positions = octree.mesh()->positions();
//
					for (size_t j = 0; j < octree.faces().size(); j++)
					{
						const Tri face = octree.face(j);
//
//						std::array<glm::vec3, 3> vertices = face.vertices(positions);
//
						float t;
//
//						// perform the actual test
//						if (Intersection::test(ray, vertices[0], vertices[1], vertices[2], t))
//						{
							((RayMeshContact*)m_contact.get())->addHit(face.index, t);
//						}
					}
				}
				else
				{
					for (int i = 0; i < 8; ++i)
					{
						recurse(ray, *octree.children(i));
					}
				}
			}
        }

	    struct OctreeHit
	    {
		    OctreeHit(const Octree* octree, float t): octree(octree), t(t) { }
		    const Octree* octree;
		    float t;
	    };

	    void collectBoxes(const Octree* octree, const Ray& ray, std::vector<OctreeHit>& boxes)
	    {
		    float t;

		    // check that we hit the bounding box
		    if (Intersection::test(octree->box(), ray, t))
		    {
			    // if the box is a leaf
			    if (octree->isLeaf())
			    {
				    // and is has some faces
				    if(!octree->faces().empty())
				    {
					    // store it into the list
					    boxes.push_back(OctreeHit{octree, t});
				    }
			    }
			    else
			    {
				    for (uint8_t i = 0; i < 8; ++i)
				    {
					    collectBoxes(octree->children(i), ray, boxes);
				    }
			    }
		    }
	    }

        template<>
        void CollisionMethodRayOctree::find()
        {
            if (m_first && m_second)
            {
				Ray ray = Ray::transform(*m_first, glm::inverse(m_mat2));
                m_contact = ContactPtr(new RayMeshContact(ray, m_second->mesh(), m_mat2));

	            auto& positions = m_second->mesh()->positions;

	            std::vector<OctreeHit> boxes;
	            boxes.reserve(m_second->numLeafs() / 2);

                {
                    auto tock = Profiler::instance()->tick("collect boxes");

                    // collect all intersected leaf boxes
                    collectBoxes(m_second, ray, boxes);

                    tock();
                }

                {

                    auto tock = Profiler::instance()->tick("sort boxes");

                    // sort box hits according to t param
                    std::sort(boxes.begin(), boxes.end(), [](const OctreeHit& hit1, const OctreeHit& hit2)
                    {
                        return hit1.t < hit2.t;
                    });

                    tock();
                }

                {
                    auto tock = Profiler::instance()->tick("primitive test");

                    // retrieve the contact
                    auto contact = (RayMeshContact *) m_contact.get();

                    for (int i = 0; i < boxes.size(); ++i)
                    {
                        auto octree = boxes[i].octree;

                        for (size_t j = 0; j < octree->faces().size(); j++)
                        {
                            const Tri face = octree->face(j);

                            std::array<glm::vec3, 3> vertices = face.vertices(positions);

                            float t, u, v;

                            // perform the actual test
                            if (Intersection::test(ray, vertices[0], vertices[1], vertices[2], t, u, v))
                            {
                                contact->addHit(face.index, t, u, v);
                            }
                        }

                        // if we found some hits they must be closer than the ones found later
                        // so there is no point in checking further
                        if(contact->pairs().size())
                        {
                            return;
                        }
                    }

                    tock();
                }
            }
        }
    }
}