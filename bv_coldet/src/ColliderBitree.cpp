#include "ColliderBitree.h"

#include "RenderingManager.h"

ColliderBitree::ColliderBitree()
{
}


ColliderBitree::~ColliderBitree()
{
	DESTROY(m_shape);
}

void ColliderBitree::entity(EntityPtr entity)
{
	ModelPtr model = DYN_CAST(Model)(entity);

	m_shape = CollisionShapePtr(new Bitree(model->mesh()->faces()));

	Collider::entity(entity);
}

void treeRender(BitreePtr tree)
{
	if (tree->isLeaf())
	{
		glm::vec4 color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		Box box = tree->box();
		RenderingManager::instance()->renderer()->render(box, color);
	}
	else
	{
		//for (size_t i = 0; i < octree->children().size(); i++)
		// (size_t i = 0; i < 8; i++)
		{
			treeRender(BitreePtr(tree->left()));
			treeRender(BitreePtr(tree->right()));
		}
	}
}

void ColliderBitree::render()
{
	BitreePtr bitree = DYN_CAST(Bitree)(m_shape);

	if (bitree) {
		treeRender(bitree);
	}
}