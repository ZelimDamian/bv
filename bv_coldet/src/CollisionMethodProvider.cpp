#include "CollisionMethodProvider.h"

#include "Box.h"
#include "Sphere.h"
#include "Octree.h"
#include "Bitree.h"

using namespace bv;
using namespace coldet;

CollisionMethodPtr CollisionMethodProvider::get(Entity::Id entity1, Entity::Id entity2)
{
    // get the second collider
    Collider::Id collider1 = Collider::find(entity1);

    if(!collider1)
    {
        Log::debug("No collider for the first entity in CollisionMethodProvider!");
        return nullptr;
    }

    // get the concept of the collision shape
    auto* concept1 = collider1.ref().concept();

    // get the second collider
    Collider::Id collider2 = Collider::find(entity2);

    if(!collider2)
    {
        Log::debug("No collider for the second entity in CollisionMethodProvider!");
        return nullptr;
    }

    // get the concept of the collision shape
    auto* concept2 = collider2.ref().concept();

    // find the appropriate collision detection method based on the shapes
    auto method = get(concept1, concept2);

	// specify the transformation of the first shape
	method->mat1(entity1->transform());
	// specify the transformation of the second shape
	method->mat2(entity2->transform());

	return method;
}

template <class TShape1, class TShape2>
CollisionMethodPtr method_cast(const Collider::ShapeConcept* shapeConcept1, const Collider::ShapeConcept* shapeConcept2)
{
	if (auto* concept1 = dynamic_cast<const Collider::ShapeModel<TShape1>*>(shapeConcept1))
	{
		auto& shape1 = concept1->shape();

		if (auto* concept2 = dynamic_cast<const Collider::ShapeModel<TShape2>*>(shapeConcept2))
		{
			auto& shape2 = concept2->shape();

			auto* method = new CollisionMethodTemplate<TShape1, TShape2>(shape1, shape2);
			return CollisionMethodPtr(method);
		}
	}
	return nullptr;
};


CollisionMethodPtr CollisionMethodProvider::get(const Collider::ShapeConcept* concept1, const Collider::ShapeConcept* concept2)
{

    // make sure we actually have collision shapes attached
    if(!concept1 || !concept2)
    {
        return nullptr;
    }

	if (auto method = method_cast<Octree, Octree>(concept1, concept2))
	{
		return method;
	}
	if (auto method = method_cast<Bitree, Bitree>(concept1, concept2))
	{
		return method;
	}
	if (auto method = method_cast<Box, Box>(concept1, concept2))
	{
		return method;
	}
	if (auto method = method_cast<Box, Sphere>(concept1, concept2))
	{
		return method;
	}
	if (auto method = method_cast<Sphere, Box>(concept1, concept2))
	{
		return method;
	}
	if (auto method = method_cast<Sphere, Sphere>(concept1, concept2))
	{
		return method;
	}
	if (auto method = method_cast<Ray, Octree>(concept1, concept2))
	{
		return method;
	}

	Log::debug("Didn't find a suitable collision method!");

	return nullptr;
}