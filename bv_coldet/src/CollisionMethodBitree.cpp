#include "CollisionMethodBitree.h"
#include "RenderingSystem.h"
#include "Log.h"
#include "Intersection.coldet.h"
#include "MeshContact.h"

namespace bv
{
    namespace coldet
    {

        template<>
        void CollisionMethodTemplate<Bitree, Bitree>::recurse(const Bitree& bitree1, const Bitree& bitree2)
        {
            if (Intersection::test(bitree2.box(), bitree1.box(), m_localizeMatrix))
            {
                if (bitree1.isLeaf() && bitree2.isLeaf())
                {
                    const Mesh::Id mesh1 = bitree1.mesh();
                    const Mesh::Id mesh2 = bitree2.mesh();

                    //RenderingManager::instance()->renderer()->render(octree1->box(), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
                    //RenderingManager::instance()->renderer()->render(octree2->box(), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));

                    size_t count1 = bitree1.faces().size();
                    size_t count2 = bitree2.faces().size();

                    if (count1 != 0 && count2 != 0)
                    {

                        for (size_t i = 0; i < count2; i++)
                        {
                            const Tri face2 = bitree2.faces(i);

                            for (size_t j = 0; j < count1; j++)
                            {
                                const Tri face1 = bitree1.faces(j);

                                m_counter++;

	                            if(Intersection::test(  face1.vertices(mesh1->positions),
	                                                    face2.vertices(mesh2->positions),
	                                                    m_localizeMatrix ))
                                {
                                    //RenderingSystem::instance()->renderer()->renderTriangle(v21, v22, v23, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
                                    //RenderingSystem::instance()->renderer()->render(*face1, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
                                    ((MeshContact*) m_contact.get())->addPair(face1.index, face2.index);
                                }
                            }
                        }
                    }
                }
                else if (bitree1.isLeaf())
                {
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(bitree1, *bitree2.left());
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(bitree1, *bitree2.right());
                }
                else if (bitree2.isLeaf())
                {
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(*bitree1.left(),  bitree2);
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(*bitree1.right(), bitree2);
                }
                else
                {
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(*bitree1.left(),  *bitree2.left());
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(*bitree1.left(),  *bitree2.right());
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(*bitree1.right(), *bitree2.left());
                    CollisionMethodTemplate<Bitree, Bitree>::recurse(*bitree1.right(), *bitree2.right());
                }
            }
        }

        template<>
		void CollisionMethodTemplate<Bitree, Bitree>::find()
        {
            if (m_first && m_second)
            {
                m_contact = ContactPtr(new MeshContact(m_first->mesh(), m_second->mesh()));
				CollisionMethodTemplate<Bitree, Bitree>::recurse(*m_first, *m_second);
            }

            if (m_counter)
            {
                Log::writeln(m_counter);
            }
        }

    }
}
