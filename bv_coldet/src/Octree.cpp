#include <Bounding.h>
#include "Octree.h"
#include "Intersection.coldet.h"

using namespace bv;
using namespace coldet;

Octree::Octree(Octree* parent, Box box)// : m_children()
{
	m_parent = parent;
	m_box = box;
	m_level = parent->level() + 1;
	m_leafs = 0;
	m_mesh = parent->m_mesh;
}

Octree::Octree(Mesh::Id mesh)
{
	m_parent = NULL;
	m_level = 0;

	m_mesh = mesh;
	m_faces = mesh->triangles;

	encloseFaces();
	distribute();
	updateNumLeafs();
}

void Octree::refit()
{
	if(isLeaf())
	{
		encloseFaces();
	}
	else
	{
		for (int i = 0; i < 8; ++i)
		{
			m_children[i].refit();
		}

		m_box = Box();

		for (int i = 0; i < 8; ++i)
		{
			m_box.enclose(m_children[i].box());
		}
	}
}

void Octree::updateNumLeafs()
{
	m_leafs = 0;

	if(!isLeaf())
	{

		for (int i = 0; i < 8; ++i)
		{
			m_children[i].updateNumLeafs();
			m_leafs += m_children[i].m_leafs;
		}
	}
	else
	{
		if(m_faces.size())
		{
			m_leafs = 1;
		}
	}
}

void Octree::encloseFaces()
{
	const int faceCount = m_faces.size();

	if (faceCount > 0)
	{
		m_box = Box();

		for (int i = 0; i < m_faces.size(); ++i)
		{
			Tri& ind = m_faces[i];

			m_box.enclose(m_mesh->pos(ind));
		}
	}
}

void Octree::distribute()
{
	//Log::write("Distributing octree: "); Log::write(m_level);

	// heuristic
	if (m_faces.size() > MAX_FACES && m_level <= MAX_LEVEL)
    {
        split();

		for (int i = m_faces.size() - 1; i >= 0; i--)
        {
			Tri & face = m_faces[i];

			for (size_t j = 0; j < 8; j++)
			{
				if (Intersection::test(m_children[j].box(), face.vertices(m_mesh->positions)))
                {
					m_children[j].faces().push_back(face);
                    break;
                }
            }
        }

        //m_faces->clear();

		for (size_t i = 0; i < 8; i++)
		{
			m_children[i].distribute();
			m_children[i].refit();
		}
    }
}

void Octree::split()
{
	m_children.reserve(8);

	const glm::vec3 halfsize = m_box.halfsize();
	const glm::vec3 quaterSize = halfsize / 2.0f;
	const glm::vec3 start = m_box.min() + quaterSize;

	for (size_t i = 0; i < 8; i++) {
				
		int x = i % 2;
		int y = i / 4 % 2;
		int z = i / 2 % 2;
				
		const glm::vec3 center = start +
									x * halfsize.x * glm::vec3(1.0f, 0.0f, 0.0f) +
									y * halfsize.y * glm::vec3(0.0f, 1.0f, 0.0f) +
									z * halfsize.z * glm::vec3(0.0f, 0.0f, 1.0f);
				
		const float EPSILON = 1.0f + 0.000001f;

		m_children.emplace_back(this, Box{ center - quaterSize * EPSILON, center + quaterSize * EPSILON });
	}
}

unsigned int Octree::numLeafs() const
{
	return m_leafs;
}

bool Octree::needsRefit()
{
	auto meshAABB = Bounding::box(m_mesh);

	return box().min() != meshAABB.min() &&
			box().max() != meshAABB.max();
}