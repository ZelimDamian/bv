#include "ContactRendererComponent.h"
#include "Collider.h"
#include "Octree.h"
#include "RenderingSystem.h"
#include "RenderMesh.h"
#include "CollisionSystem.h"
#include <DebugMesh.h>

#include "UtilitiesColDet.h"

using namespace bv;
using namespace coldet;

void ContactRendererComponent::initialize()
{
	RenderingSystem::instance()->onrender([this]
	{
		if(this->isActive())
		{
			this->render();
		}
	});
}

void ContactRendererComponent::render()
{
    Collider::Id collider = Collider::find(m_entity);

    if(!collider)
    {
        Log::error("Collider missing when updating contact mesh");
    }

    // retreive the contact
	auto results = CollisionSystem::instance()->contacts(collider);

	for (auto &&result : results)
	{
		// need to make sure that its a mesh contact
		if (const MeshContact *contact = dynamic_cast<const MeshContact *>(result.contact))
		{
			// only update mesh when there is contact
			if (contact->isFound())
			{
				// get the indices of faces in contact on the mesh belonging
				std::vector<int> faceIndices = contact->uniqueIndicesSided(result.side);

				// the surface mesh
				Mesh::Id mesh = contact->mesh(result.side);

				for (int i = 0; i < faceIndices.size(); ++i)
				{
					DebugMesh::instance()->triangle(mesh->triangles[faceIndices[i]].vertices(mesh->positions));
				}

				const glm::vec4 color = result.side == ContactSide::Second ?
				                        glm::vec4(0.8f, 0.2f, 0.1f, 1.0f) : glm::vec4(0.8f, 0.8f, 0.1f, 1.0f);

				DebugMesh::instance()->render(m_entity->transform(), RenderMode::TRIANGLES, color);
			}
		}
		else
		{
			//Log::debug("Wrong contact type found!");
		}
	}
}
