#include "Bitree.h"

using namespace bv;
using namespace coldet;

glm::vec3 center(const VectorVec3& positions, Tri ind)
{
    const glm::vec3 a = positions[ind[0]];
    const glm::vec3 b = positions[ind[1]];
    const glm::vec3 c = positions[ind[2]];

    return (a + b + c) / 3.0f;
}

Bitree::Bitree(Mesh::Id mesh)
{
	m_parent = NULL;
	m_level = 0;

	m_mesh = mesh;

	m_faces = mesh->triangles;

//	distribute();
}

Bitree::Bitree(Bitree* parent)
{
	m_parent = parent;
	m_level = parent->level() + 1;
	m_mesh = parent->m_mesh;
}

float Bitree::MIN_ERROR = 5.1f;

void Bitree::refit()
{
	for (int i = 0; i < m_faces.size(); ++i)
	{
		Tri & ind = m_faces[i];
		auto& positions = m_mesh->positions;

		m_box.enclose(positions[ind.vi[0]]);
		m_box.enclose(positions[ind.vi[1]]);
		m_box.enclose(positions[ind.vi[2]]);
	}
}

void Bitree::distribute()
{
	refit();

	if (m_level < MAX_LEVEL && m_faces.size() > MIN_TRI_COUNT
		&& (m_faces.size() > TARGET_COUNT || errorMetric(m_box) > MIN_ERROR))
	{
		int longestAxis = longestAxis = m_box.longestAxis();

		// The midpoint of all of the triangles
		glm::vec3 geometricAverage;

		size_t faceCount = m_faces.size();

		// Iterate through all of the triangles and calculate the midpoint value
		for (size_t i = 0; i < faceCount; i++)
		{
			geometricAverage += center(m_mesh->positions, m_faces[i]);
		}

		// Now average the midpoint
		geometricAverage /= faceCount;

		m_left = new Bitree(this);
		m_right = new Bitree(this);

		// Find the depth of each leaf
		for (size_t i = 0; i < faceCount; i++)
		{
			glm::vec3 middle = center(m_mesh->positions, m_faces[i]);

			// Test which tree the triangle belongs in, left or right
			if (geometricAverage[longestAxis] > middle[longestAxis])
			{
				m_left->m_faces.push_back(m_faces[i]);
			}
			else
			{
				m_right->m_faces.push_back(m_faces[i]);
			}
		}

		m_left->distribute();
		m_right->distribute();
	}
}

float Bitree::errorMetric(Box& box)
{
	glm::vec3 tmp = glm::vec3(1.0f, 1.0f, 1.0f) + box.size();
	return glm::length2(tmp);
}