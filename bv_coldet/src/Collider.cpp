#include "Collider.h"
#include "CollisionSystem.h"

using namespace bv;
using namespace coldet;

Collider::Collider(Id id, Entity::Id entity): ComponentBase(id, entity)
{
    CollisionSystem::instance();
}