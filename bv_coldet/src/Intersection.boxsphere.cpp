#include "Intersection.coldet.h"

using namespace bv;

glm::vec3 clipLocalPointToBox(const Box& box, glm::vec3 point)
{
    point = glm::min(box.max(), point);
    point = glm::max(box.min(), point);

    return point;
}

bool Intersection::test(const Box &box, const Sphere &sphere, const glm::mat4 &matrix)
{
    // Transform the sphere into local box space
    glm::vec3 localCenter = glm::vec3(matrix * glm::vec4(sphere.center() - box.center(), 1.0f));

    glm::vec3 closestOnBox = clipLocalPointToBox(box, localCenter);

    float sqrDistance = glm::length2(closestOnBox - localCenter);

    float radius = sphere.radius();

    if (sqrDistance < radius * radius)
    {
        return true;
    }

    return false;
}

bool Intersection::test(const Sphere& sphere1, const Sphere& sphere2, const glm::mat4& mat)
{
    // Transform the sphere into local box space
    glm::vec3 localCenter = glm::vec3(mat * glm::vec4(sphere2.center() - sphere1.center(), 1.0f));

    float distance = glm::length(localCenter);

    return distance < sphere1.radius() + sphere2.radius();
}
