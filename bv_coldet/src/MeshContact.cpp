#include "MeshContact.h"

#include <UtilitiesColDet.h>
#include <Intersection.Coldet.h>
#include <RayMeshContact.h>
#include <Parallel.h>

#include "CollisionSystem.h"

#include "CollisionMethodRayOctree.h"

using namespace bv;
using namespace coldet;

void MeshContact::updateInfo() const
{
	if(!m_info)
	{
		m_info = InfoPtr(new Info());
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Slave bits
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// get the indices of faces in contact on the mesh belonging
	std::vector<int> slaveFaceIndices = uniqueIndicesFirst();

	// find the indices of slave nodes in contact
	m_info->slaveVertIndices = MeshUtilities::vertIndices(slaveFaceIndices, mesh1());

	// retrieve contact from previous frame
	auto oldContact = static_cast<const MeshContact*>(CollisionSystem::instance()->oldContact(m_slave, m_master));

	// check if it was a valid contact
	if(oldContact && oldContact->isFound())
	{
		auto oldInfo = oldContact->info();

		// also look for penetrations from the old contacts
		bv::append(m_info->slaveVertIndices, oldInfo->penetratingIndices);
	}

	// make sure we don't ray-cast redundantly
	bv::filterUnique(m_info->slaveVertIndices);

	// list of slave node positions
	std::vector<glm::vec3>& slaveNodePositions = m_info->slaveVertPositions;
	slaveNodePositions.resize(m_info->slaveVertIndices.size());

	// list of slave contact normals
	VectorVec3& slaveNormals = m_info->slaveContactNormals;
	slaveNormals.resize(m_info->slaveVertIndices.size());

	// collect all the positions and normals
	for (int i = 0; i < m_info->slaveVertIndices.size(); ++i)
	{
		int idx = m_info->slaveVertIndices[i];
		// the position of the slave node
		const glm::vec3& nodePos = m_mesh1->positions[idx];
		slaveNodePositions[i] = Math::transform(nodePos, m_slave->entity()->transform());
		slaveNormals[i] = m_mesh1->vertices[idx].get<Mesh::TVertex::Nrm>();
	}

	// transform to global coordinate system
	Math::transformNormals(slaveNormals, m_slave->entity()->transform());

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// Penetration bits
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// slave vertex penetrations
	std::vector<float>& m_penetrations = m_info->penetrations;
	// allocate memory in advance
	m_penetrations.resize(m_info->slaveVertIndices.size());

	// retrieve the master bvh
	auto concept = m_master->concept<Octree>();
	assert(concept != nullptr);
	auto& octree = concept->shape();

	std::vector<int> penetrationMask(m_info->slaveVertIndices.size(), 0);

	// push each slave node out of contact
	Parallel::foreach(m_info->slaveVertIndices, [&] (size_t i)
	{
		// the index of the slave node
		int nodeIndex = m_info->slaveVertIndices[i];

		// the position of the slave node
		glm::vec3 nodePos = slaveNodePositions[i];
		// shoot ray in opposite direction from slave normal
		glm::vec3 normal = -slaveNormals[i];

		// build the ray along contact normal starting from slave node
		Ray ray(nodePos, normal);

		// build collision method
		CollisionMethodRayOctree method(ray, octree);
		method.mat2(m_master->entity()->transform());

		// execute method to get contact
		auto result = method();
		auto rayContact = static_cast<RayMeshContact*>(result.get());

		// make sure contacts are sorted according to depth - ray t param
		if (rayContact->isFound())
		{
            rayContact->sort();

//			glm::vec3 masterNormal = Math::transformNormal(m_mesh2->normals[rayContact->pairs()[0].idx], m_master->entity()->transform());
//			float dot = glm::dot(masterNormal, glm::normalize(ray.dir()));
//
			// check that we hit a front facing face on master
//			if(dot > 0.5f)
//			{
//				return;
//			}

            m_penetrations[i] = rayContact->pairs().begin()->t;
			penetrationMask[i] = 1;
		}
	});

	// add all penetrating slave vertex indices to the appropriate list
	for (int i = 0; i < penetrationMask.size(); ++i)
	{
		if(penetrationMask[i])
		{
			m_info->penetratingIndices.push_back(m_info->slaveVertIndices[i]);
		}
	}
}
