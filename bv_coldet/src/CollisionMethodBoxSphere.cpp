#include "CollisionMethodBoxSphere.h"
#include "Intersection.coldet.h"

namespace bv
{
	namespace coldet
	{
		template<>
		void CollisionMethodBoxSphere::find()
		{
            if (Intersection::test(*m_first, *m_second, m_localizeMatrix))
			{
				m_contact = ContactPtr(new Contact());
			}
		}
	}
}
