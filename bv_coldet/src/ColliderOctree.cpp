#include "ColliderOctree.h"
#include "RenderingManager.h"

ColliderOctree::ColliderOctree()
{

}


ColliderOctree::~ColliderOctree(void)
{
	DESTROY(m_shape);
}

void ColliderOctree::entity(EntityPtr entity)
{
	ModelPtr model = DYN_CAST(Model)(entity);

	if (model->octree())
	{
		m_shape = CollisionShapePtr(model->octree());
	}
	else
	{
		m_shape = CollisionShapePtr(new Octree(model->mesh()->faces()));
	}

	Collider::entity(entity);
}

void treeRender(const Octree* tree)
{
	if (tree->isLeaf())
	{
		glm::vec4 color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		Box box = tree->box();
		RenderingManager::instance()->renderer()->render(box, color);
	}
	else
	{
		//for (size_t i = 0; i < octree->children().size(); i++)
		for (size_t i = 0; i < 8; i++)
		{
			treeRender(tree->children(i));
		}
	}
}

void ColliderOctree::render()
{
	OctreePtr tree = DYN_CAST(Octree)(m_shape);

	if (tree) {
		treeRender(TO_RAW_PTR(tree));
	}
}