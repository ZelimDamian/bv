#include "CollisionMethod.h"

namespace bv
{
    namespace coldet
    {
        template<>
        void CollisionMethodTemplate<Box, Box>::find()
        {
            if (Intersection::test(*m_first, *m_second, glm::inverse(m_localizeMatrix)))
            {
                m_contact = ContactPtr(new Contact());
            }
        };
    }
}