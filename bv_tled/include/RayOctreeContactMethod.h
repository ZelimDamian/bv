#pragma once

#include "ContactMethods.h"

namespace bv
{
	namespace tled
	{
		class RayOctreeContactMethod
		{
		public:
			static ContactMethod create();
		};
	}
}