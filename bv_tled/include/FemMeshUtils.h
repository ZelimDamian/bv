#pragma once

#include "TetMesh.h"
#include "TetraGen.h"
#include "Bounding.h"
#include "HexMesh.h"
#include "HexGen.h"
#include "Solver.h"
#include "SolverCL.h"
#include "AssemblyComponent.h"
#include "AssemblyRendererComponent.h"
#include "CalculatorComponent.h"
#include "SolverComponent.h"
#include "Collider.h"
#include "Octree.h"
#include "ExporterInpComponent.h"

#include "CalculatorTet.h"
#include "CalculatorTetCL.h"
#include "CalculatorHex.h"
#include "CalculatorHexCL.h"

#include "ObjLoader.h"
#include "RenderMeshComponent.h"
#include "RigidBodyComponent.h"
#include "TetMeshQualityComponent.h"
#include "AssemblyAttributeRendererComponent.h"

namespace bv
{
	namespace tled
	{
		struct FemMeshUtils
		{
			static Entity::Id setupCollidableMesh(const std::string& name, glm::vec3 pos = glm::vec3(), const std::string& shader = "diffuse")
			{
				auto entity = Entity::create(name);
				entity->pos(pos);

				// load the mesh representing the skull
				auto mesh = ObjLoader::load(name);
				// make sure that the posititon buffers are populated with vertex data
				mesh->updatePositions();
				mesh->updateTriNormals();

				// skull should also be collideable
				using namespace coldet;
				auto collider = entity->require<Collider>();
				collider->createShape<Octree>(mesh);

				// create renderable to draw the mesh of the skull with a diffuse shader
				entity->require<RenderMeshComponent>(mesh, rendering::Shader::require(shader));

				// don't start running immediately
				entity->require<physics::RigidBodyComponent>()->deactivate();

				return entity;
			}

			template <class TMesh, class TSolver = tled::SolverCPU>
			static void setupTled(Entity::Id entity, typename TMesh::Id mesh, float density = 1000)
			{
				using namespace bv::tled;
				using namespace bv::rendering;

				// generate the indices
				mesh->updateIndices();

				// create an assembly component that contains the assembly object
				auto assemblyComponent = entity->require<AssemblyComponent>(mesh);
				entity->require<AssemblyRendererComponent<TMesh>>(mesh);

				// get the created assembly from the component
				auto& assembly = assemblyComponent->assembly();

				// create a fea material with the material properties // (density, shear, bulk)
				Material::Id material = Material::create(density, 66000.0f, 100000.0f);

				// generate a finite element for each hexahedron of the mesh
				for (int i = 0; i < mesh->hedra.size(); ++i)
				{
					ElementPtr el = assembly.add(mesh->hedra[i]);
					el->material(material);
				}

				// specify the appropriate calculator type
				entity->require<CalculatorComponent>(mesh.ref(), TSolver());

				// create solver to perform timesteps
				entity->require<SolverComponent>();
			}

            template<class TSolver = tled::SolverCPU>
            static void setupTledTet(Entity::Id entity,
                                           const glm::vec3& pos,
                                           tethex::TetMesh::Id meshId,
                                           float density = 1000.0f)
            {
                auto box = Bounding::box(meshId);

                setupTled<tethex::TetMesh, TSolver>(entity,
                                                    meshId,
                                                    density);

                entity->require<tled::AssemblyAttributeRendererComponent<tethex::TetMesh>>(meshId);
	            entity->require<tethex::TetMeshQualityComponent>(meshId)->deactivate();
                entity->require<tled::ExporterInpComponent>();
            }

			template <class TSolver>
			static Entity::Id setupTledHex( const std::string& name,
			                                const glm::vec3& pos,
										    const glm::vec3& size,
											float density = 1000.0f)
			{
				using namespace bv::tethex;

				float elSize = 0.1f;

				glm::ivec3 dims = glm::ivec3(size / elSize);

				// generate a volumetric finite element mesh
				auto hexMeshId = HexGen::cube(size, dims);

				// get the mesh instance
				hexMeshId->updateIndices();
				hexMeshId->updatePositions();

				// create an entity to contain all the data
				auto entity = Entity::create(name);
				entity->pos(pos);

				using namespace coldet;
				auto collider = entity->require<Collider>();
				const Box& box = collider->shape(Bounding::box(hexMeshId));

				setupTled<HexMesh, TSolver>(entity, hexMeshId, density);

				return entity;
			}


			template<class TSolver = tled::SolverCPU>
			static Entity::Id setupTledTetSurf(const std::string& name,
			                        const glm::vec3& pos,
			                        Mesh::Id meshId,
			                        float a,
			                        float q,
			                        glm::vec3 displacement,
			                        bool nobisect = true,
									float density = 1000.0f)
			{
				using namespace bv::tethex;

				// create an entity to contain all the data
				auto entity = Entity::create(name);
				entity->pos(pos);

				// generate a tetraheral mesh from the shell mesh
				auto generatedId = TetraGen::generate(meshId, a, q, nobisect);

                // setup all TLED related objects
                setupTledTet<SolverCL>(entity, pos, generatedId, density );

                using namespace coldet;

                auto collider = entity->require<Collider>();
                auto octree = collider->createShape<Octree>(generatedId->surface);

                auto shader = rendering::Shader::require("diffuse_color");
                auto rendermesh = entity->require<RenderMeshComponent>(generatedId->surface, shader);
				rendermesh->culling(Culling::NO);
				rendermesh->deactivate();
				auto color = glm::vec3(84, 18, 18) / 255.0f;
				rendermesh->parameter("uColor", glm::vec4(color, 1.0f));

				return entity;
			}
		};
	}
}
