#pragma once

#include "Component.h"

namespace bv
{
    namespace tled
    {
        class ContactComponent: public Component<ContactComponent>
        {
        public:
            ContactComponent(Id id, Entity::Id entity, Entity::Id other): m_master(other), ComponentBase(id, entity) { }

            void initialize();
            void update();
            void resolve();
	        void forget();

            void attachGUI();

			Entity::Id master() const { return m_master; }

        private:
            Entity::Id m_master;

            float m_multiplier { 1.0f };

	        int m_method { 0 };
        };
    }
}