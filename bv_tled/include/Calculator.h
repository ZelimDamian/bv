#pragma once

#include "Common.h"

#include "Assembly.h"
#include "Element.h"
#include "ElementHex.h"
#include "ElementTet.h"
#include "TimeStepper.h"
#include "Timing.h"
#include "Solver.h"

namespace bv
{
	namespace tled
    {
        class CalculatorBase
        {
        public:

	        // stores solution state at a point in time
	        struct State
	        {
		        BufferVec4 U;
	        };

			//virtual ~CalculatorBase() = 0;

            void reset() { m_stepper->reset(); }

            void assembly(AssemblyPtr assembly) { m_assembly = std::move(assembly); }

	        virtual void init(float dt, float damping) = 0;
	        virtual void reinit(float dt, float damping) { m_stepper->recalculate(m_assembly->M(), dt, damping); }
	        virtual void step(float time, float totalTime) = 0;
            virtual void preStep(float fraction) {}
            virtual void subStep(float subFraction) {}
            virtual void postStep(float fraction) {}

//            BufferFloat& detJ() { return m_detJ; }
//            BufferFloat& DhDx() { return m_DhDx; }
//            BufferFloat& X() { return m_X; }

			bool isConverged() { return m_stepper->isConverged(); }
			const std::vector<int>&  convergenceMask() const { return m_stepper->convergenceMask(); }

			glm::vec3 deltaU(int idx) const { return m_stepper->deltaU(idx); }

			bool doForces() const { return m_doForces; }
			void doForces(bool doForces) { m_doForces = doForces; }

			virtual void apply(const State& state) { }

		protected:
            AssemblyPtr m_assembly;
            TimeStepperPtr m_stepper;
			bool m_doForces { true };

		    virtual void applyConstraints(float fraction);
        };

        // A calcualtor class that uses an assembly of TMeshType mesh type to perform calculations
        template <class TMeshType, class TSolver>
        class Calculator;
    }
}
