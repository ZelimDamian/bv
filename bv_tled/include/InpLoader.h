#pragma once

#include "Common.h"
#include "TetMesh.h"

namespace bv
{
    namespace tled
    {
        class InpLoader
        {
        public:
            static tethex::TetMesh::Id load(const std::string& name, const std::string ext = ".inp");
        };
    }
}
