#pragma once
#include "Element.h"
#include "Array.h"

namespace bv
{
	namespace tled
	{
        // Finite element hexahedral 8 Node element
		class ElementHex : public Element
		{
		public:
			ElementHex(size_t index);

			float mass() const override { return m_material->density() * volume(); }

			// Second Piola-Kirchoff Stress
			ArrayFloat<3, 3>& SPK() { return m_SPK; };

			// Jacobian determinant
			//float detJ() const { return m_detJ; }
			void detJ(float detJ) { m_detJ = detJ; }

			// Shape function global derivatives [8][3]
			ArrayFloat<3, 8>& DhDx() { return m_DhDx; };

			// Deformation gradient [3][3]
			ArrayFloat<3, 3>& X() { return m_X; };

			// Reference configuration intial node positions
			ArrayFloat<3, 8>& positions(const BufferVec4& X);

			// Node positions with deformations applied
			ArrayFloat<3, 8> posus();

		private:
			// Second Piola-Kirchoff Stress
			ArrayFloat<3, 3> m_SPK;
			// Jacobian determinant
			float m_detJ;
			// Shape function global derivatives [8][3]
			ArrayFloat<3, 8> m_DhDx;
			// Element nodal displacements [8][3]
			ArrayFloat<3, 8> m_u;
			// Deformation gradient [3][3]
			ArrayFloat<3, 3> m_X;

			// note positions
			ArrayFloat<3, 8> m_x;
		};

//		typedef PTR(ElementHex) ElementHexPtr;
		typedef std::shared_ptr<ElementHex> ElementHexPtr;
	}
}
