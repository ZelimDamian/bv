#pragma once

#include "Common.h"
#include "Node.h"
#include "Material.h"
#include "Buffer.h"

namespace bv
{
	namespace tled
	{
		class Element
		{
		public:

			Element(int index);

			void volume(float volume) { m_V = volume; }
			float volume() const { return m_V; }

			virtual float smallestExtent();

			//NodePtrCollection& nodes() { return m_nodes; }
			std::vector<int>& nodeIndices() { return m_nodeIndices; }
			int nodeIndex(int localIndex) const { return m_nodeIndices[localIndex]; }

			glm::vec3 nodePos(int index);
			glm::vec3 nodePosu(int index);

			Element *E(float E) { m_E = E; return this; }
			float E() { return m_E; }
			Element *A(float A) { m_A = A; return this; }
			virtual float A() { return m_A; }

			//Node *link(Node * node);

			//Node *node(size_t index) { return m_nodes[index]; }
			//const Node * node(int index) const { return m_nodes[index]; }

			bool has(Node * node);

			Material::Id material() { return m_material; }
			const Material::Id material() const { return m_material; }
			void material(Material::Id material) { m_material = material; }

			virtual float mass() const { return 0.0f; }

			int index() const { return m_index; }

		protected:
			NodePtrCollection m_nodes;
			std::vector<int> m_nodeIndices;

			float m_A;
			float m_E;

			int m_index;

			// element volume
			float m_V;

			bool m_updatePositions { true };

			Material::Id m_material;
		};

		//typedef enum ElementType ElementType;

		typedef std::shared_ptr<Element> ElementPtr;
		typedef std::vector<ElementPtr> ElementCollection;
		typedef std::vector<Element *> ElementPtrCollection;
	}
}
