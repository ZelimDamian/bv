#pragma once

#include "Node.h"
#include "Element.h"
#include "Constraint.h"
#include "Load.h"
#include "Buffer.h"
#include "Tet.h"
#include "Hex.h"

namespace bv
{
    namespace tled
    {
        class Assembly
        {
        public:

            Assembly() {}

            // create an assembly from the list of points
            Assembly(const VectorVec3&);

            // all nodes in the assembly
            std::vector<Node> &nodes() { return m_nodes; }
            const std::vector<Node> &nodes() const { return m_nodes; }

            const ElementCollection& elements() const { return m_elements; }

            // all elements in the aseembly
            ElementCollection& elements() { return m_elements; }

            // all loads in the assembly
            LoadCollection &loads() { return m_loads; }
	        const LoadCollection &loads() const { return m_loads; }

            // access to const node at index
            inline const Node& node(int index) const { return m_nodes[index]; }

            // access to a node at index
            inline Node& node(int index) { return m_nodes[index]; }

            inline const ElementPtr element(int index) const { return m_elements[index]; }
            // access to an element at index
            inline ElementPtr element(int index) { return m_elements[index]; }

            ElementPtr add(ElementPtr element);

	        ElementPtr addHex(const int ids[8]);
            ElementPtr add(int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8);
            ElementPtr add(const tethex::Hex& hex);

            ElementPtr add(int i1, int i2, int i3, int i4);
            ElementPtr addTet(const int ids[4]);
	        ElementPtr add(const tethex::Tet& tet);

            void load(int index, const glm::vec3 &force);
	        void spring(int index, const glm::vec3 &end, float k);
            void displace(int index, const glm::vec3 &displacement, LoadShape shape = LoadShape::INSTANT);
	        void project(int index, const glm::vec4 &plane);
            void fix(int nodeIndex);

            bool has(ElementPtr element) const;
            bool has(const Node& node) const;

            // deformed node position at index
            glm::vec3 pos(int index) const { return X(index) + glm::vec3(m_U[index]); }

            glm::vec3 X(int index) { return glm::vec3(m_X[index]); }
	        const glm::vec3 X(int index) const { return glm::vec3(m_X[index]); }
            BufferVec4& X() { return m_X; }
//			void X(BufferVec3 &x) { m_X = x; }

            BufferVec4 &U() { return m_U; }
            glm::vec3 U(int index) const { return glm::vec3(m_U[index]); }

            BufferVec4 &R() { return m_R; }
            glm::vec3 R(int index) const { return glm::vec3(m_R[index]); }
            void R(int index, const glm::vec3& force) { m_R[index] = glm::vec4(force, 0.0f); }

            BufferFloat &M() { return m_M; }
            float& M(int index) { return m_M[index]; }

            BufferFloat &VMS() { return m_VMS; }
            BufferFloat &VMSN() { return m_VMSN; }

	        BufferVec4 &F() { return m_F; }
            glm::vec3 F(int index) const { return glm::vec3(m_F[index]); }

            // Returns the maximal time step required for convergence
            float maxStep();// { return 0.001f; }
            float cachedDt() { return m_cachedStep; }

	        // list of constrainted nodes
	        std::vector<int> constNodeIndices() const;

            // calculate the shortest edge of all unconstrainted elements
	        float smallestExtent() const;

            // removes out instant eseoads
            void hitLoads();

	        // returns the number of active loads using std::count_if on load.active()
	        size_t numActiveLoads();

            // adds the deformations to the nodes positions buffer
            VectorVec3 posus();

            // allocate memory for all underlying structures
            void allocate();

		    void updateVMSN();

		    const VectorInt &forceIndices() const { return m_forceIndices; }
			void forceIndices(const VectorInt &forceIndices) { m_forceIndices = forceIndices; }

		    const VectorInt &numForces() const { return m_numForces; }
			void numForces(const VectorInt &numForces) { m_numForces = numForces; }

		    void reset();

			std::vector<int> materialInds();

		protected:
            NodeCollection m_nodes;
            ElementCollection m_elements;
            LoadCollection m_loads;

            // Node positions
            BufferVec4 m_X;
            // Masses
            BufferFloat m_M;
	        // Von-Mises stress per element
	        BufferFloat m_VMS;
            // Von-Mises stress per node
            BufferFloat m_VMSN;
            // Node displacements
            BufferVec4 m_U;
            // Internal forces
            BufferVec4 m_R;
            // External forces
            BufferVec4 m_F;

		    // Number of valent element force contributions
		    VectorInt m_numForces;
		    // Element indices into nodal force contributions
		    VectorInt m_forceIndices;

            float m_cachedStep{ 0.1f };
		};

        using AssemblyPtr = PTR(Assembly);
    }
}