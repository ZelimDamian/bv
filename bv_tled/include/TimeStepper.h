#pragma once

#include "Common.h"
#include "Buffer.h"

namespace bv
{
	namespace tled
	{
        // Simple central difference time stepper
        class TimeStepper
        {
        public:
            TimeStepper(BufferVec4& U, BufferVec4& F, const BufferFloat& M, float dt /* timestep */, float dmp /* damping coeff */);

            // perform deformations update
            virtual void update();

            // swap the buffers
            virtual void swap();

	        virtual void recalculate(const BufferFloat& M, float dt, float dmp);

            BufferVec4& Unew() { return m_U_new; }
	        glm::vec3 deltaU(int idx) { return glm::vec3(m_U[idx] - m_U_old[idx]); }

            virtual void reset();

	        bool isConverged() { return m_isConverged; }

			const std::vector<int>& convergenceMask() const;

		protected:
            BufferVec4& m_U;
	        BufferVec4& m_F;
            BufferVec4 m_U_old;
            BufferVec4 m_U_new;
			std::vector<int> m_convergenceMask;

            // Central difference pre-calculated coefficitents
            BufferVec4 m_ABC;

			// square of the timestep
	        float m_dt2;

	        bool m_isConverged = true;

            // test the deformation buffer for convergence
            bool checkConvergence(BufferVec4* u);
		};

        typedef PTR(TimeStepper) TimeStepperPtr;
	}
}
