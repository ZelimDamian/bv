#pragma once
#include "Common.h"
#include "ElementHex.h"
#include "Assembly.h"
#include "Calculator.h"
#include "HexMesh.h"

namespace bv
{
	namespace tled
	{
		template <>
		class Calculator<tethex::HexMesh, SolverCPU>: public CalculatorBase
		{
		public:
			Calculator();

			void init(float dt, float dc);
			void step(float time, float totalTime);
			void bake();

			void calculateMass(const ElementHex* elm, float* M) const;
			void calculateDhDx(ElementHex* elm);

			void calculateForces(const ElementHex* elm, const glm::vec4* U, float* F);
			void calculateStressNH(const glm::vec3* X, const std::vector<float>& materialParams, glm::vec3* SPK) const;

		protected:

			static float Cijk[8][8][8];

			float calculateVolume(const ArrayFloat<3, 8>& x) const;
			void calculateC();

			// Calculated values
			BufferFloat m_detJ;

			// Shape function global derivatives elnum * [8][3]
			BufferFloat m_DhDx;

			// Deformation gradient elnum * [3][3]
			BufferFloat m_X;

			// Element node indices [elnum * 8]
			BufferInt m_nodeInds;
		};

		// tled calculator for hexahedral finite element meshes
		using CalculatorHex = Calculator<tethex::HexMesh, SolverCPU>;

		// tled calculator for hexahedral finite element meshes
		template <class TMesh>
		using CalculatorHexPtr = PTR(CalculatorHex);
	};
}
