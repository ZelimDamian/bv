#pragma once
#include "Component.h"
#include "AssemblyComponent.h"

namespace bv
{
	namespace tled
	{
		class ContactWithSphereComponent : public Component<ContactWithSphereComponent>
		{
		public:
			ContactWithSphereComponent();
			~ContactWithSphereComponent();

			void initialize();
			//void collided(ColliderPtr other, const ContactCollection& contacts);
			void update();

		private:
			AssemblyPtr m_assembly;
			//ColliderSpherePtr m_sphereCollider;
		};
	}
}

