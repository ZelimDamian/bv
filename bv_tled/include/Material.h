#pragma once

#include <Store.h>
#include "Common.h"

namespace bv
{
	namespace tled
	{
		class Material: public Storable<Material>
		{
		public:
			template<typename ... Args>
			Material(StorableBase::Id id, Args&& ... params) : StorableBase(id)
			{
				int index = count();

				m_params.resize((index + 1) * MAX_PARAMS);

				assert(sizeof...(Args) < MAX_PARAMS);

				int i = 0;

				for (auto&& param : { params ...})
				{
					m_params[index * MAX_PARAMS + i++] = param;
				}
			}

			float density() const { return m_params[0]; }
			void density(float density) { m_params[0] = density; }

			float params(int index) const { return m_params[m_id.id * MAX_PARAMS + index]; }
			float& params(int index) { return m_params[m_id.id * MAX_PARAMS + index]; }

			static const int MAX_PARAMS = 16; // max number of paramters per material

			static std::vector<float>& params() { return m_params; }

		private:
			static std::vector<float> m_params;
		};

		typedef PTR(Material) MaterialPtr;
	}
}
