#pragma once

#include "ContactMethods.h"

namespace bv
{
	namespace tled
	{
		class DirectPDNContactMethod
		{
		public:
			static ContactMethod create();
		};
	}
}
