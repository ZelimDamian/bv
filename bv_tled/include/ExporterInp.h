#pragma once

#include <string>

namespace bv
{
    namespace tled
    {
        class Assembly;
    }

    namespace fem
    {
        class ExporterInp
        {
        public:
            static void write(const std::string& name, const tled::Assembly& assembly);
        };
    }
}