#pragma once
#include "Component.h"
#include "Calculator.h"
#include "CalculatorComponent.h"
#include "AssemblyComponent.h"

#include <RenderingSystem.h>

namespace bv
{
	namespace tled
	{

		class SolverComponent : public Component<SolverComponent>
		{
		public:

			using State = CalculatorBase::State;

			SolverComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
			{
//				m_time = TledSystem::instance()->time();
//				m_simStep = TledSystem::instance()->simStep();
				m_active = false;
			}

			void initialize()
			{
				// we should make sure that all calculator components are ready before using them
				{
					CalculatorComponent::load();
					auto id = CalculatorComponent::find(this->m_entity);
					if(!!id)
					{
						m_calculator = &id->calculator();
					}
					else
					{
						Log::error("Missing CalculatorComponent from " + title());
					}
				}

				// we should make sure that all assembly components are ready before using them
				{
					AssemblyComponent::load();
					auto id = AssemblyComponent::find(this->m_entity);
					if(!!id)
					{
						m_assembly = &id->assembly();
					}
					else
					{
						Log::error("Missing AssemblyComponent from " + title());
					}
				}

				m_maxStep = m_assembly->maxStep();// FeaManager::instance()->timestep();

				m_calculator->init(m_maxStep, m_damping);
			}

			float& totalTime() { return m_totalTime; }
			void totalTime(float totalTime) { m_totalTime = totalTime; }

			static std::string title() { return "One Time Solver"; }

			static void init()
			{
				UpdateSystem::instance()->onupdate([&]
                {
                    update();
				});

                UpdateSystem::instance()->onsync([&]
                {
                    sync();
                });
			}

			static void update()
			{
                auto tock = Profiler::instance()->tick("TLED Solver");

				auto& comps = components();

				for (int j = 0; j < comps.size(); ++j)
				{
                    auto& comp = comps[j];

					// this if statement defeats the purpose of DOD
					if (bool active = comp.m_active)
					{
						if(comp.m_isStepping)
						{
							comp.m_active = false;
						}

                        auto& calculator = comp.m_calculator;

						// approximate 60fps
						float frameTime = Timing::delta();
//						frameTime *= comp.m_speed;

						// simulation time is different
						comp.m_time += frameTime * comp.m_speed;
                        float time = std::min(comp.m_time, comp.m_totalTime);

                        // number of substeps required for stability
						int iterations = comp.numIterations(frameTime);

						const float fraction = time / comp.totalTime();

                        // prepare the calculator
                        calculator->preStep(fraction);

						comp.m_substepTime = 0.0f;
						Timer::tick();

                        // perform the sub iterations for stability
						for (int i = 0; i < iterations; ++i)
						{
							calculator->subStep( (float)(i + 1) / iterations);
						}

						comp.m_substepTime = (float)(Timer::tock() / iterations) * 1000.0f;

                        // perform the final step actions
                        calculator->postStep(fraction);

                        // check if the solution is converged or not
                        comp.m_isConverged = calculator->isConverged();

					}
				}

                tock();
			}

            static void sync()
            {
                for (int i = 0; i < count(); ++i)
                {
                    if(!components(i).m_isConverged)
                    {
                        if(components(i).m_resetOnDiverged)
                        {
                             components(i).m_calculator->reset();
                        }
                        else
                        {
                            components(i).deactivate();
                            components(i).onDiverged();
                        }
                    }
                    else
                    {
                        components(i).onMeshUpdate();

                    }
                }
            }

            int numIterations(float frameTime)
			{
                // make sure we make at least one iteration
				return (int)(std::max((frameTime / m_maxStep), 1.0f) * m_precision) + 1;
			}

			void apply(const State& state)
			{
				m_calculator->apply(state);
			}

			void attachGUI()
			{
				auto _panel = gui();

				_panel->button("Reset", [&]
				{
				    m_assembly->reset();
				    m_calculator->reset();
					// inform the world about the change in mesh
					onMeshUpdate();
					m_time = 0.0f;
					m_calculator->postStep(0.0f);
				});

				_panel->button("Reinit", [&]
				{
					m_maxStep = m_assembly->maxStep();
					m_calculator->reinit(m_maxStep * m_speed / m_precision, m_damping);
				});

				_panel->bind("Damping", m_damping, 0.01f, 10.0f, 0.1f);

				_panel->bind("Total time", m_totalTime, 0.1f, 10.0f, 0.1f);
				_panel->bind("Speed", m_speed, 0.01f, 1.0f, 0.01f);
				_panel->bind("Precision", m_precision, 0.1f, 10.0f, 0.1f);

				_panel->button("Stepmode",
				               [this] { m_isStepping = !m_isStepping; },
				               [this] { return m_isStepping; });

				_panel->button("Do forces",
				               [this] { m_calculator->doForces(!m_calculator->doForces()); },
				               [this] { return m_calculator->doForces(); });

				_panel->check("Div reset", [this]
				{ m_resetOnDiverged = !m_resetOnDiverged;
				}, [this]
				{
					return m_resetOnDiverged;
				});

				_panel->label("Num elements", [this]
				{
				    return std::to_string(m_assembly->elements().size());
				});

				_panel->label("Simulation time", [=]
				{
					return std::to_string(m_id->m_time);
				});

				_panel->label("State", [=]
				{
					return m_id->m_isConverged ? "Converged": "Diverged";
				});

				_panel->label("Time step", [=]
				{
					return std::to_string(m_id->m_maxStep);
				});

				_panel->label("Sim time per frame", [=]
				{
					return std::to_string(m_id->numIterations(Timing::delta()) * (m_id->m_maxStep * m_id->m_speed));
				});

				_panel->label("Num iterations", [=]
				{
				    return std::to_string(m_id->numIterations(Timing::delta()));
				});
			}

			Event onMeshUpdate;
			Event onDiverged;

			bool resetOnDiverged() const { return m_resetOnDiverged; }
			void resetOnDiverged(bool resetOnDiverged) { m_resetOnDiverged = resetOnDiverged; }


			float damping() const { return m_damping; }
			void damping(float damping) { m_damping = damping; }

		protected:
			CalculatorBase* m_calculator;
			Assembly* m_assembly;

			bool m_isConverged { true };
			bool m_isStepping { false };
			bool m_resetOnDiverged { false };

			// max stable timestep
			float m_maxStep{ 0.001f };

			// time taken by each substep
			float m_substepTime{ 0.016f };

			// bulk viscosity???
			float m_damping{ 35.0f };

			// simulation time
			float m_time { 0.0f };
			// simulation speed
			float m_speed { 1.0f };
			// increment multiplier
			float m_precision { 1.0f };
			// time at which loads are full
			float m_totalTime{ 5.0f };
		};
	}
}

