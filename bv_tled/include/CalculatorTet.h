#pragma once

#include "Calculator.h"
#include "TetMesh.h"

namespace bv
{
    namespace tled
    {
		template <>
        class Calculator<tethex::TetMesh, SolverCPU> : public CalculatorBase
        {
        public:

            virtual void init(float dt, float dc);
            virtual void step(float time, float totalTime);
            virtual void bake() {}

            void calculateDhDx(ElementTet* elm);

	        void calculateForces(const ElementTet* elm, const glm::vec4* U, float* F);
	        void calculateMass(const ElementTet* elm, float* M) const;
	        void calculateStressNH(const glm::vec3* X, const std::vector<float>& materialParams, glm::vec3* SPK) const;

        protected:

	        // Calculated values
	        BufferFloat m_detJ;

	        // Element volumes
	        BufferFloat m_V;

	        // Shape function global derivatives elnum * [8][3]
	        BufferFloat m_DhDx;

	        // Deformation gradient elnum * [3][3]
	        BufferFloat m_X;

	        // Element node indices [elnum * 8]
	        BufferInt m_nodeInds;
        };

        using CalculatorTet = Calculator<tethex::TetMesh, SolverCPU>;
        using CalculatorTetPtr = PTR(CalculatorTet);
    };

}
