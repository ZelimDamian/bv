#pragma once

#include "Common.h"
#include "Node.h"

namespace bv
{
	namespace tled
	{
		enum class LoadShape
		{
			NONE = 0,
			FIX,
			INSTANT,
			LINEAR,
			LINEAR_INSTANT,
			FORCE,
			PLANE,
			SPRING
		};

		class Load
		{
		public:
			Load() { }
			Load(glm::vec3 value, LoadShape shape): Load(glm::vec4(value, 0.0f), shape) {}
			Load(glm::vec4 value, LoadShape shape);

			const glm::vec4& value() const { return m_value; }
			void value(const glm::vec4& value) { m_value = value; }

			virtual glm::vec4 evaluate(float time) const;

			LoadShape shape() const { return m_shape; }
			void shape(LoadShape shape) { m_shape = shape; }

			int hits() const { return m_hits; }
			void hits(int hits) { m_hits = hits; }
			void hit();
			void renew() { m_hits = 0; }

			bool active() const { return m_active; }
			void active(bool active) { m_active = active; }
			void activate() { active(true); }
			void deactivate() { active(false);}

			bool isPermanent() { return m_maxHits == -1; }

			static uint16_t MAX_LOAD_HITS;

			void kill();

		protected:
			glm::vec4 m_value { 0.0f };
			bool m_active { false };
			int m_hits { 0 };
			int m_maxHits { -1 };

			LoadShape m_shape { LoadShape::NONE };
		};

		typedef PTR(Load) LoadPtr;
		typedef std::vector<Load> LoadCollection;
	}
}
