#pragma once

#include "Solver.h"

namespace bv
{
	namespace tled
	{
		// just a tag to mark the OpenCL based TLED solver
		class SolverCL { };
	}
}