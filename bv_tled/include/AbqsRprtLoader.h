#pragma once

#include "Common.h"

namespace bv
{
	namespace tled
	{
		class AbqsRprtLoader
		{
		public:
			static VectorVec3 load(const std::string& name, const std::string ext = ".rpt");
		};
	}
}