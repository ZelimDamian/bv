#pragma once

#include "ContactMethods.h"

namespace bv
{
	namespace tled
	{
		class ProjectionContactMethod
		{
		public:
			static ContactMethod create();
		};
	}
}