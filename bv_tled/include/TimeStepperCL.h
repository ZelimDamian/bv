#pragma once

#include <KernelCL.h>
#include "TimeStepper.h"
#include "BufferCL.h"

namespace bv
{
	namespace tled
	{
		class TimeStepperCL: public TimeStepper
		{
		public:
			TimeStepperCL(BufferVec4& U, BufferVec4& F, const BufferFloat& M,
			              float dt /* timestep */, float dmp /* damping coeff */,
						  gpu::cl::BufferCL<glm::vec4>::Id F_cl,
						  gpu::cl::BufferCL<glm::vec4>::Id Fx_CL,
						  gpu::cl::BufferCL<glm::vec4>::Id U_CL,
                          gpu::cl::BufferCL<glm::vec4>::Id R_CL,
                          gpu::cl::BufferCL<int>::Id numForces_CL,
                          gpu::cl::BufferCL<int>::Id aggregatedForces,
                          int maxNumForces);

			// perform deformations update
			void update() override;

            void downloadU();
            void uploadU();

			void setU(const BufferVec4& U);

			// swap the buffers
			virtual void swap() override;

			virtual void reset() override;

			virtual void recalculate(const BufferFloat& M, float dt, float dmp) override;

//            gpu::cl::BufferCL<glm::vec4>::Id U_new_CL() { return m_U_old_CL; }
			gpu::cl::BufferCL<glm::vec4>::Id U_new_CL() { return m_U_new_CL; }
			gpu::cl::BufferCL<glm::vec4>::Id U_old_CL() { return m_U_old_CL; }


		protected:
			gpu::cl::BufferCL<glm::vec4>::Id m_ABC_CL;

			gpu::cl::BufferCL<glm::vec4>::Id m_U_CL;
			gpu::cl::BufferCL<glm::vec4>::Id m_U_new_CL;
			gpu::cl::BufferCL<glm::vec4>::Id m_U_old_CL;
			gpu::cl::BufferCL<glm::vec4>::Id m_F_CL;
			gpu::cl::BufferCL<glm::vec4>::Id m_Fx_CL;
            gpu::cl::BufferCL<glm::vec4>::Id m_R_CL;

			gpu::cl::Kernel::Id m_updateUKernel;
		};
	}
}