#pragma once

#include <Component.h>
#include <AssemblyComponent.h>
#include <Assembly.h>

#include <ColorLerper.h>
#include <Shader.h>
#include <RenderMesh.h>

namespace bv
{
	namespace tled
	{
		template <class TMesh>
		class AssemblyAttributeRendererComponent: public Component<AssemblyAttributeRendererComponent<TMesh>>
		{
		public:

			using ComponentBase = Component<AssemblyAttributeRendererComponent<TMesh>>;
			using Id = Handle<AssemblyAttributeRendererComponent<TMesh>>;

			AssemblyAttributeRendererComponent(Id id, Entity::Id entity, Handle<TMesh> mesh): ComponentBase(id, entity),
			                                                                              m_mesh(mesh) { }

			void initialize()
			{
				m_assembly = &this->template require<AssemblyComponent>()->assembly();

				auto renderer = RenderingSystem::instance();

				renderer->onrender([=] () mutable
			    {
				    if(this->isActive())
				    {
					    this->render(renderer);
				    }
			    });
			}

			void render(RenderingSystem* renderer)
			{
				using namespace rendering;

				Shader::Id diffuseShader = Shader::require("diffuse_colors");
				diffuseShader->uniform("lightPos", UniformType::Vec3)->set(glm::vec3(1.0f, 1.0f, 1.0f));

				// draw using triangles
				renderer->reset(Culling::CCW);
				Shader::use(diffuseShader);

				// vertex attribute values
				std::vector<float> attributes(m_mesh->vertices.size());

				if(m_mode == Mode::VMS)
				{
					m_assembly->updateVMSN();

					for (int i = 0; i < attributes.size(); ++i)
					{
						attributes[i] = m_assembly->VMSN()[i];
					}

					// min and max values for von-Mises stress
					auto& VMS = m_assembly->VMS();
					m_min = glm::length(*std::min_element(VMS.begin(), VMS.end()));
					m_max = glm::length(*std::max_element(VMS.begin(), VMS.end()));
				}
				else if(m_mode == Mode::U)
				{
					for (int i = 0; i < attributes.size(); ++i)
					{
						attributes[i] = glm::length(m_assembly->U()[i]);
					}
				}
				else if(m_mode == Mode::U1)
				{
					for (int i = 0; i < attributes.size(); ++i)
					{
						attributes[i] = m_assembly->U()[i].x;
					}

				}
				else if(m_mode == Mode::U2)
				{
					for (int i = 0; i < attributes.size(); ++i)
					{
						attributes[i] = m_assembly->U()[i].y;
					}
				}
				else if(m_mode == Mode::U3)
				{
					for (int i = 0; i < attributes.size(); ++i)
					{
						attributes[i] = m_assembly->U()[i].z;
					}
				}
				else if(m_mode == Mode::CONV)
				{
					auto& calculator = CalculatorComponent::find(this->m_entity)->calculator();
					auto& mask = calculator.convergenceMask();

					for (int i = 0; i < attributes.size(); ++i)
					{
						attributes[i] = mask[i];
					}
				}

				// min and max values for attributes
				if(m_extremate)
				{
					m_min = *std::min_element(attributes.begin(), attributes.end());
					m_max = *std::max_element(attributes.begin(), attributes.end());
				}

				// only do this if there is variation in stresses
				if (m_min < m_max)
				{
					// and use force magnitude as color
					for (int i = 0; i < m_mesh->vertices.size(); ++i)
					{
						const auto magnitude = attributes[i];
//						const auto absMagnitude = glm::length(magnitude);
//						const auto clampedValue = std::min(m_max, glm::max(m_min, absMagnitude));
						const auto maxMagnitude = m_max - m_min;
						const auto factor = (magnitude - m_min) / maxMagnitude;

						const glm::vec4 forceColor = m_colorLerper.sample(factor);

						m_mesh->surface->vertices[i].template set<TMesh::TVertex::Col>(forceColor);
					}
				}

				// render the mesh
				renderer->transform(this->m_entity->transform());
                auto renderMesh = RenderMesh::create(m_mesh->surface);
				renderer->submit(renderMesh);
			}

			void attachGUI()
			{
				gui::PanelElementPtr panel = this->gui();// needs to be explicitly typed in GCC

				panel->button("Update extrema", [&] { m_extremate = !m_extremate; }, [&] { return m_extremate; });

				panel->bind<float>("Min", [this]{ return m_min; }, [this] (const float& value) { m_min = value; }, 0.0f);
				panel->bind<float>("Max", [this]{ return m_max; }, [this] (const float& value) { m_max = value; }, 0.0f);

				auto list = { "VMS", "U", "U1", "U2", "U3", "CONV" };

				panel->combo("Mode", { std::begin(list), std::end(list) }, [this](int mode) { m_mode = Mode(mode); m_extremate = true; });
			}

			enum class Mode
			{
				VMS,    // von-Mises stress
				U,      // deformation magnitude
				U1,     // deformation in x
				U2,     // deformation in y
				U3,     // deformation in z
				CONV 	// convergence mask
			};

		private:

			// the geometry to render attributes on
			Handle<TMesh> m_mesh;

			// the assembly to use for rendering
			Assembly* m_assembly;

			// wether or not to update max/min values of the observed attribute
			bool m_extremate { true };

			// colors for interpolation
			ColorLerper m_colorLerper { glm::vec4 { 0.0f, 0.0f, 1.0f, 1.0f },
			                            glm::vec4 { 0.0f, 1.0f, 1.0f, 1.0f },
			                            glm::vec4 { 0.0f, 1.0f, 0.0f, 1.0f },
			                            glm::vec4 { 1.0f, 1.0f, 0.0f, 1.0f },
			                            glm::vec4 { 1.0f, 0.0f, 0.0f, 1.0f } };

			float m_min{ 0.0f };
			float m_max{ 0.0f };

			Mode m_mode { Mode::VMS };
		};
	}
}