#pragma once
#include "Component.h"

namespace bv
{
    namespace tled
    {
        class ExporterInpComponent: public Component<ExporterInpComponent>
        {
        public:
            ExporterInpComponent(Id id, Entity::Id entity): ComponentBase(id, entity) {}

            void run();
            void attachGUI();
        };
    }
}