#pragma once

#include "Component.h"
#include "Calculator.h"
#include "AssemblyComponent.h"
#include "Solver.h"

namespace bv
{
	namespace tled
	{
		class CalculatorComponent : public Component<CalculatorComponent>
		{
		public:
			template <class TMesh, class TSolver = SolverCPU>
			CalculatorComponent(Id id, Entity::Id entity, const TMesh& tag,
			                    TSolver solver = SolverCPU()): ComponentBase(id, entity)
			{
				m_calculator = std::shared_ptr<CalculatorBase>(new Calculator<TMesh, TSolver>());
			}

			static void init() {}

			void initialize()
			{
				// we should make sure that all assembly components are loaded before usage
				AssemblyComponent::load();
				auto assemblyComponent = require<AssemblyComponent>();

				m_calculator->assembly(&assemblyComponent->assembly());
			}

			CalculatorBase& calculator() { return *m_calculator.get(); }

		private:
			std::shared_ptr<CalculatorBase> m_calculator;
		};
	}
}
