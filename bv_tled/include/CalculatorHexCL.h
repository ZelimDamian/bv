#pragma once

#include "Calculator.h"
#include "CalculatorHex.h"
#include "SolverCL.h"
#include "HexMesh.h"
#include "BufferCL.h"
#include "KernelCL.h"

namespace bv
{
	namespace tled
	{

		template<>
		class Calculator<tethex::HexMesh, SolverCL>: public CalculatorHex
		{
		public:
			void init(float dt, float dc) override;
			void preStep(float fraction) override;
            void subStep(float subFraction) override;
            void postStep(float fraction) override;

		private:

            // Kernel to calculate the nodal forces for each element
            gpu::cl::Kernel::Id m_tled_kernel;

			// Kernel to apply constraints
			gpu::cl::Kernel::Id m_constraints_kernel;

			// Kernel to collect the per element forces into per node forces
			gpu::cl::Kernel::Id m_collect_kernel;

			// Node deformations
			gpu::cl::BufferCL<glm::vec4>::Id m_U_CL;

            // Extral forces
            gpu::cl::BufferCL<glm::vec4>::Id m_R_CL;

			// Nodal Forces
			gpu::cl::BufferCL<glm::vec4>::Id m_F_CL;

			// Nodal Forces per element
			gpu::cl::BufferCL<glm::vec4>::Id m_Fx_CL;

			// Calculated values
			gpu::cl::BufferCL<float>::Id m_V_CL;

			// Material properties
			gpu::cl::BufferCL<float>::Id m_mat_CL;

			// Shape function global derivatives elnum * [8][3]
			gpu::cl::BufferCL<float>::Id m_DhDx_CL;

			// Deformation gradient elnum * [3][3]
			//BufferCLPtr<float> m_X_CL;

			// Element node indices [elnum * 8]
			gpu::cl::BufferCL<int>::Id m_nodeInds_CL;

            // Constraint node indices [nodeNum]
            gpu::cl::BufferCL<int>::Id m_nodeConstraintFlags_CL;

            gpu::cl::BufferCL<glm::vec4>::Id m_constraintMagnitudes_CL;

            void applyConstraints(float fraction) override;
		};

		using CalculatorHexCL = Calculator<tethex::HexMesh, SolverCL>;
	}
}