#pragma once

#include "Common.h"
#include "Buffer.h"
#include <cstring>

namespace bv
{
	class MatrixRoutines
	{
	public:
		MatrixRoutines() {}
		~MatrixRoutines() {}

		inline static float det2x2(const float *A) {
			return A[0 * 2 + 0] * A[1 * 2 + 1] - A[0 * 2 + 1] * A[1 * 2 + 0];
		}

		inline static float det3(const float* A)
		{
			return A[0] * A[4] * A[8] - A[0] * A[5] * A[7] - A[3] * A[1] * A[8]
				   + A[3] * A[2] * A[7] + A[6] * A[1] * A[5] - A[6] * A[2] * A[4];
		}

		template <class T, typename Dim>
		static T norm(const Array<T, Dim>& A)
		{
			static_assert(Dim::C == Dim::R, "Matrix should be square");

			float norm = 0;

			for (int i = 0; i < Dim::R; i++)
			{
				norm += A(i) * A(i);
			}
			return std::sqrt(norm);
		}

		// Matrix operations
		template <class T, typename Dim>
		static void inv3x3(Array<T, Dim>& a, Array<T, Dim>& r)
		{
			float detAInv = 1 / det3(a.data());
			r(0, 0) = (a(1, 1) * a(2, 2) - a(1, 2) * a(2, 1)) * detAInv;
			r(0, 1) = (a(0, 2) * a(2, 1) - a(0, 1) * a(2, 2)) * detAInv;
			r(0, 2) = (a(0, 1) * a(1, 2) - a(0, 2) * a(1, 1)) * detAInv;
			r(1, 0) = (a(1, 2) * a(2, 0) - a(1, 0) * a(2, 2)) * detAInv;
			r(1, 1) = (a(0, 0) * a(2, 2) - a(0, 2) * a(2, 0)) * detAInv;
			r(1, 2) = (a(0, 2) * a(1, 0) - a(0, 0) * a(1, 2)) * detAInv;
			r(2, 0) = (a(1, 0) * a(2, 1) - a(1, 1) * a(2, 0)) * detAInv;
			r(2, 1) = (a(0, 1) * a(2, 0) - a(0, 0) * a(2, 1)) * detAInv;
			r(2, 2) = (a(0, 0) * a(1, 1) - a(0, 1) * a(1, 0)) * detAInv;
		}

		template<class T, template<int, int> class ADim, int AC, int AR, template <int, int> class BDim, int BC, int BR>
		static void multAB(const Array<T, ADim<AC, AR>>& A, const Array<T, BDim<BC, BR>>& B, Array<T, Dims<BC, AR>>& C)
		{
			for (int i = 0; i < AR; i++) {
				for (int j = 0; j < BC; j++) {
					float value = 0.0f;
					for (int k = 0; k < BR; k++) {
						const float& a = A(i, k);
						const float& b = B(k, j);
						value += a * b;
					}
					C(i, j) = value;
				}
			}
		}

		template<class T, template<int, int> class ADims, int AR, int AC, template<int, int> class BDims, int BR, int BC>
		static void multATB(const Array<T, ADims<AC, AR>>& A, const Array<T, BDims<BC, BR>>& B, Array<T, Dims<BC, AC>>& C)
		{
			for (int i = 0; i < AC; i++) {
				for (int j = 0; j < BC; j++) {
					float value = 0.0f;
					for (int k = 0; k < BR; k++) {
						const float& a = A(k, i);
						const float& b = B(k, j);
						value += a * b;
					}
					C(i, j) = value;
				}
			}
		}

		template<class T, template<int, int> class ADims, int AR, int AC, template <int, int> class BDims, int BR, int BC>
		static void multABT(const Array<T, ADims<AC, AR>>& A, const Array<T, BDims<BC, BR>>& B, Array<T, Dims<BR, AR>>& C)
		{
			for (int i = 0; i < AR; i++) {
				for (int j = 0; j < BR; j++) {
					float value = 0.0f;
					for (int k = 0; k < AC; k++) {
						const float& a = A(i, k);
						const float& b = B(j, k);
						value += a * b;
					}
					C(i, j) = value;
				}
			}
		}

		template<class T, template<int, int> class TDims, int AR, int AC>
		static void mult(const Array<T, TDims<AC, AR>>& src, const T value, Array<T, TDims<AC, AR>>& dst)
		{
			const size_t size = (size_t)(AR * AC);

			const T* s_ptr = src.data();
			T* d_ptr = dst.data();

			for (size_t i = 0; i < size; i++)
			{
				d_ptr[i] = s_ptr[i] * value;
			}
		}

		template<class T>
		static void p_multAB(const T* A, size_t rowsA, size_t colsA, const T* B, size_t rowsB, size_t colsB, T* C)
		{
			std::memset(C, 0, sizeof(T) * rowsA * colsB);
			for (int i = 0; i < rowsA; i++)
			{
				for (int j = 0; j < colsB; j++)
				{
					for (int k = 0; k < rowsB; k++)
					{
						C[i * colsB + j] += A[i * colsA + k] * B[k * colsB + j];
					}
				}
			}
		}

		template<class T>
		static void p_multATB(const T* A, size_t rowsA, size_t colsA, const T* B, size_t rowsB, size_t colsB, T* C)
		{
			std::memset(C, 0, sizeof(T) * colsA * colsB);
			for (int i = 0; i < colsA; i++)
			{
				for (int j = 0; j < colsB; j++)
				{
					for (int k = 0; k < rowsB; k++)
					{
						C[i * colsB + j] += A[k * colsA + i] * B[k * colsB + j];
					}
				}
			}
		}

		template<class T>
		static void p_multABT(const T* A, size_t rowsA, size_t colsA, const T* B, size_t rowsB, size_t colsB, T* C)
		{
			std::memset(C, 0, sizeof(T) * rowsA * rowsB);
			for (int i = 0; i < rowsA; i++)
			{
				for (int j = 0; j < rowsB; j++)
				{
					for (int k = 0; k < colsA; k++)
					{
						C[i * rowsB + j] += A[i * colsA + k] * B[j * colsB + k];
					}
				}
			}
		}

		template<class T>
		static void mult_x(T* A, size_t rA, T* B, size_t cB, T* C)
		{
			T* pA = NULL; T* pB = NULL; T* pR = NULL;

			pA = A;
			pB = B;
			// rows of R
			for (int i = 0; i < rA; i++)
			{
				for (int j = 0; j < cB; j++) // Columns of R
				{
					//pR = R + i*cB + j;
					pR[i*cB + j] = *pA * (*pB);
					A++;
				}
				pA++;
			}
		}
	};
}
