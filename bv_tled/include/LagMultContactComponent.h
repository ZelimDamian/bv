#pragma once

#include "Component.h"
#include "Mesh.h"

namespace bv
{
    namespace tled
    {
        class LagMultContactComponent: public Component<LagMultContactComponent>
        {
        public:
            LagMultContactComponent(Id id, Entity::Id entity, Entity::Id other): m_master(other), ComponentBase(id, entity) { }

            void initialize();
            void update();

            void attachGUI();

        private:
            Entity::Id m_master;
            bool m_applyLagMult { true };
            float m_penaltyParam { 1.0f };

			Mesh::Id _rayMeshId;

			bool surfaceNeedsUpdate { false };
        };
    }
}