#pragma once

#include "Common.h"
#include "Node.h"

namespace bv
{
    namespace tled
    {
        // currently fixed constraints only
        class Constraint
        {
        public:
            Constraint(int node, int localIndex);

            int index() const { return m_index; }

			int nodeIndex() const { return m_index / 3; }

			NodePtr node() { return m_node; }
            void node(NodePtr node) { m_node = node; }

        private:
			int m_index;

            NodePtr m_node;
        };

        typedef PTR(Constraint) ConstraintPtr;
        typedef std::vector<ConstraintPtr> ConstraintCollection;
    }
}