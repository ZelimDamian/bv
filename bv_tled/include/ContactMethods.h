#pragma once

#include "Common.h"
#include "Store.h"

namespace bv
{
	class Entity;

	namespace tled
	{
		class Assembly;
	}

	namespace coldet
	{
		class MeshContact;
	}

	namespace physics
	{
		class RigidBodyComponent;
	}

	namespace tled
	{
		class ContactMethod
		{
		public:

			struct ContactActionData
			{
				Handle<Entity> slave;
				Handle<Entity> master;
				tled::Assembly* assembly;
				const coldet::MeshContact* contact;
				physics::RigidBodyComponent* body;
				float magic;
			};

			struct ContactResolveData
			{
				std::vector<float> penetrations;
				std::vector<glm::vec4> planes;
				std::vector<glm::vec3> slavePositions;
				std::vector<int> slaveIndices;

                void clear()
                {
                    penetrations.clear();
                    planes.clear();
                    slavePositions.clear();
                    slaveIndices.clear();
                }
			};

			using ResolveAction = std::function<void(const ContactResolveData*)>;
			using ContactAction = std::function<ResolveAction(ContactActionData, ContactResolveData*)>;

			ContactMethod(const std::string &name, const ContactAction &action):
					m_name(name), m_action(action) {}

			ResolveAction execute(const ContactActionData& data, ContactResolveData* resolveData)
			{
				return m_action(data, resolveData);
			}

			const std::string &name() const { return m_name; }

		private:
			ContactAction m_action;
			std::string m_name;
		};

		class ContactMethods
		{
		public:
			static std::vector<ContactMethod> methods;
			static std::vector<std::string> names();
		};
	}
}