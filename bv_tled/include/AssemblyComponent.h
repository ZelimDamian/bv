#pragma once
#include "Component.h"
#include "Assembly.h"
#include "Calculator.h"
#include "RenderingSystem.h"
#include "Shader.h"

#include "Entity.h"
#include "Mesh.h"
#include "TetMesh.h"

namespace bv
{
	namespace tled
	{
		class AssemblyComponent : public Component<AssemblyComponent>
		{
		public:
			AssemblyComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
			{ 
			
			}

			template <class TMesh>
			AssemblyComponent(Id id, Entity::Id entity, Handle<TMesh> meshId) :
				ComponentBase(id, entity), m_assembly(TMesh::get(meshId).positions())
			{

			}

			AssemblyComponent(Id id, Entity::Id entity, tethex::TetMesh::Id meshId) :
					ComponentBase(id, entity), m_assembly(meshId->positions)
			{
				m_assembly.numForces(meshId->valences);
				m_assembly.forceIndices(meshId->valenceIndices);
			}

			/*AssemblyComponent(AssemblyComponent&& comp) :
				ComponentBase(comp.m_id, comp.m_entity)
			{
				m_assembly = std::move(comp.m_assembly);
			}*/

			static void init() {}

			void initialize()
			{
				//m_assembly.allocate();

//				auto& renderer = RenderingSystem::instance();
//
//				auto onrender = [this, &renderer]
//				{
//
//				};
//
//				renderer.onrender(onrender);
			}

			Assembly& assembly() { return m_assembly; }
			const Assembly& assembly() const { return m_assembly; }

			void attachGUI()
			{
				{
					using namespace bv::gui;

					auto _panel = ComponentBase::gui();

					auto subpanel = _panel->panel("Material");
					subpanel->bind("Shear modulus", m_assembly.elements()[0]->material()->params(1), 0, 10000000, 1000);
					subpanel->bind("Bulk modulus", m_assembly.elements()[0]->material()->params(2), 0, 10000000, 1000);
				}
			}

			static std::string title() { return "Assembly component"; }

		private:
			Assembly m_assembly;
		};
	}
}
