#pragma once
#include "Element.h"
#include "Array.h"

namespace bv
{
	namespace tled
	{
		// Tetrahedral 4 node finite element
		class ElementTet : public Element
		{
		public:
			ElementTet(int index);
			~ElementTet();

			float mass() const override;

			ArrayFloat<3, 4>& positions(const BufferVec4& X);
			ArrayFloat<3, 4> posus();

		private:
			// node positions
			ArrayFloat<3, 4> m_x;
		};

//		typedef PTR(ElementTet) ElementTetPtr;
		typedef std::shared_ptr<ElementTet> ElementTetPtr;
	};
}
