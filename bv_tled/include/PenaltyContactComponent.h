#pragma once

#include "Component.h"
#include "Mesh.h"

namespace bv
{
    namespace tled
    {
        class PenaltyContactComponent: public Component<PenaltyContactComponent>
        {
        public:
            PenaltyContactComponent(Id id, Entity::Id entity, Entity::Id other): m_master(other), ComponentBase(id, entity) { }

            void initialize();
            void update();

            void attachGUI();

        private:
            Entity::Id m_master;
            bool m_applyPenalty { true };

			Mesh::Id _rayMeshId;
	        float m_penaltyParam { 1.0f };
            float m_magic { 1000.0f };
        };
    }
}