#pragma once

namespace bv
{
	namespace tled
	{
		class Solver { };

		// tag to mark CPU based TLED solver
		class SolverCPU: public Solver { };
	}
}