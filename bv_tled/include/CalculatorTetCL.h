#pragma once

#include "SolverCL.h"
#include "TetMesh.h"
#include "BufferCL.h"
#include "KernelCL.h"
#include "CalculatorTet.h"

namespace bv
{
    namespace tled
    {
        template<>
        class Calculator<tethex::TetMesh, SolverCL>: public CalculatorTet
        {
        public:

	        void init(float dt, float dc) override;
            void preStep(float fraction) override;
            void subStep(float subFraction) override;
            void postStep(float fraction) override;

	        void apply(const State& state) override;

        private:

	        // Kernel to calculate the nodal forces for each element
	        gpu::cl::Kernel::Id m_tled_kernel;

	        // Node deformations
	        gpu::cl::BufferCL<glm::vec4>::Id m_U_CL;

	        // Nodal Forces
	        gpu::cl::BufferCL<glm::vec4>::Id m_F_CL;

            // External forces
            gpu::cl::BufferCL<glm::vec4>::Id m_R_CL;

	        // Nodal Forces
	        gpu::cl::BufferCL<glm::vec4>::Id m_Fx_CL;

	        // Calculated values
	        gpu::cl::BufferCL<float>::Id m_V_CL;

            // Nodal masses
            gpu::cl::BufferCL<float>::Id m_M_CL;

	        // per element von Mises stress
	        gpu::cl::BufferCL<float>::Id m_VMS_CL;

	        // Material properties
	        gpu::cl::BufferCL<float>::Id m_mat_CL;

	        // Shape function global derivatives elnum * [8][3]
	        gpu::cl::BufferCL<float>::Id m_DhDx_CL;

	        // Element node indices [elnum * 8]
	        gpu::cl::BufferCL<int>::Id m_nodeInds_CL;

			// Element material indices
			gpu::cl::BufferCL<int>::Id m_materialInds_CL;

	        // Number of elements attached to each node
	        gpu::cl::BufferCL<int>::Id m_numForces_CL;

            BufferInt m_forceIndices;

            gpu::cl::BufferCL<int>::Id m_forceIndices_CL;

            // Kernel to collect the per element forces into per node forces
            gpu::cl::Kernel::Id m_collect_kernel;

            // Kernel to collect the per element forces into per node forces
            gpu::cl::Kernel::Id m_gravity_kernel;

            // Kernel to apply constraints
            gpu::cl::Kernel::Id m_constraints_kernel;

            // Constraint node indices [nodeNum * 3]
            gpu::cl::BufferCL<int>::Id m_nodeConstraintFlags_CL;

	        // Nodal force index aggratation
	        gpu::cl::BufferCL<int>::Id m_aggregatedForces_CL;

            // The magnitudes of deforamation for the constrained nodes [nodeNum]
            gpu::cl::BufferCL<glm::vec4>::Id m_constraintMagnitudes_CL;

            void applyConstraints(float fraction) override;
        };

        using CalculatorTetCL = Calculator<tethex::TetMesh, SolverCL>;
    }
}

