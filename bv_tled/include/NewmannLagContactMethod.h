//
// Created by Zelim on 19/01/2016.
//

#pragma once
#include "ContactMethods.h"

namespace bv
{
	namespace tled
	{
		class NewmannLagContactMethod
		{
		public:
			static ContactMethod create();
		};
	}
}

