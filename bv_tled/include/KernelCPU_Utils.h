#pragma once

namespace bv
{
	namespace tled
	{
	    struct Utils
	    {

		    static void memset(float *pDst, float value, size_t size)
		    {
			    for (int i = 0; i < size; i++)
			    {
				    pDst[i] = value;
			    }
		    }

		    static void memcpy(float *pDst/*private*/, size_t size, float *pSrc)
		    {
			    for (int i = 0; i < size; i++)
			    {
				    pDst[i] = pSrc[i];
			    }
		    }

		    // Multiply by a scalar
		    static void p_multAs(float *A, size_t size, float s, float *C)
		    {
			    //const float * end = A + size;

			    for (int i = 0; i < size; i++)
			    {
				    C[i] = A[i] * s;
			    }
		    }

		    // Multiply two matrices
		    static void p_multAB(const float *A, size_t colsA, size_t rowsA, const float *B, size_t colsB, size_t rowsB,
		                         float *C)
		    {
			    memset(C, 0.0f, rowsA * colsB);
			    for (int i = 0; i < rowsA; i++)
			    {
				    for (int j = 0; j < colsB; j++)
				    {
					    for (int k = 0; k < rowsB; k++)
					    {
						    C[i * colsB + j] += A[i * colsA + k] * B[k * colsB + j];
					    }
				    }
			    }
		    }

		    static void p_multATB(const float *A, size_t colsA, size_t rowsA, const float *B, size_t colsB,
		                          size_t rowsB, float *C)
		    {
			    memset(C, 0.0f, colsA * colsB);
			    for (int i = 0; i < colsA; i++)
			    {
				    for (int j = 0; j < colsB; j++)
				    {
					    for (int k = 0; k < rowsB; k++)
					    {
						    C[i * colsB + j] += A[k * colsA + i] * B[k * colsB + j];
					    }
				    }
			    }
		    }

		    static void p_multABT(const float *A, size_t colsA, size_t rowsA, const float *B, size_t colsB,
		                          size_t rowsB, float *C)
		    {
			    memset(C, 0.0f, rowsA * rowsB);
			    for (int i = 0; i < rowsA; i++)
			    {
				    for (int j = 0; j < rowsB; j++)
				    {
					    for (int k = 0; k < colsA; k++)
					    {
						    C[i * rowsB + j] += A[i * colsA + k] * B[j * colsB + k];
					    }
				    }
			    }
		    }

		    static void calculateStressNH(const float *X,
		                                  const float *materialParams, float *SPK)
		    {
			    float XT11, XT12, XT13, XT21, XT22, XT23, XT31, XT32, XT33;
			    float C11, C12, C13, C22, C23, C33;

			    // Transpose of deformation gradient
			    //XT11 = X[0][0]; XT12 = X[1][0]; XT13 = X[2][0];
			    XT11 = X[0];
			    XT12 = X[3];
			    XT13 = X[6];
			    //XT21 = X[0][1]; XT22 = X[1][1]; XT23 = X[2][1];
			    XT21 = X[1];
			    XT22 = X[4];
			    XT23 = X[7];
			    //XT31 = X[0][2]; XT32 = X[1][2]; XT33 = X[2][2];
			    XT31 = X[2];
			    XT32 = X[5];
			    XT33 = X[8];

			    // Right Cauchy-Green deformation tensor
			    C11 = XT11 * XT11 + XT12 * XT12 + XT13 * XT13;
			    C12 = XT11 * XT21 + XT12 * XT22 + XT13 * XT23;
			    C13 = XT11 * XT31 + XT12 * XT32 + XT13 * XT33;
			    C22 = XT21 * XT21 + XT22 * XT22 + XT23 * XT23;
			    C23 = XT21 * XT31 + XT22 * XT32 + XT23 * XT33;
			    C33 = XT31 * XT31 + XT32 * XT32 + XT33 * XT33;

			    // Determinant of deformation gradient
			    float J = XT11 * (XT22 * XT33 - XT32 * XT23) + XT12 * (XT23 * XT31 - XT21 * XT33) +
			              XT13 * (XT21 * XT32 - XT22 * XT31);
			    // Determinant of C
			    float detC = C11 * (C22 * C33 - C23 * C23) - C22 * C13 * C13 - C12 * (C33 * C12 - 2 * C13 * C23);

			    float M = materialParams[1];
			    float K = materialParams[2];

			    float x1 = (float) pow((double) J, -2.0 / 3.0) * M;
			    float x2 = (K * J * (J - 1) - x1 * (C11 + C22 + C33) / 3) / detC;

			    //if (J == 0.0f || detC == 0.0f)
			    //{
			    //	x1 = 0;
			    //	x2 = 0;
			    //}

			    // 2nd Piola-Kirchhoff stress
			    //SPK[0][0] = (C22*C33 - C23*C23)*x2 + x1; // S00
			    SPK[0] = (C22 * C33 - C23 * C23) * x2 + x1; // S00
			    //SPK[1][1] = (C11*C33 - C13*C13)*x2 + x1; // S11
			    SPK[4] = (C11 * C33 - C13 * C13) * x2 + x1; // S11
			    //SPK[2][2] = (C11*C22 - C12*C12)*x2 + x1; // S22
			    SPK[8] = (C11 * C22 - C12 * C12) * x2 + x1; // S22
			    //SPK[0][1] = (C13*C23 - C12*C33)*x2; // S01
			    SPK[1] = (C13 * C23 - C12 * C33) * x2; // S01
			    //SPK[1][0] = SPK[0][1];
			    SPK[3] = SPK[1];
			    //SPK[1][2] = (C12*C13 - C23*C11)*x2; // S12
			    SPK[5] = (C12 * C13 - C23 * C11) * x2; // S12
			    //SPK[2][1] = SPK[1][2];
			    SPK[7] = SPK[5];
			    //SPK[0][2] = (C12*C23 - C13*C22)*x2; // S02
			    SPK[2] = (C12 * C23 - C13 * C22) * x2; // S02
			    //SPK[2][0] = SPK[0][2];
			    SPK[6] = SPK[2];
		    }
	    };
	}
}
