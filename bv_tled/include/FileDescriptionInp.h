#pragma once

namespace bv
{
    namespace fem
    {
        struct FileDescriptionInp
        {
	        struct Tokens
	        {
		        static const char* PART;
		        static const char* NODE;
		        static const char* NSET;
		        static const char* ELEMENT;
		        static const char* SOLID_SECTION;
		        static const char* END_PART;
		        static const char* ASSEMBLY;
		        static const char* END_ASSEMBLY;
		        static const char* INSTANCE;
		        static const char* MATERIAL;
		        static const char* ELASTIC;
		        static const char* HYPERELASTIC;
		        static const char* DENSITY;
		        static const char* BOUNDARY;
		        static const char* STEP;
		        static const char* CLOAD;
		        static const char* END_STEP;
	        };

            struct Templates
            {
                static const char* PART;
                static const char* NODE;
	            static const char* NSET;
                static const char* ELEMENT;
	            static const char* SOLID_SECTION;
	            static const char* END_PART;
                static const char* ASSEMBLY;
	            static const char* END_ASSEMBLY;
                static const char* INSTANCE;
                static const char* MATERIAL;
	            static const char* ELASTIC;
	            static const char* HYPERELASTIC;
	            static const char* DENSITY;
	            static const char* BOUNDARY;
	            static const char* STEP;
	            static const char* CLOAD;
	            static const char* END_STEP;
            };
        };
    }
}