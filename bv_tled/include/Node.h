#pragma once
#include "Common.h"

namespace bv
{
	namespace tled
	{
		class Node
		{
		public:
			Node() {}
			Node(glm::vec4* pos) { m_pos = pos; }

			glm::vec4& force() { return *m_force; }
			float& force(int index) { return m_force->operator[](index); }
			void force(glm::vec4* force) { m_force = force; }

			void pos(glm::vec4* pos) { m_pos = pos; }
			glm::vec4& pos() { return *m_pos; }
			const glm::vec4& pos() const { return *m_pos; }

			glm::vec3 posu() const { return glm::vec3(*m_pos + *m_u); }

			void translate(glm::vec3 translation) { *m_pos += glm::vec4(translation, 0.0f); }

			void u(glm::vec4* u) { m_u = u; }
			glm::vec4& u() { return *m_u; }

			int index() const { return m_index; }
			void index(int index) { m_index = index; }
			int dof() const { return m_index * 3; }

			static const float DRAW_SCALE;

		private:
			int m_index;

			glm::vec4 *m_force;
			glm::vec4 *m_pos;
			glm::vec4 *m_u;

		};

		typedef PTR(Node) NodePtr;
		typedef std::vector<Node> NodeCollection;
		typedef std::vector<Node *> NodePtrCollection;
	}
}
