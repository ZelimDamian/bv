#pragma once

#include "Component.h"
#include "RenderMesh.h"
#include "Assembly.h"
#include "AssemblyComponent.h"
#include "SolverComponent.h"

namespace bv
{
	namespace tled
	{
		template <class TMesh>
		class AssemblyRendererComponent: public Component<AssemblyRendererComponent<TMesh>>
		{
		protected:
			using ComponentBase = Component<AssemblyRendererComponent<TMesh>>;
			using Id = Handle<AssemblyRendererComponent<TMesh>>;

		public:
			AssemblyRendererComponent(Id id, Entity::Id entity): ComponentBase(id, entity) { }

			AssemblyRendererComponent(Id id, Entity::Id entity, Handle<TMesh>& meshId):
					ComponentBase(id, entity), m_meshId(meshId) { }

			static void init() {}

			void initialize()
			{
				auto assemblyComponent = this->template require<AssemblyComponent>();
				m_assembly = &assemblyComponent->assembly();

				using ColorMesh = GenMesh<PosColorVertex>;

				/// setup rendering of the mesh

				// acquire a mesh to represent the loads
				ColorMesh::Id loadsMeshId = ColorMesh::create();

				// mesh to represent the boundary conditions
				ColorMesh::Id bcMeshId = ColorMesh::create();
				
				// render the mesh on a render event
				auto renderer = RenderingSystem::instance();

				renderer->onrender([=] () mutable {

					if(!this->m_active) { return; }

				    using namespace bv::rendering;

					renderer->transform(this->m_entity->transform());

					for (int i = 0; i < m_meshId->vertices.size(); ++i)
					{
						m_meshId->surface->vertices[i].template set<TMesh::TVertex::Pos>(m_meshId->positions[i]);
					}

				    Shader::Id colorShader = Shader::require("colors");
				    Uniform::Id uColor = Uniform::require(colorShader, "uColor", UniformType::Vec4);

				    Shader::Id diffuseShader = Shader::require("diffuse_colors");
					diffuseShader->uniform("lightPos", UniformType::Vec3)->set(glm::vec3(1.0f, 1.0f, 1.0f));

				    // check if we want to render the assembly mesh using wireframe
				    {
					    // draw using lines
					    renderer->reset(Culling::NO, RenderMode::LINES);
					    Shader::use(colorShader);
						uColor->set(glm::vec4(0.2f, 0.2f, 0.0f, 1.0f));

					    auto renderMesh = RenderMesh::create(m_meshId);

					    // render the mesh
					    renderer->transform(this->m_entity->transform());
					    renderer->submit(renderMesh);
				    }

					if(m_drawBC)
					{
						// draw using points
						renderer->reset(Culling::CW, RenderMode::LINES);
						// the points should be colored
						Shader::use(colorShader);

						// allocate space for all positions
						loadsMeshId->resize(m_assembly->numActiveLoads() * 2);

						auto loadsMesh = GenMesh<PosColorVertex>::get(loadsMeshId);
						// update the mesh to take the updated positions
						for (int i = 0, k = 0; i < m_assembly->loads().size(); ++i)
						{
							const auto& load = m_assembly->loads()[i];

							if(load.active())
							{
								// specify the position
								if(load.shape() == LoadShape::PLANE)
								{
									auto u = m_assembly->U(i); // displacement
									auto plane = load.value(); // plane equation
									auto d_u = glm::dot(glm::vec3(plane), u); // d param for node
									auto g = plane.w - d_u; // penetration

//									if(g > 0.0f)
									{
										auto proj = u + g * glm::vec3(plane); // projected point
										loadsMesh->vertices[k++].set<TMesh::TVertex::Pos>(m_assembly->X(i) + u);
										loadsMesh->vertices[k++].set<TMesh::TVertex::Pos>(m_assembly->X(i) + proj);
									}
								}
								else if(load.shape() == LoadShape::SPRING)
								{
									auto start = m_assembly->pos(i);
									auto end = m_assembly->X(i) + glm::vec3(load.value());
									loadsMesh->vertices[k++].set<TMesh::TVertex::Pos>(start);
									loadsMesh->vertices[k++].set<TMesh::TVertex::Pos>(end);
								}
								else
								{
									auto pos = m_meshId->positions[i];
									auto x = m_assembly->X(i);
									glm::vec3 disp { load.value().x, load.value().y, load.value().z };
									loadsMesh->vertices[k++].set<TMesh::TVertex::Pos>(glm::vec3(pos + disp));
								}
							}
						}

						// display loads in red color
						uColor->set(glm::vec4(0.9f, 0.1f, 0.1f, 1.0f));

						// create a renderable
						auto loadsRenderable = RenderMesh::create(loadsMeshId);
						// position the laods mesh relative to the entity's position
						renderer->transform(this->m_entity->transform());
						// render the loads mesh
						renderer->submit(loadsRenderable);

						// render the loads as points
						renderer->reset(Culling::CW, RenderMode::POINTS);
						renderer->transform(this->m_entity->transform());
						renderer->submit(loadsRenderable);
					}
				});

				//////////////////////////////////////////////////////////////////////////////
				/// Mesh updating

				auto onMeshUpdate = [this]
				{
					// apply the deformations to node positions
					m_meshId->positions = m_assembly->posus();
					m_meshId->surface->positions = m_meshId->positions;

					// update the vertices from positions buffer
					m_meshId->updateVertices();
					// make sure that the surface also knows about mesh changes
					m_meshId->surface->updateVertices();
					m_meshId->surface->updateAllNormals();
				};

				for (auto& comp: AssemblyRendererComponent::components())
				{
					if (auto solver = SolverComponent::find(comp.entity()))
					{
						solver->onMeshUpdate += onMeshUpdate;
					}
				}
			}

			Assembly& assembly()
			{
				return *m_assembly;
			}

			void attachGUI()
			{
				using namespace bv::gui;

				gui::PanelElementPtr panel = this->gui();// needs to be explicitly typed in GCC

				panel->button("Draw BC", [&]
				{
					m_drawBC = !m_drawBC;
				}, [&]
				{
					return m_drawBC;
				});

			}

			Handle<TMesh> mesh() const { return m_meshId; }

		private:
			Assembly* m_assembly;
			Handle<TMesh> m_meshId;

			bool m_renderWireframe { false };

			bool m_drawBC { true };
		};
	}
}