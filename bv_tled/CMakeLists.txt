SETUP(bv_tled)

LINK(core)
LINK(physics)
LINK(tethex)
LINK(coldet)
LINK(gpu)
LINK(gpu_cl)

SET(SOURCE_FILES
    include/Assembly.h
    src/Assembly.cpp
    include/AssemblyComponent.h
    src/AssemblyComponent.cpp
    include/Element.h
    src/Element.cpp
    include/Node.h
    src/Node.cpp
    include/Material.h
    src/Material.cpp

    include/Constraint.h
    src/Constraint.cpp

    include/Load.h
    src/Load.cpp
    include/ForceLoad.h
    src/ForceLoad.cpp
    include/DispLoad.h
    src/DispLoad.cpp

    include/ElementHex.h
    src/ElementHex.cpp
    include/ElementTet.h
    src/ElementTet.cpp

    include/Calculator.h
    src/Calculator.cpp
    include/CalculatorHex.h
    src/CalculatorHex.cpp
    include/CalculatorComponent.h
    src/CalculatorComponent.cpp
#        src/CalculatorHexCL.cpp
#        include/CalculatorHexCLComponent.h
#        src/CalculatorHexCLComponent.cpp
    include/CalculatorTet.h
    src/CalculatorTet.cpp

    include/TimeStepper.h
    src/TimeStepper.cpp

    include/SolverComponent.h
    src/SolverComponent.cpp

    include/ContactComponent.h
    src/ContactComponent.cpp

    include/AssemblyRendererComponent.h

    include/CalculatorHexCL.h
    src/CalculatorHexCL.cpp
    include/CalculatorTetCL.h
    src/CalculatorTetCL.cpp

    include/TimeStepperCL.h
    src/TimeStepperCL.cpp

    include/PenaltyContactComponent.h
    src/PenaltyContactComponent.cpp

    include/LagMultContactComponent.h
    src/LagMultContactComponent.cpp
)

ADD_CLASS(ExporterInp)
ADD_CLASS(ExporterInpComponent)
ADD_CLASS(FileDescriptionInp)
ADD_CLASS(AbqsRprtLoader)
ADD_CLASS(ContactMethods)
ADD_CLASS(RayOctreeContactMethod)
ADD_CLASS(NewmannLagContactMethod)
ADD_CLASS(ProjectionContactMethod)
ADD_CLASS(DirectPDNContactMethod)
ADD_CLASS(InpLoader)

ADD_HEADER(AssemblyAttributeRendererComponent)

BOOTSTRAP(${target})
