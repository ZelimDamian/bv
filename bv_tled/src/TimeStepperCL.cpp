#include "TimeStepperCL.h"

using namespace bv;
using namespace tled;
using namespace gpu;
using namespace cl;

#define NO_COPY 0

TimeStepperCL::TimeStepperCL(BufferVec4 & U, BufferVec4 &F, const BufferFloat &M,
                             float dt, float dmp,
							 BufferCL<glm::vec4>::Id F_CL,
							 BufferCL<glm::vec4>::Id Fx_CL,
							 BufferCL<glm::vec4>::Id U_CL,
                             BufferCL<glm::vec4>::Id R_CL,
                             BufferCL<int>::Id numForces_CL,
                             BufferCL<int>::Id aggregatedForces_CL,
                             int maxNumForces)
		: TimeStepper(U, F, M, dt, dmp)
{
	// create buffers on device
	m_U_old_CL = BufferCL<glm::vec4>::create(BufferFormat::RW, m_U_old.size());

	m_U_new_CL = BufferCL<glm::vec4>::create(BufferFormat::W, m_U_new.size());

	m_F_CL = F_CL;
	m_Fx_CL = Fx_CL;

	m_U_CL = U_CL;

    m_R_CL = R_CL;

	// Upload coefficients
	m_ABC_CL = BufferCL<glm::vec4>::create(BufferFormat::R, m_ABC.size());

	// retreive kernel
//	if(false)
	if(true)
	{
		m_updateUKernel = Kernel::require("central_diff_collect");
	}
	else
	{
		m_updateUKernel = Kernel::require("verlet");
        dmp = 0.989f;
	}

    recalculate(M, dt, dmp);

	// set kernel arguments
	//__global float* m_ABC,
	//__global float* m_U,
	//__global float* m_U_old,
	//__global float* m_U_new,
	//__global float* m_Fx,
	//__global float* m_F
	m_updateUKernel->set(0, m_ABC_CL);
	m_updateUKernel->set(1, m_U_CL);
	m_updateUKernel->set(2, m_U_old_CL);
	m_updateUKernel->set(3, m_U_new_CL);
	m_updateUKernel->set(4, m_F_CL);
    m_updateUKernel->set(5, m_Fx_CL);
    m_updateUKernel->set(6, m_R_CL);
    m_updateUKernel->set(7, numForces_CL);
	m_updateUKernel->set(8, aggregatedForces_CL);
	m_updateUKernel->set(9, maxNumForces);
}

void TimeStepperCL::recalculate(const BufferFloat& M, float dt, float dmp)
{
	TimeStepper::recalculate(M, dt, dmp);

	m_ABC_CL->upload(m_ABC);
}

void TimeStepperCL::reset()
{
	TimeStepper::reset();

	m_Fx_CL->upload(glm::vec4());
	m_F_CL->upload(glm::vec4());

	// copy on device is faster
    *m_R_CL = *m_F_CL;

	// set all U's to zeros
	*m_U_CL = *m_F_CL;
	*m_U_new_CL = *m_F_CL;
	*m_U_old_CL = *m_F_CL;
}

void TimeStepperCL::update()
{
	const size_t nodeCount = m_U.size();

	m_updateUKernel->execute({ nodeCount });
}

template <class T>
void swap_impl(T& first, T& second)
{
	T tmp = first;
	first = second;
	second = tmp;
}

void TimeStepperCL::swap()
{
#if NO_COPY
	swap_impl(m_U_CL, m_U_old_CL);
//	swap_impl(m_U_CL, m_U_new_CL);
//  swap_impl(m_U_old_CL, m_U_new_CL);

//	m_updateUKernel->set(3, m_U_CL);
//	m_updateUKernel->set(4, m_U_old_CL);
//	m_updateUKernel->set(5, m_U_new_CL);
#else
	*m_U_old_CL = *m_U_CL;
	*m_U_CL = *m_U_new_CL;
#endif
}

void TimeStepperCL::downloadU()
{
	m_U_CL->download();

	if(!checkConvergence(&m_F_CL->data()))
	{
//		*m_U_CL = *m_U_old_CL;
//		*m_U_new_CL = *m_U_old_CL;

		m_Fx_CL->upload(glm::vec4());
//		m_F_CL->upload(glm::vec4());

		// copy on device is faster
		*m_R_CL = *m_F_CL;

//		this->reset();

		m_isConverged = false;
	}
	else
	{
		m_U_old = m_U;
		m_U = m_U_CL->data();
		m_isConverged = true;
	}
}

void TimeStepperCL::setU(const BufferVec4& U)
{
	m_U = U;
	m_U_old_CL->upload(U);
	*m_U_CL = *m_U_old_CL;
	*m_U_new_CL = *m_U_old_CL;
}
