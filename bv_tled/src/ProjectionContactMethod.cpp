#include "ProjectionContactMethod.h"

#include "CollisionSystem.h"
#include "CollisionMethodRayOctree.h"
#include "RayMeshContact.h"
#include "MeshContact.h"
#include "RigidBodyComponent.h"
#include "Assembly.h"

#include "UtilitiesColDet.h"
#include "ClassNamer.h"
#include "DebugMesh.h"

using namespace bv;
using namespace coldet;
using namespace tled;

//ContactMethod ProjectionContactMethod::create()
//{
//	auto action = [](ContactMethod::ContactActionData data)
//	{
//		auto& collisioner = CollisionSystem::instance();
//
//		const auto contact = data.contact;
//
//		if (!contact->isFound())
//		{
//			return;
//		}
//
//		auto colliders = collisioner.colliders(contact);
//
//		auto slave = colliders.first;
//		auto master = colliders.second;
//
//		auto concept = master->concept<Octree>();
//		auto& octree = concept->shape();
//
//		const auto& pairs = contact->pairs();
//
//		// get the indices of faces in contact on the mesh belonging
//		auto slaveFaceIndices = contact->uniqueIndicesFirst();
//		// find the indices of slave nodes in contact
//		auto slaveNodeIndices = Utilities::vertIndices(slaveFaceIndices, contact->mesh1());
//
//		// we also need to add the displaced nodes to the list as they are also likely to
//		auto assembly = data.assembly;
//		auto& loads = assembly->loads();
//		for (int i = 0; i < loads.size(); ++i)
//		{
//			if(loads[i].active() && loads[i].shape() != LoadShape::LINEAR)
//			{
//				slaveNodeIndices.push_back(i);
//			}
//		}
//
//		bv::filterUnique(slaveNodeIndices);
//
//		std::vector<glm::vec3> slaveNodePositions(slaveNodeIndices.size());
//
//		for (int i = 0; i < slaveNodeIndices.size(); ++i)
//		{
//			// the position of the slave node
//			const glm::vec3& nodePos = contact->mesh1()->m_positions[slaveNodeIndices[i]];
//			slaveNodePositions[i] = Math::transform(nodePos, contact->slave()->entity()->transform());
//		}
//
//		std::vector<float> nodePenetrations(slaveNodeIndices.size());
//		std::vector<glm::vec3> contactNormals(slaveNodeIndices.size());
//
//		// collect the normals of the master surface
//		std::vector<glm::vec3> masterNormals = Utilities::collectNormals<ContactSide::Second>(contact);
//
//		// calculate the average normal of master surface
//		const glm::vec3 localMasterNormal = glm::normalize(Math::average(masterNormals));
//
//		// contact normal needs to be transformed to global coords
//		const glm::vec3 contactNormal = Math::transformNormal(localMasterNormal, data.master->transform());
//
//		auto& debug = DebugMesh::instance();
//
//		for (int i = 0; i < slaveNodePositions.size(); ++i)
//		{
//			int nodeIndex = slaveNodeIndices[i];
//			const glm::vec3& nodePos = slaveNodePositions[i];
//
//			Ray ray(slaveNodePositions[i], contactNormal);
//
//			// build collision method
//			CollisionMethodRayOctree method(ray, octree);
//			method.mat2(data.master->transform());
//
//			// execute method to get contact
//			auto result = method();
//			auto rayContact = static_cast<RayMeshContact*>(result.get());
//
////			debug.point(ray.point(0.0f));
//
//			// make sure contacts are sorted according to depth - ray t param
//			if (rayContact->isFound())
//			{
//				rayContact->sort();
//
//				// get all ray hits
//				const auto& hits = rayContact->pairs();
//
//				// get the very first hit along ray
//				const float t = nodePenetrations[i] = hits[0].t;
//				const glm::vec3 hit = ray.point(t);
//				const glm::vec3 gap = hit - nodePos;
//				const glm::vec3 oldU = data.assembly->U(nodeIndex);
//				const glm::vec3 newU = gap + oldU;
//
//				const auto& triIdx = hits[0].idx;
//				const glm::vec3 triNormal = Math::transformNormal(octree.mesh()->normals[triIdx], data.master->transform());
//				contactNormals[i] = triNormal;
//
//				const float psi = 0.05f;
//				const float d = glm::dot(triNormal, newU);
//				const float old_d = glm::dot(triNormal, oldU);
//				const float dd = glm::abs(d - old_d);
//				const float undershoot = psi * dd;
//				const glm::vec4 plane(triNormal, d - undershoot);
//
//				auto& load = data.assembly->loads()[nodeIndex];
//
//				if(load.active())
//				{
//					const float threshold = 0.005f;
//
//					if (glm::abs(dd) > threshold)
//					{
//						data.assembly->project(nodeIndex, plane);
//					}
//					else
//					{
//						load.renew();
//					}
//				}
//				else
//				{
//					data.assembly->project(nodeIndex, plane);
//				}
////				debug.point(hit);
//			}
//			else
//			{
////				debug.point(ray.point(10.0f));
//			}
//		}
//
////		debug.render();
//
//		auto masterBody = data.body;
//		// update surface mesh if it is dirty
//		if(masterBody)
//		{
//			{
//				// apply all nodal forces from the surface to the master
//				for (int i = 0; i < nodePenetrations.size(); ++i)
//				{
//					const int nodeIndex = slaveNodeIndices[i];
//
//					float dt = Timing::delta();
//					float dtSqr = dt * dt;
////					float mass = masterBody->mass() / nodePenetrations.size(); //
//					float mass = assembly->M(nodeIndex);
//					const glm::vec3 gap = nodePenetrations[i] * contactNormals[i];
//
//					glm::vec3 force = - gap * mass / dtSqr;
//					masterBody->applyForceGlobal(force, slaveNodePositions[i]);
////					masterBody->applyForce(force);
//				}
//			}
//		}
//	};
//
//	return ContactMethod(ClassNamer::name<ProjectionContactMethod>(), action);
//}
