#include "ExporterInp.h"

#include "FileWriter.h"

#include "FileDescriptionInp.h"

#include "Assembly.h"

#include <stdlib.h>

using namespace bv;
using namespace fem;
using namespace tled;

using Tokens = FileDescriptionInp::Templates;

void ExporterInp::write(const std::string& name, const Assembly& assembly)
{
	const std::string partName = name + "_PART";

    FileWriter file(name + ".inp");

    file.formatln(Tokens::PART, partName.c_str());
    file.formatln(Tokens::NODE, partName.c_str());

    // write all the nodes
    for (int i = 0; i < assembly.nodes().size(); ++i)
    {
        file.write(i + 1); file.write(", ");
        file.writeln(assembly.node(i).pos());
    }

    static const std::string type = "C3D4";

    file.formatln(Tokens::ELEMENT, "ELS", type.c_str());

    // write all the elements
    for (int i = 0; i < assembly.elements().size(); ++i)
    {
		auto& nodeIndices = assembly.element(i)->nodeIndices();

        file.write(i + 1);				file.write(", ");
        file.write(nodeIndices[0] + 1); file.write(", ");
	    file.write(nodeIndices[1] + 1); file.write(", ");
        file.write(nodeIndices[2] + 1); file.write(", ");
        file.write(nodeIndices[3] + 1);

        file.newline();
    }


	file.newline();

	const std::string materialName = "MAT-1";
	file.formatln(Tokens::SOLID_SECTION, "ELS", materialName.c_str());

	file.newline();

    file.writeln(Tokens::END_PART);

	file.newline();
	file.newline();

	file.writeln(Tokens::ASSEMBLY);
	file.formatln(Tokens::INSTANCE, partName.c_str(), partName.c_str());

	file.newline();

	file.formatln(Tokens::NSET, "BC-NSET", partName.c_str());

	// write all the elements
	for (int i = 0; i < assembly.loads().size(); ++i)
	{
		if(assembly.loads()[i].active() && assembly.loads()[i].shape() == LoadShape::FIX)
		{
			file.write(i);

			// no need for the trailing comma
			if(i < assembly.loads().size() - 1)
			{
				file.write(", ");
			}
		}
	}

	file.newline();
	file.writeln(Tokens::END_ASSEMBLY);

	file.newline();
	file.newline();

	auto material = assembly.elements()[0]->material();

	file.formatln(Tokens::MATERIAL, materialName.c_str());
	file.formatln(Tokens::DENSITY, material->params(0));
	file.formatln(Tokens::HYPERELASTIC, "NEO HOOKE", material->params(1), material->params(2) );

	float totalTime = 1.0f;
	file.formatln(Tokens::STEP, totalTime);
	file.formatln(Tokens::BOUNDARY, "BC-NSET");

	file.newline();

	file.writeln(Tokens::CLOAD);
	// write all the loads
	for (int i = 0; i < assembly.loads().size(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			auto value = assembly.loads()[i].evaluate(1.0f);
			if(assembly.loads()[i].active() && assembly.loads()[i].shape() != LoadShape::FIX)
			{
				file.write(i + 1); file.write(", ");
				file.write(j + 1); file.write(", ");
				file.write(value[j]);
				file.newline();
			}
		}
	}

	file.writeln(Tokens::END_STEP);
}