#include "ContactComponent.h"
#include "CollisionMethodProvider.h"
#include "UpdateSystem.h"
#include "AssemblyComponent.h"
#include "RenderMesh.h"
#include "Collider.h"
#include "AssemblyRendererComponent.h"
#include "TetMesh.h"
#include "CollisionSystem.h"
#include "ContactMethods.h"
#include "RigidBodyComponent.h"

#include "UtilitiesColDet.h"

using namespace bv;
using namespace tled;
using namespace coldet;
using namespace tethex;

// represents a contact resolver
struct Resolver
{
    // functor to resolve contact
    ContactMethod::ResolveAction action;
    // data required to resolve contact
    ContactMethod::ContactResolveData data;

    // execute resolver
    void operator () () { action(&data); }
};

static std::vector<Resolver> _resolvers;

void ContactComponent::initialize()
{
    auto mesh = require<AssemblyRendererComponent<TetMesh>>()->mesh();
    
    if(!mesh)
    {
        Log::error("Missing collider mesh in ContactComponent");
    }

    auto otherMesh = Utilities::colliderMesh(m_master);
    otherMesh->updateTriNormals();

	UpdateSystem::instance()->onupdate([this]
	{
        if(m_active)
        {
            update();
        }
	});

    UpdateSystem::instance()->onsync([this]
    {
        if(m_active)
        {
            resolve();
        }
    });

    _resolvers.emplace_back();
}

void ContactComponent::update()
{
	using namespace coldet;

	auto collider1 = Collider::find(m_entity);
	auto collider2 = Collider::find(m_master);

	if(!collider1 || !collider2)
	{
		Log::error("Collider missing when updating contact mesh in " + this->title());
	}

	if (auto concept = collider1->concept<Octree>())
	{
		auto octree = &concept->shape();
		if(octree->needsRefit())
		{
			octree->refit();
		}
	}
	else if (auto concept = collider1->concept<Bitree>())
	{
		auto bitree = &concept->shape();
		bitree->refit();
	}
	else
	{
		Log::error("Missing mesh on the master entity in " + title());
	}

	// retreive the contact
	auto result = CollisionSystem::instance()->contact(collider1, collider2);

	// need to make sure that its a mesh contact
	auto meshContact = dynamic_cast<const MeshContact*>(result.contact);

    // get the assembly
    auto& assembly = require<AssemblyComponent>()->assembly();
    // and the rigid body
    auto rigidBody = physics::RigidBodyComponent::find(m_master);

    // data required for the contact detection stage
    auto data = ContactMethod::ContactActionData { m_entity, m_master, &assembly, meshContact,
                                                   &rigidBody.ref(), m_multiplier};

    auto resolver = &_resolvers[(int)m_id];

    // the data to use for contact resolution
    auto resolved = &resolver->data;

    // make sure that its clear before filling it with new
    resolved->clear();

    // execute contact method and return contact resolution action
    resolver->action = ContactMethods::methods[m_method].execute(data, resolved);
}

void ContactComponent::forget()
{
	_resolvers[(int)m_id].data.clear();
}

void ContactComponent::resolve()
{
    for (int i = 0; i < count(); ++i)
    {
        _resolvers[i]();
    }
}

void ContactComponent::attachGUI()
{
    auto m_panel = gui();

	m_panel->bind("Multiplier", m_multiplier, 1.0f, 1000.0f);

	auto methodNames = ContactMethods::names();

	auto onselect = [this] (int idx)
	{
		m_method = idx;
	};

	m_panel->combo("Methods", methodNames, onselect);
}
