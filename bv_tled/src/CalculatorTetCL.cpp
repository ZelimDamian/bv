#include "CalculatorTetCL.h"
#include "TimeStepperCL.h"
#include <RigidBodyComponent.h>

using namespace bv;
using namespace tled;
using namespace tethex;
using namespace gpu;
using namespace cl;

void CalculatorTetCL::init(float dt, float damping)
{
	CalculatorTet::init(dt, damping);

	//kickstart GPU
	GpuSystem::instance()->load();

	// Buffer for volume values
	m_V_CL = BufferCL<float>::create(BufferFormat::R, m_V);

	// Buffer for per element von Mises stresses
	m_VMS_CL = BufferCL<float>::create(BufferFormat::W, m_assembly->elements().size());

	// Buffer for Shape function global derivatives elnum * [4][3]
	m_DhDx_CL = BufferCL<float>::create(BufferFormat::R, m_DhDx);

	// Buffer for Element node indices [elnum * 4]
	m_nodeInds_CL = BufferCL<int>::create(BufferFormat::R, m_nodeInds);

	m_materialInds_CL = BufferCL<int>::create(BufferFormat::R, BufferInt(m_assembly->materialInds()));

	// material properties
	m_mat_CL = BufferCL<float>::create(BufferFormat::R, BufferFloat(Material::params()));

	auto& U = m_assembly->U();
	auto& F = m_assembly->F();

	// Node deformations
	m_U_CL = BufferCL<glm::vec4>::create(BufferFormat::R, U.size());

    const size_t elCount = m_assembly->elements().size();
    const size_t nodeCount = m_assembly->nodes().size();

//    {
		const auto& m_numForces = m_assembly->numForces();
		m_forceIndices = BufferInt(m_assembly->forceIndices());

		// force aggregation
		std::vector<int> aggregatedForces(nodeCount);
		int sum = 0;
		for (int i = 0; i < nodeCount; ++i)
		{
			aggregatedForces[i] = sum;
			sum += m_numForces[i];
		}

		m_numForces_CL = BufferCL<int>::create(BufferFormat::R, BufferInt(m_numForces));

		m_aggregatedForces_CL = BufferCL<int>::create(BufferFormat::R, BufferInt(aggregatedForces));

        // shift all local node force indices to the global dofs
        for (int i = 0; i < elCount; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                const int nodeIndex = m_nodeInds[i * 4 + j];
                const int elNodeOffset = aggregatedForces[nodeIndex];

                m_forceIndices[i * 4 + j] += elNodeOffset;
            }
        }

        m_forceIndices_CL = BufferCL<int>::create(BufferFormat::R, m_forceIndices);
//    }

    // Nodal Forces
    // A separate force for value for each element node
	//int totalNumForces = std::accumulate(m_numForces_CL->data().begin(), m_numForces_CL->data().end(), 0);
    m_Fx_CL = BufferCL<glm::vec4>::create(BufferFormat::W, elCount * 4);
    m_F_CL = BufferCL<glm::vec4>::create(BufferFormat::RW, U.size());

    m_R_CL = BufferCL<glm::vec4>::create(BufferFormat::RW, U.size());

    // Load our kernel
    m_tled_kernel = Kernel::require("tled_tet");
    m_constraints_kernel = Kernel::require("apply_constraints");
    m_gravity_kernel = Kernel::require("apply_pre_constraints");
    m_collect_kernel = Kernel::require("collect_force_tet");

//    __global float* m_V,
//    __global float* m_DhDx,
//    __global float* U,
//    __global float* F,
//    __global int* m_nodeInds,
//    __global int* m_forceIndices,
//    __global float *matParams)
    m_tled_kernel->args(m_V_CL,
                        m_DhDx_CL,
                        m_U_CL,
                        m_Fx_CL,
                        m_nodeInds_CL,
                        m_mat_CL,
                        m_forceIndices_CL,
						m_VMS_CL,
						m_materialInds_CL
	);

    BufferFloat& M = m_assembly->M();

	{
		int maxForces = *std::max_element(m_numForces_CL->data().begin(), m_numForces_CL->data().end());
		auto timeStepper = new TimeStepperCL(U, F, M, dt, damping, m_F_CL, m_Fx_CL, m_U_CL, m_R_CL, m_numForces_CL,
											 m_aggregatedForces_CL, maxForces);

		m_stepper = timeStepper;
	}

	m_M_CL = BufferCL<float>::create(BufferFormat::R, M);

    // setup the constraint application
	auto m_U_new_CL = static_cast<TimeStepperCL*>(m_stepper)->U_new_CL();
	auto m_U_old_CL = static_cast<TimeStepperCL*>(m_stepper)->U_old_CL();

	m_nodeConstraintFlags_CL = BufferCL<int>::create(BufferFormat::R, m_assembly->loads().size());
	m_constraintMagnitudes_CL = BufferCL<glm::vec4>::create(BufferFormat::R, m_assembly->loads().size());

	m_constraints_kernel->set(0, m_nodeConstraintFlags_CL);
	m_constraints_kernel->set(1, m_constraintMagnitudes_CL);
	m_constraints_kernel->set(2, m_U_new_CL);
	m_constraints_kernel->set(3, m_U_CL);
	m_constraints_kernel->set(4, m_U_old_CL);
	m_constraints_kernel->set(5, m_R_CL);

    // setup the force collect as well
    m_collect_kernel->args(m_Fx_CL, m_F_CL, m_numForces_CL, 0 );
}

void CalculatorTetCL::preStep(float fraction)
{
    // update the material params on the GPU
    m_mat_CL->upload(BufferFloat(Material::params()));

	// upload external forces
	m_R_CL->upload(m_assembly->R());
	m_assembly->R().set(glm::vec4());

	// upldate gravity values
	const glm::vec3 gravity = physics::RigidBodyComponent::gravity();

	auto m_U_new_CL = static_cast<TimeStepperCL*>(m_stepper)->U_new_CL();
	m_gravity_kernel->args(m_U_new_CL,
	                       m_nodeConstraintFlags_CL,
	                       m_constraintMagnitudes_CL,
	                       m_M_CL, m_R_CL,
	                       gravity.x, gravity.y, gravity.z);

	// apply gravity on top of external forces
	const size_t nodeCount = m_assembly->nodes().size();
	m_gravity_kernel->execute({nodeCount});

	// apply constraints and upload data
	applyConstraints(fraction);
}

void CalculatorTetCL::subStep(float subFraction)
{
    const size_t elCount = m_assembly->elements().size();
    const size_t nodeCount = m_assembly->nodes().size();

	if(m_doForces)
	{
		m_tled_kernel->execute({elCount});
//		m_collect_kernel->execute({nodeCount });
	}

    // run the central difference udpate
    m_stepper->update();

	m_constraints_kernel->set(6, subFraction);
    m_constraints_kernel->execute({nodeCount});

    // swap U -> U_old and U_new -> U
    m_stepper->swap();
}

void CalculatorTetCL::postStep(float fraction)
{

    auto stepper = static_cast<TimeStepperCL*>(m_stepper);

    stepper->downloadU();
	m_F_CL->download(m_assembly->F());

	// stresses per element
	m_VMS_CL->download(m_assembly->VMS());

}

void CalculatorTetCL::applyConstraints(float step)
{
	m_nodeConstraintFlags_CL->data().zero();
	m_constraintMagnitudes_CL->data().zero();

	// apply displacements for the next step
	const LoadCollection& loads = m_assembly->loads();

	for (int i = 0; i < loads.size(); i++)
	{
		auto& load = loads[i];

		if(load.active())
		{
			m_nodeConstraintFlags_CL->data()[i] = (int) load.shape();
			m_constraintMagnitudes_CL->data()[i] = load.evaluate(step);
		}
	}

	// instant loads are to be removed
	m_assembly->hitLoads();

	m_nodeConstraintFlags_CL->upload();
	m_constraintMagnitudes_CL->upload();
}

void CalculatorTetCL::apply(const State &state)
{
	auto stepper = static_cast<TimeStepperCL*>(m_stepper);
	stepper->setU(state.U);
}
