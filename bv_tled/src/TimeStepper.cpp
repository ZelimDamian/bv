#include "TimeStepper.h"

#include "FileWriter.h"

using namespace bv;
using namespace bv::tled;

TimeStepper::TimeStepper(BufferVec4& U, BufferVec4& F, const BufferFloat& M, float dt, float dmp):
		m_U(U), m_F(F), m_convergenceMask(U.size(), 1)
{
	// create copies
	m_U_old = U;
	m_U_new = U;

	m_ABC.resize(U.size());

	recalculate(M, dt, dmp);
}

void TimeStepper::update()
{
	//_force = &F;
	for (int i = 0; i < m_U.size(); i++)
	{
		const auto& u = m_U.at(i);
		const auto& u_old = m_U_old.at(i);
		const auto& f = m_F.at(i);

		const float A = m_ABC[i].x;
		const float B = m_ABC[i].y;
		const float C = m_ABC[i].z;
		
		auto& u_new = m_U_new.at(i);

        u_new = A * f + B * u + C * u_old;
	}
}

void TimeStepper::reset()
{
	m_F.zero();
	m_U.zero();
	m_U_old.zero();
	m_U_new.zero();
}

//FileWriter _writer("stepperLog.log");

void TimeStepper::recalculate(const BufferFloat& M, float dt, float dmp)
{
	float dt2 = m_dt2 = dt * dt;

	for (size_t i = 0; i < M.size(); i++)
	{
		const float m = M[i];
		const float m_dt2 = m / dt2;
		const float m_2dt = m / (2 * dt);

		const float A = 1 / (m_dt2 + dmp * m_2dt);
		const float B = 2 * (A * m_dt2);
		const float C = dmp * (A * m_2dt) - B / 2;

		m_ABC[i].x = A;
		m_ABC[i].y = B;
		m_ABC[i].z = C;

//		m_ABC[i].x = dt2 / m; // for verlet
//		m_ABC[i].y = dmp;
	}
}

void TimeStepper::swap()
{
	if (checkConvergence(&m_U_new))
	{
		m_U_old = m_U;
		m_U = m_U_new;
		m_isConverged = true;
	}
	else
	{
		//static bool written = false;

		//if(!written)
		//{
		//	// report the values
		//	for (int i = 0; i < m_U.size(); ++i)
		//	{
		//		_writer.write(m_U[i]);
		//		_writer.write(" --> ");
		//		_writer.write(_force->at(i));

		//		_writer.writeln();
		//	}

		//	written = true;
		//}

		// revert to previous state
		m_U_new = m_U = m_U_old;
		m_isConverged = false;
	}
}

bool TimeStepper::checkConvergence(BufferVec4* u)
{
	for (size_t i = 0; i < u->size(); i++)
	{
        m_convergenceMask[i] = !glm::any(glm::isnan((*u)[i]));
	}

	return std::all_of(std::begin(m_convergenceMask), std::end(m_convergenceMask), [] (int v) { return v; });
}

const std::vector<int>& TimeStepper::convergenceMask() const
{
	return m_convergenceMask;
}


