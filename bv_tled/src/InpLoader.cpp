#include "InpLoader.h"

#include <FileReader.h>
#include "FileDescriptionInp.h"

using namespace bv;
using namespace tled;
using namespace tethex;

using Tokens = fem::FileDescriptionInp::Tokens;
enum class State
{
    Ready,
    Node,
    Element,
	Surface
};

void processNode(const char* line, TetMesh::Id mesh, State state)
{
    glm::vec3 pos;
    int index = -1;
    c_scanf(line, "%d, %f, %f, %f", &index, &pos.x, &pos.z, &pos.y);

    if(index >= 0)
    {
        mesh->vertex(pos);
    }
}

void processElement(const char* line, TetMesh::Id mesh, State state)
{
    int inds[4] { -1, -1, -1, -1 };

    int index = -1;
    c_scanf(line, "%d, %d, %d, %d, %d", &index, &inds[0], &inds[1], &inds[2], &inds[3]);

	inds[0]--;
	inds[1]--;
	inds[2]--;
	inds[3]--;

    if(index >= 0)
    {
        mesh->hedra.emplace_back(inds);
    }
}

void processTri(const char* line, TetMesh::Id mesh, State state)
{
	int inds[3] { -1, -1, -1 };

	int index = -1;
	c_scanf(line, "%d, %d, %d, %d", &index, &inds[0], &inds[2], &inds[1]);

	inds[0]--;
	inds[1]--;
	inds[2]--;

	if(index >= 0)
	{
		mesh->surface->tri(inds[0], inds[1], inds[2]);
	}
}

void processLine(const char* line, TetMesh::Id mesh, State state)
{
    switch (state)
    {
        case State::Node:
            processNode(line, mesh, state); break;
	    case State::Surface:
		    processTri(line, mesh, state); break;
        case State::Element:
            processElement(line, mesh, state); break;
        default: return;
    }
}

TetMesh::Id InpLoader::load(const std::string &name, const std::string ext)
{
    TetMesh::Id mesh = TetMesh::create();

    // build full filename
    const std::string filename = "abaqus/" + name + ext;
    // read the file contents
    const std::string contents = FileReader::read(filename);

    // start reading the contents line by line
    char *line = c_strtok(const_cast<char*>(contents.c_str()), "\n");

    State state = State::Ready;

    // while we have lines to read
    while (line != NULL)
    {
	    char* start = line;
	    char* end = line + 128;

        switch (line[0])
        {
            case ' ': state = State::Ready; break;
            case '*':
            {
	            if(strstr(line, Tokens::NODE))
	            {
		            state = State::Node;
	            }
	            else if(strstr(line, Tokens::ELEMENT))
	            {
		            if(strstr(line, "C3D4"))
		            {
			            state = State::Element;
		            }
		            else if(strstr(line, "CPS3"))
		            {
						state = State::Surface;
		            }
	            }
	            else
	            {
		            state = State::Ready;
	            }
			} break;

            default: processLine(line, mesh, state); break;
        }

        // read next line
        line = c_strtok(NULL, "\n");
    }

    mesh->updatePositions();
    mesh->updateValences();
    mesh->updateSurfaceVertices();

	mesh->surface->updateIndices();
	mesh->surface->setupConnections();

    return mesh;
}
