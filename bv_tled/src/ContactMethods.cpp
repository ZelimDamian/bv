#include "ContactMethods.h"

#include "Assembly.h"
#include "MeshContact.h"
#include "RigidBodyComponent.h"

#include "RayOctreeContactMethod.h"
#include "NewmannLagContactMethod.h"
#include "ProjectionContactMethod.h"
#include "DirectPDNContactMethod.h"

using namespace bv;
using namespace tled;
using namespace coldet;
using namespace physics;

using NodeAction = std::function<void(int)>;
using Data = ContactMethod::ContactActionData;

void process(Data data, const NodeAction &nodeAction)
{
	// only update mesh when there is contact
	if(!data.contact->isFound())
	{
		return;
	}

	bool surfaceNeedsUpdate = false;

	// clear the mesh before filling it
//			        _rayMeshId->clear();

	auto info = data.contact->info();

	const auto& nodePenetrations = info->penetrations;

	for (int i = 0; i < nodePenetrations.size(); ++i)
	{
		if(nodePenetrations[i] == 0.0f)
		{
			continue;
		}

		nodeAction(i);

		// reference configuration position
//		const glm::vec3 x = Math::transform(assembly->X(nodeIndex), contact->slaveTransform());
//		const glm::vec3 pos = Math::transform(assembly->pos(nodeIndex), contact->slaveTransform());

		// predicted node position
//		const glm::vec3 posu = x + newU;

//				        _rayMeshId->vertex(pos);
//				        _rayMeshId->vertex(posu);
		// mark surface mesh as dirty
		surfaceNeedsUpdate = true;
	}

	auto masterBody = data.body;
	// update surface mesh if it is dirty
	if(surfaceNeedsUpdate && masterBody)
	{
		{
			// apply all nodal forces from the surface to the master
			for (int i = 0; i < nodePenetrations.size(); ++i)
			{
				const int nodeIndex = info->slaveVertIndices[i];

				float dt = Timing::delta();
				float dtSqr = dt * dt;
				float mass = masterBody->mass() / nodePenetrations.size(); //assembly->M(nodeIndex);
//				float mass = assembly->M(nodeIndex);
				const glm::vec3 gap = nodePenetrations[i] * info->masterContactNormal;

				glm::vec3 force = - gap * mass / dtSqr;
//				masterBody->applyForceGlobal(force, info->slaveVertPositions[i]);
				masterBody->applyForce(force);
			}
		}
	}
}

std::vector<ContactMethod> ContactMethods::methods = {
		DirectPDNContactMethod::create(),
//		ProjectionContactMethod::create(),
//		RayOctreeContactMethod::create(),
		NewmannLagContactMethod::create()
//		{
//				"Kinematic Persistent",
//				[](Data data)
//				{
//					auto nodeAction = [=](int i)
//					{
//						auto info = data.contact->info();
//						const glm::vec3 gap = info->penetrations[i] * info->masterContactNormal;
//
//						const int nodeIndex = info->slaveVertIndices[i];
//
//						const glm::vec3 oldU = data.assembly->U(nodeIndex);
//						const glm::vec3 newU = gap + oldU;
//
////						assembly.displace(nodeIndex, gap, LoadShape::INSTANT);
////						assembly->displace(nodeIndex, newU, LoadShape::LINEAR_INSTANT);
//						data.assembly->displace(nodeIndex, newU, LoadShape::LINEAR);
//					};
//
//					process(data, nodeAction);
//				}
//		},
//		{
//				"Kinematic Undershoot",
//				[](Data data)
//		        {
//			        auto nodeAction = [=](int i)
//			        {
//				        auto info = data.contact->info();
//				        const glm::vec3 gap = info->penetrations[i] * info->masterContactNormal;
//
//				        const int nodeIndex = info->slaveVertIndices[i];
//
//				        const glm::vec3 oldU = data.assembly->U(nodeIndex);
//				        const glm::vec3 newU = gap * 0.9f + oldU;
//
////						assembly.displace(nodeIndex, gap, LoadShape::INSTANT);
//				        data.assembly->displace(nodeIndex, newU, LoadShape::LINEAR_INSTANT);
////						assembly.displace(nodeIndex, gap, LoadShape::LINEAR_INSTANT);
////						assembly.U(nodeIndex) = newU;
//			        };
//
//			        process(data, nodeAction);
//		        }
//		},
//		{
//				"Undershoot Threshold",
//				[](Data data)
//		        {
//			        auto nodeAction = [data](int i)
//			        {
//				        auto info = data.contact->info();
//				        const float penetration = info->penetrations[i];
//				        const glm::vec3 gap = penetration * info->masterContactNormal;
//
//				        const int nodeIndex = info->slaveVertIndices[i];
//
//				        const glm::vec3 oldU = data.assembly->U(nodeIndex);
//				        const glm::vec3 newU = gap * 0.99f + oldU;
//
//				        const float threshold = 0.001f;
//
//				        auto& load0 = data.assembly->loads()[nodeIndex * 3 + 0];
//				        auto& load1 = data.assembly->loads()[nodeIndex * 3 + 1];
//				        auto& load2 = data.assembly->loads()[nodeIndex * 3 + 2];
//
//				        if(load1.active() && load1.active() && load2.active())
//				        {
//					        if(penetration > threshold)
//					        {
//						        data.assembly->displace(nodeIndex, newU, LoadShape::LINEAR_INSTANT);
//					        }
//					        else
//					        {
//						        load0.renew();
//						        load1.renew();
//						        load2.renew();
//					        }
//				        }
//				        else
//				        {
//					        data.assembly->displace(nodeIndex, newU, LoadShape::LINEAR_INSTANT);
//				        }
//			        };
//
//			        process(data, nodeAction);
//		        }
//		}
};

std::vector<std::string> ContactMethods::names()
{
	std::vector<std::string> names(methods.size());
	std::transform(methods.begin(), methods.end(), names.begin(), [] (const ContactMethod & method) { return method.name(); });
	return names;
}
