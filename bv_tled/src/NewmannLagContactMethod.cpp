//
// Created by Zelim on 19/01/2016.
//

#include "NewmannLagContactMethod.h"

#include "CollisionSystem.h"
#include "CollisionMethodRayOctree.h"
#include "RayMeshContact.h"
#include "MeshContact.h"
#include "RigidBodyComponent.h"

#include "Assembly.h"
#include <UtilitiesColDet.h>
#include <Parallel.h>

#include "DebugMesh.h"

using namespace bv;
using namespace coldet;
using namespace tled;

ContactMethod NewmannLagContactMethod::create()
{
	auto action = [](ContactMethod::ContactActionData data, ContactMethod::ContactResolveData* resolved) -> ContactMethod::ResolveAction
	{
		auto& masterOctree = data.master->component<Collider>()->concept<Octree>()->shape();
		auto& slaveOctree = data.slave->component<Collider>()->concept<Octree>()->shape();

		auto masterMesh = masterOctree.mesh();
		auto slaveMesh = slaveOctree.mesh();

		// find the indices of slave nodes in contact
		auto& slaveNodeIndices = resolved->slaveIndices;

		// we also need to add the displaced nodes to the list as they are also likely to
		auto assembly = data.assembly;
		auto& loads = assembly->loads();
		for (int i = 0; i < loads.size(); ++i)
		{
			if(loads[i].active() && loads[i].shape() == LoadShape::FORCE)
			{
				slaveNodeIndices.push_back(i);
			}
		}

		const auto contact = data.contact;

		// add all the new nodes in contact
		if (contact && contact->isFound())
		{
			const auto& pairs = contact->pairs();

			// get the indices of faces in contact on the mesh belonging
			auto slaveFaceIndices = contact->uniqueIndicesFirst();

			auto contactNodes = MeshUtilities::vertIndices(slaveFaceIndices, contact->mesh1());

			// append contact nodes
			slaveNodeIndices.insert(slaveNodeIndices.end(), contactNodes.begin(), contactNodes.end());

			// make sure nodes are not repeated
			bv::filterUnique(slaveNodeIndices);
		}


		auto& slaveNodePositions = resolved->slavePositions = std::vector<glm::vec3>(slaveNodeIndices.size());

		for (int i = 0; i < slaveNodeIndices.size(); ++i)
		{
			// the position of the slave node
			const glm::vec3& nodePos = slaveMesh->positions[slaveNodeIndices[i]];
			slaveNodePositions[i] = Math::transform(nodePos, data.slave->transform());
		}

		auto& nodePenetrations = resolved->penetrations = std::vector<float>(slaveNodeIndices.size());
		resolved->planes.reserve(slaveNodeIndices.size());

		// collect the normals of the master surface
//		std::vector<glm::vec3> masterNormals = Utilities::collectNormals<ContactSide::Second>(contact);
//        const glm::vec3 localContactNormal = Math::average(masterNormals);
//        const glm::vec3 contactNormal = Math::transformNormal(localContactNormal, contact->masterTransform());

		auto tester = [&] (size_t i)
		{
			int nodeIndex = slaveNodeIndices[i];
			const glm::vec3& nodePos = slaveNodePositions[i];

			// contact normal needs to be transformed to global coords
			const glm::vec3 localContactNormal = slaveMesh->vertices[nodeIndex].normal();
			const glm::vec3 contactNormal = Math::transformNormal(localContactNormal, data.slave->transform());

			Ray ray(nodePos, -contactNormal);

			// build collision method
			CollisionMethodRayOctree method(ray, masterOctree);
			method.mat2(data.master->transform());

			// execute method to get contact
			auto result = method();
			auto rayContact = static_cast<RayMeshContact*>(result.get());

			// make sure contacts are sorted according to depth - ray t param
			if (rayContact->isFound())
			{
				rayContact->sort();

				// get all ray hits
				const auto& hits = rayContact->pairs();

				// get the very first hit along ray
				const float t = nodePenetrations[i] = hits[0].t;

				glm::vec3 masterNormal = -Math::transformNormal(masterMesh->normals[hits[0].idx], data.master->transform());

				// check if there was a penetration
				if(t < 0.0f)
				{
					return;
				}

				const glm::vec3 hit = ray.point(t);

//					debug.point(ray.point(0.0f));
//					debug.point(hit);
//					debug.render();

				float dot = glm::dot(masterNormal, glm::normalize(ray.dir()));

				// check that we hit a front facing face on master
				if(dot <= 0.3f )
				{
					return;
				}

				const glm::vec3 gap = hit - nodePos;
				const glm::vec3 oldU = data.assembly->U(nodeIndex);
				const glm::vec3 newU = gap + oldU;

				float dt = Timing::delta();
				float dtSqr = dt * dt;
				float mass = assembly->M(nodeIndex);
				resolved->planes[i] = glm::vec4(gap * mass / dtSqr, 0.0f);
			}
		};

		if (slaveNodeIndices.size())
		{
			// run it in parallel
			Parallel::foreach(slaveNodeIndices, tester);
		}

        auto onresove = [data](const ContactMethod::ContactResolveData* resolved)
        {
            const auto masterBody = data.body;
            // update surface mesh if it is dirty
            if(masterBody)
            {
                // apply all nodal forces from the surface to the master
				for (int i = 0; i < resolved->slaveIndices.size(); ++i)
                {
                    if(resolved->penetrations[i] > 0.0f)
                    {
                        const int nodeIndex = resolved->slaveIndices[i];

                        const auto& load = data.assembly->loads()[nodeIndex];

                        if(load.shape() != LoadShape::LINEAR && load.shape() != LoadShape::FIX)
                        {
                            //data.assembly->load(nodeIndex, forces[i]);
                        }

                        const glm::vec3 force = -glm::vec3(resolved->planes[i]);
//				const glm::vec3 normal = -contactNormals[i];
//				glm::vec3 force = normal * glm::max(0.0f, glm::dot(normal, f));
                       // masterBody->applyForceGlobal(force, slaveNodePositions[i]);
//					masterBody->applyForce(force);
                    }
                }
            }
        };

        return onresove;
	};

	return ContactMethod(ClassNamer::name<NewmannLagContactMethod>(), action);
}

