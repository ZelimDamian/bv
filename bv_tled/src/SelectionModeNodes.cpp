#include "SelectionModeNodes.h"
#include "MouseManager.h"
#include "KeyboardManager.h"
#include "SelectionManager.h"
#include "SceneManager.h"

#include "Entity.h"
#include "AssemblyComponent.h"
#include "Assembly.h"
#include "Intersection.h"
#include "FeaNode.h"

SelectionModeNodes::SelectionModeNodes()
{
}


SelectionModeNodes::~SelectionModeNodes()
{
}


void SelectionModeNodes::select(iSelection* selectionPtr, SelectionVisibiliityMode selectionMode)
{
	using namespace fea;

	Selection<Entity>* entitySelection = SelectionManager::instance()->entitySelection();
	Selection<FeaNode>* nodeSelection = SelectionManager::instance()->selection<FeaNode>();

	glm::vec3 boxsize = SceneManager::instance()->current()->boundingBox().size() * FeaNode::DRAW_SCALE;
	Box nodeBox(glm::vec3(), boxsize);

	// Make sure the right type of selection is being used
	if (!nodeSelection)
	{
		Log::error("Wrong selection type used! It wants Selection<FeaNode>.");
		return;
	}

	// make sure that we have at least one entity selected
	if (entitySelection->items().size() > 0)
	{
		EntityPtr entity = entitySelection->items()[0];

		AssemblyComponentPtr assemblyComponentPtr = entity->component<AssemblyComponent>();

		// make sure that we have an assembly component added to the entity
		if (assemblyComponentPtr)
		{
			AssemblyPtr assembly = assemblyComponentPtr->assembly();

			if (MouseManager::instance()->mouse()->button(MouseButton::RIGHT))
			{
				if (KeyboardManager::instance()->keyboard()->key(KeyCode::LALT))
				{
					nodeSelection->clear();
				}
				else
				{
					Ray ray = MouseManager::instance()->mouseRay();

					if (Intersection::found(entity->box(), ray))
					{
						FeaNodeCollection& nodes = assembly->nodes();

						for (size_t i = 0; i < nodes.size(); i++)
						{
							nodeBox.center(nodes[i]->posu());
							if (Intersection::found(nodeBox, ray))
							{
								nodeSelection->toggle(nodes[i]);
								// Return here if we only want to select one node
								// But it needs to be sorted
								//return;
							}
						}
					}
				}
			}
		}
	}
}