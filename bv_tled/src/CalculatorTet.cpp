#include "CalculatorTet.h"
#include "MatrixRoutines.h"

using namespace bv;
using namespace bv::tled;

void CalculatorTet::init(float dt, float dc)
{
	const ElementCollection& elements = m_assembly->elements();

	size_t elNum = elements.size();

	// Detrminant of the Jacobian matrix
	m_detJ = BufferFloat(elNum);

	m_V = BufferFloat(elNum);

	// Shape function global derivatives elnum * [4][3]
	m_DhDx = BufferFloat(elNum * 4 * 3);

	// Deformation gradient elnum * [3][3]
	m_X = BufferFloat(elNum * 3 * 3);

	// Element node indices [elnum * 4]
	m_nodeInds = BufferInt(elNum * 4);
	int nodeCount = 0;

	for (size_t i = 0; i < elNum; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			const int& nodeIndex = elements[i]->nodeIndex(j);
			m_nodeInds[nodeCount++] = nodeIndex;
		}
	}

	// Mass data pointer
	BufferFloat& M = m_assembly->M();
	float *pM = M.data();

	// Pre-Calculate the element deformation derivatives
	for (size_t i = 0; i < elNum; i++)
	{
		// make sure we are dealing with a tetrahedral element
		if (ElementTet* el = (ElementTet*)elements[i].get())
		{
			// use visitor pattern for double dispatch
			calculateDhDx(el);
			calculateMass(el, pM);
		}
	}

	auto& U = m_assembly->U();
	auto& F = m_assembly->F();

	m_stepper = new TimeStepper(U, F, M, dt, dc);

}

void CalculatorTet::step(float time, float totalTime)
{
	// Retreive forces, nodal displacements, and nodes
	auto& F = m_assembly->F();
	auto& U = m_assembly->U();

	// reset forces to zero
	F.set(glm::vec4());

	ElementCollection& elements = m_assembly->elements();

	int elmCount = (int)elements.size();

	// Calculate the element forces
	for (size_t i = 0; i < elmCount; i++)
	{
		// make sure we are dealing with a tetrahedral element
		if (ElementTet* el = (ElementTet*)elements[i].get())
		{
			calculateForces(el, U.data(), F.reinterpret<float>());
		}
	}

	size_t nodeCount = U.size();

	// Retreive external forces
	auto& R = m_assembly->R();

	// subtract all external forces from m_F
	for (int i = 0; i < nodeCount; i++)
	{
		F.at(i) = R.at(i) - F.at(i);
	}

	// perform Time-Step update
	m_stepper->update();

	applyConstraints(time / totalTime);

	// ROTATE BUFFERS
	m_stepper->swap();
}

void CalculatorTet::calculateDhDx(ElementTet* elm)
{
	// Shape function natural derivatives
	const float dhdr[] =
	{
		-1, -1, -1,
	     1, 0, 0,
	     0, 1, 0,
	     0, 0, 1
	};

	// natural derivatives
	ArrayFloat<3, 4> DhDr(dhdr);

	// nodal positions
	ArrayFloat<3, 4>& x = elm->positions(m_assembly->X());

	// Jacobian
	ArrayFloat<3, 3> J;

	MatrixRoutines::multATB(DhDr, x, J);

	// Compute element volume - note alternative, but equivalent formula
	float detJ = MatrixRoutines::det3(J.data());
	float volume = fabs(detJ / 6);

	m_V[elm->index()] = volume;

	elm->volume(volume);

	ArrayFloat<3, 3> invJ;
	MatrixRoutines::inv3x3(J, invJ);

	ArrayFloat<3, 4> DhDx;
	MatrixRoutines::multABT(DhDr, invJ, DhDx);

	ArrayFloat<3, 4>::embed(DhDx, m_DhDx, elm->index() * DhDx.size());
//	m_DhDx.embed(elm->index() * DhDx.size(), DhDx);
}

void CalculatorTet::calculateForces(const ElementTet* elm, const glm::vec4* U, float* F)
{
	int index = elm->index();

	ArrayFloat<3, 4> u;

	for (int i = 0; i < 4; i++)
	{
		int j = m_nodeInds.at(index * 4 + i);

		u(i, 0) = U[j][0];
		u(i, 1) = U[j][1];
		u(i, 2) = U[j][2];
	}
	// Displacement derivs
	ArrayFloat<3, 4> DhDx;

//	m_DhDx.arrayCopy(index * DhDx.size(), DhDx);

	ArrayFloat<3, 3> X;
	MatrixRoutines::multATB(u, DhDx, X);

	// Deformation gradient: X = DuDx + I
	X.at2(0, 0) += 1; X.at2(1, 1) += 1; X.at2(2, 2) += 1;	// X is now def. grad.

	// 2nd Piola-Kirchhoff stress
	ArrayFloat<3, 3> SPK;

	// TODO:: Un-Hard-Code Neo-Hookean model
	const glm::vec3* pX = X.reinterpret<glm::vec3>();
	glm::vec3* pSPK = SPK.reinterpret<glm::vec3>();

	const auto& mat = elm->material()->params();
	calculateStressNH(pX, mat, pSPK);

//	float detJ = m_detJ.at(elm->index());

	ArrayFloat<3, 3> temp;
	MatrixRoutines::multAB(X, SPK, temp);
	MatrixRoutines::mult(temp, m_V[elm->index()], temp);

	// element forces
	ArrayFloat<4, 3> Fe;
	MatrixRoutines::multABT(temp, DhDx, Fe);

	// Add element forces to global forces
	for (int i = 0; i < 4; i++)
	{
		int j = m_nodeInds.at(index * 4 + i) * 3;
		F[j + 0] += Fe(0, i);
		F[j + 1] += Fe(1, i);
		F[j + 2] += Fe(2, i);
	}
}

void CalculatorTet::calculateMass(const ElementTet* elm, float *M) const
{
	float Mass = elm->mass() / 4;

	for (int i = 0; i < 4; i++)
	{
		int j = elm->nodeIndex(i);

		M[j] += Mass;
	}
}

void CalculatorTet::calculateStressNH(const glm::vec3 *X, const std::vector<float> &materialParams, glm::vec3 *SPK) const
{

	float XT11, XT12, XT13, XT21, XT22, XT23, XT31, XT32, XT33;
	float C11, C12, C13, C22, C23, C33;

	// Transpose of deformation gradient
	XT11 = X[0][0]; XT12 = X[1][0]; XT13 = X[2][0];
	XT21 = X[0][1]; XT22 = X[1][1]; XT23 = X[2][1];
	XT31 = X[0][2]; XT32 = X[1][2]; XT33 = X[2][2];

	// Right Cauchy-Green deformation tensor
	C11 = XT11*XT11 + XT12*XT12 + XT13*XT13;
	C12 = XT11*XT21 + XT12*XT22 + XT13*XT23;
	C13 = XT11*XT31 + XT12*XT32 + XT13*XT33;
	C22 = XT21*XT21 + XT22*XT22 + XT23*XT23;
	C23 = XT21*XT31 + XT22*XT32 + XT23*XT33;
	C33 = XT31*XT31 + XT32*XT32 + XT33*XT33;

	// Determinant of deformation gradient
	float J = XT11*(XT22*XT33 - XT32*XT23) + XT12*(XT23*XT31 - XT21*XT33) + XT13*(XT21*XT32 - XT22*XT31);
	// Determinant of C
	float detC = C11*(C22*C33 - C23*C23) - C22*C13*C13 - C12*(C33*C12 - 2 * C13*C23);

	float M = materialParams[1];
	float K = materialParams[2];

	float x1 = (float)pow((double)J, -2.0 / 3.0) * M;
	float x2 = (K*J*(J - 1) - x1*(C11 + C22 + C33) / 3) / detC;

	// 2nd Piola-Kirchhoff stress
	SPK[0][0] = (C22*C33 - C23*C23)*x2 + x1; // S00
	SPK[1][1] = (C11*C33 - C13*C13)*x2 + x1; // S11
	SPK[2][2] = (C11*C22 - C12*C12)*x2 + x1; // S22
	SPK[0][1] = (C13*C23 - C12*C33)*x2;      // S01
	SPK[1][0] = SPK[0][1];                   //
	SPK[1][2] = (C12*C13 - C23*C11)*x2;      // S12
	SPK[2][1] = SPK[1][2];                   //
	SPK[0][2] = (C12*C23 - C13*C22)*x2;      // S02
	SPK[2][0] = SPK[0][2];                   //
}

