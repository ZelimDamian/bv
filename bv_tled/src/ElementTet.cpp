#include "ElementTet.h"

using namespace bv;
using namespace bv::tled;

ElementTet::ElementTet(int index) : Element(index)
{
	m_nodeIndices.resize(4);
	m_nodes.resize(4);
}

ElementTet::~ElementTet()
{
}

float ElementTet::mass() const
{
	return m_material->density() * volume();
}

ArrayFloat<3, 4>& ElementTet::positions(const BufferVec4& X)
{
	if (m_updatePositions)
	{
		for (size_t i = 0; i < m_nodeIndices.size(); i++)
		{
			m_x.at(i * 3 + 0) = X[m_nodeIndices[i]].x;
			m_x.at(i * 3 + 1) = X[m_nodeIndices[i]].y;
			m_x.at(i * 3 + 2) = X[m_nodeIndices[i]].z;
		}

		m_updatePositions = false;
	}
	return m_x;
}

// Node positions of the element O(8, 3)
ArrayFloat<3, 4> ElementTet::posus()
{
	ArrayFloat<3, 4> posus;

	for (size_t i = 0; i < 4; i++)
	{
		const glm::vec3& pos = m_nodes[i]->posu();

		posus.at(i * 3 + 0) = pos[0];
		posus.at(i * 3 + 1) = pos[1];
		posus.at(i * 3 + 2) = pos[2];
	}

	return posus;
}

