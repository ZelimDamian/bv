#include "FileDescriptionInp.h"

using namespace bv;
using namespace fem;

using Tokens = FileDescriptionInp::Tokens;
using Templates = FileDescriptionInp::Templates;

const char* Tokens::PART = "*PART";
const char* Tokens::NODE = "*NODE";
const char* Tokens::ELEMENT = "*ELEMENT";
const char* Tokens::SOLID_SECTION = "*SOLID SECTION";
const char* Tokens::END_PART = "*END PART";
const char* Tokens::ASSEMBLY = "*ASSEMBLY";
const char* Tokens::END_ASSEMBLY = "*END ASSEMBLY";
const char* Tokens::INSTANCE = "*INSTANCE";
const char* Tokens::NSET = "*NSET";
const char* Tokens::MATERIAL = "*MATERIAL";
const char* Tokens::ELASTIC = "*ELASTIC";
const char* Tokens::HYPERELASTIC = "*HYPERELASTIC";
const char* Tokens::DENSITY = "*DENSITY";
const char* Tokens::BOUNDARY = "*BOUNDARY";
const char* Tokens::STEP = "*STEP";
const char* Tokens::CLOAD = "*CLOAD";
const char* Tokens::END_STEP = "*END STEP";

const char* Templates::PART = "*PART, NAME = %s";
const char* Templates::NODE = "*NODE, NSET = %s_NSET";
const char* Templates::ELEMENT = "*ELEMENT, ELSET = %s, TYPE = %s";
const char* Templates::SOLID_SECTION = "*SOLID SECTION, ELSET = %s, MATERIAL = %s";
const char* Templates::END_PART = "*END PART";
const char* Templates::ASSEMBLY = "*ASSEMBLY, NAME=ASSEMBLY";
const char* Templates::END_ASSEMBLY = "*END ASSEMBLY";
const char* Templates::INSTANCE = "*INSTANCE, NAME=%s-INST, PART=%s \n*END INSTANCE";
const char* Templates::NSET = "*NSET, NSET = %s, INSTANCE = %s-INST";
const char* Templates::MATERIAL = "*MATERIAL, NAME = %s";
const char* Templates::ELASTIC = "*ELASTIC\n%f, %f";
const char* Templates::HYPERELASTIC = "*HYPERELASTIC, %s\n%f, %f";
const char* Templates::DENSITY = "*DENSITY\n%f";
const char* Templates::BOUNDARY = "*BOUNDARY \n%s, ENCASTRE";
const char* Templates::STEP = "*STEP, NAME=STEP-1 \n*DYNAMIC, EXPLICIT\n,%f";
const char* Templates::CLOAD = "*CLOAD";
const char* Templates::END_STEP = "*END STEP";
