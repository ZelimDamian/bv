#include "ContactWithSphereComponent.h"
#include "Entity.h"

#include "FeaNode.h"

#include "Entity.h"

using namespace fea;

ContactWithSphereComponent::ContactWithSphereComponent()
{
}


ContactWithSphereComponent::~ContactWithSphereComponent()
{
}

void ContactWithSphereComponent::initialize()
{
	m_assembly = this->entity()->require<fea::AssemblyComponent>()->assembly();
}

void ContactWithSphereComponent::update()
{
	if (m_sphereCollider)
	{
		const Sphere* sphere = (Sphere*)TO_RAW_PTR(m_sphereCollider->shape());

		glm::vec3 sphereCenter = glm::vec3(m_sphereCollider->entity()->matrix() * glm::vec4(sphere->pos(), 1.0f));

		FeaNodeCollection& nodes = m_assembly->nodes();

		for (size_t i = 0; i < nodes.size(); i++)
		{

			FeaNodePtr node = nodes[i];

			glm::vec3 point = node->posu();

			glm::vec3 translation = point - sphereCenter;

			float distance = glm::length(translation);

			if (distance < sphere->radius())
			{
				translation = -glm::normalize(translation) * (distance - sphere->radius());

				// The displacement load should be calculated differently
				const glm::vec3& referenceConfiguration = node->pos();
				glm::vec3 displacement = point + translation - referenceConfiguration;

				m_assembly->displace(node->index(), displacement);

				//selection->items().at(i)->move(translation);
			}
		}
	}
}

void ContactWithSphereComponent::collided(ColliderPtr other, const ContactCollection& contacts)
{
	m_sphereCollider = DYN_CAST(ColliderSphere)(other);
}