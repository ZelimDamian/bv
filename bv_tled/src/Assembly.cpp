#include "Assembly.h"
#include "ElementHex.h"
#include "ElementTet.h"

#include "ForceLoad.h"
#include "DispLoad.h"

using namespace bv;
using namespace bv::tled;

Assembly::Assembly(const VectorVec3& X)
{
    m_X.resize(X.size());

    for (int i = 0; i < X.size(); ++i)
    {
        m_X[i] = glm::vec4(X[i], 1.0f);
    }

	allocate();
}

void Assembly::fix(int nodeIndex)
{
	m_loads[nodeIndex] = Load(glm::vec4(0.0f), LoadShape::FIX);
}

ElementPtr Assembly::add(ElementPtr element)
{
	m_elements.push_back(element);
	return element;
}

ElementPtr Assembly::addHex(const int ids[8])
{
	return add(ids[0], ids[1], ids[2], ids[3], ids[4], ids[5], ids[6], ids[7]);
}

ElementPtr Assembly::add(const tethex::Hex &hex)
{
	return addHex(hex.indices);
}

// Create a Hexahedral 8 node element with indices
ElementPtr Assembly::add(int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7)
{
	ElementHexPtr elm = (ElementHexPtr)(new ElementHex(m_elements.size()));

	elm->nodeIndices()[0] = i0;
	elm->nodeIndices()[1] = i1;
	elm->nodeIndices()[2] = i2;
	elm->nodeIndices()[3] = i3;
	elm->nodeIndices()[4] = i4;
	elm->nodeIndices()[5] = i5;
	elm->nodeIndices()[6] = i6;
	elm->nodeIndices()[7] = i7;

	add(elm);

	return elm;
}

ElementPtr Assembly::add(const tethex::Tet &tet)
{
	return addTet(tet.indices);
}

// Create a tetrahedral 4 node element with indicies
ElementPtr Assembly::addTet(const int ids[4])
{
	return add(ids[0], ids[1], ids[2], ids[3]);
}

ElementPtr Assembly::add(int i1, int i2, int i3, int i4)
{
	ElementTetPtr elm = ElementTetPtr(new ElementTet(m_elements.size()));

	elm->nodeIndices()[0] = (i1);
	elm->nodeIndices()[1] = (i2);
	elm->nodeIndices()[2] = (i3);
	elm->nodeIndices()[3] = (i4);

	add(elm);

	return elm;
}

// Apply a natrual boundary condition
void Assembly::load(int index, const glm::vec3& force)
{
	Load load(force, LoadShape::FORCE);
	m_loads[index] = load;
	m_R[index] = glm::vec4(force, 1.0f);
}

void Assembly::spring(int index, const glm::vec3& end, float k)
{
	Load load(glm::vec4(end, k), LoadShape::SPRING);
	m_loads[index] = load;
}

void Assembly::displace(int index, const glm::vec3& displacement, LoadShape shape)
{
	const Load load(displacement, shape);
	m_loads[index] = load;
}

void Assembly::project(int index, const glm::vec4 &plane)
{
	const Load load(plane, LoadShape::PLANE);
	m_loads[index] = load;
}

bool Assembly::has(ElementPtr element) const
{
	for (size_t i = 0; i < m_elements.size(); i++)
	{
		if (m_elements[i] == element)
		{
			return true;
		}
	}

	return false;
}

bool Assembly::has(const Node& node) const
{
	return std::find_if(m_nodes.begin(), m_nodes.end(), [&](const Node& existing) { return node.pos() == node.pos(); }) != m_nodes.end();
}

void Assembly::hitLoads()
{
	for (int i = 0; i < m_loads.size(); ++i)
	{
		m_loads[i].hit();
	}

	//std::sort(m_loads.begin(), m_loads.end(), [this](const Load& load) { return load.active(); });
}

size_t Assembly::numActiveLoads()
{
	return std::count_if(m_loads.begin(), m_loads.end(), [] (const Load& load) { return load.active(); });
}

void Assembly::reset()
{
	for (auto &&m_load : m_loads)
	{
		if(!m_load.isPermanent())
		{
			m_load.kill();
		}
	}
}

void Assembly::allocate()
{
	size_t num = m_X.size();

	m_U.resize(num);
	m_R.resize(num);
	m_M.resize(num);
    m_VMSN.resize(num);
	m_F.resize(num);

	m_loads.resize(num);

	for (int i = 0; i < num; i++)
	{
		Node node(&m_X.at(i));

		node.index(i);
		node.u(&m_U.at(i));
		node.force(&m_F.at(i));

		m_nodes.push_back(node);
	}
}

// node positions in current configation (X + U)
VectorVec3 Assembly::posus()
{
	VectorVec3 posus(m_X.size());

	for (int i = 0; i < m_X.size(); ++i)
	{
		posus[i] = glm::vec3(m_X[i] + m_U[i]);
	}

	return posus;
}

std::vector<int> Assembly::constNodeIndices() const
{
	std::vector<int> inds;

	for (int i = 0; i < m_loads.size(); ++i)
	{
		if(m_loads[i].active())
		{
			inds.push_back(i);
		}
	}

	filterUnique(inds);

	return inds;
}

float Assembly::smallestExtent() const
{
	float smallest = 99999999.0f;

    // array of constrainted indices
	auto constInd = constNodeIndices();

	for (int i = 0; i < m_elements.size(); ++i)
	{
		const auto& element = m_elements[i];

        // whether all nodes of the element are constraited
		bool allFixed = true;

        // check if any of the nodes in the element are not in the constrainted list
		for (int j = 0; j < element->nodeIndices().size(); ++j)
		{
			int nodeIndex = element->nodeIndices()[j];
			if(std::find(constInd.begin(), constInd.end(), nodeIndex) == constInd.end())
			{
				allFixed = false;
			}
		}

		if(!allFixed)
		{
			for (int i = 0; i < element->nodeIndices().size(); ++i)
			{
				int node1 = element->nodeIndices()[i];
				for (int j = i + 1; j < element->nodeIndices().size(); ++j)
				{
					int node2 = element->nodeIndices()[j];
					float d = glm::length(m_X[node1] - m_X[node2]);
					if (smallest > d)
					{
						smallest = d;
					}
				}
			}
		}
	}

	return smallest;
}

void Assembly::updateVMSN()
{
	m_VMS.resize(m_elements.size());

	m_VMSN.set(0.0f);

	// collect VM stress for each node
	for (int i = 0; i < m_elements.size(); ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			int nodeIndex = m_elements[i]->nodeIndices()[j];

			m_VMSN[nodeIndex] += m_VMS[i];
		}
	}

	// average based on the number of connected elements
	for (int i = 0; i < m_nodes.size(); ++i)
	{
		m_VMSN[i] /= m_numForces[i];
	}

}

float Assembly::maxStep()
{
//	return 0.0005f;

	float smallestLength = smallestExtent();

	// density
	float ro = m_elements[0]->material()->params(0);
	// bulk modulus
	float K = m_elements[0]->material()->params(2);
	// shear modulus
	float G = m_elements[0]->material()->params(1);
	// Lame's first paramter
	float lambda = K - 2.0f*G/3.0f;

//	// lame parameters
//	float lambda = (3.0f*K - M) / 2.0f;
//	float mu = (3.0f*M - K) / 4.0f;
//
//	// Poisson's coefficient
//	float nu = lambda / (2.0f*(lambda + mu));
//	// Elastic modulus
//	float E = mu*(3.0f*lambda + 2.0f*mu) / (lambda + mu);

	// wave speed : http://www.engmech.cz/2012/proceedings/pdf/292_Plesek_J-FT.pdf
	float c = (float) sqrt((lambda + 2*G) / ro);

	float timeStepFactor = 1.0f;

	// max unconditionally stable time-step (Debunne et al 2001)
	float timeStep = timeStepFactor*smallestLength / c;

	// Abaqus Explicit scales it down
	timeStep *= 1/sqrt(3.0f);

	// TODO: had to decrease even further
	timeStep /= 4.0f;

	m_cachedStep = timeStep;

	return timeStep;
}

std::vector<int> Assembly::materialInds()
{
	std::vector<int> inds(m_elements.size());

	for (int i = 0; i < m_elements.size(); ++i)
	{
		inds[i] = (int) m_elements[i]->material()->id();
	}

	return inds;
}


