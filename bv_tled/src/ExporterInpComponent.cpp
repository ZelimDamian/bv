#include "ExporterInpComponent.h"

#include "AssemblyComponent.h"
#include "ExporterInp.h"

using namespace bv;
using namespace fem;
using namespace tled;

void ExporterInpComponent::run()
{
	auto assemblyComponent = AssemblyComponent::find(m_entity);

	if(!assemblyComponent)
	{
	Log::error("Missing " + AssemblyComponent::title() + " in " + title());
	}

	auto& assembly = assemblyComponent->assembly();

	fem::ExporterInp::write(m_entity->name(), assembly);
}

void ExporterInpComponent::attachGUI()
{
	auto panel = this->gui();

	auto exportAction = [this]
	{
		this->run();
	};

	panel->button("Export", exportAction);
}
