#include "Element.h"

using namespace bv::tled;

Element::Element(int index)
{
	m_index = index;
}

//glm::vec3 Element::nodePos(int index)
//{
//	assert(index >= 0 && index < m_nodes.size());
//
//	return m_nodes[index]->pos();
//}

glm::vec3 Element::nodePosu(int index)
{
	assert(index >= 0 && index < m_nodes.size());

	return m_nodes[index]->posu();
}

bool Element::has(Node* node)
{
	for (size_t i = 0; i < m_nodes.size(); i++)
	{
		if (m_nodes[i] == node)
		{
			return true;
		}
	}

	return false;
}


float Element::smallestExtent()
{
	float smallest = 99999999.0f;

	for (int i = 0; i < m_nodes.size(); ++i)
	{
		for (int j = i + 1; j < m_nodes.size(); ++j)
		{
			float d = glm::length(m_nodes[i]->pos() - m_nodes[j]->pos());
			if(smallest > d)
				smallest = d;
		}
	}

	return smallest;
}
