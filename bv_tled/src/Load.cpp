#include "Load.h"

using namespace bv::tled;

uint16_t Load::MAX_LOAD_HITS = 6;

Load::Load(glm::vec4 value, LoadShape shape): m_value(value), m_shape(shape), m_active(true)
{
	if(shape == LoadShape::LINEAR_INSTANT || shape == LoadShape::PLANE)
	{
		m_maxHits = MAX_LOAD_HITS;
	} else if(shape == LoadShape::FORCE) {
		m_maxHits = 3;
	}
}

glm::vec4 Load::evaluate(float time) const
{
	switch (m_shape) {
	case LoadShape::INSTANT: return m_value;
	case LoadShape::LINEAR: return time * m_value;
	case LoadShape::LINEAR_INSTANT: return time * m_value;
	default: return m_value;
	}
}

void Load::hit()
{
	if (m_active && m_maxHits >= 0)
	{
		m_hits++;
		if (m_hits > m_maxHits - 1)
		{
			kill();
		}
	}
}

void Load::kill()
{
	deactivate();
	m_shape = LoadShape::NONE;
	m_value = glm::vec4(0.0f);
}