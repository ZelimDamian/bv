#include "ShowCenterComponent.h"
#include "../BirthView/ColliderSphere.h"

#include "RenderingManager.h"

ShowCenterComponent::ShowCenterComponent()
{
}


ShowCenterComponent::~ShowCenterComponent()
{
}

void ShowCenterComponent::render()
{
	if (ColliderSpherePtr sphereCollider = this->entity()->component<ColliderSphere>())
	{
		const Sphere* sphere = (Sphere*)TO_RAW_PTR(sphereCollider->shape());
		glm::vec3 sphereCenter = glm::vec3(sphereCollider->entity()->matrix() * glm::vec4(sphere->pos(), 1.0f));

		RenderingManager::instance()->renderer()->render(glm::vec3(), sphereCenter, glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
	}
	RenderingManager::instance()->renderer()->render(glm::vec3(), this->entity()->pos(), glm::vec4(1.0f));
}