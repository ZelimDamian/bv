#include "DirectPDNContactMethod.h"

#include "CollisionSystem.h"
#include "CollisionMethodRayOctree.h"
#include "RayMeshContact.h"
#include "MeshContact.h"
#include "RigidBodyComponent.h"
#include "Assembly.h"

#include "UtilitiesColDet.h"
#include "DebugMesh.h"

#include "Profiler.h"
#include <CalculatorComponent.h>
#include <Parallel.h>


using namespace bv;
using namespace coldet;
using namespace tled;

static LoadShape SHAPE = LoadShape::PLANE;

ContactMethod DirectPDNContactMethod::create()
{
	auto action = [](ContactMethod::ContactActionData data, ContactMethod::ContactResolveData *resolved) -> ContactMethod::ResolveAction
	{
		auto& masterOctree = data.master->component<Collider>()->concept<Octree>()->shape();
		auto& slaveOctree = data.slave->component<Collider>()->concept<Octree>()->shape();

		auto masterMesh = masterOctree.mesh();
		auto slaveMesh = slaveOctree.mesh();

		// find the indices of slave nodes in contact
		std::vector<int>& slaveNodeIndices = resolved->slaveIndices;

		// we also need to add the displaced nodes to the list as they are also likely to
		const auto& loads = data.assembly->loads();
		for (int i = 0; i < loads.size(); ++i)
		{
			if(loads[i].active() && loads[i].shape() == SHAPE)
			{
				slaveNodeIndices.push_back(i);
			}
		}

		const auto contact = data.contact;

		// add all the new nodes in contact
		if (contact && contact->isFound())
		{
			const auto& pairs = contact->pairs();

			// get the indices of faces in contact on the mesh belonging
			auto slaveFaceIndices = contact->uniqueIndicesFirst();

            auto contactNodes = MeshUtilities::vertIndices(slaveFaceIndices, contact->mesh1());

			// append contact nodes
			slaveNodeIndices.insert(slaveNodeIndices.end(), contactNodes.begin(), contactNodes.end());

			// make sure nodes are not repeated
			bv::filterUnique(slaveNodeIndices);
		}


		auto& slaveNodePositions = resolved->slavePositions = std::vector<glm::vec3>(slaveNodeIndices.size());

		for (int i = 0; i < slaveNodeIndices.size(); ++i)
		{
			// the position of the slave node
			const glm::vec3& nodePos = slaveMesh->positions[slaveNodeIndices[i]];
			slaveNodePositions[i] = Math::transform(nodePos, data.slave->transform());
		}

		auto& nodePenetrations = resolved->penetrations = std::vector<float>(slaveNodeIndices.size());
		resolved->planes = std::vector<glm::vec4>(slaveNodeIndices.size());

        auto tester = [&] (size_t i)
        {
			int nodeIndex = slaveNodeIndices[i];
			const glm::vec3& nodePos = slaveNodePositions[i];

			// contact normal needs to be transformed to global coords
			const glm::vec3 localContactNormal = -slaveMesh->vertices[nodeIndex].normal();
			const glm::vec3 contactNormal = Math::transformNormal(localContactNormal, data.slave->transform());

			const float margin = 0.01f; // roll before take-off for the ray to shoot from behind the node
			Ray ray(nodePos + margin * contactNormal, -contactNormal);

			// build collision method
			CollisionMethodRayOctree method(ray, masterOctree);
			method.mat2(data.master->transform());

			// execute method to get contact
			auto result = method();
			auto rayContact = static_cast<RayMeshContact*>(result.get());

			// make sure contacts are sorted according to depth - ray t param
			if(rayContact->isFound())
			{
				rayContact->sort();

				// get all ray hits
				const auto& hits = rayContact->pairs();

				// get the very first hit along ray
				const float t = nodePenetrations[i] = hits[0].t;

				// check if there was a penetration
				if(t - margin > margin)
				{
					return;
				}

				glm::vec3 masterNormal = Math::transformNormal(masterMesh->normals[hits[0].idx], data.master->transform());
				float dot = glm::dot(masterNormal, glm::normalize(ray.dir()));

				// check that we hit a front facing face on master
				if(dot <= 0.5f)
				{
					return;
				}

				const glm::vec3 hit = ray.point(t);
				const glm::vec3 gap = hit - nodePos;
				const glm::vec3 oldU = data.assembly->U(nodeIndex);
				const glm::vec3 newU = gap + oldU;

				const float d = glm::dot(contactNormal, newU);
				const glm::vec4 plane(contactNormal, d);

                resolved->planes[i] = plane;
            }
        };

		// run it in parallel
		if (slaveNodeIndices.size())
		{
            auto tock = Profiler::instance()->tick("DirectPDN slave node testing");

			Parallel::foreach(slaveNodeIndices, tester);
			//        tester(slaveNodeIndices.begin(), slaveNodeIndices.end());

            tock();
		}

        auto onresolve = [data] (const ContactMethod::ContactResolveData* resolved)
        {
            auto masterBody = data.body;
//                // update surface mesh if it is dirty
            if (masterBody)
            {
//              auto calculatorComponent = data.slave->component<CalculatorComponent>();
//              auto& calculator = calculatorComponent->calculator();

                // apply all nodal forces from the surface to the master
                for (int i = 0; i < resolved->slaveIndices.size(); ++i)
                {
                    const int nodeIndex = resolved->slaveIndices[i];

                    // process slave
	                const auto plane = resolved->planes[i];

                    if(glm::length2(plane) > 0)
                    {
                        const auto& load = data.assembly->loads()[nodeIndex];

                        if(!load.active() || load.shape() == SHAPE)
                        {
                            data.assembly->project(nodeIndex, plane);
                        }

//	                    {
//		                    auto x = resolved->slavePositions[i]; // node position
//		                    auto U = x - data.assembly->X(nodeIndex); // displacement
//		                    auto dx = glm::dot(glm::vec3(plane), U); // d param for node
//		                    auto g = plane.w - dx; // penetration
//
//		                    if(g > 0.0f)
//		                    {
//			                    auto proj = x + g * glm::vec3(plane); // projected point
//			                    // debug draw
//			                    auto debug = DebugMesh::instance();
//			                    debug->point(x);
//			                    debug->point(proj);
//		                    }
//	                    }

                        // process master
						const glm::vec3 f (-data.assembly->F(nodeIndex));

						if(!glm::any(glm::isnan(f)))
						{
							const glm::vec3 normal (-resolved->planes[i]);
							glm::vec3 force = normal * glm::max(0.0f, glm::dot(normal, f));
							masterBody->applyForceGlobal(force, resolved->slavePositions[i]);
						}
                    }
                }

	            DebugMesh::instance()->render();
            }
        };

        return onresolve;
	};

	return ContactMethod(ClassNamer::name<DirectPDNContactMethod>(), action);
}
