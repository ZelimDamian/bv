#include "ElementHex.h"

#include "CalculatorHex.h"

using namespace bv;
using namespace bv::tled;

ElementHex::ElementHex(size_t index) : Element(index)
{
	m_nodeIndices.resize(8);
	m_nodes.resize(8);
}

// Node positions of the element O(8, 3)
ArrayFloat<3, 8>& ElementHex::positions(const BufferVec4& X)
{
	if (m_updatePositions)
	{
		for (size_t i = 0; i < m_nodeIndices.size(); i++)
		{
			m_x.at(i * 3 + 0) = X[m_nodeIndices[i]].x;
			m_x.at(i * 3 + 1) = X[m_nodeIndices[i]].y;
			m_x.at(i * 3 + 2) = X[m_nodeIndices[i]].z;
		}

		m_updatePositions = false;
	}
	return m_x;
}


// Node positions of the element O(8, 3)
ArrayFloat<3, 8> ElementHex::posus()
{
	ArrayFloat<3, 8> posus;

	const size_t size = m_nodes.size();

	for (size_t i = 0; i < size; i++)
	{
		const glm::vec3& pos = m_nodes[i]->posu();

		posus.at(i * 3 + 0) = pos[0];
		posus.at(i * 3 + 1) = pos[1];
		posus.at(i * 3 + 2) = pos[2];
	}

	return posus;
}
