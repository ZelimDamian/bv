#include "AbqsRprtLoader.h"

#include <FileReader.h>

using namespace bv;
using namespace tled;

VectorVec3 AbqsRprtLoader::load(const std::string &name, const std::string ext)
{
	VectorVec3 U;

	const std::string filename = "abaqus/" + name + ext;
	const std::string raw = FileReader::read(filename);
	const std::string contents = StringUtils::reduce(raw);

	char *lineCharPtr = c_strtok(const_cast<char*>(contents.c_str()), "\n");

	while (lineCharPtr != NULL)
	{
		const char firstChar = lineCharPtr[0];

		switch (firstChar)
		{
			case ' ':
			{
				glm::vec3 vec;
				int index = -1;
				c_scanf(lineCharPtr, " %d %f %f %f", &index, &vec.x, &vec.y, &vec.z);
				if(index >= 0)
				{
					U.push_back(vec);
				}

				break;
			}
			default: break;
		}

		lineCharPtr = c_strtok(NULL, "\n");
	}

	return U;
}
