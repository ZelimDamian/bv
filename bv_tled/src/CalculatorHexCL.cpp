#include "CalculatorHexCL.h"
#include "TimeStepperCL.h"

using namespace bv;
using namespace tled;
using namespace tethex;
using namespace gpu;
using namespace cl;

void CalculatorHexCL::init(float dt, float damping)
{
	CalculatorHex::init(dt, damping);

	//kickstart GPU
	GpuSystem::instance()->load();

	// Buffer for Calculated values
	m_V_CL = BufferCL<float>::create(BufferFormat::R, m_detJ);

	// Buffer for Shape function global derivatives elnum * [8][3]
	m_DhDx_CL = BufferCL<float>::create(BufferFormat::R, m_DhDx);

	// Buffer for Element node indices [elnum * 8]
	m_nodeInds_CL = BufferCL<int>::create(BufferFormat::R, m_nodeInds);

	// material properties
	m_mat_CL = BufferCL<float>::create(BufferFormat::R, BufferFloat(m_assembly->elements()[0]->material()->params()));

	auto& U = m_assembly->U();
	auto& F = m_assembly->F();

	// Node deformations
	m_U_CL = BufferCL<glm::vec4>::create(BufferFormat::R, U.size());

    const size_t elCount = m_assembly->elements().size();
    const size_t nodeCount = m_assembly->nodes().size();

	// Nodal Forces
	// A separate force for value for each element node
	m_Fx_CL = BufferCL<glm::vec4>::create(BufferFormat::W, U.size() * 8);
	m_F_CL = BufferCL<glm::vec4>::create(BufferFormat::RW, U.size());

    m_R_CL = BufferCL<glm::vec4>::create(BufferFormat::R, U.size());
    m_R_CL->upload(glm::vec4());

	// Load our kernel
	m_tled_kernel = Kernel::require("tled_hex");
    m_constraints_kernel = Kernel::require("apply_constraints");
	m_collect_kernel = Kernel::require("collect_force_hex");

	//__kernel void tled_hex(
	//	__global float* m_detJ,
	//	__global float* m_DhDx,
	//	__global float* U,
	//	__global float* F,
	//	__global int* m_nodeIndices,
	//  __global float* m_mat);
	m_tled_kernel->set(0, m_V_CL);
	m_tled_kernel->set(1, m_DhDx_CL);
	m_tled_kernel->set(2, m_U_CL);
	m_tled_kernel->set(3, m_Fx_CL);
	m_tled_kernel->set(4, m_nodeInds_CL);
	m_tled_kernel->set(5, m_mat_CL);


	BufferFloat& M = m_assembly->M();
    auto timeStepper = new TimeStepperCL(U, F, M, dt, damping,
		m_F_CL, m_Fx_CL, m_U_CL, m_R_CL, m_nodeInds_CL, m_nodeInds_CL, 8);
    m_stepper = timeStepper;

    // setup the constraint application
    {
        auto m_U_new_CL = timeStepper->U_new_CL();

        m_nodeConstraintFlags_CL =
                BufferCL<int>::create(BufferFormat::R, m_U_new_CL->data().size());
        m_nodeConstraintFlags_CL->upload(0);

        m_constraintMagnitudes_CL = BufferCL<glm::vec4>::create(BufferFormat::R, m_U_new_CL->data().size());
	    m_constraintMagnitudes_CL->upload(glm::vec4());

        m_constraints_kernel->set(0, m_nodeConstraintFlags_CL);
        m_constraints_kernel->set(1, m_constraintMagnitudes_CL);
        m_constraints_kernel->set(2, m_U_new_CL);
    }

    // setup the force collect as well
    m_collect_kernel->set(0, m_Fx_CL);
    m_collect_kernel->set(1, m_F_CL);

}

void CalculatorHexCL::preStep(float fraction)
{
    // update the material params on the GPU
    m_mat_CL->upload(BufferFloat(m_assembly->element(0)->material()->params()));
}

void CalculatorHexCL::subStep(float subFraction)
{
	const size_t elCount = m_assembly->elements().size();
	const size_t nodeCount = m_assembly->nodes().size();

	m_tled_kernel->execute({elCount});
	m_collect_kernel->execute({nodeCount});

	// run the central difference udpate
	m_stepper->update();

	m_constraints_kernel->set(3, subFraction);
	m_constraints_kernel->execute({nodeCount});
//	m_gravity_kernel->execute({nodeCount});

	// swap U -> U_old and U_new -> U
	m_stepper->swap();
}

void CalculatorHexCL::postStep(float fraction)
{
    // apply constraints and upload data
    applyConstraints(fraction);

    auto stepper = static_cast<TimeStepperCL *>(m_stepper);
    stepper->downloadU();
}

void CalculatorHexCL::applyConstraints(float fraction)
{
	m_nodeConstraintFlags_CL->data().set(0);
	m_constraintMagnitudes_CL->data().set(glm::vec4());

	// apply displacements for the next step
	const LoadCollection& loads = m_assembly->loads();

	for (int i = 0; i < loads.size(); i++)
	{
		if(loads[i].active())
		{
			glm::vec4 value;

			if (loads[i].shape() == LoadShape::LINEAR || loads[i].shape() == LoadShape::LINEAR_INSTANT)
			{
				value = loads[i].evaluate(fraction);
			}
			else if(loads[i].shape() == LoadShape::INSTANT)// || load.shape() == LoadShape::LINEAR_INSTANT)
			{
				value = loads[i].value();
			}

			m_nodeConstraintFlags_CL->data()[i] = (int) loads[i].shape();
			m_constraintMagnitudes_CL->data()[i] = value;
		}
	}

	// instant loads are to be removed
	m_assembly->hitLoads();

	m_nodeConstraintFlags_CL->upload();
	m_constraintMagnitudes_CL->upload();
}
