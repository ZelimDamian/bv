#include "NodeSelection.h"
#include "SceneManager.h"
#include "RenderingManager.h"

NodeSelection::NodeSelection()
{
}


NodeSelection::~NodeSelection()
{
}

void NodeSelection::render()
{
	glm::vec3 boxsize = SceneManager::instance()->current()->boundingBox().size() * FeaNode::DRAW_SCALE;
	Box box(glm::vec3(), boxsize);

	Renderer* renderer = RenderingManager::instance()->renderer();

	for (FeaNodePtr nodePtr : m_items)
	{
		box.center(nodePtr->posu());

		renderer->render(box, glm::vec4(1.0f, 1.0f, 0.5f, 1.0f));
	}
}