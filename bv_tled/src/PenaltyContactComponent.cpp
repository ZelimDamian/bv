#include "PenaltyContactComponent.h"
#include "CollisionMethodProvider.h"
#include "RenderingSystem.h"
#include "AssemblyComponent.h"
#include "RenderMesh.h"
#include "Collider.h"
#include "AssemblyRendererComponent.h"
#include "TetMesh.h"
#include "CollisionSystem.h"
#include "RigidBodyComponent.h"

#include "UtilitiesColDet.h"

using namespace bv;
using namespace tled;
using namespace coldet;
using namespace tethex;

template <class TMesh>
auto getAssemblyComponent(Entity::Id entity) -> Handle<AssemblyComponent>
{
    return AssemblyComponent::find(entity);
}

template <class TMesh>
auto getAssemblyMesh(Entity::Id entity) -> Handle<TMesh>
{
    auto assemblyComponent = AssemblyRendererComponent<TMesh>::find(entity);

    if(!!assemblyComponent)
    {
        return assemblyComponent->mesh();
    }

    return TMesh::Id::invalid();
}

void PenaltyContactComponent::initialize()
{

    TetMesh::Id tetmesh = getAssemblyMesh<TetMesh>(m_entity);
    auto mesh = tetmesh->surface;
    if(!mesh)
    {
        Log::error("Missing collider mesh in PenaltyContactComponent");
    }

    auto otherMesh = Utilities::colliderMesh(m_master);
    otherMesh->updateTriNormals();

	UpdateSystem::instance()->onupdate([this]
	{
        if(m_active)
        {
            update();
        }
	});

	_rayMeshId = Mesh::create();
}

void PenaltyContactComponent::update()
{
    using namespace coldet;

    auto collider1 = Collider::find(m_entity);
    auto collider2 = Collider::find(m_master);

    if(!collider1 || !collider2)
    {
        Log::error("Collider missing when updating contact mesh");
    }

    // retreive the contact
	auto result = CollisionSystem::instance()->contact(collider1, collider2);
	auto contact = result.contact;

    if(contact == nullptr)
    {
        // no contact found yet
        return;
    }

    // need to make sure that its a mesh contact
    if(const MeshContact* meshContact = dynamic_cast<const MeshContact*>(contact))
    {
        // only update mesh when there is contact
        if(meshContact->isFound())
        {
            Mesh::Id masterMeshId = meshContact->mesh2();
	        
//            if(auto concept = dynamic_cast<Collider::ShapeModel<Octree>*>(collider2->concept()))
//            {
//				Octree *octree = &concept->shape();
//                masterMeshId = octree->mesh();
//            }
//            else
//            {
//                Log::error("Missing mesh on the other entity in PenaltyContactComponent");
//            }


            /////////////////////////////////////////////

            // the volumetric FE mesh
            auto tetmesh = getAssemblyMesh<TetMesh>(m_entity);

            // make sure that the two meshes are the same
            assert(meshContact->mesh1() == tetmesh->surface);

	        bool surfaceNeedsUpdate = false;

            // get the assembly
            auto assemblyComponent = getAssemblyComponent<TetMesh>(m_entity);
            auto& assembly = assemblyComponent->assembly();
            // reset the deformations on each iteration for DEBUG
//                assembly.reset();

            // only update mesh if apply flag is true
//            if(m_applyU)
            {

				// clear the mesh before filling it
				_rayMeshId->clear();

	            auto info = meshContact->info();

	            const auto& nodePenetrations = info->penetrations;

	            for (int i = 0; i < nodePenetrations.size(); ++i)
	            {
		            const int nodeIndex = info->slaveVertIndices[i];

		            if(nodePenetrations[i] == 0.0f)
		            {
			            continue;
		            }

					const glm::vec3 gap = nodePenetrations[i] * info->masterContactNormal;

					const glm::vec3 oldU = assembly.U(nodeIndex);
					const glm::vec3 newU = gap + oldU;

					float dt = assembly.cachedDt();
					float dtSqr = dt * dt;

					if(m_applyPenalty)
					{
						assembly.R(nodeIndex, gap * m_penaltyParam * m_magic);/// dtSqr;
					}

					// reference configuration position
					const glm::vec3 x = Math::transform(assembly.X(nodeIndex), m_entity->transform());
					const glm::vec3 pos = Math::transform(assembly.pos(nodeIndex), m_entity->transform());

					// predicted node position
					const glm::vec3 posu = x + newU;

					_rayMeshId->vertex(pos);
					_rayMeshId->vertex(posu);

					// render the triangle
					//_rayMeshId->vertex(vert1);
					//_rayMeshId->vertex(vert2);

					//_rayMeshId->vertex(vert1);
					//_rayMeshId->vertex(vert3);

					//_rayMeshId->vertex(vert2);
					//_rayMeshId->vertex(vert3);

					//_rayMeshId->vertex(glm::vec3(m_entity->transform() * glm::vec4(posu, 1.0f)));


					// mark surface mesh as dirty
					surfaceNeedsUpdate = true;
                }

				using namespace rendering;

				auto renderer = RenderingSystem::instance();

				renderer->onrender_once([renderer, this]
				{
					Mesh& rayMesh = _rayMeshId.ref();
					auto renderMesh = RenderMesh::create(_rayMeshId);

					Shader::Id colorShader = Shader::require("colors");
					colorShader->uniform("uColor", UniformType::Vec4)->set(glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));

					// draw using lines
					renderer->reset(Culling::NO, RenderMode::LINES);
					renderer->transform(glm::mat4());
					Shader::use(colorShader);

					renderer->submit(renderMesh);
				});

	            // update surface mesh if it is dirty
	            if(surfaceNeedsUpdate)
	            {
//		            tetmesh.positions() = assembly.posus();

		            using namespace physics;

		            auto rigidBody = RigidBodyComponent::find(m_master);

		            if(!!rigidBody)
		            {
			            // find max penetration
//			            float maxPenetration = *std::max_element(nodePenetrations.begin(), nodePenetrations.end());

						// apply all nodal forces from the surface to the master
			            for (int i = 0; i < nodePenetrations.size(); ++i)
			            {
//				            glm::vec3 nodalForce = assembly.F(slaveNodeIndices[i]);
				            const glm::vec3 normalReaction = -nodePenetrations[i] * m_penaltyParam * info->masterContactNormal;
//				            glm::vec3 normalReaction = -masterContactNormal * maxPenetration * m_penaltyParam;
				            rigidBody->applyForceGlobal(normalReaction, info->slaveVertPositions[i]);
			            }
		            }
		            else
		            {
			            Log::error("Missing RigidBodyComponent on master entity in " + title());
		            }

		            if (auto concept = collider1->concept<Octree>())
		            {
			            Octree *octree = &concept->shape();
			            octree->refit();
		            }
		            else
		            {
			            Log::error("Missing mesh on the master entity in " + title());
		            }
	            }
            }
        }
    }
    else
    {
        Log::error("Wrong contact type in " + title());
    }
}

void PenaltyContactComponent::attachGUI()
{
    auto m_panel = gui();

    m_panel->button("Apply Penalty", [&]
    {
        m_applyPenalty = !m_applyPenalty;
    }, [&]
    {
        return m_applyPenalty;
    });

	m_panel->bind("Penalty param", m_penaltyParam, 0.0f, 100.0f, 0.01f);
    m_panel->bind("Magic param", m_magic, 0.0f, 10000.0f, 10.0f);
}
