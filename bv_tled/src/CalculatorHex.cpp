#pragma warning(disable: 4996) // for std::copy

#include "CalculatorHex.h"
#include "MatrixRoutines.h"

using namespace bv;
using namespace bv::tled;
using namespace bv::tethex;

CalculatorHex::Calculator()
{
	calculateC();
}

void CalculatorHex::init(float dt, float dc)
{
	const ElementCollection& elements = m_assembly->elements();

	size_t elNum = elements.size();

	// Detrminant of the Jacobian matrix
	m_detJ = BufferFloat(elNum);

	// Shape function global derivatives elnum * [8][3]
	m_DhDx = BufferFloat(elNum * 8 * 3);

	// Deformation gradient elnum * [3][3]
	m_X = BufferFloat(elNum * 3 * 3);

	// Element node indices [elnum * 8]
	m_nodeInds = BufferInt(elNum * 8);
	int nodeCount = 0;

	for (size_t i = 0; i < elNum; i++)
	{
		for (size_t j = 0; j < 8; j++)
		{
			int nodeIndex = elements[i]->nodeIndex(j);
			m_nodeInds[nodeCount++] = nodeIndex;
		}
	}

	// Mass data pointer
	BufferFloat& M = m_assembly->M();

	float *pM = M.data();

	// Pre-Calculate the element deformation derivatives
	for (size_t i = 0; i < elNum; i++)
	{
		// make sure we are dealing with a tetrahedral element
		if (ElementHex* el = static_cast<ElementHex*>(elements[i].get()))
		{
			// use visitor pattern for double dispatch
			calculateDhDx(el);
			calculateMass(el, pM);
		}
	}

	auto& U = m_assembly->U();
	auto& F = m_assembly->F();

	m_stepper = new TimeStepper(U, F, M, dt, dc);
}

void CalculatorHex::step(float time, float totalTime)
{
	// Retreive forces, nodal displacements, and nodes
	auto& F = m_assembly->F();
	auto& U = m_assembly->U();

	// reset forces to zero
	F.zero();

	ElementCollection& elements = m_assembly->elements();
	
	size_t elmCount = elements.size();

	// Calculate the element forces
	for (size_t i = 0; i < elmCount; i++)
	{
		// make sure we are dealing with a tetrahedral element
		ElementHex* el = static_cast<ElementHex*>(elements[i].get());
		calculateForces(el, U.data(), F.reinterpret<float>());
	}

	int nodeCount = (int) U.size();

	// Retreive external forces
	auto& R = m_assembly->R();

	// subtract all external forces from m_F
	for (int i = 0; i < nodeCount; i++)
	{
		F.at(i) = R.at(i) - F.at(i);
	}

	// perform Time-Step update
	m_stepper->update();

	applyConstraints(time / totalTime);

	// ROTATE BUFFERS
	m_stepper->swap();
}

void CalculatorHex::calculateDhDx(ElementHex *elm)
{
	static const float c = 1.0f / 8.0f;

	// Shape function natural derivatives
	static const float dhdr[8 * 3] =
			{
					-c, -c, -c,
					c, -c, -c,
					c, c, -c,
					-c, c, -c,
					-c, -c, c,
					c, -c, c,
					c, c, c,
					-c, c, c
			};

	static const ArrayFloat<3, 8> DhDr(dhdr);

	// Gather the node positions into a buffer
	ArrayFloat<3, 8>& x = elm->positions(m_assembly->X());

	// Calculate the Jacobian
	ArrayFloat<3, 3> J;
	MatrixRoutines::multATB(DhDr, x, J);

	// The index of the element we are working with
	int elIndex = elm->index();

	// Jacobian determinant
	float detJ = MatrixRoutines::det3(J.data());
	//elm->detJ(detJ);
	// this value is used in the calculation so we keep it a buffer
	// to be used in a parallel implementation
	m_detJ.at(elIndex) = detJ;

	// Compute element volume
	float vol = calculateVolume(x);

	// we don't need to use this value in force calculation
	// so just plunk it into the element, instead sticking into a buffer
	elm->volume(vol);

	// Calculate the inverse of the Jacobian matrix
	ArrayFloat<3, 3> invJ;
	MatrixRoutines::inv3x3(J, invJ);
	//MatMult8333T(DhDr, invJ, DhDx);

	ArrayFloat<3, 8> DhDx;

//                m_DhDx.arrayCopy(elIndex * 24, DhDx);
	{
		// Compute shape function global derivatives
		MatrixRoutines::multABT(DhDr, invJ, DhDx);
	}

//	m_DhDx.embed(elIndex * DhDx.size(), DhDx);
}


//void CalculatorHex::calculateDhDx(ElementHex* elm)

float CalculatorHex::calculateVolume(const ArrayFloat<3, 8>& x) const
{
	// Calc volume
	float vol = 0;

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			for (int k = 0; k < 8; k++)
			{
				vol += x(i * 3 + 0) * x(j * 3 + 1) * x(k * 3 + 2) * Cijk[i][j][k];
			}
		}
	}
	return vol;
}

void CalculatorHex::calculateMass(const ElementHex* elm, float *M) const
{
	float Mass = elm->mass() / 8;

	for (int i = 0; i < 8; i++)
	{
		int j = elm->nodeIndex(i);
		M[j] += Mass;
	}
}

void CalculatorHex::calculateForces(const ElementHex* elm, const glm::vec4* U, float* F)
{
	int index = elm->index();

	ArrayFloat<3, 8> u;

	for (int i = 0; i < 8; i++)
	{
		int j = m_nodeInds.at(index * 8 + i);

		const glm::vec4& U_vec = U[j];
		
		u.at2(i, 0) = U_vec[0];
		u.at2(i, 1) = U_vec[1];
		u.at2(i, 2) = U_vec[2];
	}
	// Displacement derivs
	ArrayFloat<3, 8> DhDx;

//	m_DhDx.arrayCopy(index * 24, DhDx);

	ArrayFloat<3, 3> X;
	MatrixRoutines::multATB(u, DhDx, X);

	// Deformation gradient: X = DuDx + I
	X.at2(0, 0) += 1; X.at2(1, 1) += 1; X.at2(2, 2) += 1;	// X is now def. grad.

	// 2nd Piola-Kirchhoff stress
	ArrayFloat<3, 3> SPK;

	// TODO:: Un-Hard-Code Neo-Hookean model
	calculateStressNH(X.reinterpret<glm::vec3>(),
	                  elm->material()->params(),
	                  SPK.reinterpret<glm::vec3>());

	float detJ = m_detJ.at(elm->index());

	// Compute element nodal forces: Fe = 8*detJ*X*S*dh'
	ArrayFloat<3, 3> temp;
	MatrixRoutines::multAB(X, SPK, temp);
	MatrixRoutines::mult(temp, 8 * detJ, temp);

	// element forces
	ArrayFloat<8, 3> Fe;
	MatrixRoutines::multABT(temp, DhDx, Fe);

	// Add element forces to global forces
	for (int i = 0; i < 8; i++)
	{
		int j = m_nodeInds.at(index * 8 + i) * 3;
		F[j + 0] += Fe.at2(0, i);
		F[j + 1] += Fe.at2(1, i);
		F[j + 2] += Fe.at2(2, i);
	}
}

// NEO-HOOKEAN SPK
void CalculatorHex::calculateStressNH(const glm::vec3* X,
	const std::vector<float>& materialParams, glm::vec3* SPK) const
{
	float XT11, XT12, XT13, XT21, XT22, XT23, XT31, XT32, XT33;
	float C11, C12, C13, C22, C23, C33;

	// Transpose of deformation gradient
	XT11 = X[0][0]; XT12 = X[1][0]; XT13 = X[2][0];
	XT21 = X[0][1]; XT22 = X[1][1]; XT23 = X[2][1];
	XT31 = X[0][2]; XT32 = X[1][2]; XT33 = X[2][2];

	// Right Cauchy-Green deformation tensor
	C11 = XT11*XT11 + XT12*XT12 + XT13*XT13;
	C12 = XT11*XT21 + XT12*XT22 + XT13*XT23;
	C13 = XT11*XT31 + XT12*XT32 + XT13*XT33;
	C22 = XT21*XT21 + XT22*XT22 + XT23*XT23;
	C23 = XT21*XT31 + XT22*XT32 + XT23*XT33;
	C33 = XT31*XT31 + XT32*XT32 + XT33*XT33;

	// Determinant of deformation gradient
	float J = XT11*(XT22*XT33 - XT32*XT23) + XT12*(XT23*XT31 - XT21*XT33) + XT13*(XT21*XT32 - XT22*XT31);
	// Determinant of C
	float detC = C11*(C22*C33 - C23*C23) - C22*C13*C13 - C12*(C33*C12 - 2 * C13*C23);

	float M = materialParams[1];
	float K = materialParams[2];

	float x1 = (float)pow((double)J, -2.0 / 3.0) * M;
	float x2 = (K*J*(J - 1) - x1*(C11 + C22 + C33) / 3) / detC;


	// 2nd Piola-Kirchhoff stress
	SPK[0][0] = (C22*C33 - C23*C23)*x2 + x1; // S00
	SPK[1][1] = (C11*C33 - C13*C13)*x2 + x1; // S11
	SPK[2][2] = (C11*C22 - C12*C12)*x2 + x1; // S22
	SPK[0][1] = (C13*C23 - C12*C33)*x2;      // S01
	SPK[1][0] = SPK[0][1];                   //
	SPK[1][2] = (C12*C13 - C23*C11)*x2;      // S12
	SPK[2][1] = SPK[1][2];                   //
	SPK[0][2] = (C12*C23 - C13*C22)*x2;      // S02
	SPK[2][0] = SPK[0][2];                   //
}

float CalculatorHex::Cijk[8][8][8];

void CalculatorHex::calculateC() 
{
	static const float a = (float)(1. / 12);
	static const float Cvec[8 * 8 * 8] =
	{ 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, -a, -a, a, a, 0, 0,
	0, a, 0, -a, 0, 0, 0, 0,
	0, a, a, 0, -a, 0, 0, -a,
	0, -a, 0, a, 0, -a, 0, a,
	0, -a, 0, 0, a, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, a, -a, 0, 0, 0,

	0, 0, a, a, -a, -a, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	-a, 0, 0, -a, 0, a, a, 0,
	-a, 0, a, 0, 0, 0, 0, 0,
	a, 0, 0, 0, 0, -a, 0, 0,
	a, 0, -a, 0, a, 0, -a, 0,
	0, 0, -a, 0, 0, a, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,

	0, -a, 0, a, 0, 0, 0, 0,
	a, 0, 0, a, 0, -a, -a, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	-a, -a, 0, 0, 0, 0, a, a,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, a, 0, 0, 0, 0, -a, 0,
	0, a, 0, -a, 0, a, 0, -a,
	0, 0, 0, -a, 0, 0, a, 0,

	0, -a, -a, 0, a, 0, 0, a,
	a, 0, -a, 0, 0, 0, 0, 0,
	a, a, 0, 0, 0, 0, -a, -a,
	0, 0, 0, 0, 0, 0, 0, 0,
	-a, 0, 0, 0, 0, 0, 0, a,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, a, 0, 0, 0, 0, -a,
	-a, 0, a, 0, -a, 0, a, 0,

	0, a, 0, -a, 0, a, 0, -a,
	-a, 0, 0, 0, 0, a, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	a, 0, 0, 0, 0, 0, 0, -a,
	0, 0, 0, 0, 0, 0, 0, 0,
	-a, -a, 0, 0, 0, 0, a, a,
	0, 0, 0, 0, 0, -a, 0, a,
	a, 0, 0, a, 0, -a, -a, 0,

	0, a, 0, 0, -a, 0, 0, 0,
	-a, 0, a, 0, -a, 0, a, 0,
	0, -a, 0, 0, 0, 0, a, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	a, a, 0, 0, 0, 0, -a, -a,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, -a, -a, 0, a, 0, 0, a,
	0, 0, 0, 0, a, 0, -a, 0,

	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, a, 0, 0, -a, 0, 0,
	0, -a, 0, a, 0, -a, 0, a,
	0, 0, -a, 0, 0, 0, 0, a,
	0, 0, 0, 0, 0, a, 0, -a,
	0, a, a, 0, -a, 0, 0, -a,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, -a, -a, a, a, 0, 0,

	0, 0, 0, -a, a, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, a, 0, 0, -a, 0,
	a, 0, -a, 0, a, 0, -a, 0,
	-a, 0, 0, -a, 0, a, a, 0,
	0, 0, 0, 0, -a, 0, a, 0,
	0, 0, a, a, -a, -a, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0 };

	std::copy(std::begin(Cvec), std::end(Cvec), std::begin(Cijk[0][0]));
}

void CalculatorHex::bake()
{
//	size_t nodeCount = m_assembly->nodes().size();

//	glm::vec3* U = m_assembly->U().data();
//	glm::vec3* P = m_assembly->positions().data();

//	for (size_t i = 0; i < nodeCount; i++)
//	{
//		P[i] += U[i];
//		U[i] = glm::vec3(0.0f);
//	}
}