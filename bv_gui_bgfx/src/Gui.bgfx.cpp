#include "Gui.h"

#include <imgui.h>
#include <entry.h>
#include <ViewportElement.h>
#include <FileReader.h>

#include "MouseSystem.h"
#include "KeyboardSystem.h"
#include "WindowSystem.h"
#include "RenderingSystem.h"


using namespace bv;
using namespace bv::gui;

void input(bv::Key key);

inline void SetupImGuiStyle( bool bStyleDark_, float alpha_  )
{
	ImGuiStyle& style = ImGui::GetStyle();

	// light style from Pacôme Danhiez (user itamago) https://github.com/ocornut/imgui/pull/511#issuecomment-175719267
	style.Alpha = 1.0f;
	style.FrameRounding = 3.0f;
	style.Colors[ImGuiCol_Text]                  = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_TextDisabled]          = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
	style.Colors[ImGuiCol_WindowBg]              = ImVec4(0.94f, 0.94f, 0.94f, 0.94f);
	style.Colors[ImGuiCol_ChildWindowBg]         = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
//	style.Colors[ImGuiCol_PopupBg]               = ImVec4(1.00f, 1.00f, 1.00f, 0.94f);
	style.Colors[ImGuiCol_Border]                = ImVec4(0.00f, 0.00f, 0.00f, 0.39f);
	style.Colors[ImGuiCol_BorderShadow]          = ImVec4(1.00f, 1.00f, 1.00f, 0.10f);
	style.Colors[ImGuiCol_FrameBg]               = ImVec4(1.00f, 1.00f, 1.00f, 0.94f);
	style.Colors[ImGuiCol_FrameBgHovered]        = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	style.Colors[ImGuiCol_FrameBgActive]         = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	style.Colors[ImGuiCol_TitleBg]               = ImVec4(0.96f, 0.96f, 0.96f, 1.00f);
	style.Colors[ImGuiCol_TitleBgCollapsed]      = ImVec4(1.00f, 1.00f, 1.00f, 0.51f);
	style.Colors[ImGuiCol_TitleBgActive]         = ImVec4(0.82f, 0.82f, 0.82f, 1.00f);
	style.Colors[ImGuiCol_MenuBarBg]             = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarBg]           = ImVec4(0.98f, 0.98f, 0.98f, 0.53f);
	style.Colors[ImGuiCol_ScrollbarGrab]         = ImVec4(0.69f, 0.69f, 0.69f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered]  = ImVec4(0.59f, 0.59f, 0.59f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrabActive]   = ImVec4(0.49f, 0.49f, 0.49f, 1.00f);
	style.Colors[ImGuiCol_ComboBg]               = ImVec4(0.86f, 0.86f, 0.86f, 0.99f);
	style.Colors[ImGuiCol_CheckMark]             = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	style.Colors[ImGuiCol_SliderGrab]            = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
	style.Colors[ImGuiCol_SliderGrabActive]      = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	style.Colors[ImGuiCol_Button]                = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	style.Colors[ImGuiCol_ButtonHovered]         = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	style.Colors[ImGuiCol_ButtonActive]          = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
	style.Colors[ImGuiCol_Header]                = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
	style.Colors[ImGuiCol_HeaderHovered]         = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
	style.Colors[ImGuiCol_HeaderActive]          = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	style.Colors[ImGuiCol_Column]                = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_ColumnHovered]         = ImVec4(0.26f, 0.59f, 0.98f, 0.78f);
	style.Colors[ImGuiCol_ColumnActive]          = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	style.Colors[ImGuiCol_ResizeGrip]            = ImVec4(1.00f, 1.00f, 1.00f, 0.50f);
	style.Colors[ImGuiCol_ResizeGripHovered]     = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	style.Colors[ImGuiCol_ResizeGripActive]      = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
	style.Colors[ImGuiCol_CloseButton]           = ImVec4(0.59f, 0.59f, 0.59f, 0.50f);
	style.Colors[ImGuiCol_CloseButtonHovered]    = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);
	style.Colors[ImGuiCol_CloseButtonActive]     = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);
	style.Colors[ImGuiCol_PlotLines]             = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_PlotLinesHovered]      = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogram]         = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_PlotHistogramHovered]  = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_TextSelectedBg]        = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
	style.Colors[ImGuiCol_ModalWindowDarkening]  = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);

	if( bStyleDark_ )
	{
		for (int i = 0; i <= ImGuiCol_COUNT; i++)
		{
			ImVec4& col = style.Colors[i];
			float H, S, V;
			ImGui::ColorConvertRGBtoHSV( col.x, col.y, col.z, H, S, V );

			if( S < 0.1f )
			{
				V = 1.0f - V;
			}
			ImGui::ColorConvertHSVtoRGB( H, S, V, col.x, col.y, col.z );
			if( col.w < 1.00f )
			{
				col.w *= alpha_;
			}
		}
	}
	else
	{
		for (int i = 0; i <= ImGuiCol_COUNT; i++)
		{
			ImVec4& col = style.Colors[i];
			if( col.w < 1.00f )
			{
				col.x *= alpha_;
				col.y *= alpha_;
				col.z *= alpha_;
				col.w *= alpha_;
			}
		}
	}
}

void Gui::init()
{
	SetupImGuiStyle( false, 1.0f);

	ImGuiIO& io = ImGui::GetIO();

//	auto font_file = FileReader::ROOT_DIR + "/fonts/ArialUni.ttf";
//	io.Fonts->AddFontFromFileTTF(font_file.c_str(), 18.0f, NULL, io.Fonts->GetGlyphRangesCyrillic());

    io.KeyMap[ImGuiKey_Tab]        = (int)ImGuiKey_Tab;
    io.KeyMap[ImGuiKey_LeftArrow]  = (int)ImGuiKey_LeftArrow;
    io.KeyMap[ImGuiKey_RightArrow] = (int)ImGuiKey_RightArrow;
    io.KeyMap[ImGuiKey_UpArrow]    = (int)ImGuiKey_UpArrow;
    io.KeyMap[ImGuiKey_DownArrow]  = (int)ImGuiKey_DownArrow;
    io.KeyMap[ImGuiKey_Home]       = (int)ImGuiKey_Home;
    io.KeyMap[ImGuiKey_End]        = (int)ImGuiKey_End;
    io.KeyMap[ImGuiKey_Delete]     = (int)ImGuiKey_Delete;
    io.KeyMap[ImGuiKey_Backspace]  = (int)ImGuiKey_Backspace;
    io.KeyMap[ImGuiKey_Enter]      = (int)ImGuiKey_Enter;
    io.KeyMap[ImGuiKey_Escape]     = (int)ImGuiKey_Escape;

    // create the main rendering context
    auto viewport = mainPanel()->add<ViewportElement>("viewport");

	auto window = WindowSystem::instance();

	// set initial size for the main and side panels
	mainPanel()->state(0, 0, window->width() * 0.8f, WindowSystem::instance()->height() - 2);
	sidePanel()->state(window->width() * 0.8f, 1, window->width() * 0.2f, window->height() - 2);

    auto onload = [&]()
    {
        imguiCreate();
    };

	for (int i = 0; i < (int) Key::COUNT; ++i)
	{
		Key key = (Key)i;
		KeyboardSystem::instance()->onpressed(key, [=]
		{
			input(key);
		});
	}

	auto prevWidth = window->width();
	auto prevHeight = window->height();

	auto onresize = [=] () mutable
	{
		auto scaleX = (float)window->width() / prevWidth;
		auto scaleY = (float)window->height() / prevHeight;

		// render the panel and its children
		for (auto& pair : m_windows)
		{
			auto rect = pair.second->rect();
			rect.scale({scaleX, scaleY});
			pair.second->state(rect);
		}

		prevWidth = window->width();
		prevHeight = window->height();
	};

	window->onresize(onresize);

    auto onrender = [this] () mutable
    {
		auto mouse = MouseSystem::instance();
		auto window = WindowSystem::instance();

        int width = window->width();
        int height = window->height();

//        auto windowSize = Recti {0, 0, width, height};
//        RenderingSystem::instance()->setViewSize(windowSize);

        imguiBeginFrame(
              mouse->pos().x
            , height - mouse->pos().y
            , (mouse->left() ? IMGUI_MBUT_LEFT  :0)
              | (mouse->right() ? IMGUI_MBUT_RIGHT :0)
            , mouse->z()
            , (uint16_t) width
            , (uint16_t) height );

        // render main menu
        m_menu.renderImpl();

        // render the panel and its children
        for (auto& pair : m_windows)
        {
            pair.second->render();
        }

        // finish gui cycle
        imguiEndFrame();

	    auto& io = ImGui::GetIO();
	    for (size_t i = 0; i < ImGuiKey_COUNT; i++)
	    {
		    io.KeysDown[i] = false;
	    }

	    for(auto it = std::begin(m_windows); it != std::end(m_windows);)
	    {
		    if (!it->second->isOpen())
		    {
			    it = m_windows.erase(it); // previously this was something like m_map.erase(it++);
		    }
		    else { ++it; }
	    }
    };

    RenderingSystem::instance()->onload(onload);
    RenderingSystem::instance()->onPreRender(onrender);
}

void Gui::update()
{
}

void Gui::destroy()
{
	ImGui::Shutdown();
}

bool Gui::isPointInScene(const glm::ivec2 &point)
{
    return !ImGui::IsMouseHoveringAnyWindow() && !ImGui::GetIO().WantCaptureMouse;
}

void input(bv::Key key)
{
	ImGuiIO& io = ImGui::GetIO();
	switch (key)
    {
        case bv::Key::UNKNOWN:          break;

        case bv::Key::LCTRL:            io.KeyCtrl = true; break;
	    case bv::Key::LALT:             io.KeyAlt = true; break;
	    case bv::Key::LSHIFT:           io.KeyShift = true; break;

	    case bv::Key::ESCAPE:           io.KeysDown[ImGuiKey_Escape] = true; break;
        case bv::Key::RETURN:           io.KeysDown[ImGuiKey_Enter] = true; break;
        case bv::Key::TAB:              io.KeysDown[ImGuiKey_Tab] = true; break;
        case bv::Key::SPACE:            io.AddInputCharacter(' '); break;
        case bv::Key::BACKSPACE:        io.KeysDown[ImGuiKey_Backspace] = true; break;
        case bv::Key::UP:               io.KeysDown[ImGuiKey_UpArrow] = true; break;
        case bv::Key::DOWN:             io.KeysDown[ImGuiKey_DownArrow] = true; break;
        case bv::Key::LEFT:             io.KeysDown[ImGuiKey_LeftArrow] = true; break;
        case bv::Key::RIGHT:            io.KeysDown[ImGuiKey_RightArrow] = true; break;
        case bv::Key::INSERT:           break;
        case bv::Key::DEL:              io.KeysDown[ImGuiKey_Delete] = true; break;
        case bv::Key::HOME:             io.KeysDown[ImGuiKey_Home] = true; break;
        case bv::Key::END:              io.KeysDown[ImGuiKey_End] = true; break;
        case bv::Key::PAGEUP:           io.KeysDown[ImGuiKey_PageUp] = true; break;
        case bv::Key::PAGEDOWN:         io.KeysDown[ImGuiKey_PageDown] = true; break;
        case bv::Key::PRINT:            break;
        case bv::Key::PLUS:             io.AddInputCharacter('+'); break;
        case bv::Key::MINUS:            io.AddInputCharacter('-'); break;
        case bv::Key::LEFTBRACKET:      io.AddInputCharacter('('); break;
        case bv::Key::RIGHTBRACKET:     io.AddInputCharacter(')'); break;
        case bv::Key::SEMICOLON:        io.AddInputCharacter(';'); break;
        case bv::Key::QUOTE:            io.AddInputCharacter('\''); break;
        case bv::Key::COMMA:            io.AddInputCharacter(','); break;
        case bv::Key::PERIOD:           io.AddInputCharacter('.'); break;
        case bv::Key::SLASH:            io.AddInputCharacter('\\'); break;
        case bv::Key::BACKSLASH:        io.AddInputCharacter('/'); break;
        case bv::Key::TILDE:            io.AddInputCharacter('~'); break;
        case bv::Key::F1:               break;
        case bv::Key::F2:               break;
        case bv::Key::F3:               break;
        case bv::Key::F4:               break;
        case bv::Key::F5:               break;
        case bv::Key::F6:               break;
        case bv::Key::F7:               break;
        case bv::Key::F8:               break;
        case bv::Key::F9:               break;
        case bv::Key::F10:              break;
        case bv::Key::F11:              break;
        case bv::Key::F12:              break;
        case bv::Key::NUMPAD0:          io.AddInputCharacter('0'); break;
        case bv::Key::NUMPAD1:          io.AddInputCharacter('1'); break;
        case bv::Key::NUMPAD2:          io.AddInputCharacter('2'); break;
        case bv::Key::NUMPAD3:          io.AddInputCharacter('3'); break;
        case bv::Key::NUMPAD4:          io.AddInputCharacter('4'); break;
        case bv::Key::NUMPAD5:          io.AddInputCharacter('5'); break;
        case bv::Key::NUMPAD6:          io.AddInputCharacter('6'); break;
        case bv::Key::NUMPAD7:          io.AddInputCharacter('7'); break;
        case bv::Key::NUMPAD8:          io.AddInputCharacter('8'); break;
        case bv::Key::NUMPAD9:          io.AddInputCharacter('9'); break;
        case bv::Key::NUM0:             io.AddInputCharacter('0'); break;
        case bv::Key::NUM1:             io.AddInputCharacter('1'); break;
        case bv::Key::NUM2:             io.AddInputCharacter('2'); break;
        case bv::Key::NUM3:             io.AddInputCharacter('3'); break;
        case bv::Key::NUM4:             io.AddInputCharacter('4'); break;
        case bv::Key::NUM5:             io.AddInputCharacter('5'); break;
        case bv::Key::NUM6:             io.AddInputCharacter('6'); break;
        case bv::Key::NUM7:             io.AddInputCharacter('7'); break;
        case bv::Key::NUM8:             io.AddInputCharacter('8'); break;
        case bv::Key::NUM9:             io.AddInputCharacter('9'); break;
        case bv::Key::A:                io.AddInputCharacter('A'); break;
        case bv::Key::B:                io.AddInputCharacter('B'); break;
        case bv::Key::C:                io.AddInputCharacter('C'); break;
        case bv::Key::D:                io.AddInputCharacter('D'); break;
        case bv::Key::E:                io.AddInputCharacter('E'); break;
        case bv::Key::F:                io.AddInputCharacter('F'); break;
        case bv::Key::G:                io.AddInputCharacter('G'); break;
        case bv::Key::H:                io.AddInputCharacter('H'); break;
        case bv::Key::I:                io.AddInputCharacter('I'); break;
        case bv::Key::J:                io.AddInputCharacter('J'); break;
        case bv::Key::K:                io.AddInputCharacter('K'); break;
        case bv::Key::L:                io.AddInputCharacter('L'); break;
        case bv::Key::M:                io.AddInputCharacter('M'); break;
        case bv::Key::N:                io.AddInputCharacter('N'); break;
        case bv::Key::O:                io.AddInputCharacter('O'); break;
        case bv::Key::P:                io.AddInputCharacter('P'); break;
        case bv::Key::Q:                io.AddInputCharacter('Q'); break;
        case bv::Key::R:                io.AddInputCharacter('R'); break;
        case bv::Key::S:                io.AddInputCharacter('S'); break;
        case bv::Key::T:                io.AddInputCharacter('T'); break;
        case bv::Key::U:                io.AddInputCharacter('U'); break;
        case bv::Key::V:                io.AddInputCharacter('V'); break;
        case bv::Key::W:                io.AddInputCharacter('W'); break;
        case bv::Key::X:                io.AddInputCharacter('X'); break;
        case bv::Key::Y:                io.AddInputCharacter('Y'); break;
        case bv::Key::Z:                io.AddInputCharacter('Z'); break;

//        case entry::Key::GamepadA;				return Key::Tab:
//        case entry::Key::GamepadB;				return Key::Tab:
//        case entry::Key::GamepadX;				return Key::Tab:
//        case entry::Key::GamepadY;				return Key::Tab:
//        case entry::Key::GamepadThumbL;				return Key::Tab:
//        case entry::Key::GamepadThumbR;				return Key::Tab:
//        case entry::Key::GamepadShoulderL;				return Key::Tab:
//        case entry::Key::GamepadShoulderR;				return Key::Tab:
//        case entry::Key::GamepadUp;				return Key::Tab:
//        case entry::Key::GamepadDown;				return Key::Tab:
//        case entry::Key::GamepadLeft;				return Key::Tab:
//        case entry::Key::GamepadRight;				return Key::Tab:
//        case entry::Key::GamepadBack;				return Key::Tab:
//        case entry::Key::GamepadStart;				return Key::Tab:
//        case GamepadGuide;				return Key::Tab:

//        Count
    }
}
