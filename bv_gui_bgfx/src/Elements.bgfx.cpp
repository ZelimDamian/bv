#include "Gui.h"

#include <imgui.h>

#include "LabelElement.h"
#include "SliderElement.h"
#include "DragElement.h"
#include "ButtonElement.h"
#include "ListELement.h"
#include "GraphElement.h"
#include "ViewportElement.h"
#include "WindowSystem.h"

using namespace bv;
using namespace bv::gui;

extern std::vector<std::vector<bgfx::TextureHandle>>_frameBufferTextures;

void LabelElement::renderImpl()
{
//    imguiLabel("%s", text().c_str());
    ImGui::LabelText(title().c_str(), "%s", text().c_str());
}

void SliderElement::renderImpl()
{
    float& valueRef = m_binding.value();
//    imguiSlider(m_title.c_str(), valueRef, m_min, m_max, m_step);
	ImGui::PushID(this);
    ImGui::SliderFloat(m_title.c_str(), &valueRef, m_min, m_max);
	ImGui::PopID();
}

void SliderElementInt::renderImpl()
{
    int& valueRef = m_binding.value();
//    imguiSlider(m_title.c_str(), valueRef, m_min, m_max);
	ImGui::PushID(this);
    if(ImGui::SliderInt(m_title.c_str(), &valueRef, m_min, m_max))
    {
	    m_onchange(valueRef);
    }
	ImGui::PopID();
}

void DragElement::renderImpl()
{
    auto& valueRef = m_binding.value();

	ImGui::PushID(this);
    ImGui::DragFloat(m_title.c_str(), &valueRef, m_step == 0.0f ? (m_max - m_min) / 100.0f : m_step, m_min, m_max, "%.5f");
	ImGui::PopID();
}

namespace bv { namespace gui
{
    template<>
    void DragElementInt::renderImpl()
    {
        auto value = m_source();
        ImGui::PushID(this);
        ImGui::DragInt(m_title.c_str(), &value, m_step == 0.0f ? (m_max - m_min) / 100.0f : m_step, m_min, m_max);
        {
	        if(!ImGui::IsItemActive())
	        {
		        m_target(value);
	        }
        }

        ImGui::PopID();
    }

	template<>
	void DragElementFloat::renderImpl()
	{
		auto value = m_source();
		ImGui::PushID(this);
		ImGui::DragFloat(m_title.c_str(), &value, m_step == 0.0f ? (m_max - m_min) / 100.0f : m_step, m_min, m_max, "%.5f");
		{
			if(!ImGui::IsItemActive())
			{
				m_target(value);
			}
		}

		ImGui::PopID();
	}

	template<>
	void DragElementVec3::renderImpl()
	{
		ImGui::PushID(this);

		auto value = m_source();
		ImGui::DragFloat3(m_title.c_str(), &value.x, m_step == 0.0f ? (m_max - m_min) / 100.0f : m_step, m_min,
							  m_max, "%.5f");
		{
			if(!ImGui::IsItemActive())
			{
				m_target(value);
			}
		}
		ImGui::PopID();
	}

	template<>
	void GraphElementFloat::renderImpl()
	{
		ImGui::PushID(this);

		m_history.push_back(m_source());

		if (m_history.size() > m_showNum)
		{
			m_history.erase(m_history.begin(), m_history.end() - m_showNum);
		}

		auto source = [](void* history, int idx)
		{
//			idx = history.size() * (float)idx / m_showNum;
			return reinterpret_cast<float*>(history)[idx];
		};

	    ImGui::PlotLines(m_title.c_str(), source, m_history.data(), std::min(m_showNum, (int)m_history.size()),
			0, nullptr, m_min, m_max, ImVec2(0, 80));

		ImGui::PopID();
	}
}}

void PanelElement::renderImpl()
{
    if(m_parent == nullptr)
    {
	    if (m_rect.width != 0 && m_rect.height != 0)
	    {
		    ImGui::SetNextWindowSize(ImVec2(m_rect.width, m_rect.height));
		    ImGui::SetNextWindowPos(ImVec2(m_rect.x, m_rect.y));
	    }

	    auto alwaysOnBackFlag = ImGuiWindowFlags_NoBringToFrontOnFocus;

	    if(ImGui::Begin(m_title.c_str(), &m_open, ImVec2{(float) m_rect.width, (float) m_rect.height}, -1.0f,
		                                   this->m_title == "Main" ? alwaysOnBackFlag : 0))
	    {
		    renderChildren();
	    }

	    auto pos = ImGui::GetWindowPos();
	    auto size = ImGui::GetWindowSize();

	    auto window = WindowSystem::instance();

	    m_rect.x = (int) pos.x;
	    m_rect.y = (int) pos.y;

	    if (!ImGui::IsWindowCollapsed())
	    {
		    size.x = std::min(window->width(), (int) size.x);
		    size.y = std::min(window->height(), (int) size.y);

		    m_rect.width = (int) size.x;
		    m_rect.height = (int) size.y;
	    }

		ImGui::End();

        if(!m_open)
        {
            this->close();
        }
    }
	else
    {
	    if(ImGui::CollapsingHeader(m_title.c_str(), nullptr, true, true))
	    {
		    renderChildren();
	    }
    }
}

void CheckBoxElement::renderImpl()
{
	ImGui::PushID(this);

	bool value = m_active();
	if(ImGui::Checkbox(m_title.c_str(), &value))
	{
		// trigger the event
		m_onclick();
	}

	ImGui::PopID();
}

void ButtonElement::renderImpl()
{
    ImGui::PushID(this);

    if(m_active && m_active())
    {
        ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.6, 0.2, 0.2, 1.0));
    }
    else
    {
        ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.4, 0.2, 0.2, 1.0));
    }

    if(ImGui::Button(m_title.c_str()))
    {
        // trigger the event
        m_onclick();
    }

//    if(m_active && m_active())
    {
        ImGui::PopStyleColor(1);
        ImGui::PopID();
    }
}

void ComboElement::renderImpl()
{

	ImGui::PushID(this);

	const char** listbox_items = new const char*[m_list.size()];

	for (int i = 0; i < m_list.size(); ++i)
	{
		listbox_items[i] = m_list[i].c_str();
	}

	if(ImGui::Combo(m_title.c_str(), &m_selectedIndex, listbox_items, m_list.size(), 4))
	{
		m_onchange(m_selectedIndex);
	}

	delete listbox_items;

	ImGui::PopID();
}

void ListElement::renderImpl()
{

	ImGui::PushID(this);

    const char** listbox_items = new const char*[m_list.size()];

    for (int i = 0; i < m_list.size(); ++i)
    {
        listbox_items[i] = m_list[i].c_str();
    }

    if(ImGui::ListBox(m_title.c_str(), &m_selectedIndex, listbox_items, m_list.size(), 4))
    {
	    m_onchange(m_selectedIndex);
    }

    delete[] listbox_items;

	ImGui::PopID();
}


void MultiListElement::renderImpl()
{
	//std::vector<int> selected;

	for (int i = 0; i < m_list.size(); ++i)
	{
		auto it = std::find_if(m_selectedList.begin(), m_selectedList.end(), [i](int value) { return i == value; });
		bool isFound = it != m_selectedList.end();

		// draw the button and check if it was pressed
		auto color = imguiRGBA(128, 128, 128, 0);

		if (isFound)
		{
			color = imguiRGBA(32, 32, 32, 0);
		}

		if (imguiButton(m_list[i].c_str(), true, ImguiAlign::LeftIndented, color))
		{
			if (!isFound)
			{
				m_selectedList.push_back(i);
			}
			else
			{
				m_selectedList.erase(it);
			}
		}
	}

	if (!m_selectedList.empty())
	{
		m_selectedList = m_selectedList;
		m_onchange(m_selectedList);
	}
}

void ViewportElement::renderImpl()
{
	// the frame buffer
	int bufferId = m_context->buffer().id;

	// texture containing the renderbuffer
    auto texId = _frameBufferTextures[bufferId][0];

	auto pos = ImGui::GetCursorScreenPos();

	// draw the texture on gui
	ImGui::ImageButton(texId, ImGui::GetContentRegionAvail(), ImVec2(0.0f, 1.0f), ImVec2(1.0f, 0.0f), 0);

	auto sz = ImGui::GetItemRectSize();

	auto rect = Recti { (int)pos.x, (int)pos.y, (int)sz.x, (int)sz.y };

	size(rect);

	// Check if mouse is hovering the current viewport
	if(ImGui::IsItemHovered())
	{
		auto camera = m_context->camera();
		CameraSystem::instance()->current(camera);
	}
}

void Menu::renderImpl()
{
    if(m_parent == nullptr)
    {
        if(ImGui::BeginMainMenuBar())
        {
            for (auto& m_child : m_children)
            {
                m_child->renderImpl();
            }
        }

        ImGui::EndMainMenuBar();
    }
    else
    {
        if(m_children.size() > 0)
        {
            if (ImGui::BeginMenu(m_title.c_str()))
            {
                for (auto &m_child : m_children)
                {
                    if(m_child)
                    {
                        m_child->renderImpl();
                    }
                }
                ImGui::EndMenu();
            }
        }
        else
        {
			auto shortcut = this->shortcut();

            if(ImGui::MenuItem(m_title.c_str(), shortcut.c_str()))
            {
	            trigger();
            }
        }
    }
}
