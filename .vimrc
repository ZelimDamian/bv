nnoremap <space> :w<CR>:!make -C ../mc_make/<CR>
nnoremap <S-R> :!./runtime/mc <CR>

map <A-Left> <Esc>:tabprev<CR>
map <A-Right> <Esc>:tabnext<CR>
map <A-n> <Esc>:tabnew

nnoremap <CR> o<ESC>

nnoremap <C-h> gT
nnoremap <C-l> gt

set path=$PWD/**

vmap // :s:^://<CR>