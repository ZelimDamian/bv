#####
#
# Copyright 2014 Alex Tsui, Zelim Gerikhanov (vertex_colors)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#####

#
# http://wiki.blender.org/index.php/Dev:2.5/Py/Scripts/Guidelines/Addons
#
import os
import bpy
import mathutils
from bpy.props import (BoolProperty,
    FloatProperty,
    StringProperty,
    EnumProperty,
    )
from bpy_extras.io_utils import (ImportHelper,
    ExportHelper,
    unpack_list,
    unpack_face_list,
    axis_conversion,
    )

#if "bpy" in locals():
#   import imp
#   if "import_off" in

bl_info = {
    "name": "OFF format",
    "description": "Import-Export OFF, Import/export simple OFF mesh.",
    "author": "Alex Tsui, Mateusz Kłoczko, Zelim Gerikhanov (vertex_colors)",
    "version": (0, 3),
    "blender": (2, 74, 0),
    "location": "File > Import-Export",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"
                "Scripts/My_Script",
    "category": "Import-Export"}

# helper class to create empty structs
class struct(object):
    pass

def makeMaterial(name, diffuse, specular, alpha):
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = diffuse
    mat.diffuse_shader = 'LAMBERT' 
    mat.diffuse_intensity = 1.0 
    mat.specular_color = specular
    mat.specular_shader = 'COOKTORR'
    mat.specular_intensity = 0.5
    mat.alpha = alpha
    mat.ambient = 1
    return mat

class ImportOFF(bpy.types.Operator, ImportHelper):
    """Load an OFF Mesh file"""
    bl_idname = "import_mesh.off"
    bl_label = "Import OFF Mesh"
    filename_ext = ".off"
    filter_glob = StringProperty(
        default="*.off",
        options={'HIDDEN'},
    )

    axis_forward = EnumProperty(
            name="Forward",
            items=(('X', "X Forward", ""),
                   ('Y', "Y Forward", ""),
                   ('Z', "Z Forward", ""),
                   ('-X', "-X Forward", ""),
                   ('-Y', "-Y Forward", ""),
                   ('-Z', "-Z Forward", ""),
                   ),
            default='-Z',
            )
    axis_up = EnumProperty(
            name="Up",
            items=(('X', "X Up", ""),
                   ('Y', "Y Up", ""),
                   ('Z', "Z Up", ""),
                   ('-X', "-X Up", ""),
                   ('-Y', "-Y Up", ""),
                   ('-Z', "-Z Up", ""),
                   ),
            default='Y',
            )

    def execute(self, context):
        #from . import import_off

        keywords = self.as_keywords(ignore=('axis_forward',
            'axis_up',
            'filter_glob',
        ))
        global_matrix = axis_conversion(from_forward=self.axis_forward,
            from_up=self.axis_up,
            ).to_4x4()

        off_name = "off_mesh" #bpy.path.display_name_from_filepath(**keywords)
        
        # create a mesh
        self.mesh = bpy.data.meshes.new(name=off_name)
        
        # start by creating a scene object
        scene = bpy.context.scene
        obj = bpy.data.objects.new(off_name, self.mesh)
                
        mesh = load(self, context, **keywords)
        
        if not mesh:
            return {'CANCELLED'}
        
        obj.name = mesh.name
        obj.data = mesh
        
        scene.objects.link(obj)
        scene.objects.active = obj
        obj.select = True

        obj.matrix_world = global_matrix

        scene.update()

        return {'FINISHED'}

class ExportOFF(bpy.types.Operator, ExportHelper):
    """Save an OFF Mesh file"""
    bl_idname = "export_mesh.off"
    bl_label = "Export OFF Mesh"
    filter_glob = StringProperty(
        default="*.off",
        options={'HIDDEN'},
    )
    check_extension = True
    filename_ext = ".off"

    axis_forward = EnumProperty(
            name="Forward",
            items=(('X', "X Forward", ""),
                   ('Y', "Y Forward", ""),
                   ('Z', "Z Forward", ""),
                   ('-X', "-X Forward", ""),
                   ('-Y', "-Y Forward", ""),
                   ('-Z', "-Z Forward", ""),
                   ),
            default='-Z',
            )
    axis_up = EnumProperty(
            name="Up",
            items=(('X', "X Up", ""),
                   ('Y', "Y Up", ""),
                   ('Z', "Z Up", ""),
                   ('-X', "-X Up", ""),
                   ('-Y', "-Y Up", ""),
                   ('-Z', "-Z Up", ""),
                   ),
            default='Y',
            )

    def execute(self, context):
        keywords = self.as_keywords(ignore=('axis_forward',
            'axis_up',
            'filter_glob',
            'check_existing',
        ))
        global_matrix = axis_conversion( to_forward=self.axis_forward,
            to_up=self.axis_up ).to_4x4()
        keywords['global_matrix'] = global_matrix
        return save(self, context, **keywords)

def menu_func_import(self, context):
    self.layout.operator(ImportOFF.bl_idname, text="OFF Mesh (.off)")

def menu_func_export(self, context):
    self.layout.operator(ExportOFF.bl_idname, text="OFF Mesh (.off)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_import.append(menu_func_import)
    bpy.types.INFO_MT_file_export.append(menu_func_export)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_import.remove(menu_func_import)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)

def load(operator, context, filepath):
    # Parse mesh from OFF file
    # TODO: Add support for NOFF and COFF
    filepath = os.fsencode(filepath)
    file = open(filepath, 'r')
    file.readline()
    vcount, fcount, ecount = [int(x) for x in file.readline().split()]
    verts = []
    facets = []
    edges = []
    materials = []
    colors = []
    
    i=0;
    while i<vcount:
        line = file.readline()
        try:
            px, py, pz = [float(x) for x in line.split()]
        except ValueError:
            i=i+1
            continue
        verts.append((px, py, pz))
        i=i+1

    i=0;
    facet_count = 0
    while i<fcount:
        line = file.readline()
        try:
            splitted  = line.split()
            ids   = list(map(int, splitted))
            num_ids = ids[0]
            if len(ids) > 3:
                facets.append(tuple(ids[1 : num_ids + 1]))
                facet_count = facet_count + 1
                #if 1 == 1: #len(ids) > num_ids + 1:
                    # there are face colors in the file
                    # operator.color_map.data
                color = tuple(ids[-3:])
                colors.append(color)

            elif len(ids) == 3:
                edges.append(tuple(ids[1:]))
                
        except ValueError:
            i=i+1
            continue
        i=i+1

    colorIds = []

    # find unique colors and map back to the original
    uniqueColors = [] #[color for color in colors if color not in seen and not seen.add(color)]

    for color in colors:
        if color not in uniqueColors:
            colorIds.append(len(uniqueColors))
            uniqueColors.append(color)
        else:
            colorIds.append(uniqueColors.index(color))

    # create a material for each unique color
    for color in uniqueColors:
        # generate the material name from the color values
        name = str(color)
        # color needs to normlized
        # store the newly created material in the dictionary
        materials.append(makeMaterial(name, (color[0] / 255, color[1] / 255, color[2] / 255), (1,1,1), 1))
        

    # Assemble mesh
    off_name = bpy.path.display_name_from_filepath(filepath)
    mesh = operator.mesh
    mesh.from_pydata(verts,edges,facets)
    
    for material in materials:
        mesh.materials.append(material)
    
    if len(mesh.polygons) == len(colorIds):
        i = 0
        for poly in mesh.polygons:
            poly.material_index = colorIds[i]
            i = i + 1
    
    mesh.validate()
    mesh.update()

    return mesh

def save(operator, context, filepath,
    global_matrix = None):
    # Export the selected mesh
    APPLY_MODIFIERS = True # TODO: Make this configurable
    if global_matrix is None:
        global_matrix = mathutils.Matrix()
        
    scene = context.scene
    obj = scene.objects.active
    mesh = obj.to_mesh(scene, APPLY_MODIFIERS, 'PREVIEW')

    # Apply the inverse transformation
    obj_mat = obj.matrix_world
    mesh.transform(global_matrix * obj_mat)

    verts = mesh.vertices[:]
    facets = [ f for f in mesh.tessfaces ]

    # Write geometry to file
    filepath = os.fsencode(filepath)
    fp = open(filepath, 'w')

    fp.write('OFF\n')
    fp.write('%d %d 0\n' % (len(verts), len(facets)))

    for vert in verts:
        fp.write('%.16f %.16f %.16f\n' % vert.co[:])

    for facet in facets:
        fp.write('%d' % len(facet.vertices))
        for vid in facet.vertices:
            fp.write(' %d' % vid)
        color = mesh.materials[facet.material_index].diffuse_color
        fp.write(' %d' % (color[0] * 255.0))
        fp.write(' %d' % (color[1] * 255.0))
        fp.write(' %d' % (color[2] * 255.0))
        
        fp.write('\n')

    fp.close()

    return {'FINISHED'}

if __name__ == "__main__":
    register()


