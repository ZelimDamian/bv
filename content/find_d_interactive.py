import bpy
import bmesh
import math
from mathutils import Vector, Matrix

def cross(v1, v2):
    return [ v1[1]*v2[2] - v1[2]*v2[1], \
             v1[2]*v2[0] - v1[0]*v2[2], \
             v1[0]*v2[1] - v1[1]*v2[0] ]

def dot(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
  
def length(v):
    return math.sqrt(dot(v, v))

def distance(v1, v2):
    return length([v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]])

def normalize(v):
    l = length(v)
    return [v[0] / l, v[1] / l, v[2] / l]

def mul(v, s):
    return [v[0] * s, v[1] * s, v[2] * s]

def div(v, s):
    return mul(v, 1/s)

def createPlane(normal, d):
    bpy.ops.mesh.primitive_plane_add(enter_editmode=False, radius = 100.0)
    #rotation=(angles[0], angles[1], angles[2])
    
    ob = bpy.context.object
    ob.name = 'Plane'
    
    initial = [0, 0, 1]

    initial_dot = dot(initial, normal)

    angle = math.acos(initial_dot)
    axis = cross(initial, normal)
    
    ob.matrix_world  = Matrix.Rotation(angle, 4, axis) * \
        Matrix.Translation([0, 0, d])
    
    ob.select = False
    
    return ob

def abs(val):
    return val if val >= 0 else -val

#createMeshFromPrimitive()

normal = normalize([0, 0.65, -1])

threshold = 0.1

verts = bpy.context.active_object.data.vertices

def find_diameter(d):

    onplane = []

    for idx, vert in enumerate(verts):
        v_dot = dot(normal, vert.co)
        dist = abs(v_dot - d)

        vert.select = False

        if dist < threshold:
            onplane.append(vert)

    # trigger viewport update
    bpy.context.scene.objects.active = bpy.context.scene.objects.active

    center = [0, 0, 0]

    for vert in onplane:
        center[0] += vert.co[0]
        center[1] += vert.co[1]
        center[2] += vert.co[2]

    center = div(center, len(onplane))

    avgDist = 0

    for vert in onplane:
        vert.select = True
        dist = distance(vert.co, center)
        avgDist += dist

    avgDist /= len(onplane)

    diameter = avgDist * 2
    
    return diameter


bpy.ops.object.mode_set(mode='EDIT')  
bpy.context.tool_settings.mesh_select_mode = (True, False, False) # force verts  

bpy.ops.object.mode_set(mode='OBJECT')  

plane = createPlane(normal, 50)

def dial_ratio(diameter):
    return diameter / 91.9

min_d = 1
max_d = 59.8
steps = 100

#for i in range(0, steps):
#    d = min_d + (max_d-min_d) * i / steps
#    diameter = find_diameter(d)
#    dial = dial_ratio(diameter)
#    
#    print("%f, %f, %f" % (d, diameter, dial))
#
max_diam = -9999999

def handler(scene = None):
    global max_diam
    global plane
    d = length(plane.location)
    diameter = find_diameter(d)
    if max_diam < diameter:
        max_diam = diameter
        print(diameter)
        print(d)
    
if handler not in bpy.app.handlers.scene_update_pre:
    bpy.app.handlers.scene_update_pre.append(handler)

