import bpy
import bmesh
import math
from mathutils import Vector, Matrix

def cross(v1, v2):
    return [ v1[1]*v2[2] - v1[2]*v2[1], \
             v1[2]*v2[0] - v1[0]*v2[2], \
             v1[0]*v2[1] - v1[1]*v2[0] ]

def dot(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
  
def length(v):
    return math.sqrt(dot(v, v))

def normalize(v):
    l = length(v)
    return [v[0] / l, v[1] / l, v[2] / l]

def createPlane(normal, d):
    bpy.ops.mesh.primitive_plane_add(enter_editmode=False, radius = 100.0)
    #rotation=(angles[0], angles[1], angles[2])
    
    ob = bpy.context.object
    ob.name = 'Plane'
    
    initial = [0, 0, 1]

    initial_dot = dot(initial, normal)

    angle = math.acos(initial_dot)
    axis = cross(initial, normal)
    
    ob.matrix_world  = Matrix.Rotation(angle, 4, axis) * \
        Matrix.Translation([0, 0, d])
    
    ob.select = False
    
    return ob


#createMeshFromPrimitive()


normal = normalize([0, 0.65, -1])

max_ndot = -999999999999
max_dot = -9999999999999
max_idx = 0

for idx, vert in enumerate(bpy.context.active_object.data.vertices):
    v_dot = dot(normal, vert.co)
    v_ndot = dot(normal, normalize(vert.co))
    if v_dot > max_dot:
        max_ndot = v_ndot
        max_dot = v_dot
        max_idx = idx

print(max_dot)

createPlane(normal, max_dot)

bpy.data.objects['Skull'].data.vertices[max_idx].select = True

bpy.ops.object.editmode_toggle()

bpy.ops.object.mode_set(mode='EDIT')  
bpy.context.tool_settings.mesh_select_mode = (True, False, False) # force edges  
