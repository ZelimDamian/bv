
Machine SN, 1816,  Op Hours, 3237 

Job Number, 1,  plate number, 2605
  11.3zs 
   Created with 3ZWorks Version 9.3.3.1 Mar 21, 2014 for 3ZSTUDIO 
    Configuration Id:   1100    Title: Rev 06 - EZS Z25    Slice thickness:   0.0254 mm 
    Started,  08/22/2016, 14:23:21 
     Current, 08/23/2016, 09:55:56 
     Elapsed,      actual,  19:32:35  
     Elapsed,   estimated,  17:35:00 
    Remaining,   estimated,   1:50:00 
    Duration,   estimated,  19:25:00 
    Last Layer Completed, 184,    Total, 244 
    Table Height (inches),  current, 0.268,   start, 0.082 
    Model Height (inches),  current, 0.186 
    Plate ID, 2605, TOF, 0.082000

Printhead Draw Time (hour:min:sec)
    Build,   04:41:44,    total hours, 706,    SN: E100410S 
    Support, 04:12:53,    total hours, 688,    SN: E200747S 

Printhead Failures (pre-layer, post-layer, stoppages)
    Build,     0,   0,   0 
    Support,   0,   0,   0 

Printhead Offsets Calibration (inches)
    Build 
        start,  X, -0.0081,  Y, +0.0315 
        finish, X, -0.0081,  Y, +0.0315
        change, X, +0.0000,  Y, +0.0000
    Support
        start,  X, -0.0152,  Y, -1.2336 
        finish, X, -0.0152,  Y, -1.2336
        change, X, +0.0000,  Y, +0.0000

Pre-Job Printhead Volume Calibration 
    Build Low Volume,  Iterations, 0 
        Target Weight (mg),  0,    Hold Voltage (v),  0 
        Actual Weight (mg),  0,    Fire Voltage (v),  0 
           difference (mg),  0,      difference (v),  0 
    Build High Volume,  Iterations, 0 
        Target Weight (mg),  0,    Hold Voltage (v),  0 
        Actual Weight (mg),  0,    Fire Voltage (v),  0 
           difference (mg),  0,      difference (v),  0 
    Support Low Volume,  Iterations, 0 
        Target Weight (mg),  0,    Hold Voltage (v),  0 
        Actual Weight (mg),  0,    Fire Voltage (v),  0 
           difference (mg),  0,      difference (v),  0 
    Support High Volume,  Iterations, 0 
        Target Weight (mg),  0,    Hold Voltage (v),  0 
        Actual Weight (mg),  0,    Fire Voltage (v),  0 
           difference (mg),  0,      difference (v),  0 

Post-Job Printhead Volume Calibration
    Build Low Volume
        Actual Weight (mg),   0,  difference (mg),   0
    Build High Volume
        Actual Weight (mg),   0,  difference (mg),   0
    Support Low Volume
        Actual Weight (mg),   0,  difference (mg),   0
    Support High Volume
        Actual Weight (mg),   0,  difference (mg),   0

Tank Levels (percent full) 
    start,    Build,  71%,    Support,  66% 
    current,   Build,  72%,    Support,  62% 

Cutter failures 
    Pickup,  0 
    Dropoff, 0 

Thermal out-of-tolerance errors encountered during job, 0

Command errors encountered during job, 0
