import bpy
import bmesh
import math
# this will select every 'even' numbered edge from the list of edges.  
  
# while in object mode, with no edges selected in the underlying object  
obj = bpy.context.active_object  
  
bpy.ops.object.mode_set(mode='EDIT')  
bpy.context.tool_settings.mesh_select_mode = (False, True, False) # force edges  
  
bpy.ops.object.mode_set(mode='OBJECT')  

def length(v):
    return math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z)

shortestId = 0
shortestLen = 999999999

THRESHOLD = 0.0015

# Get a BMesh representation
bm = bmesh.new()   # create an empty BMesh
bm.from_mesh(obj.data)   # fill it in from a Mesh


# Modify the BMesh, can do anything here...

for idx, edge in enumerate(bm.edges):
    start = edge.verts[0].co
    end = edge.verts[1].co
    l = length(end - start)
    if shortestLen > l:
        shortestLen=l
        shortestId = idx

# Finish up, write the bmesh back to the mesh
bm.to_mesh(obj.data)

for idx, edge in enumerate(bm.edges):
    start = edge.verts[0].co
    end = edge.verts[1].co
    l = length(end - start)
    if(l < THRESHOLD):
        obj.data.edges[idx].select = True
        #bmesh.ops.collapse(bm, edges=[bm.edges[idx]])

obj.data.edges[shortestId].select = True

print(shortestId)
print(shortestLen)

bpy.ops.object.editmode_toggle()
