#!/bin/bash
./tools/windows/shadercDebug -i ./include -f ./$1/fs_$1.sc --type f --platform windows -p ps_4_0 -o ./dx11/fs_$1.bin
./tools/windows/shadercDebug -i ./include -f ./$1/vs_$1.sc --type v --platform windows -p vs_4_0 -o ./dx11/vs_$1.bin

./tools/windows/shadercDebug -i ./include -f ./$1/fs_$1.sc --type f --platform windows -p ps_3_0 -o ./dx9/fs_$1.bin
./tools/windows/shadercDebug -i ./include -f ./$1/vs_$1.sc --type v --platform windows -p vs_3_0 -o ./dx9/vs_$1.bin
