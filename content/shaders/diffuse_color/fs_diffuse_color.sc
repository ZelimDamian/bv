$input v_pos, v_normal, v_texcoord0

#include "common.sh"

uniform vec3 uLightPos;
uniform vec4 uColor;

//SAMPLER2D(uDiffTex, 0);

void main()
{
	vec3 lLightPos = vec3(-5.0, -2.0, 3.0);
	vec4 v_color0 = uColor;
	float ndotl = max(0.0, dot(normalize(v_normal), normalize(lLightPos)));
	float spec = pow(ndotl, 10.0);

	vec3 texValue = vec3_splat(0.0); //texture2D(uDiffTex, v_texcoord0).xyz;

	gl_FragColor = vec4(vec3_splat(texValue.x) + v_color0.xyz * (ndotl + spec), 1.0) * 0.9 + /* ambient */ vec4(0.2, 0.2, 0.2, 1.0);
}
