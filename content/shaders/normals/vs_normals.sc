$input a_position, a_normal, a_texcoord0
$output v_pos, v_normal, v_texcoord0

#include "common.sh"

uniform mat4 u_NormalMatrix;

void main()
{
	gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0));
	v_normal = vec3(mul(u_NormalMatrix, vec4(a_normal, 1.0)));
}
