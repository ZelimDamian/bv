$input v_pos, v_normal, v_texcoord0

#include "common.sh"

void main()
{
	gl_FragColor = vec4(v_normal, 1.0f);
}
