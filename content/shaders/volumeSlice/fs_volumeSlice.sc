$input v_pos, v_normal, v_texcoord0

#include "common.sh"

uniform vec3 lightDir;
uniform float uSlice;

SAMPLER3D(uVolTex, 0);

void main()
{
	float ndotl = max(0.0, dot(normalize(v_normal), lightDir));
	float spec = pow(ndotl, 30.0);

	// sample the texture with the coords at the slice we want
	float texValue = texture3D(uVolTex, vec3(v_texcoord0, uSlice)).x;

	gl_FragColor = vec4(vec3_splat(texValue), texValue);
}
