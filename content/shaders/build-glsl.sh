#!/bin/bash

echo "Building $1 on $(uname)"

if [ "$(uname)" == "Darwin" ]; then
    ./tools/osx/shadercDebug -i ./include -f ./$1/fs_$1.sc -o ./glsl/fs_$1.bin --type f --platform osx
    ./tools/osx/shadercDebug -i ./include -f ./$1/vs_$1.sc -o ./glsl/vs_$1.bin --type v --platform osx
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    echo 'Not supported'
elif [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    ./tools/windows/shadercDebug.exe -i ./include -f ./$1/fs_$1.sc -o ./glsl/fs_$1.bin --type f --platform osx
    ./tools/windows/shadercDebug.exe -i ./include -f ./$1/vs_$1.sc -o ./glsl/vs_$1.bin --type v --platform osx
fi
