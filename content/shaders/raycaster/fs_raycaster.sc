$input vUV

#include "common.sh"

SAMPLER3D(uVolumeTex, 0);
SAMPLER2D(uMapTex, 1);

uniform vec3 uEyePos;
uniform vec3 uStepSize;

uniform vec3 uCutMin;
uniform vec3 uCutMax;

//constants
//const int MAX_SAMPLES = 300;	//total samples for each ray march step

#define STEPS 600

void main()
{
    vec3 texMin = uCutMin;//vec3(0.0, 0.0, 0.0);	//minimum texture access coordinate
    vec3 texMax = uCutMax;//vec3(1.0, 1.0, 1.0);	//maximum texture access coordinate

    vec3 dataPos = vUV * 0.99;
    vec3 fromEye = normalize(vUV - vec3_splat(0.5) - uEyePos);
    vec3 rayStep = fromEye / float(STEPS);

    // holder for the source value
    vec4 src = vec4_splat(0.0);

//    gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);

    //gl_FragColor = vec4(geomDir + rayStep * 200, 1.0);
    //return;

    float sample;
    float prev_alpha;

    //bool stop = false;
    int i = 0;
    while ( ++i < STEPS )
	{
        // advance ray
        dataPos = dataPos + rayStep;

        if(dot(sign(dataPos-texMin),sign(texMax-dataPos)) > 3.0)
        {
            return;
        }

        // data fetching from the red channel of volume texture
        #if BGFX_SHADER_LANGUAGE_HLSL
            sample = texture3DLod(uVolumeTex, dataPos, 0).r;
            //sample*= texture2DLod(uMapTex, vec2(sample * 8, 0), 0).r;
        #else
            sample = texture3D(uVolumeTex, dataPos).r;
            //sample*= texture2D(uMapTex, vec2(sample, 0)).r;
        #endif // BGFX_SHADER_LANGUAGE_HLSL

//        prev_alpha = sample - (sample * gl_FragColor.a);
//        gl_FragColor.rgb = prev_alpha * (vec3)(sample) + gl_FragColor.rgb;
//        gl_FragColor.a += prev_alpha;

        if(sample == 0.0)
        {
            continue;
        }

        //Front to back blending
        // dst.rgb = dst.rgb + (1 - dst.a) * src.a * src.rgb
        // dst.a   = dst.a   + (1 - dst.a) * src.a
        src = vec4_splat(sample);
        src.rgb *= src.a;
        gl_FragColor += src * (1.0 - gl_FragColor.a);

        //early ray termination
        //if the currently composited colour alpha is already fully saturated
        //we terminated the loop
        if( gl_FragColor.a >= 1.0)
        {
            return;
        }
    }
}
