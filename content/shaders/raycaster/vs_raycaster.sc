$input a_position
$output vUV

#include "common.sh"

uniform vec3 uCutMin;
uniform vec3 uCutMax;

void main()
{
	vec3 clampedPos = max(min(a_position, uCutMax - vec3_splat(0.5)), uCutMin - vec3_splat(0.5));
	gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0) );
	vUV = clampedPos + vec3(0.5, 0.5, 0.5);
}
