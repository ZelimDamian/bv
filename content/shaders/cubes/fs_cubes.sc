$input v_normal, v_color0

#include "common.sh"

uniform vec3 lightDir;

void main()
{
	vec4 col = v_color0;
	float ndotl = max(0.0, dot(normalize(v_normal), lightDir));
	float spec = pow(ndotl, 50.0);
	gl_FragColor = vec4(col.xyz, 1.0);// + vec4(vec3_splat(ndotl + spec), 1.0) + /* ambient */ vec4(0.3, 0.3, 0.3, 1.0);
}
