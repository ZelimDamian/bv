$input v_pos, v_normal, v_color0, v_lightDir

#include "common.sh"

void main()
{
	float ndotl = max(0.0, dot(normalize(v_normal), v_lightDir));
	float spec = pow(ndotl, 30.0);

	gl_FragColor = vec4(v_color0.xyz + vec3_splat(ndotl * 0.5), 1.0) + vec4(0.1, 0.1, 0.1, 1.0);
	gl_FragColor = v_color0;
//	gl_FragColor = vec4(vec3_splat(ndotl), 1.0);
}
