$input a_position, a_normal, a_color0
$output v_pos, v_normal, v_color0, v_lightDir

#include "common.sh"

uniform vec3 lightPos;

void main()
{
	gl_Position = mul(u_modelViewProj, vec4(a_position, 1.0) );
	v_pos = a_position;
	v_normal = a_normal;
	v_color0 = a_color0;
	v_lightDir = normalize(lightPos - a_position);
}
