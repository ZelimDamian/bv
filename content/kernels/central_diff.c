#ifndef __OPENCL_VERSION__
typedef struct { float x, y, z, w; } float4;
#define __global
#endif

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void central_diff(
        __global const  float* restrict m_ABC,
        __global const  float* restrict m_U,
        __global const  float* restrict m_U_old,
        __global        float* restrict m_U_new,
        __global const  float* restrict m_F,
        __global const  float* restrict m_Fx,
        __global const  float* restrict m_R,
        int m_maxForces)
{
    size_t nodeIndex = get_global_id(0);

    size_t dofIndex = nodeIndex * 3;


	// U at t
    const float u[3] = {
            m_U[dofIndex + 0],
            m_U[dofIndex + 1],
            m_U[dofIndex + 2]
    };

	// U at t-1
    const float u_old[3] = {
            m_U_old[dofIndex + 0],
            m_U_old[dofIndex + 1],
            m_U_old[dofIndex + 2]
    };

	float f[3] = {
			m_R[dofIndex + 0] - m_F[dofIndex + 0],
			m_R[dofIndex + 1] - m_F[dofIndex + 1],
			m_R[dofIndex + 2] - m_F[dofIndex + 2]
	};

    const float A = m_ABC[dofIndex + 0];
    const float B = m_ABC[dofIndex + 1];
    const float C = m_ABC[dofIndex + 2];

    // calculate the new update value: U at t+1
    m_U_new[dofIndex + 0] = A * f[0] + B * u[0] + C * u_old[0];
    m_U_new[dofIndex + 1] = A * f[1] + B * u[1] + C * u_old[1];
    m_U_new[dofIndex + 2] = A * f[2] + B * u[2] + C * u_old[2];
}