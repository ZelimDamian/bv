#ifndef __OPENCL_VERSION__
    typedef struct { float x, y, z, w; } float4;
    #define __global
#endif

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void central_diff_collect(
        __global const  float4* restrict m_ABC,

        __global const  float4* restrict m_U,
        __global const  float4* restrict m_U_old,
        __global        float4* restrict m_U_new,

        __global        float4* restrict m_F,
        __global const  float4* restrict m_Fx,

        __global const  float4* restrict m_R,

        __global const  int*    restrict m_numForces,
        __global const  int*    restrict m_aggregateForces,
        int m_maxForces)
{
    const size_t nodeIndex = get_global_id(0);

    // U at t
    const float4 u = m_U[nodeIndex];

    // U at t-1
    const float4 u_old = m_U_old[nodeIndex];

    // force accumulator
    float4 f = (float4)(0.0f);

    // number of forces to collect
	const int numForces = min(m_numForces[nodeIndex], m_maxForces);
    for (int i = 0; i < numForces; ++i)
//    for (int i = 0; i < m_maxForces; ++i)
    {
        const int xdofIndex = m_aggregateForces[nodeIndex] + i;
        f += m_Fx[xdofIndex];
    }

	m_F[nodeIndex] = f;

    f = m_R[nodeIndex] - f;

    const float A = m_ABC[nodeIndex].x;
    const float B = m_ABC[nodeIndex].y;
    const float C = m_ABC[nodeIndex].z;

    // calculate the new update value: U at t+1
    if(!any(isnan(f)))
    {
        m_U_new[nodeIndex] = A * f + B * u + C * u_old;
    }
}
