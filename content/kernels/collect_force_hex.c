#ifndef __OPENCL_VERSION__
typedef struct { float x, y, z, w; } float4;
#define __global
#endif

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void collect_force_hex(
		__global float* m_Fx,
		__global float* m_F)
{
	int nodeIndex = get_global_id(0);
	int xnodeIndex = nodeIndex * 8;

	float f[3] = { 0.0f };
	// Accumulate force
	for (int i = 0; i < 8; i++)
	{
		const int xdofIndex = (xnodeIndex + i) * 3;
		f[0] += m_Fx[xdofIndex + 0];
		f[1] += m_Fx[xdofIndex + 1];
		f[2] += m_Fx[xdofIndex + 2];
	}

	int dofIndex = nodeIndex * 3;
	// Save the accumulated force into the buffer
	m_F[dofIndex + 0] = f[0];
	m_F[dofIndex + 1] = f[1];
	m_F[dofIndex + 2] = f[2];
}
