#ifndef __OPENCL_VERSION__
    typedef struct { float x, y, z, w; } float4;
    #define __global
#else

    #ifdef cl_khr_fp64
        #pragma OPENCL EXTENSION cl_khr_fp64 : enable
    #elif defined(cl_amd_fp64)
        #pragma OPENCL EXTENSION cl_amd_fp64 : enable
    #else
        //#error "Double precision floating point not supported by OpenCL implementation."
        #define double float
    #endif

#endif

inline void memset(float* restrict pDst, const float value, const size_t size)
{
    for(int i = 0; i < size; i++)
    {
        pDst[i] = value;
    }
}

inline void memcpy(float* restrict pDst/*private*/, const size_t size, const __global float* restrict pSrc)
{
    for(int i = 0; i < size; i++)
    {
        pDst[i] = pSrc[i];
    }
}

// Multiply by a scalar
inline void p_multAs(float* restrict A, size_t size, float s, float* restrict C)
{
    //const float * end = A + size;

    for(int i = 0; i < size; i++)
    {
        C[i] = A[i] * s;
    }
}

// Multiply two matrices
//inline void p_multAB(const float* restrict A, size_t colsA, size_t rowsA, const float* restrict B, size_t colsB, size_t rowsB, float* restrict C)
#define p_multAB(A, colsA, rowsA, B, colsB, rowsB, C)                       \
{                                                                           \
    for (int i = 0; i < rowsA; i++)                                         \
    {                                                                       \
        for (int j = 0; j < colsB; j++)                                     \
        {                                                                   \
			float c = 0.0f;                                                 \
            for (int k = 0; k < rowsB; k++)                                 \
            {                                                               \
                c += A[i * colsA + k] * B[k * colsB + j];                   \
            }                                                               \
			C[i * colsB + j] = c;                                           \
        }                                                                   \
    }                                                                       \
}

//inline void p_multATB(const float* restrict A, size_t colsA, size_t rowsA, const float* restrict B, size_t colsB, size_t rowsB, float* restrict C)
#define p_multATB(A, colsA, rowsA, B, colsB, rowsB, C)                      \
{                                                                           \
    for (int i = 0; i < colsA; i++)                                         \
    {                                                                       \
        for (int j = 0; j < colsB; j++)                                     \
        {                                                                   \
			float c = 0.0f;                                                 \
            for (int k = 0; k < rowsB; k++)                                 \
            {                                                               \
                c += A[k * colsA + i] * B[k * colsB + j];                   \
            }                                                               \
			C[i * colsB + j] = c;                                           \
        }                                                                   \
    }                                                                       \
}

//inline void p_multABT(const float* restrict A, size_t colsA, size_t rowsA, const float* restrict B, size_t colsB, size_t rowsB, float* C)
#define p_multABT(A, colsA, rowsA, B, colsB, rowsB, C)                       \
{                                                                            \
    for (int i = 0; i < rowsA; i++)                                          \
    {                                                                        \
        for (int j = 0; j < rowsB; j++)                                      \
        {                                                                    \
			float c = 0.0f;                                                  \
            for (int k = 0; k < colsA; k++)                                  \
            {                                                                \
                c += A[i * colsA + k] * B[j * colsB + k];                    \
            }                                                                \
			C[i * rowsB + j] = c;                                            \
        }                                                                    \
    }                                                                        \
}

inline void calculateStressNH(const float* restrict X,
                       float M, float K, float* restrict SPK)
{
	// Transpose of deformation gradient
	//XT11 = X[0][0]; XT12 = X[1][0]; XT13 = X[2][0];
#define XT11 X[0]
#define XT12 X[3]
#define XT13 X[6]
	//XT21 = X[0][1]; XT22 = X[1][1]; XT23 = X[2][1];
#define XT21 X[1]
#define XT22 X[4]
#define XT23 X[7]
	//XT31 = X[0][2]; XT32 = X[1][2]; XT33 = X[2][2];
#define XT31 X[2]
#define XT32 X[5]
#define XT33 X[8]

	// Right Cauchy-Green deformation tensor
	const float C11 = XT11 * XT11 + XT12 * XT12 + XT13 * XT13;
	const float C12 = XT11 * XT21 + XT12 * XT22 + XT13 * XT23;
	const float C13 = XT11 * XT31 + XT12 * XT32 + XT13 * XT33;
	const float C22 = XT21 * XT21 + XT22 * XT22 + XT23 * XT23;
	const float C23 = XT21 * XT31 + XT22 * XT32 + XT23 * XT33;
	const float C33 = XT31 * XT31 + XT32 * XT32 + XT33 * XT33;

	// Determinant of deformation gradient
	const float J = XT11 * (XT22 * XT33 - XT32 * XT23) + XT12 * (XT23 * XT31 - XT21 * XT33) +
	                XT13 * (XT21 * XT32 - XT22 * XT31);
	// Determinant of C
	const float detC = C11 * (C22 * C33 - C23 * C23) - C22 * C13 * C13 - C12 * (C33 * C12 - 2 * C13 * C23);

	float x1 = (float) pow((double) J, -2.0 / 3.0) * M;
	float x2 = (K * J * (J - 1) - x1 * (C11 + C22 + C33) / 3) / detC;

#define EPSILON                     (0.000001f)

	if (fabs(J) <= EPSILON)
	{
		x1 = 0;
	}
	if (fabs(detC) <= EPSILON)
	{
    	x2 = 0;
    }

    // 2nd Piola-Kirchhoff stress
    //SPK[0][0] = (C22*C33 - C23*C23)*x2 + x1; // S00
    SPK[0] = (C22*C33 - C23*C23)*x2 + x1; // S00
    //SPK[1][1] = (C11*C33 - C13*C13)*x2 + x1; // S11
    SPK[4] = (C11*C33 - C13*C13)*x2 + x1; // S11
    //SPK[2][2] = (C11*C22 - C12*C12)*x2 + x1; // S22
    SPK[8] = (C11*C22 - C12*C12)*x2 + x1; // S22
    //SPK[0][1] = (C13*C23 - C12*C33)*x2; // S01
    SPK[1] = (C13*C23 - C12*C33)*x2; // S01
    //SPK[1][0] = SPK[0][1];
    SPK[3] = SPK[1];
    //SPK[1][2] = (C12*C13 - C23*C11)*x2; // S12
    SPK[5] = (C12*C13 - C23*C11)*x2; // S12
    //SPK[2][1] = SPK[1][2];
    SPK[7] = SPK[5];
    //SPK[0][2] = (C12*C23 - C13*C22)*x2; // S02
    SPK[2] = (C12*C23 - C13*C22)*x2; // S02
    //SPK[2][0] = SPK[0][2];
    SPK[6] = SPK[2];
}

#define NUM_NODES 4
#define NUM_DOFS NUM_NODES * 3
#define MAX_MAPARAMS 16

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void tled_tet(
        __global const  float * restrict m_V,
        __global const  float * restrict m_DhDx,
        __global const  float4* restrict m_U,
        __global        float4* restrict m_Fx,
        __global const  int*    restrict m_nodeInds,
        __global const  float * restrict m_matParams,
        __global const  int*    restrict m_forceIndices,
        __global        float * restrict m_VMS,
        __global const  int*    restrict m_materialInds
)
{
	const size_t elIndex = get_global_id(0);

	float u[NUM_DOFS];//(3, 4); u.fillup();

	for (int i = 0; i < NUM_NODES; i++)
	{
		const int j = m_nodeInds[elIndex * NUM_NODES + i];

		u[i * 3 + 0] = m_U[j].x;
		u[i * 3 + 1] = m_U[j].y;
		u[i * 3 + 2] = m_U[j].z;
	}

	// Displacement derivs
	float DhDx[NUM_DOFS];
	memcpy(DhDx, NUM_DOFS, m_DhDx + elIndex * NUM_DOFS);//m_DhDx + elIndex * 12; //, 3, 4);
	float X[9];//(3, 3); X.fillup(); //= *Matrix::view(m_X, elIndex * 9, 3, 3);

	//FeaMatrixRoutines::mult(&u, DhDx, &X, true, false);
	p_multATB(u, 3, NUM_NODES, DhDx, 3, NUM_NODES, X);

	// Deformation gradient: X = DuDx + I
	X[0] += 1; X[4] += 1; X[8] += 1;	// X is now def. grad.

	//Matrix SPK = Matrix(3, 3); SPK.fillup(0.0f);
	float SPK[9];

	//calculateStressNH(X.data<glm::vec3>(), elm->material()->params(), SPK.data<glm::vec3>());
    const float M = m_matParams[m_materialInds[elIndex] * MAX_MAPARAMS + 1];
    const float K = m_matParams[m_materialInds[elIndex] * MAX_MAPARAMS + 2];
	calculateStressNH(X, M, K, SPK);

	// Compute element nodal forces: Fe = V*X*S*dh'
	float temp[9];
	p_multAB(X, 3, 3, SPK, 3, 3, temp);

	float V = m_V[elIndex];
	p_multAs(temp, 9, V, temp);

	// element forces
	float Fe[NUM_DOFS];
	p_multABT(temp, 3, 3, DhDx, 3, NUM_NODES, Fe);

	// Add element forces to global forces
	for (int j = 0; j < NUM_NODES; j++)
	{
		const int forceIndex = /*elNodeOffset + */ m_forceIndices[elIndex * NUM_NODES + j];

		m_Fx[forceIndex] = (float4)(Fe[NUM_NODES * 0 + j], Fe[NUM_NODES * 1 + j], Fe[NUM_NODES * 2 + j], 0.0f);
	}

	/* von Mises stress */
	const float VMS = sqrt( SPK[0]*SPK[0] + SPK[4]*SPK[4] + SPK[8]*SPK[8]
	             - SPK[0]*SPK[4] - SPK[4]*SPK[8] - SPK[0]*SPK[8]
	             + 3*(SPK[1]*SPK[1] + SPK[5]*SPK[5] + SPK[2]*SPK[2]) );

	m_VMS[elIndex] = VMS;
}