#ifndef __OPENCL_VERSION__
typedef struct { float x, y, z, w; } float4;
#define __global
#endif

void memset(float* pDst, const float value, const size_t size)
{
    for(int i = 0; i < size; i++)
    {
        pDst[i] = value;
    }
}

void memcpy(float* pDst/*private*/, const size_t size, __global float* pSrc)
{
    for(int i = 0; i < size; i++)
    {
        pDst[i] = pSrc[i];
    }
}

// Multiply by a scalar
void p_multAs(float* A, size_t size, float s, float* C)
{
    //const float * end = A + size;

    for(int i = 0; i < size; i++)
    {
        C[i] = A[i] * s;
    }
}

// Multiply two matrices
void p_multAB(const float* A, size_t colsA, size_t rowsA, const float* B, size_t colsB, size_t rowsB, float* C)
{
    memset(C, 0, rowsA * colsB);
    for (int i = 0; i < rowsA; i++)
    {
        for (int j = 0; j < colsB; j++)
        {
            for (int k = 0; k < rowsB; k++)
            {
                C[i * colsB + j] += A[i * colsA + k] * B[k * colsB + j];
            }
        }
    }
}

void p_multATB(const float* A, size_t colsA, size_t rowsA, const float* B, size_t colsB, size_t rowsB, float* C)
{
    memset(C, 0, colsA * colsB);
    for (int i = 0; i < colsA; i++)
    {
        for (int j = 0; j < colsB; j++)
        {
            for (int k = 0; k < rowsB; k++)
            {
                C[i * colsB + j] += A[k * colsA + i] * B[k * colsB + j];
            }
        }
    }
}

void p_multABT(const float* A, size_t colsA, size_t rowsA, const float* B, size_t colsB, size_t rowsB, float* C)
{
    memset(C, 0, rowsA * rowsB);
    for (int i = 0; i < rowsA; i++)
    {
        for (int j = 0; j < rowsB; j++)
        {
            for (int k = 0; k < colsA; k++)
            {
                C[i * rowsB + j] += A[i * colsA + k] * B[j * colsB + k];
            }
        }
    }
}

void calculateStressNH(const float* X,
                       __global const float* restrict materialParams, float* restrict SPK)
{
    float XT11, XT12, XT13, XT21, XT22, XT23, XT31, XT32, XT33;
    float C11, C12, C13, C22, C23, C33;

    // Transpose of deformation gradient
    //XT11 = X[0][0]; XT12 = X[1][0]; XT13 = X[2][0];
    XT11 = X[0]; XT12 = X[3]; XT13 = X[6];
    //XT21 = X[0][1]; XT22 = X[1][1]; XT23 = X[2][1];
    XT21 = X[1]; XT22 = X[4]; XT23 = X[7];
    //XT31 = X[0][2]; XT32 = X[1][2]; XT33 = X[2][2];
    XT31 = X[2]; XT32 = X[5]; XT33 = X[8];

    // Right Cauchy-Green deformation tensor
    C11 = XT11*XT11 + XT12*XT12 + XT13*XT13;
    C12 = XT11*XT21 + XT12*XT22 + XT13*XT23;
    C13 = XT11*XT31 + XT12*XT32 + XT13*XT33;
    C22 = XT21*XT21 + XT22*XT22 + XT23*XT23;
    C23 = XT21*XT31 + XT22*XT32 + XT23*XT33;
    C33 = XT31*XT31 + XT32*XT32 + XT33*XT33;

    // Determinant of deformation gradient
    float J = XT11*(XT22*XT33 - XT32*XT23) + XT12*(XT23*XT31 - XT21*XT33) + XT13*(XT21*XT32 - XT22*XT31);
    // Determinant of C
    float detC = C11*(C22*C33 - C23*C23) - C22*C13*C13 - C12*(C33*C12 - 2 * C13*C23);

    float M = materialParams[1];
    float K = materialParams[2];

    float x1 = (float)pow(J, -2.0f / 3.0f) * M;
    float x2 = (K*J*(J - 1) - x1*(C11 + C22 + C33) / 3) / detC;

    //if (J == 0.0f || detC == 0.0f)
    //{
    //	x1 = 0;
    //	x2 = 0;
    //}

    // 2nd Piola-Kirchhoff stress
    //SPK[0][0] = (C22*C33 - C23*C23)*x2 + x1; // S00
    SPK[0] = (C22*C33 - C23*C23)*x2 + x1; // S00
    //SPK[1][1] = (C11*C33 - C13*C13)*x2 + x1; // S11
    SPK[4] = (C11*C33 - C13*C13)*x2 + x1; // S11
    //SPK[2][2] = (C11*C22 - C12*C12)*x2 + x1; // S22
    SPK[8] = (C11*C22 - C12*C12)*x2 + x1; // S22
    //SPK[0][1] = (C13*C23 - C12*C33)*x2; // S01
    SPK[1] = (C13*C23 - C12*C33)*x2; // S01
    //SPK[1][0] = SPK[0][1];
    SPK[3] = SPK[1];
    //SPK[1][2] = (C12*C13 - C23*C11)*x2; // S12
    SPK[5] = (C12*C13 - C23*C11)*x2; // S12
    //SPK[2][1] = SPK[1][2];
    SPK[7] = SPK[5];
    //SPK[0][2] = (C12*C23 - C13*C22)*x2; // S02
    SPK[2] = (C12*C23 - C13*C22)*x2; // S02
    //SPK[2][0] = SPK[0][2];
    SPK[6] = SPK[2];
}

#define NUM_NODES 8
#define NUM_DOFS NUM_NODES * 3

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void tled_hex(
        __global float4* m_detJ,
        __global float4* m_DhDx,
        __global float4* U,
        __global float4* F,
        __global int* m_nodeInds,
        __global float4* matParams
)
{
    size_t elIndex = get_global_id(0);

    float u[NUM_DOFS];//(3, 8); u.fillup();

    for (int i = 0; i < NUM_NODES; i++)
    {
        int j = m_nodeInds[elIndex * NUM_NODES + i];

		u[i * 3 + 0] = U[j * 3 + 0];
		u[i * 3 + 1] = U[j * 3 + 1];
		u[i * 3 + 2] = U[j * 3 + 2];
    }
    // Displacement derivs
    float DhDx[NUM_DOFS];
    memcpy(DhDx, NUM_DOFS, m_DhDx + elIndex * NUM_DOFS);//m_DhDx + elIndex * 24; //, 3, 8);
    float X[9];//(3, 3); X.fillup(); //= *Matrix::view(m_X, elIndex * 9, 3, 3);

    //FeaMatrixRoutines::mult(&u, DhDx, &X, true, false);
    p_multATB(u, 3, NUM_NODES, DhDx, 3, NUM_NODES, X);

    // Deformation gradient: X = DuDx + I
    X[0] += 1; X[4] += 1; X[8] += 1;	// X is now def. grad.

    //Matrix SPK = Matrix(3, 3); SPK.fillup(0.0f);
    float SPK[9];

    //calculateStressNH(X.data<glm::vec3>(), elm->material()->params(), SPK.data<glm::vec3>());
    calculateStressNH(X, matParams, SPK);

    float V = m_detJ[elIndex] * 8;

    // Compute element nodal forces: Fe = 8*detJ*X*S*dh'
    float temp[9];
    //FeaMatrixRoutines::mult(&X, &SPK, &temp);
    p_multAB(X, 3, 3, SPK, 3, 3, temp);
    p_multAs(temp, 9, V, temp);

    // element forces
    //Matrix Fe(8, 3);
    float Fe[NUM_DOFS];
    //FeaMatrixRoutines::mult(&temp, DhDx, &Fe, false, true);
    p_multABT(temp, 3, 3, DhDx, 3, NUM_NODES, Fe);

    // Add element forces to global forces
    for (int i = 0; i < NUM_NODES; i++)
    {
        const int nodeIndex = m_nodeInds[elIndex * NUM_NODES + i];
	    const int FxIndex = nodeIndex * NUM_NODES + i;
	    const int xdofIndex = FxIndex * 3;

	    F[xdofIndex + 0] = Fe[NUM_NODES * 0 + i];
	    F[xdofIndex + 1] = Fe[NUM_NODES * 1 + i];
	    F[xdofIndex + 2] = Fe[NUM_NODES * 2 + i];
    }
}