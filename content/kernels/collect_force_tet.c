#ifndef __OPENCL_VERSION__
typedef struct { float x, y, z, w; } float4;
#define __global
#endif

//
///**********************************************************************
//Copyright University of East Anglia.
//********************************************************************/
//__kernel void collect_force_tet(
//		__global float* m_Fx,
//		__global float* m_F,
//		const int m_maxForces)
//{
//	int dofIndex = get_global_id(0);
//
//	float f = { 0.0f };
//
//	for (int i = 0; i < m_maxForces; ++i)
//	{
//		const int xdofIndex = dofIndex * m_maxForces + i * 3;
//		f += m_Fx[xdofIndex + 0];
//	}
//
//	// Save the accumulated force into the buffer
//	m_F[dofIndex] = f;
//}


/*********************************************************************
Copyright University of East Anglia
********************************************************************/
__kernel void collect_force_tet(
		__global float* m_Fx,
		__global float* m_F,
		__global int* m_numForces,
        const int m_maxForces)
{
	int nodeIndex = get_global_id(0);

	float f[3] = { 0.0f };

//	const int numForces = m_numForces[nodeIndex];
//    for (int i = 0; i < numForces; ++i)
    for (int i = 0; i < m_maxForces; ++i)
    {
        const int xdofIndex = (nodeIndex * m_maxForces + i) * 3;
        f[0] += m_Fx[xdofIndex + 0];
        f[1] += m_Fx[xdofIndex + 1];
        f[2] += m_Fx[xdofIndex + 2];
    }

	// Save the accumulated force into the buffer
	m_F[nodeIndex * 3 + 0] = f[0];
	m_F[nodeIndex * 3 + 1] = f[1];
	m_F[nodeIndex * 3 + 2] = f[2];
}
