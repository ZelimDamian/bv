#ifndef __OPENCL_VERSION__
	typedef struct { float x, y, z, w; } float4;
	#define __global
#endif

enum LoadShape
{
	NONE = 0,
	FIX,
	INSTANT,
	LINEAR,
	LINEAR_INSTANT,
	FORCE,
	PLANE,
	SPRING
};

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void apply_pre_constraints(
		__global       float4* restrict m_U_new,
		__global const  int* restrict m_constraintFlags,
		__global const  float4* restrict m_constraintMagnitude,
		__global        float* restrict m_M,
		__global        float4* restrict m_R,
		const float G0,
		const float G1,
		const float G2)
{
	const size_t index = get_global_id(0);
	const int constraint_flag = m_constraintFlags[index];

    // node mass
    const float m = m_M[index];

	// apply a force equalt to node's weight
	float3 force = (float3)(G0, G1, G2) * m;

	if(constraint_flag == SPRING)
	{
		const float4 node = m_U_new[index];
		const float4 end = m_constraintMagnitude[index];

		const float3 x = end.xyz - node.xyz;
		const float k = end.w;

		// simple linear spring force
//		force += x * k;
	}

	m_R[index] += (float4)(force, 0.0f);
}
