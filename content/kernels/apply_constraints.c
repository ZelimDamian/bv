/**********************************************************************
Copyright University of East Anglia.
********************************************************************/

#ifndef __OPENCL_VERSION__
    typedef struct { float x, y, z, w; } float4;
    #define __global
#endif

enum LoadShape
{
	NONE = 0,
	FIX,
	INSTANT,
	LINEAR,
	LINEAR_INSTANT,
	FORCE,
	PLANE,
	SPRING
};

__kernel void apply_constraints(
        __global const int* restrict m_constraintFlags,
        __global const float4* restrict m_constraintMagnitude,
        __global       float4* restrict m_U_new,
        __global       float4* restrict m_U,
        __global       float4* restrict m_U_old,
        __global       float4* restrict m_R,
		const float subFraction)
{
	const size_t nodeIndex = get_global_id(0);

	const int constraint_flag = m_constraintFlags[nodeIndex];

    if(constraint_flag == FIX)
    {
        m_U_new[nodeIndex] = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
    }
	else if(constraint_flag == INSTANT)// || constraint_flag == LINEAR_INSTANT)
    {
	    m_U_new[nodeIndex] = m_U[nodeIndex] + m_constraintMagnitude[nodeIndex] * subFraction;
    }
	else if(constraint_flag == LINEAR || constraint_flag == LINEAR_INSTANT)
    {
        m_U_new[nodeIndex] = m_constraintMagnitude[nodeIndex];
    }
    else if(constraint_flag == PLANE)
    {
        float3 u = m_U_new[nodeIndex].xyz;

        // plane equation for the projection
		float4 plane = m_constraintMagnitude[nodeIndex];

	    // d param for the node
	    float d_u = dot(u, plane.xyz);

	    // penetration = d_p - d_x;
	    float penetration = plane.w - d_u;

	    // check if node is in the negative half-space relative to plane
	    if(penetration > 0.0f)
	    {
			// project onto plane
		    float3 proj = u + plane.xyz * penetration;
            m_U_new[nodeIndex] = (float4)(proj, 0.0f);
		    m_U[nodeIndex] = m_U_new[nodeIndex];
	    }
    }
	else if(constraint_flag == SPRING)
	{
		const float4 node = m_U_new[nodeIndex];
		const float4 end = m_constraintMagnitude[nodeIndex];

		const float3 x = end.xyz - node.xyz;
		const float k = end.w;

		// simple linear spring force
		m_R[nodeIndex] = (float4)(x * k, 0.0f);
	}
}
