#ifndef __OPENCL_VERSION__
    typedef struct { float x, y, z, w; } float4;
    #define __global
#endif

/**********************************************************************
Copyright University of East Anglia.
********************************************************************/
__kernel void verlet(
        __global const  float4* restrict m_ABC,

        __global const  float4* restrict m_U,
        __global const  float4* restrict m_U_old,
        __global        float4* restrict m_U_new,

        __global        float4* restrict m_F,
        __global const  float4* restrict m_Fx,

        __global const  float4* restrict m_R,

        __global const  int*    restrict m_numForces,
        int m_maxForces)
{
    size_t nodeIndex = get_global_id(0);

	// U at t
    const float4 u = m_U[nodeIndex];

	// U at t-1
    const float4 u_old = m_U_old[nodeIndex];

    // force accumulator
    float4 f = (float4)(0.0f);

    // number of forces to collect
	const int numForces = min(m_numForces[nodeIndex], m_maxForces);
    for (int i = 0; i < numForces; ++i)
//    for (int i = 0; i < m_maxForces; ++i)
    {
        const int xdofIndex = nodeIndex * m_maxForces + i;
        f += m_Fx[xdofIndex];
    }

    m_F[nodeIndex] = f;

    f = m_R[nodeIndex] - f;

	const float dt2_m = m_ABC[nodeIndex].x;
	const float dmp = m_ABC[nodeIndex].y;

    m_U_new[nodeIndex] = u + (u - u_old) * dmp + f * dt2_m;

//    const float A = m_A[nodeIndex];
//    const float B = m_B[nodeIndex];
//    const float C = m_C[nodeIndex];
//
//    // calculate the new update value: U at t+1
//    m_U_new[dofIndex + 0] = A * f[0] + B * u[0] + C * u_old[0];
//    m_U_new[dofIndex + 1] = A * f[1] + B * u[1] + C * u_old[1];
//    m_U_new[dofIndex + 2] = A * f[2] + B * u[2] + C * u_old[2];
}