#include "PlanetComponent.h"

#include "PlanetMesher.h"
#include <RenderMeshComponent.h>

using namespace bv;
using namespace planets;

static std::vector<Mesh::Id> _meshes;

void PlanetComponent::initialize()
{
    auto mesh = PlanetMesher::instance()->mesh(m_planet);
    mesh->updatePositions();
    mesh->setupConnections();
    mesh->updateAllNormals();
    mesh->updateIndices();

    _meshes.push_back(mesh);

    // create renderable to draw the mesh of the planet with a diffuse shader
    m_entity->require<RenderMeshComponent>(mesh, "diffuse");
}

void PlanetComponent::update()
{
//    PlanetMesher::instance()->mesh(m_planet, _meshes[(int)m_id]);
}
