#include "PatchSphere.h"

#include "Tri.h"
#include "Sphere.h"

using namespace bv;
using namespace bv::planets;

glm::vec3 samplePos(float radius, float u, float v)
{
    glm::vec3 vec { glm::sin(v) * glm::cos(u), glm::sin(v) * glm::sin(u), glm::cos(v)};
    return vec * radius;
}

glm::vec3 sampleNormal(float radius, float u, float v)
{
    return samplePos(radius, u, v) / radius;
}

glm::vec3 PatchSphere::sample(const glm::vec2& coords) const
{
    return samplePos(m_radius, coords.x, coords.y);
}


#define MAX_LEVEL 9

bool PatchTree::shouldSplit() const
{
    return level < MAX_LEVEL;
}

void PatchTree::split()
{
    if(!shouldSplit()) return;

    children.resize(4);

    auto halfsize = (patch.bounds[1] - patch.bounds[0]) / 2.0f;
    auto offset = patch.bounds[0];

    for (int i = 0; i < 4; ++i)
    {
        children[i].level = level + 1;
        children[i].patch.bounds[0].x = offset.x + (i % 2) * halfsize.x;
        children[i].patch.bounds[0].y = offset.y + (i / 2) * halfsize.y;
        children[i].patch.bounds[1] = children[i].patch.bounds[0] + halfsize;

        children[i].split();
    }
}

