#include "PlanetMesher.h"

using namespace bv;
using namespace planets;

std::array<glm::vec2, 9> pointsFrom(const Patch& patch)
{
    std::array<glm::vec2, 9> points;

    auto bounds = patch.bounds;
    auto min = bounds[0];
    auto max = bounds[1];
    auto mid = (max + min) / 2.0f;
    auto half = (max - min) / 2.0f;

//    points[0] = glm::vec2(min.x, min.y);
    points[0] = mid - half;
    points[1] = glm::vec2(mid.x, mid.y - half.y);
    points[2] = glm::vec2(mid.x + half.x, mid.y - half.y);

    points[3] = glm::vec2(mid.x - half.y, mid.y);
    points[4] = glm::vec2(mid.x         , mid.y);
    points[5] = glm::vec2(mid.x + half.x, mid.y);

    points[6] = glm::vec2(mid.x - half.y, mid.y + half.y);
    points[7] = glm::vec2(mid.x         , mid.y + half.y);
    points[8] = mid + half;

    return points;
}

std::array<int, 24> triIndices {
0, 1, 3,
1, 4, 3,
1, 2, 4,
2, 5, 4,
3, 4, 6,
4, 7, 6,
4, 5, 7,
5, 8, 7 };

Mesh::Id PlanetMesher::mesh(const PatchSphere& sphere)
{
    auto mesh = Mesh::create();

    this->mesh(sphere, mesh);

    return mesh;
}

void PlanetMesher::mesh(const PatchSphere &sphere, Mesh::Id mesh)
{
    mesh->clear();

    auto builder = [mesh, &sphere] (const PatchTree* leaf) mutable
    {
        int offset = mesh->vertices.size();

        auto points = pointsFrom(leaf->patch);
        for (int i = 0; i < points.size(); ++i)
        {
            auto point = points[i];
            auto sampled = sphere.sample(points[i]);
            auto normal = glm::normalize(sampled);
            auto frequency = 10.0f;
            auto height = glm::sin(point.x * frequency);
            auto scale = 0.05f;
            sampled += height * scale * normal;
            mesh->vertex(glm::vec3(sampled.x, sampled.y, sampled.z));
//            mesh->vertex(glm::vec3(point.x, point.y, 0.0f));
        }

        for (int i = 0; i < triIndices.size() / 3; ++i)
        {
            mesh->tri(offset + triIndices[i * 3 + 0],
                      offset + triIndices[i * 3 + 1],
                      offset + triIndices[i * 3 + 2]);
        }
    };

    sphere.iterate(builder);

    mesh->updateIndices();
    mesh->updateAllNormals();
    mesh->removeDoubles(0.01f);
}