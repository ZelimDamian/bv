#include "PlanetScene.h"

#include "PlanetComponent.h"
#include "RenderingSystem.h"

using namespace bv;
using namespace planets;

void BendentalScene::init()
{
    auto planet = Entity::create("planet");

    planet->require<PlanetComponent>(1.0f);
}