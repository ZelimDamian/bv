#pragma once

#include "Mesh.h"
#include "PatchSphere.h"
#include "Singleton.h"

namespace bv
{
    namespace planets
    {
        class PlanetMesher: public Singleton<PlanetMesher>
        {
        public:
            Mesh::Id mesh(const PatchSphere& sphere);
            void mesh(const PatchSphere& sphere, Mesh::Id mesh);
        };
    }
}