#pragma once

#include "Common.h"

namespace bv
{
    namespace planets
    {
        struct Patch
        {
            glm::vec2 bounds[2];
        };

        struct PatchTree
        {
            bool isLeaf() const { return children.empty(); }
            void split();

            bool shouldSplit() const;

            template <class F>
            void forEachLeaf(F& func) const
            {
                if(isLeaf())
                {
                    func(this);
                }
                else
                {
                    for (int i = 0; i < children.size(); ++i)
                    {
                        children[i].forEachLeaf(func);
                    }
                }
            }

            Patch patch;
            uint8_t level { 0 };
            std::vector<PatchTree> children;
        };

        class PatchSphere
        {
        public:

            PatchSphere(float radius) : m_radius(radius)
            {
                m_tree.patch.bounds[0] = { 0, 0 };
                m_tree.patch.bounds[0] = { 2 * M_PI, 2 * M_PI };

                m_tree.split();
            }

            template <class F>
            void iterate(F& func) const
            {
                m_tree.forEachLeaf(func);
            }

            glm::vec3 sample(const glm::vec2& coords) const;

        protected:

            float m_radius;

            PatchTree m_tree;
        };
    }
}
