#include "Component.h"
#include "PatchSphere.h"

namespace bv
{
    namespace planets
    {
        class PlanetComponent: public Component<PlanetComponent>
        {
        public:
            PlanetComponent(Id id, Entity::Id entity, float radius): ComponentBase(id, entity),
                                                                     m_planet(radius){}

            static void init()
            {
                subscribeToUpdates();
            }

            void initialize();
            void update();


        private:
            PatchSphere m_planet;
        };
    }
}