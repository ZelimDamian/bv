#include "LevelSet.h"

using namespace bv;
using namespace bv::levelset;

// returns an array of index offsets that can be added to current index to get to the n'th neighbour
// neighbour ordering is CCW starting at pos x direction
std::array<int32_t , 8> calcOffsets(int width, int height)
{
    return std::array<int32_t, 8> { 1, 1 - width, -width, -1 - width, -1, -1 + width, width, 1 + width };
}

LevelSet::LevelSet(uint16_t width, uint16_t height) : m_width(width), m_height(height)
{
    m_phi.resize(width * height);
    m_f.resize(width * height);

    for (int i = 0; i < width; ++i)
        for (int j = 0; j < height; ++j)
        {
            //m_phi[i] = (int8_t) (float(i % 4) / 4 * 255);
            //m_phi[i + j * width] = (sinf((float)i / width) + 1) / 2 * 255;
            m_phi[i + j * width] = i % 2 * 125;
        }

    m_neighbourOffsets = calcOffsets(width, height);
}

void LevelSet::seed(glm::uvec2 center, uint16_t radius)
{
    for (int i = center.x - radius; i < center.x + radius; ++i)
    {
        for (int j = center.y - radius; j < center.y + radius; ++j)
        {
            float d = (i/radius)*(i/radius) + (j/radius)*(j/radius);
            if (d > 0.90 && d < 1.1)
            {
                uint32_t index = i + j * m_width;
                m_Lin.push_back(index);
            }
        }
    }
}

uint16_t speed(uint16_t threshold, uint16_t value)
{
    return (uint16_t)std::fabs((float)(threshold - value));
}

void LevelSet::evolve(uint16_t threshold)
{
    // compute speed
    for (int j = 0; j < m_phi.size(); ++j)
    {
        m_f[j] = speed(threshold, m_phi[j]);
    }

    // swtichin all Lout's if speed f(x) is more than 0
    for (int i = 0; i < m_Lout.size(); ++i)
    {
        uint32_t x = m_Lout[i];
        if(m_f[x] > 0)
        {
            switchIn(x);
        }
    }

    // elliminate all redundant Lin's
    for (int i = 0; i < m_Lin.size(); ++i)
    {
        uint32_t x = m_Lin[i];
        bool redundant = true;

        // for all neighbours
        for (int i = 0; i < 8; ++i)
        {
            int y = x + m_neighbourOffsets[i];
            // check if they are all less than zero
            if(m_phi[y] >= 0)
            {
                // if any are not cell is not redundant
                redundant = false;
            }
        }

        if(redundant)
        {
            // remove from the list
            std::remove(m_Lin.begin(), m_Lin.end(), x);
            // set phi value to -3
            m_phi[x] = -3;
        }
    }

    // swtichout all Lin's if speed f(x) is less than 0
    for (int i = 0; i < m_Lin.size(); ++i)
    {
        uint32_t x = m_Lin[i];
        if(m_f[x] > 0)
        {
            switchOut(x);
        }
    }

    // elliminate all redundant Lout's
    for (int i = 0; i < m_Lout.size(); ++i)
    {
        uint32_t x = m_Lout[i];
        bool redundant = true;

        // for all neighbours
        for (int i = 0; i < 8; ++i)
        {
            int y = x + m_neighbourOffsets[i];
            // check if they are all less than zero
            if(m_phi[y] <= 0)
            {
                // if any are not cell is not redundant
                redundant = false;
            }
        }

        if(redundant)
        {
            // remove from the list
            std::remove(m_Lout.begin(), m_Lout.end(), x);
            // set phi value to 3
            m_phi[x] = 3;
        }
    }
}

void LevelSet::switchIn(uint32_t x)
{
    // Step one: delete x from Lout and add to Lin and set phi(x) to -1
    std::remove_if(m_Lout.begin(), m_Lout.end(), [=](const int& in) { return in == x; });
    m_Lin.push_back(x);
    m_phi[x] = -1;
    // Step two: for all neighbours with phi equal to 3 add to Lout and set phi(neighbour) to 1
    for (int i = 0; i < 8; ++i)
    {
        int y = x + m_neighbourOffsets[i];
        if(m_phi[y] == 3)
        {
            m_phi[y] = 1;
            m_Lout.push_back(y);
        }
    }
}

void LevelSet::switchOut(uint32_t x)
{
    // Step one: delete x from Lin and add to Lout and set phi(x) to 1
    std::remove(m_Lin.begin(), m_Lin.end(), x);
    m_Lout.push_back(x);
    m_phi[x] = 1;
    // Step two: for all neighbours with phi equal to -3 add to Lin and set phi(neighbour) to -1
    for (int i = 0; i < 8; ++i)
    {
        int y = x + m_neighbourOffsets[i];
        if(m_phi[y] == -3)
        {
            m_phi[y] = -1;
            m_Lin.push_back(y);
        }
    }
}
