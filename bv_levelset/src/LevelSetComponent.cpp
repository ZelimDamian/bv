#include <RenderingSystem.h>
#include <RenderMesh.h>
#include "LevelSetComponent.h"
#include "MeshFactory.h"
#include "Shader.h"

using namespace bv;
using namespace bv::levelset;
using namespace bv::rendering;

std::vector<Texture::Id> _textures;

LevelSet _levelSet(32, 32);
float _threshold;


static Mesh::Id quadId;

RenderMesh::Id buildQuad()
{
    quadId = MeshFactory::quad(1.0f, 1.0f);
    return RenderMesh::create(quadId);
}

void LevelSetComponent::init()
{
    // create a texutre to hold phi
    Texture::Id phiTexture = Texture::create2D(_levelSet.width(), _levelSet.height(), TextureType::R8);
    _textures.push_back(phiTexture);

    // create a quad mesh to draw phi texture on
    RenderMesh::Id renderQuadId = buildQuad();

    // create a shader and a texture uniform for the quad
    Shader::Id quadShader = Shader::require("diffuse");
    Uniform::Id uDiffTex = Uniform::require(quadShader, "uDiffTex", UniformType::Int);

    // seed a small circle in the center
    _levelSet.seed(glm::uvec2((float)_levelSet.width() / 2, (float)_levelSet.height() / 2), 10);

    auto renderer = RenderingSystem::instance();

    // subsribe to the render event
    renderer->onrender([phiTexture, uDiffTex, quadShader, renderQuadId, renderer]()
    {
        // TODO: update method
//        _levelSet.evolve(_threshold);

        // make sure objects are drawn normally
        renderer->reset();

        // setup the shader for use
        Shader::use(quadShader);

//         update the texture with the latest evolution of the levelset
        Texture::update2D(phiTexture, _levelSet.phi().data());

//         setup the texture for use
        Texture::use(phiTexture, uDiffTex, 0);

        glm::vec3 translation = glm::vec3(0.0f, 1.0f, 0.0f);
        glm::mat4 xform = glm::translate(glm::mat4(), translation);
	                       renderer->transform(xform);

//        // inoke the actual quad rendering
	                       renderer->submit(renderQuadId);
    });
}
