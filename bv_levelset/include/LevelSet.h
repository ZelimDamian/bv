#include "Common.h"

#include <list>
#include <array>

/*
 * Algorithm based on: http://iss.bu.edu/yshi/Publications/TR-ECE-2005-02.pdf
 * */
namespace bv
{
    namespace levelset
    {
        class LevelSet
        {
        public:
            LevelSet(uint16_t width, uint16_t height);

            void seed(glm::uvec2 center, uint16_t radius);

            void evolve(uint16_t threshold);

            std::vector<int8_t> &phi() { return m_phi; }

            uint16_t width() const { return m_width; }
            uint16_t height() const { return m_height; }

        private:
            std::vector<int8_t> m_phi;
            std::vector<int8_t> m_f;

            std::vector<uint32_t> m_Lin;
            std::vector<uint32_t> m_Lout;

            uint16_t m_width, m_height;

            std::array<int32_t, 8> m_neighbourOffsets;

            // algorithms
            void switchIn(uint32_t x);
            void switchOut(uint32_t x);
        };
    }
}
