#pragma once

#include "Common.h"
#include "Component.h"
#include "Texture.h"
#include "LevelSet.h"

namespace bv
{
    namespace levelset
    {
        class LevelSetComponent: public Component<LevelSetComponent>
        {
        public:
            LevelSetComponent(Id id, Entity::Id entityId) : ComponentBase(id, entityId) {}
            static void init();
        };
    }
}