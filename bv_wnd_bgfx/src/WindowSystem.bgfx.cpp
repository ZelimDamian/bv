#include "WindowSystem.h"

#include "Timing.h"

#include "MouseSystem.h"
#include "RenderingSystem.h"
#include "KeyboardSystem.h"
#include "Gui.h"

#include <bx/timer.h>
#include <bgfx/bgfx.h>
#include <entry.h>
#include <input.h>
#include <entry_p.h>


using namespace bv;
using namespace entry;

bv::Key map(entry::Key::Enum key);

WindowSystem::WindowSystem() { }

void WindowSystem::run()
{
    Systems::instance()->init();

    // inform that we are about to start running
    m_beforeRun();

	auto newWidth = m_width;
	auto newHeight = m_height;

    // we want to use debug text and vertically sync
    uint32_t debug = BGFX_DEBUG_TEXT, reset = BGFX_RESET_VSYNC;
    MouseState mouseState;
    while (!processEvents(newWidth, newHeight, debug, reset, &mouseState) && !m_shouldExit)
    {
	    if(newWidth != m_width || newHeight != m_height)
	    {
			state(newWidth, newHeight);
	    }

        // Update time
	    double now = bx::getHPCounter() / double(bx::getHPFrequency());
	    static double time = now;
        Timing::update(now - time);

        // send mouse state to the mouse system
        MouseSystem::instance()->state( mouseState.m_mx,
                                      height() - mouseState.m_my,
                                      mouseState.m_mz,
                                      mouseState.m_buttons[entry::MouseButton::Left],
                                      mouseState.m_buttons[entry::MouseButton::Right],
                                      (
		                                KeyboardSystem::instance()->key(Key::LALT) &&
		                                mouseState.m_buttons[entry::MouseButton::Left]
                                      ) || mouseState.m_buttons[entry::MouseButton::Middle]);

        for (int i = 0; i < (int) entry::Key::Count; ++i)
        {
            auto key = static_cast<entry::Key::Enum>(i);

            if(inputGetKeyState(key, nullptr))
            {
                KeyboardSystem::instance()->key(map(key), true);
            }
            else
            {
                KeyboardSystem::instance()->key(map(key), false);
            }
        }

	    auto modifiers = inputGetModifiersState();
	    auto shift = 0 != (modifiers & (entry::Modifier::LeftShift | entry::Modifier::RightShift) );
	    auto ctrl  = 0 != (modifiers & (entry::Modifier::LeftCtrl  | entry::Modifier::RightCtrl ) );
	    auto alt   = 0 != (modifiers & (entry::Modifier::LeftAlt   | entry::Modifier::RightAlt  ) );

	    KeyboardSystem::instance()->key(Key::LSHIFT, shift);
	    KeyboardSystem::instance()->key(Key::LCTRL, ctrl);
	    KeyboardSystem::instance()->key(Key::LALT, alt);

        // performing internal system updates
        Systems::instance()->update();

        // run the renderer
        RenderingSystem::instance()->_render();
    }

    Systems::instance()->destroy();
}

bv::Key map(entry::Key::Enum key)
{
    switch(key)
    {
        case entry::Key::Enum::None: return bv::Key::UNKNOWN;
        case entry::Key::Enum::Esc: return bv::Key::ESCAPE;

        case entry::Key::Enum::Return: return bv::Key::RETURN;
        case entry::Key::Enum::Tab: return bv::Key::TAB;
        case entry::Key::Enum::Space: return bv::Key::SPACE;
        case entry::Key::Enum::Backspace: return bv::Key::BACKSPACE;
        case entry::Key::Enum::Up: return bv::Key::UP;
        case entry::Key::Enum::Down: return bv::Key::DOWN;
        case entry::Key::Enum::Left: return bv::Key::LEFT;
        case entry::Key::Enum::Right: return bv::Key::RIGHT;
        case entry::Key::Enum::Insert: return bv::Key::INSERT;
        case entry::Key::Enum::Delete: return bv::Key::DEL;
        case entry::Key::Enum::Home: return bv::Key::HOME;
        case entry::Key::Enum::End: return bv::Key::END;
        case entry::Key::Enum::PageUp: return bv::Key::PAGEUP;
        case entry::Key::Enum::PageDown: return bv::Key::PAGEDOWN;
        case entry::Key::Enum::Print: return bv::Key::PRINT;
        case entry::Key::Enum::Plus: return bv::Key::PLUS;
        case entry::Key::Enum::Minus: return bv::Key::MINUS;
        case entry::Key::Enum::LeftBracket: return bv::Key::LEFTBRACKET;
        case entry::Key::Enum::RightBracket: return bv::Key::RIGHTBRACKET;
        case entry::Key::Enum::Semicolon: return bv::Key::SEMICOLON;
        case entry::Key::Enum::Quote: return bv::Key::QUOTE;
        case entry::Key::Enum::Comma: return bv::Key::COMMA;
        case entry::Key::Enum::Period: return bv::Key::PERIOD;
        case entry::Key::Enum::Slash: return bv::Key::SLASH;
        case entry::Key::Enum::Backslash: return bv::Key::BACKSLASH;
		case entry::Key::Enum::Tilde: return bv::Key::TILDE;
        case entry::Key::Enum::F1: return bv::Key::F1;
        case entry::Key::Enum::F2: return bv::Key::F2;
        case entry::Key::Enum::F3: return bv::Key::F3;
        case entry::Key::Enum::F4: return bv::Key::F4;
        case entry::Key::Enum::F5: return bv::Key::F5;
        case entry::Key::Enum::F6: return bv::Key::F6;
        case entry::Key::Enum::F7: return bv::Key::F7;
        case entry::Key::Enum::F8: return bv::Key::F8;
        case entry::Key::Enum::F9: return bv::Key::F9;
        case entry::Key::Enum::F10: return bv::Key::F10;
        case entry::Key::Enum::F11: return bv::Key::F11;
        case entry::Key::Enum::F12: return bv::Key::F12;
        case entry::Key::Enum::NumPad0: return bv::Key::NUMPAD0;
        case entry::Key::Enum::NumPad1: return bv::Key::NUMPAD1;
        case entry::Key::Enum::NumPad2: return bv::Key::NUMPAD2;
        case entry::Key::Enum::NumPad3: return bv::Key::NUMPAD3;
        case entry::Key::Enum::NumPad4: return bv::Key::NUMPAD4;
        case entry::Key::Enum::NumPad5: return bv::Key::NUMPAD5;
        case entry::Key::Enum::NumPad6: return bv::Key::NUMPAD6;
        case entry::Key::Enum::NumPad7: return bv::Key::NUMPAD7;
        case entry::Key::Enum::NumPad8: return bv::Key::NUMPAD8;
        case entry::Key::Enum::NumPad9: return bv::Key::NUMPAD9;
        case entry::Key::Enum::Key0: return bv::Key::NUM0;
        case entry::Key::Enum::Key1: return bv::Key::NUM1;
        case entry::Key::Enum::Key2: return bv::Key::NUM2;
        case entry::Key::Enum::Key3: return bv::Key::NUM3;
        case entry::Key::Enum::Key4: return bv::Key::NUM4;
        case entry::Key::Enum::Key5: return bv::Key::NUM5;
        case entry::Key::Enum::Key6: return bv::Key::NUM6;
        case entry::Key::Enum::Key7: return bv::Key::NUM7;
        case entry::Key::Enum::Key8: return bv::Key::NUM8;
        case entry::Key::Enum::Key9: return bv::Key::NUM9;
        case entry::Key::Enum::KeyA: return bv::Key::A;
        case entry::Key::Enum::KeyB: return bv::Key::B;
        case entry::Key::Enum::KeyC: return bv::Key::C;
        case entry::Key::Enum::KeyD: return bv::Key::D;
        case entry::Key::Enum::KeyE: return bv::Key::E;
        case entry::Key::Enum::KeyF: return bv::Key::F;
        case entry::Key::Enum::KeyG: return bv::Key::G;
        case entry::Key::Enum::KeyH: return bv::Key::H;
        case entry::Key::Enum::KeyI: return bv::Key::I;
        case entry::Key::Enum::KeyJ: return bv::Key::J;
        case entry::Key::Enum::KeyK: return bv::Key::K;
        case entry::Key::Enum::KeyL: return bv::Key::L;
        case entry::Key::Enum::KeyM: return bv::Key::M;
        case entry::Key::Enum::KeyN: return bv::Key::N;
        case entry::Key::Enum::KeyO: return bv::Key::O;
        case entry::Key::Enum::KeyP: return bv::Key::P;
        case entry::Key::Enum::KeyQ: return bv::Key::Q;
        case entry::Key::Enum::KeyR: return bv::Key::R;
        case entry::Key::Enum::KeyS: return bv::Key::S;
        case entry::Key::Enum::KeyT: return bv::Key::T;
        case entry::Key::Enum::KeyU: return bv::Key::U;
        case entry::Key::Enum::KeyV: return bv::Key::V;
        case entry::Key::Enum::KeyW: return bv::Key::W;
        case entry::Key::Enum::KeyX: return bv::Key::X;
        case entry::Key::Enum::KeyY: return bv::Key::Y;
        case entry::Key::Enum::KeyZ: return bv::Key::Z;

//        case entry::Key::GamepadA: return Key::Tab;
//        case entry::Key::GamepadB: return Key::Tab;
//        case entry::Key::GamepadX: return Key::Tab;
//        case entry::Key::GamepadY: return Key::Tab;
//        case entry::Key::GamepadThumbL: return Key::Tab;
//        case entry::Key::GamepadThumbR: return Key::Tab;
//        case entry::Key::GamepadShoulderL: return Key::Tab;
//        case entry::Key::GamepadShoulderR: return Key::Tab;
//        case entry::Key::GamepadUp: return Key::Tab;
//        case entry::Key::GamepadDown: return Key::Tab;
//        case entry::Key::GamepadLeft: return Key::Tab;
//        case entry::Key::GamepadRight: return Key::Tab;
//        case entry::Key::GamepadBack: return Key::Tab;
//        case entry::Key::GamepadStart: return Key::Tab;
//        case GamepadGuide: return Key::Tab;

//        Count
    }
	return bv::Key::UNKNOWN;
}
