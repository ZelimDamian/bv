#pragma once

#include "Component.h"

namespace bv
{
	namespace bendental
	{
		class WireGCodePlayer: public Component<WireGCodePlayer>
		{
		public:
			WireGCodePlayer(Id id, Entity::Id entity): ComponentBase(id, entity) {}

			static void init() { subscribeToUpdates(); }

			void update();

		private:
			float m_time { 0.0f };
			float m_totalTime { 5.0f };

			Mesh::Id m_mesh { Mesh::create() };
		};
	}
}