#pragma once

#include "Component.h"

namespace bv
{
	namespace bendental
	{
		class SplitterWirePointsComponent: public Component<SplitterWirePointsComponent>
		{
		public:
			SplitterWirePointsComponent(Id id, Entity::Id entity): ComponentBase(id, entity) { }

			static void init() { subscribeToUpdates(); }

			void initialize();

			void update();

			void attachGUI();


            const Entities &points() const { return m_points; }
            void points(const Entities &points) { m_points = points; }

        private:
			Entities m_points;
		};
	}
}