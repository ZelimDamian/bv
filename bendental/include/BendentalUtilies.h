#pragma once

#include "MathLib.h"

namespace bv
{
    namespace Utilities
    {
        int findNearestIndex (const VectorVec3& v, const glm::vec3& point)
        {
            int nearestIndex = 0;
            float nearestDistance = 1e10;

            for (int i = 0; i < v.size(); ++i)
            {
                float distance = Math::distance(point, v[i]);

                if(distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestIndex = i;
                }
            }

            return nearestIndex;
        };

        int findFurthestIndex (const VectorVec3& v, const glm::vec3& point)
        {
            int index = 0;
            float furthest = -1e10;

            for (int i = 0; i < v.size(); ++i)
            {
                float distance = Math::distance(point, v[i]);

                if(distance > furthest)
                {
                    furthest = distance;
                    index = i;
                }
            }

            return index;
        };

	    // reorder points so that the next point to a given point is the closest geometrically
        VectorVec3 linkOrder(VectorVec3 unorederedPoints, int startIndex = -1)
        {
            VectorVec3 points(unorederedPoints.size());

            auto pop = [] (VectorVec3& v, int i)
            {
                auto value = v[i];
                v.erase(v.begin() + i);
                return value;
            };

	        if(startIndex == -1)
	        {
		        startIndex = findFurthestIndex(unorederedPoints, glm::vec3());
	        }

            for (int i = 0; i < points.size(); ++i)
            {
                auto point = pop(unorederedPoints, startIndex);
                startIndex = findNearestIndex(unorederedPoints, point);

                points[i] = point;
            }

            return points;
        }
    }
}