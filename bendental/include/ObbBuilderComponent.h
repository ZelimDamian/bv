#pragma once

#include "Component.h"

namespace bv
{
    class ObbBuilderComponent: public Component<ObbBuilderComponent>
    {
    public:
        ObbBuilderComponent(Id id, Entity::Id entity);

        static void init() { subscribeToUpdates(); }

        void initialize();
        void update();

        const Obb &obb() const { return m_obb; }
        void obb(const Obb &obb) { m_obb = obb; }

    private:
        Obb m_obb;

        Mesh::Id m_mesh;
    };
}