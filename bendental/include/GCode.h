#pragma once

#include "Store.h"

namespace bv
{
	namespace bendental
	{
		enum class Semantic
		{
			Roll,
			Turn,
			Raise
		};

		class GCode: public KeyedStorable<Semantic, GCode>
		{
		public:
			GCode(Id id, const Semantic& semantic, const std::string &code, float coefficient):
					KeyedStorableBase(id, semantic), m_coefficient(coefficient), m_code(code){ }

			float evaluate(float m) const { return m * m_coefficient; }
			float inverted(float m) const { return m / m_coefficient; }

			const std::string &code() const { return m_code; }
			void code(const std::string & code) { m_code = code; }

			Semantic semantic() const { return key(); }
			std::string semanticName() const;

			float coefficient() const { return m_coefficient; }
			void coefficient(float coefficient) { m_coefficient = coefficient; }

		private:
			float m_coefficient;
			std::string m_code;
		};

		struct GCodeEntry
		{
			GCode::Id code;
			float value;

			float evaluate() const { return code->evaluate(value); }
			float inverted() const { return code->inverted(value); }
		};

	}
}
