#pragma once

#include "Component.h"

namespace bv
{
    namespace bendental
    {
	    using PointsSourceFunc = std::function<VectorVec3()>;

        class WireComponent: public Component<WireComponent>
        {
        public:
            WireComponent(Id id, Entity::Id entity): ComponentBase(id, entity) {}

            static void init() { subscribeToUpdates(); }

            void initialize();

            void update();

            void attachGUI();

            float radius() const { return m_radius; }
            void radius(float radius) { m_radius = radius; }

			const PointsSourceFunc &sourceAfter() const { return m_sourceAfter; }
	        void sourceAfter(const PointsSourceFunc &sourceAfter) { m_sourceAfter = sourceAfter; }

	        const PointsSourceFunc &sourceBefore() const { return m_sourceBefore; }
	        void sourceBefore(const PointsSourceFunc &sourceBefore) { m_sourceBefore = sourceBefore; }

			// list of points on the wire
			VectorVec3 points() const;
			// same as points() but with subsections
			VectorVec3 path() const;

        private:

            Mesh::Id m_wireMesh;

            float m_radius { 0.01f };

            int m_selectedSource { 0 };

            int m_step { 0 };
	        int m_substep { 0 };
            int m_steps { 10 };

            bool m_showAll { false };

	        PointsSourceFunc m_sourceBefore;
	        PointsSourceFunc m_sourceAfter;
        };
    }
}
