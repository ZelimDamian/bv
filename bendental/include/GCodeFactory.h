#pragma once

#include "MathLib.h"
#include <string>
#include <System.h>
#include "GCode.h"

namespace bv
{
	namespace bendental
	{
		class GCodeFactory: public SystemSingleton<GCodeFactory>
		{
		public:
			void init() override;
			std::vector<GCodeEntry> generate(const VectorVec3& points);
			std::string stringify(const std::vector<GCodeEntry>& gcodes);

			void loadSettingsFile();
		};
	}
}