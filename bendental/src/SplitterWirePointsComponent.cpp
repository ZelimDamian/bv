#include "SplitterWirePointsComponent.h"

#include "RenderMeshComponent.h"
#include "MeshSplitter.h"
#include "DebugMesh.h"
#include "RenderMesh.h"

using namespace bv;
using namespace geom;
using namespace bendental;

void SplitterWirePointsComponent::initialize()
{
	auto rmc = this->require<RenderMeshComponent>();
	rmc->deactivate();
	auto mesh = rmc->mesh();

	auto submeshes = MeshSplitter::splitDisjoint(mesh);

	std::sort(submeshes.begin(), submeshes.end(), [] (const Mesh::Id mesh1, Mesh::Id mesh2)
	{
		auto center1 = mesh1->box().center();
		auto center2 = mesh2->box().center();

		int sortAxis = 2;
		return center1[sortAxis] < center2[sortAxis];
	});

	for (int i = 0; i < submeshes.size(); ++i)
	{
		auto subentity = Entity::create(m_entity->name() + "_" + std::to_string(i));
		m_points.push_back(subentity);

		auto rmc = subentity->require<RenderMeshComponent>(submeshes[i], "diffuse");

		auto submesh = rmc->mesh();
		auto center = submesh->normalize();
		subentity->pos(center);
	}
}

void SplitterWirePointsComponent::update() { }

void SplitterWirePointsComponent::attachGUI() { }