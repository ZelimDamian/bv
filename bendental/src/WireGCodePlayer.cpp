#include "WireGCodePlayer.h"

#include "GCode.h"
#include "GCodeFactory.h"
#include <WireComponent.h>
#include <DebugMesh.h>

using namespace bv;
using namespace bendental;

void WireGCodePlayer::update()
{
	auto wireComponent = require<WireComponent>();
	auto points = wireComponent->points();
	auto entries = GCodeFactory::instance()->generate(points);

	VectorVec3 playbackPoints;

	float angle = 0.0f;
	glm::vec3 point;

	playbackPoints.push_back(point);

	for (int i = 0; i < entries.size(); ++i)
	{
		auto& entry = entries[i];
		auto semantic = entry.code->semantic();
		auto value = entry.evaluate();

		switch(semantic)
		{
			case Semantic::Roll:
			{
				auto orientation = glm::angleAxis(angle, Math::up());
				auto direction = orientation * -Math::right();
				point = point + value * direction;
				playbackPoints.push_back(point);

				break;
			}
			case Semantic::Turn:
			{
				angle = value;

				break;
			}
		}
	}

	auto debug = DebugMesh::instance();

	for (int i = 0; i < playbackPoints.size() - 1; ++i)
	{
		debug->point(playbackPoints[i + 0]);
		debug->point(playbackPoints[i + 1]);
	}

	debug->render();

//	m_time += Timing::delta();
//	m_time = glm::min(m_time, m_totalTime);
//
//	float t = m_time / m_totalTime;
//
//	float gCodeIndex = t * gcodes.size();
}
