#include "BendentalScene.h"

#include "FileReader.h"
#include "RenderMeshComponent.h"
#include <ObjLoader.h>
#include <StlLoader.h>
#include <SplitterWirePointsComponent.h>
#include <WireComponent.h>
#include <MeshUtilities.h>
#include <Bounding.h>
#include <Bounding.Eigen.h>
#include <DebugMesh.h>
#include <ObbBuilderComponent.h>
#include <BendentalUtilies.h>
#include <FileWriter.h>
#include <GCodeFactory.h>
#include <WireGCodePlayer.h>

using namespace bv;
using namespace bendental;

std::string getName(const std::string& path)
{
#ifdef _WIN32
    auto slash = path.rfind('\\') + 1;
#else
    auto slash = path.rfind('/') + 1;
#endif

    auto dot = path.rfind('.');

    return path.substr(slash, dot - slash);
}


void BendentalScene::init()
{
	auto files = FileReader::multipick("stl");

    std::sort(files.begin(), files.end(), [] (const std::string& s1, const std::string& s2)
    {
        return getName(s1) < getName(s2);
    });

	Entities pointEntities;

	for (int i = 0; i < files.size(); ++i)
	{
		auto mesh = StlLoader::load_absolute(files[i]);

		MeshUtilities::flipYZ(mesh);

		mesh->updateVertices();
		mesh->updateVertNormals();

		auto entity = Entity::create(getName(files[i]));
		{
			auto center = mesh->normalize();
			entity->pos(center);

			auto rm =  entity->require<RenderMeshComponent>(mesh, "diffuse_color");
			auto color = glm::vec4(233, 239, 243, 1.0f) / 255.0f;
			rm->parameter("uColor", color);

            entity->require<ObbBuilderComponent>();
		}

		pointEntities.push_back(entity);
	}

    auto wire = Entity::create("wire");
    auto wc = wire->require<WireComponent>();

	auto findEndPoints = [=]
	{
		static const VectorVec3 axes
		{
				Math::right(), Math::up(), Math::back()
		};

		VectorVec3 unorederedPoints;

		for (int i = 0; i < pointEntities.size(); ++i)
		{
			auto entity = pointEntities[i];
			auto obb = entity->component<ObbBuilderComponent>()->obb();
			auto longestAxis = obb.aabb.longestAxis();
			auto size = obb.aabb.size();
			auto halflength = size[longestAxis] / 2.0f;

			auto localAxis = axes[longestAxis];
			auto axis = Math::transformNormal(localAxis, obb.mtx);

			auto center = entity->pos();
			unorederedPoints.push_back(center - axis * halflength);
			unorederedPoints.push_back(center + axis * halflength);
		}

		return unorederedPoints;
	};

	auto unorderedPoints = findEndPoints();
	auto startIndex = Utilities::findFurthestIndex(unorderedPoints, glm::vec3());

	wc->sourceBefore([=]
	{
		auto unorderedPoints = findEndPoints();
		return Utilities::linkOrder(unorderedPoints, startIndex);
	});

	wc->sourceAfter([=]
	{
		return wc->sourceBefore()();
	});

    {
        auto writeGCodes = [=]
        {
            auto points = wc->sourceBefore()();
            auto gcodes = GCodeFactory::instance()->generate(points);
	        auto string = GCodeFactory::instance()->stringify(gcodes);

            std::string filepath = FileReader::save("gcodes", "txt");

            FileWriter writer(filepath, false);
            writer.write(string);
        };

        auto menu = gui::Gui::instance()->menu()->item("Dental");

        menu->item("Generate G-Codes", [=]
        {
            writeGCodes();
        });

	    menu->item("Load settings file", [=]
	    {
		    GCodeFactory::instance()->loadSettingsFile();
	    });

	    GCodeFactory::instance()->init();
        writeGCodes();
    }

	wire->require<WireGCodePlayer>();
}
