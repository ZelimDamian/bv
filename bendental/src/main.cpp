#include <CollisionSystem.h>

#include "ManipulatorSystem.h"
#include "PickingSystem.h"

#include "BendentalScene.h"

extern "C" int _main_(int _argc, char** _argv);
int _main_(int argc, char** argv)
{
    using namespace bv;

    //jump start
    EntitySystem::instance();
    ManipulatorSystem::instance();
    SelectionSystem::instance();
    coldet::CollisionSystem::instance();
    coldet::PickingSystem::instance();
    gui::Gui::instance();

    Scene::create<bendental::BendentalScene>();

    // run the main simulation loop
    WindowSystem::instance()->run();

    return 0;
}