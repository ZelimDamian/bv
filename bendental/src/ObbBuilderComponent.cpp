#include "ObbBuilderComponent.h"

#include <Bounding.Eigen.h>
#include <RenderMeshComponent.h>
#include <MeshFactory.h>

using namespace bv;

ObbBuilderComponent::ObbBuilderComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
{
    auto rm =  m_entity->component<RenderMeshComponent>();
    auto mesh = rm->mesh();

    // TODO: no need to recalculate obb all the time
    m_obb = Bounding::obb_cov(mesh->positions, mesh->indices);

    m_mesh = Mesh::create();
}

void ObbBuilderComponent::initialize()
{
}

void ObbBuilderComponent::update()
{
    auto rm =  m_entity->component<RenderMeshComponent>();

    if(rm->isActive())
    {
        using namespace rendering;

        auto translation = glm::translate(glm::mat4 {}, m_entity->pos());

        auto renderer = RenderingSystem::instance();

        auto onrender = [=]() mutable
        {
            MeshFactory::shape(m_obb.aabb, m_mesh);
            m_mesh->updateWireIndices();

            auto shader = Shader::requse("colors");
            auto uniform = shader->uniform("uColor", UniformType::Vec4);
            auto color = glm::vec4(233, 239, 243, 1.0f) / 255.0f;
            renderer->apply(uniform, uniform->data(color));

            renderer->apply(translation * m_obb.mtx);
            renderer->apply(RenderState {Culling::NO, RenderMode::LINES});

            auto rendermesh = RenderMesh::create(m_mesh);
            renderer->render(rendermesh, shader);
        };

        renderer->onrender_once(onrender);
    }
}