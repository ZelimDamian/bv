#include "WireComponent.h"

#include "RenderMeshComponent.h"
#include "MeshSplitter.h"
#include "DebugMesh.h"
#include "RenderMesh.h"

#include "SplitterWirePointsComponent.h"

using namespace bv;
using namespace geom;
using namespace bendental;

struct ContourSource
{
	std::string name;
	std::function<VectorVec3(WireComponent::Id)> source;
};

VectorVec3 makeCircle(int subdiv, float radius)
{
	VectorVec3 circle(subdiv + 1);

	for (int i = 0; i < subdiv + 1; ++i)
	{
		float x = glm::cos( 2.0f * (float) i / subdiv * glm::pi<float>()) * radius;
		float y = glm::sin( 2.0f * (float) i / subdiv * glm::pi<float>()) * radius;

		circle[i] = glm::vec3{ x, y, 0.0f};
	}

	return circle;
}

VectorVec3 makeSquare(float a, float b)
{
	VectorVec3 circle(5);

	circle[0] = glm::vec3(-a/2, -b/2, 0.0f);
	circle[1] = glm::vec3( a/2, -b/2, 0.0f);
	circle[2] = glm::vec3( a/2,  b/2, 0.0f);
	circle[3] = glm::vec3(-a/2,  b/2, 0.0f);
	circle[4] = glm::vec3(-a/2, -b/2, 0.0f);

	return circle;
}

static std::vector<ContourSource> _contourSources
{
	{"circle", [] (WireComponent::Id c) { return makeCircle(16, c->radius()); }},
	{"square", [] (WireComponent::Id c) { return makeSquare(2.0f * c->radius(), 1.0f * c->radius()); }}
};

void WireComponent::initialize()
{
	m_wireMesh = Mesh::create();

	auto renderer = RenderingSystem::instance();
	auto onrender = [this, renderer]
	{
		if(!isActive()) { return; }

		auto contour = _contourSources[m_selectedSource].source(this->id());

		auto path = this->path();
		MeshFactory::extrude(contour, path, m_wireMesh);

		auto rendermesh = rendering::RenderMesh::create(m_wireMesh);

		renderer->reset(Culling::NO, RenderMode::TRIANGLES);
		renderer->submit(rendermesh);
	};

	renderer->onrender(onrender);
}

void WireComponent::update() { }

void WireComponent::attachGUI()
{
	auto gui = this->gui();

	gui->bind<float>("Radius", [this] { return m_radius;}, [this] (float value) { m_radius = value; },
		0.0001f, 1.0f, 0.0001f);

	std::vector<std::string> names(_contourSources.size());

	for (int i = 0; i < names.size(); ++i)
	{
		names[i] = _contourSources[i].name;
	}

	gui->list("Contour", names, [this] (int sel)
	{
		m_selectedSource = sel;
	});

    gui->bind<int>("Step", [this] { return m_step; }, [this] (int val) { m_step = val; }, 0, m_steps);
    gui->bind<int>("Steps", [this] { return m_steps; }, [this] (int val) { m_steps = val; }, 0, 30);

    gui->button("Show all", [this] { m_showAll =!m_showAll; }, [this] { return m_showAll; });
}

VectorVec3 WireComponent::path() const
{
	auto points = this->points();

	decltype(points) path;

	//        for (int k = 0; k < m_steps; ++k)
	{
		for (int i = 0; i < points.size(); ++i)
		{
			auto point = points[i];
			path.push_back(point);

			if (i < points.size() - 1)
			{
				auto nextpoint = points[i + 1];
				for (int j = 0; j < m_substep; ++j)
				{
					float t = float(j) / m_substep;

					auto subpoint = point + (nextpoint - point) * t;
					path.push_back(subpoint);
				}
			}
		}
	}

	return path;
}

VectorVec3 WireComponent::points() const
{
	VectorVec3 points;

	auto beforePoints = m_sourceBefore();
	auto afterPoints = m_sourceAfter();

	if (beforePoints.size() != afterPoints.size())
	{
		Log::error(std::string("Wrong point sources for ") + title());
	}

	//        for (int k = 0; k < m_steps; ++k)
	{
		float stage = float(m_step) / m_steps;

		for (int i = 0; i < beforePoints.size(); ++i)
		{
			auto before = beforePoints[i];
			auto after = afterPoints[i];

			auto point = before + (after - before) * stage;
			points.push_back(point);
		}
	}

	return points;
}
