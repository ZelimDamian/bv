#include "GCode.h"

std::string bv::bendental::GCode::semanticName() const
{
	auto semantic = this->semantic();

	switch (semantic)
	{
		case Semantic::Roll: return "Roll";
		case Semantic::Turn: return "Turn";
		case Semantic::Raise: return "Raise";
	}
}
