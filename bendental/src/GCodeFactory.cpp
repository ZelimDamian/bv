#include "GCodeFactory.h"

#include "FileReader.h"
#include <sstream>

#include "mini.h"

using namespace bv;
using namespace bendental;

void GCodeFactory::loadSettingsFile()
{
	auto contents = FileReader::open("ini");

	if(contents.size() > 0)
	{
		mINI ini;
		ini.parse(contents);

		for (auto &&gcode : GCode::handles())
		{
			auto name = gcode->semanticName();

			auto code = ini[name + ".code"];
			auto coeff = ini[name + ".coeff"];

			if(code.size() > 0 && coeff.size() > 0)
			{
				gcode->code(code);
				gcode->coefficient(std::stof(coeff));
			}
		}
	}
}

void GCodeFactory::init()
{
	auto roll = GCode::create(Semantic::Roll, "G1 X", 1.0f);
	auto turn = GCode::create(Semantic::Turn, "G1 Y", 1.0f);
	auto raise = GCode::create(Semantic::Raise, "G1 Z", 1.0f);

}

std::vector<GCodeEntry> GCodeFactory::generate(const VectorVec3& points)
{
    std::vector<GCodeEntry> entries;

	if(points.size() > 1)
	{
		auto segment = points[1] - points[0];
		auto direction = glm::normalize(segment);
		float distance = glm::length(segment);

		auto rollCode = GCode::find(Semantic::Roll);
		auto turnCode = GCode::find(Semantic::Turn);
		auto raizeCode = GCode::find(Semantic::Raise);

		float previousAngle = 0.0f;

		for (int i = 1; i < points.size() - 1; ++i)
		{
			segment = points[i+1] - points[i];

			entries.push_back(GCodeEntry{ rollCode, distance });

			distance = glm::length(segment);

			float angle = Math::signedAngleBetween(direction, segment, Math::up());

			entries.push_back(GCodeEntry{ turnCode, angle });

			previousAngle = angle;
		}

		entries.push_back(GCodeEntry{ rollCode, distance });
	}

    return entries;
}

std::string stringify(const GCodeEntry& entry)
{
	return entry.code->code() + std::to_string(entry.evaluate());
}

std::string GCodeFactory::stringify(const std::vector<GCodeEntry> &gcodes)
{
	std::string s;

	for (int i = 0; i < gcodes.size(); ++i)
	{
		s += ::stringify(gcodes[i]) + "\n";
	}

	return s;
}
