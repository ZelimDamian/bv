#include "VolumeRendererComponent.h"

#include "CameraSystem.h"

using namespace bv;
using namespace bv::volume;

// dod way
static std::vector<glm::vec3> _localEyes;


void VolumeRendererComponent::init()
{
    RenderingSystem::instance()->onload([&]
    {
        for (auto& comp : s_instances)
        {
            comp.m_renderer = std::make_shared<VolumeRenderer>(comp.m_volume);
        }
    });

    RenderingSystem::instance()->onrender([&]()
    {
        const glm::vec3 eye = CameraSystem::instance()->current()->eye();

        //_localEyes.resize(components().size());
        // dod way
        for (int i = 0; i < components().size(); ++i)
        {
			const glm::mat4 &xform = components(i).entity()->transform();
            _localEyes[i] =  glm::vec3(glm::inverse(xform) * glm::vec4(eye, 1.0f));
        }

        // prefetch
        auto renderingSystem = RenderingSystem::instance();

        // run for all components
        for (int i = 0; i < components().size(); i++)
        {
            VolumeRendererComponent & comp = components()[i];
            if(comp.isActive())
            {
                const glm::mat4 &xform = comp.entity()->transform();
	            renderingSystem->transform(xform);
                comp.m_renderer->render(_localEyes[i]);
            }
        }
    });


}

void VolumeRendererComponent::attachGUI()
{
    auto _panel = gui();

    _panel->bind<glm::vec3>("Min cut", [&] { return m_renderer->min(); }, [&] (const glm::vec3& value) { m_renderer->min() = value; });
    _panel->bind<glm::vec3>("Max cut", [&] { return m_renderer->max(); }, [&] (const glm::vec3& value) { m_renderer->max() = value; });
}

void VolumeRendererComponent::allocate()
{
    _localEyes.emplace_back();
}

void VolumeRendererComponent::update() { }
