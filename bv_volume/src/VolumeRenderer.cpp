#include "VolumeRenderer.h"

#include "RenderingSystem.h"
#include "MeshFactory.h"

using namespace bv;
using namespace bv::volume;
using namespace bv::rendering;

VolumeRenderer::VolumeRenderer(const VolumeBasePtr vol)
{
    auto renderer = RenderingSystem::instance();

    renderer->onload([&, vol]
    {
        // create a mesh for the cube to render the volume
        Mesh::Id cubeMeshId = MeshFactory::cube(1.0f, 1.0f, 1.0f);
        m_cubeRenderId = RenderMesh::create(cubeMeshId);

        TextureType type;

        // find texture format based on bytes per voxel
        if(vol->voxelBytes() == 1)
        {
            type = TextureType::R8;
        } else {
            type = TextureType::R16;
        }

        m_volumeTex = Texture::create3D(vol->width(), vol->height(), vol->depth(), type);
        Texture::update3D(m_volumeTex, vol->data());

        m_stepSize = 1.0f / vol->dims();
        //glm::vec3 scale = vol->scale() / vol->dataScale();

        std::array<uint8_t, 8> mapData = { 255, 255, 255, 255, 255, 255, 255, 255 };

        m_mapTex = Texture::create2D(mapData.size(), 1, TextureType::R8);
        Texture::update2D(m_mapTex, &mapData[0]);

        // load our shader
        m_raycaster = Shader::require("raycaster");

        // the uniform pointing to our volume shader
        m_uVolumeTex = Uniform::require(m_raycaster, "uVolumeTex", UniformType::Int);
        m_uMapTex = Uniform::require(m_raycaster, "uMapTex", UniformType::Int);
        m_uEyePos = Uniform::require(m_raycaster, "uEyePos", UniformType::Vec3);
        m_uStepSize = Uniform::require(m_raycaster, "uStepSize", UniformType::Vec3);
        m_uCutMin = Uniform::require(m_raycaster, "uCutMin", UniformType::Vec3);
        m_uCutMax = Uniform::require(m_raycaster, "uCutMax", UniformType::Vec3);

        // specify the step size for the raycaster
        m_uStepSize->set(m_stepSize);

        // make sure we destroy everything we created when the rendering
        // system is being destoyeh
//    RenderingSystem::instance()->ondestroy([&]
//    {
                        //bgfx::destroyUniform(m_uEyePos);
                        //bgfx::destroyUniform(m_uMapTex);
                        //bgfx::destroyUniform(m_uVolumeTex);
                        //bgfx::destroyUniform(m_uStepSize);
                        //bgfx::destroyUniform(m_uScale);
                        //bgfx::destroyUniform(m_uVolumeTex);
//    });
    });
}

VolumeRenderer::~VolumeRenderer()
{
}

void VolumeRenderer::render(const glm::vec3& eye)
{
    auto renderer = RenderingSystem::instance();

    // Set render states.
    renderer->reset(Culling::NO);

    // tell shader where the camera is
	m_uEyePos->set(eye);

	// tell the shader what the cut planes are
    m_uCutMin->set(m_cutMin);
	m_uCutMax->set(m_cutMax);

    // Set vertex and fragment shaders.
    Shader::use(m_raycaster);

    // set texture
    Texture::use(m_volumeTex, m_uVolumeTex, 0);
    Texture::use(m_mapTex, m_uMapTex, 1);

	renderer->submit(m_cubeRenderId);
}
