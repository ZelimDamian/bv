#include "VolumeLoader.h"

#include <fstream>
#include <FileReader.h>

using namespace bv;
using namespace bv::volume;

#define PREFIX FileReader::ROOT_DIR + "/volumes/"
static const std::string EXT = ".raw";

VolumeLoader::VolumeLoader() {
}

VolumeLoader::~VolumeLoader() {
}

size_t getFileSize(std::ifstream& is)
{
    // Determine the file depth
    is.seekg(0, std::ios_base::end);

    // size of the file
    size_t size = (size_t) is.tellg();

    // go to the end of the file
    is.seekg(0, std::ios_base::beg);

    return size;
}

// loads multiple files by incrementing an index in the filename
size_t VolumeLoader::loadDataFiles(const char *filename, size_t startIndex, size_t step,
        size_t maxSlices, std::vector<uint8_t>& mainbuffer, const char *suffix, size_t headerSize) const
{
    size_t depth = 0;
    uint8_t* prevFileData;
    size_t prevFileSize;
    for (int i = 0; i < maxSlices; ++i)
    {
        // compose the filename to find the next file in the directory
        std::string fileNameStr = PREFIX + std::string(filename) + std::to_string(startIndex + i * step) + std::string(suffix);

        size_t dataSize = 0;

        // try to load file with file name
        uint8_t* fileData = loadDataFile(fileNameStr.c_str(), dataSize, headerSize);

        // check if the file was found
        if (fileData) {
            for (int j = 0; j < dataSize; ++j) {
                mainbuffer.push_back(fileData[j]);
            }
            // store the last successfully loaded file
            prevFileData = fileData;
            prevFileSize = dataSize;

            // increment the number of slices we found
            depth++;
        } else {
            if(m_isFillingGaps) {
                // copy in the previous successfully loaded data
                for (int j = 0; j < prevFileSize; ++j) {
                    mainbuffer.push_back(prevFileData[j]);
                }

                // we have an extra slice
                depth++;
            }
            else
            {
                break;
            }
        }
        delete fileData;
    }

    return depth;
}

// loads a whole raw file
uint8_t* VolumeLoader::loadDataFile(const char* name, size_t& dataSizeRef, size_t headerSize) const
{
    auto filename = PREFIX + name + EXT;

    // Open the stream
	std::ifstream is(filename, std::ios::binary); // std::ios::binary is absolutely essential in VC++, but not GCC

    if(!is.is_open())
    {
        // TODO:: should probably throw here?
        dataSizeRef = 0;
        Log::error("Error loading file: %s", filename.c_str());
        return NULL;
    }

    // seek through the file and return the size
    size_t filesize = getFileSize(is);

	Log::writeln("Loading a file: size %u bytes", filesize);

    // skip the header size bytes
    is.seekg(headerSize);

//	Log::writeln("Skipping header: %u bytes", headerSize);

    // calc data size into the external reference
    dataSizeRef = filesize - headerSize;

    // allocate space
	uint8_t* dst = new uint8_t[dataSizeRef];

//	Log::writeln("Read %u bytes", dataSizeRef);

    // read the rest of the file
    is.read((char*)dst, (ptrdiff_t) dataSizeRef);

    // Close the file
    is.close();

    return dst;
}
