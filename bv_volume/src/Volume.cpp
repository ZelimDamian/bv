#include "Volume.h"
#include <algorithm> // for std::max/min on MSVC

using namespace bv;
using namespace bv::volume;

VolumeBase::VolumeBase(void* data, size_t width, size_t height, size_t length)
    : m_data(data), m_width(width), m_height(height), m_depth(length)
{
    m_size = width * height * length;
}

VolumeBase::VolumeBase(void* data, size_t size) : m_data(data), m_size(size)
{
}

VolumeBase::~VolumeBase()
{}

VolumeBase::VolumeBase(VolumeBase && rhs)
{
    m_size = rhs.m_size;
    m_data = rhs.m_data;
    rhs.m_data = NULL;
}

VolumeBase::VolumeBase(const VolumeBase &rhs)
{
    m_size = rhs.m_size;
    m_data = std::copy((char*)rhs.m_data, (char*)rhs.m_data + m_size, (char*)m_data);
}

void VolumeBase::setDim(size_t width, size_t height, size_t length)
{
    assert(width * height * length == m_size);

    m_width = width;
    m_height = height;
    m_depth = length;
}

