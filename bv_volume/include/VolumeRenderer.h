#pragma once

#include "Common.h"
#include "Volume.h"
#include "Texture.h"
#include "Shader.h"
#include "RenderMesh.h"

namespace bv
{
    namespace volume {
        class VolumeRenderer
        {
        public:
            VolumeRenderer(const VolumeBasePtr vol);
            ~VolumeRenderer();

            glm::vec3& min() { return m_cutMin; }
            glm::vec3& max() { return m_cutMax; }

			float& minx() { return m_cutMin.x; }
			void minx(float minx) { m_cutMin.x = minx; }

			float& miny() { return m_cutMin.y; }
			void miny(float miny) { m_cutMin.y = miny; }

			float& minz() { return m_cutMin.z; }
			void minz(float minz) { m_cutMin.z = minz; }


			float& maxx() { return m_cutMax.x; }
			void maxx(float maxx) { m_cutMax.x = maxx; }

			float& maxy() { return m_cutMax.y; }
			void maxy(float maxy) { m_cutMax.y = maxy; }

			float& maxz() { return m_cutMax.z; }
			void maxz(float maxz) { m_cutMax.z = maxz; }

            void render(const glm::vec3& eye);

        private:

            glm::vec3 m_stepSize;

            glm::vec3 m_cutMin { 0.0f, 0.0f, 0.0f };
            glm::vec3 m_cutMax { 1.0f, 1.0f, 1.0f };

            rendering::Uniform::Id m_uVolumeTex;
            rendering::Uniform::Id m_uMapTex;
            rendering::Uniform::Id m_uEyePos;
            rendering::Uniform::Id m_uStepSize;
            rendering::Uniform::Id m_uCutMin;
            rendering::Uniform::Id m_uCutMax;

            rendering::Shader::Id m_raycaster;
            rendering::Texture::Id m_volumeTex;
            rendering::Texture::Id m_mapTex;

            rendering::RenderMesh::Id m_cubeRenderId;
        };

        using VolumeRendererPtr = std::shared_ptr<VolumeRenderer>;
    }
}
