#pragma once

#include "Volume.h"
#include "Log.h"

namespace bv
{
    namespace volume
    {
        class VolumeLoader
        {
        public:
            VolumeLoader();
            virtual ~VolumeLoader();

            // Loads a list of raw files by concatenating the contents
            template <typename T>
            VolumePtr<T> stitch(const char* filename, size_t startIndex, size_t step,
                    size_t maxSlices, const char* suffix, size_t width, size_t height, size_t headerSize = 0) const
            {
                Log::writeln("Loading volume from a list of files");

                std::vector<uint8_t> buffer;
                size_t depth = loadDataFiles(filename, startIndex,
                        step, maxSlices, buffer, suffix, headerSize);

                size_t numVoxels = width * height * depth;

                Log::writeln("Number of slices: %u", depth);

                T* data = preprocess<T>(&buffer[0], numVoxels, buffer.size());

                // volume for use
                return std::make_shared<Volume<T>>(data, width, height, depth);
            }

            // Loads a single raw volume file containing the whole volume
            template <typename T>
            VolumePtr<T> load(const char* filename, size_t width, size_t height, size_t length, size_t headerSize = 0) const
            {
                size_t dataSize;
                uint8_t* buffer = loadDataFile(filename, dataSize, headerSize);

                size_t numVoxels = width * height * length;

                T* data = preprocess<T>(buffer, numVoxels, dataSize);

                // we need to delete data now
                delete buffer;
                
                // volume for use
                return std::make_shared<Volume<T>>(data, width, height, length);
            }

            void isFillingGaps(bool fillGaps) { m_isFillingGaps = fillGaps; }

        private:
            // loads data into dst from the file with name and returns the size of the data read
            // skips headerSize * bytes
            // assumes that data is allocated
            uint8_t* loadDataFile(const char* filename, size_t& size, size_t headerSize = 0) const;

            size_t loadDataFiles(const char* filename, size_t startIndex, size_t step,
                    size_t maxSlices, std::vector<uint8_t>& buffer, const char* suffix, size_t headerSize = 0) const;

            // converts the data into the right format
            // also takes care of the buffer memory (deletes when appropriate)
            template <typename T>
            T* preprocess(uint8_t* src, size_t numVoxels, size_t size) const
            {
//                Log::writeln("Size of data: %u", size);
//                Log::writeln("Number of voxels: %u", numVoxels);

                // The preprocessed resulting volume memory ptr
                T* dst;

                size_t bytesPerVoxel = size / numVoxels;
                int sizeDifference = sizeof(T) - bytesPerVoxel;
//                float sizeRatio = sizeof(T) / bytesPerVoxel;

                if(bytesPerVoxel == sizeof(T))
                {
                    // values are the same so perform an optimized copy
                    //dst = src;
                    // or just return srcptr
//                    return (T*)src;
                    // only need to stop the outer things from deleting memory
                    Log::writeln("Volume formats are same - directly copying data");
                    Log::writeln("Size of data: %u", size);
                    dst = new T[numVoxels];

                    std::copy(src, src + size, (uint8_t*)dst);

//					for (size_t i = 0; i < size; i+=size/100000)
					{
//                        if(src[i])
//                            Log::writeln("Direct copy: [%u]", src[i]);
					}
                }
                else if (sizeDifference < 0)
                {
                    // we are compressing values
                    Log::writeln("Volume formats are different - COMPRESSING data");

                    Log::writeln("Bytes per voxel: %u", bytesPerVoxel);
                    Log::writeln("Size diff bytes: %i", sizeDifference);

                    // turn the difference into a positive value for shifting
                    sizeDifference = - sizeDifference;

                    // we will need a separate memory for expanding data
                    dst = new T[numVoxels];

                    // we are stretching our values
                    for (int i = 0; i < numVoxels; ++i)
					{

                        uint16_t src16 = ((uint16_t*)src)[i];

                        uint8_t src2[2] = { src[i * 2], src[i * 2 + 1] };
                        dst[i] = src2[0] << 4 | src2[1] >> 4;
                        dst[i] = dst[i] << 2;
//                        dst[i] = src16 >> (8 * sizeDifference);

//                        if(i % numVoxels / 1000 == 0)
                        {
//                            if(src16 != 0)
                            {
//                                Log::writeln("Debug value: [ %u, %u ]--> %u", src2[0], src2[1], dst[i]);
//                                Log::writeln("Debug value: [ %u ] --> %u", src16, dst[i]);
                            }
                        }
                    }
                }
                else
                {
                    Log::writeln("Volume formats are different - STRETCHING data");

                    Log::writeln("Bytes per voxel: %u", bytesPerVoxel);
                    Log::writeln("Size diff bytes: %i", sizeDifference);

                    // we will need a separate memory for expanding data
                    dst = new T[numVoxels];

                    // we are stretching our values
                    for (int i = 0; i < numVoxels; ++i) 
					{
                        // reinterpret shifted src ptr as T and copy data
                        T datum = (T)src[i]; // ((T*)src)[(i)]; !!! this is different
                        // do shifting to move the significant bits upwards (leftwards)
                        dst[i] = datum << (sizeDifference * 8);

//                        if(i % numVoxels / 1000 == 0)
                        {
//                            if(src[i] != 0)
                            {
//                                Log::writeln("Debug value: %i --> %f", src[i], dst[i]);
                            }
                        }
                    }
                }

                return dst;
            }

            bool m_isFillingGaps {false};
        };
    }
}
