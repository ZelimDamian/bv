#pragma once

#include "Common.h"

namespace bv
{
    namespace volume
    {
        class VolumeBase
        {
        public:
            VolumeBase(void* data, size_t width, size_t height, size_t length);
            VolumeBase(void* data, size_t size);
            VolumeBase(VolumeBase&& volume);
            VolumeBase(const VolumeBase& volume);
            virtual ~VolumeBase();

            const void* data() const { return m_data; }

            uint16_t width() const { return m_width; }
            uint16_t height() const { return m_height; }
            uint16_t depth() const { return m_depth; }

            glm::vec3 dims() const { return glm::vec3(m_width, m_height, m_depth); }

            void setDim(size_t width, size_t height, size_t length);

            virtual size_t sizeInBytes() const = 0;
            virtual size_t voxelBytes() const = 0;

        protected:
            void* m_data;
            size_t m_size;

            uint16_t m_width;
            uint16_t m_height;
            uint16_t m_depth;
        };

        template <typename T>
        class Volume : public VolumeBase
        {
        public:
            Volume() : VolumeBase(nullptr, 0, 0, 0) {}

            Volume(T* data, size_t width, size_t height, size_t length) : VolumeBase(data, width, height, length)
            { }

            Volume(const std::vector<T>& data, size_t width, size_t height, size_t length) : VolumeBase(nullptr/* base cstr is done first*/, width, height, length)
            {
                m_data = new T[m_size];
                std::copy( data.begin(), data.end(), (T*)m_data );
            }

            // very important move constructor
            Volume(Volume && rhs) : VolumeBase(std::move(rhs)) {}

            ~Volume()
            {
                delete (T*)m_data;
            }

            const T* data() const { return m_data; }

            template<typename S>
            S sample(float x, float y, float z)
            {
                return (S) sample(x, y, z);
            }

            template<typename S>
            S sample(const glm::vec3& point)
            {
                return (S) sample(point.x, point.y, point.z);
            }

            T sample(size_t x, size_t y, size_t z)
            {
                assert(x < m_width && x >= 0);
                assert(y < m_height && y >= 0);
                assert(z < m_depth && z >= 0);

                size_t index = x + y * m_width + z * m_width * m_height;

                return ((T*)m_data)[index];
            }

            T sample(float x, float y, float z)
            {
//                std::min(std::max(x, 0.0f), 1.0f);
//                std::min(std::max(y, 0.0f), 1.0f);
//                std::min(std::max(z, 0.0f), 1.0f);

//                assert(x > 0.0f && x < 1.0f);
//                assert(y > 0.0f && y < 1.0f);
//                assert(z > 0.0f && z < 1.0f);

                size_t x_i = (size_t)(x * (m_width - 1));
                size_t y_i = (size_t)(y * (m_height - 1));
                size_t z_i = (size_t)(z * (m_depth - 1));

                return sample(x_i, y_i, z_i);
            }

            size_t sizeInBytes() const { return m_size * sizeof(T); }
            size_t voxelBytes() const { return sizeof(T); }
        };

        template <typename T>
        using VolumePtr = std::shared_ptr<Volume<T>>;

        using VolumeBasePtr = std::shared_ptr<VolumeBase>;
    }
}
