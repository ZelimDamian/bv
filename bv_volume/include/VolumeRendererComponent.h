#pragma once

#include "Component.h"
#include "VolumeRenderer.h"

namespace bv {
    namespace volume {
        class VolumeRendererComponent : public Component<VolumeRendererComponent>
        {
        public:
            VolumeRendererComponent(Id id, Entity::Id entity, VolumeBasePtr vol)
                    : ComponentBase(id, entity), m_volume(vol)
            {
            }

			//VolumeRendererComponent(const VolumeRendererComponent&) = delete;

            static void init();
            static void allocate();
			static void update();

            void attachGUI();

            static std::string title() { return "Volume renderer"; }
        private:
            VolumeRendererPtr m_renderer;
            VolumeBasePtr m_volume;
        };
    }
}
