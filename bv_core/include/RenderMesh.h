#pragma once

#include "Common.h"
#include "Store.h"
#include "Tri.h"
#include "Mesh.h"
#include "Buffer.h"
#include "Vertex.h"
#include "RenderingSystem.h"
#include "VertexDecl.h"

namespace bv
{
    namespace rendering
    {
        class RenderMesh: public Storable<RenderMesh>
        {
        public:
            RenderMesh(Id id) : StorableBase(id) { }

            template<class M>
            static Id create(Handle<M> meshId)
            {
                // get the reference to the instance of mesh
                auto mesh = M::get(meshId);

                // only create buffer if there is something to upload
                if (mesh->vertices.size() == 0)
                {
                    // set to an invalid unspecified id if mesh is empty
                    mesh->renderMeshId = RenderMesh::Id::invalid();
                }
                else
                {
                    // in case we already have a _renderMesh created
                    if (!!mesh->renderMeshId)
                    {
                        auto area = bv::areaof(mesh->vertices);

                        if(area == mesh->renderMeshId->m_allocated)
                        {
                            updateBuffers(mesh->renderMeshId, mesh->vertices);
                        }
                        else
                        {
                            // destroy the memory associated with it
                            deallocate(mesh->renderMeshId);

                            // delegate to the rendering backend to build the actual VBO and IBO objects
                            createBuffers(mesh->renderMeshId, mesh->vertices, mesh->indices);

                        }
                    }
                    else
                    {
                        // use the generic Storable's creation logic
                        Id lastId =  StorableBase::create();
                        // assign the newly created render mesh id to the mesh
                        mesh->renderMeshId = lastId;

                        // delegate to the rendering backend to build the actual VBO and IBO objects
                        createBuffers(mesh->renderMeshId, mesh->vertices, mesh->indices);

                    }
                }

                return mesh->renderMeshId;
            }

        private:

			// destroys an instance of this class with the specified handle id
			static void deallocate(Id id)
			{
				RenderingSystem::instance()->destroy(id);
			}

            template <typename TVertex>
            static void createBuffers(Id renderMeshId,
							   const std::vector<TVertex>& vertices,
							   const std::vector<uint32_t>& indices)
            {
                auto renderer = RenderingSystem::instance();
                Id id = renderMeshId;

                // check if a render mesh for the mesh already exists
                if(!renderer->vboExists(id))
                {
                    // TODO: this is not necessarily valid
                    renderer->createVbo();
                    renderer->createIbo();
                }

                int declId = VertexDeclaration<TVertex>::decl();

                auto area = bv::areaof(vertices);

				renderer->upload(id, vertices.data(), area, declId);

                renderMeshId->m_allocated = area;

                // check if we do have any index data
                if(indices.size() > 0)
                {
                    renderer->uploadIndices(id, indices.data(), bv::areaof(indices));
                }
            }

            template <typename TVertex>
            static void updateBuffers(Id renderMeshId,
                                      const std::vector<TVertex>& vertices)
            {
                auto renderer = RenderingSystem::instance();
                Id id = renderMeshId;

                auto area = bv::areaof(vertices);

                // check if a render mesh for the mesh already exists
                if(!renderer->vboExists(id))
                {
                    Log::error("Can't update non-existent buffer!");
                }

                if(area != id->m_allocated)
                {
                    Log::error("Can't update with non-matching size!");
                }

                int declId = VertexDeclaration<TVertex>::decl();

                renderer->update(id, vertices.data(), area);

            }

        private:
            size_t m_allocated;
        };
    }
}