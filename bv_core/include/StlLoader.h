#pragma once

#include "MathLib.h"
#include "Log.h"
#include "Mesh.h"
#include <string>

namespace bv
{
	class StlLoader
	{
	public:

		static Mesh::Id load(const std::string& filepath);
		static Mesh::Id load_absolute(const std::string& filepath);
		static void load_absolute(const std::string& filepath, std::vector<glm::vec3>& positions,
		                     std::vector<glm::vec3>& normals, std::vector<uint32_t> &indices);
	};
}