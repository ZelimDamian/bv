#pragma once

#include <vector>
#include <array>
#include "MathLib.h"

namespace bv
{
    /////////////////////////////////////////////////////////////
    //// Policy classes from Modern C++ Design by A. Alexandrescu
    /////////////////////////////////////////////////////////////


    // A policy to make a class data reinterpretable
    // requires the class the have a data() method that returns a pointer
    // to data to be reinterpreted
    template< class T> class Reinterpretable; // primary template

    // partial specialization as per http://en.cppreference.com/w/cpp/language/template_parameters
    template< template<class, typename...> class TT, class T, typename ... Rest>
    class Reinterpretable<TT<T, Rest...>>
    {
    protected:
        using Type = TT<T, Rest...>;
    public:
        // reinterpret the data as a different type
        template <typename S>
        S* reinterpret() { return reinterpret_cast<S*>(static_cast<Type*>(this)->data()); }

        // access data as a const pointer of a different type
        template <typename Y>
        const Y* reinterpret() const {  return reinterpret_cast<const Y*>(static_cast<const Type*>(this)->data()); }

        template <typename Y>
        const size_t sizeas() { return static_cast<Type*>(this)->sizeInBytes() * sizeof(T) / sizeof(Y); }
    };

    // A policy to make a class indexable with a single index (as a vector)
    // or 2 indices (as a matrix)
    // requires the class to provide linear indexing method at(int index)
    // and the number of columns through cols() method
    template <class T> class Indexable; // primary specialization

    template <template <class, typename...> class TT, class T, typename ... Args> // partial specialization
    class Indexable<TT<T, Args...>>
    {
    protected:
        typedef TT<T, Args...> Type;

    public:
        // linear indexing
        T& operator () (int i) { return _at(i); }
        const T& operator () (int i) const { return _at(i); }

        // linear indexing with square brackets
        T& operator [] (int i) { return _at(i); }
        const T& operator [] (int i) const { return _at(i); }

        // matrix indexing based on the number of rows and columns
        T& operator () (int i, int j) { return at2(i, j); }
        const T& operator () (int i, int j) const { return at2(i, j); }

        // matrix indexing based on the number of rows and columns
        T& at2(int i, int j) { return _at(j + i * _cols()); }
        const T& at2(int i, int j) const { return _at(j + i * _cols()); }

    private:
        T& _at(int index) { return static_cast<Type*>(this)->at(index); }
        const T& _at(int index) const { return static_cast<const Type*>(this)->at(index); }

        int _cols() const { return static_cast<const Type*>(this)->cols(); }
    };
    // Buffer class to store data
    // C - columns
    // R - rows
    template <typename T>
    class Buffer
        : public Reinterpretable<Buffer<T>>,
          public Indexable<Buffer<T>>
    {
    public:
        // default constructor
        Buffer() : m_c(0), m_r(0) {}
		explicit Buffer(size_t C) : m_c(C), m_r(1) { m_data.resize(C); set(T()); }
		explicit Buffer(size_t R, size_t C) : m_c(C), m_r(R) { m_data.resize(C*R); set(T()); }

	    // vector copy constructor
	    explicit Buffer(const std::vector<T>& rhs) : m_data(rhs), m_c(rhs.size()), m_r(1) { }

        // copy constructor
        Buffer(const Buffer& rhs) : m_data(rhs.m_data), m_c(rhs.m_c), m_r(rhs.m_r) { }

        // move constructor
        Buffer(Buffer&& rhs) : m_data(std::move(rhs.m_data)) {}

        // perform a copy assignement to an lvalue
        Buffer& operator=(const Buffer& other) // move assignment
        {
            assert(this != &other); // self-assignment check not required
            m_data = other.m_data;
            return *this;
        }

        // perform a copy assignment with move to an rvalue reference
        Buffer& operator=(Buffer&& other)
        {
            assert(this != &other); // self-assignment check not required
            m_data = std::move(other.m_data);
            return *this;
        }

        // change size of the buffer (this will clear the data)
        void resize(size_t sizex, size_t sizey = 1)
        {
//            m_data.clear();
            m_data.resize(sizex * sizey);
//            std::vector<T>(sizex * sizey).swap(m_data);
        }

        // set all elements of the buffer to value
        void set(const T& value)
        {
            for (int i = 0; i < m_data.size(); ++i)
            {
                m_data[i] = value;
            }
        }

        void zero()
        {
            for (int i = 0; i < m_data.size(); ++i)
            {
                m_data[i] = T { 0 };
            }
        }

        // linear indexing
        inline T& at(int i) { return m_data[i]; }
        inline const T& at(int i) const { return m_data[i]; }

        // access to underlying data as a pointer
        T* data() { return m_data.data(); }
        const T* data() const { return m_data.data(); }
	    T* ptr() { return m_data.data(); }
	    const T* ptr() const { return m_data.data(); }

//        // copy a slice of the buffer into an array
//        template <int C, int R>
//        void arrayCopy(size_t offset, Array<T, Dims<C, R>>& array) const
//        {
//            const T* d = this->data() + offset;
//            static const int COUNT = C * R;
//            for (int i = 0; i < COUNT; ++i)
//            {
//                array.at(i) = d[i];
//            }
//        }
//
//        // inject the values from the array at the offset continuously
//        template <int C, int R>
//        void embed(size_t offset, const Array<T, Dims<C, R>>& array)
//        {
//            T* d = data() + offset;
//
//            static const int COUNT = C * R;
//
//            for (int i = 0; i < COUNT; ++i)
//            {
//                d[i] = array.at(i);
//            }
//        }

        // reserve some space in the data container
        void reserve(size_t size) { m_data.resize(size); }

        // returns true if the data is empty and false otherwise
        bool empty() const { return m_data.empty(); }

        // removes all data from the data container
        void clear() { m_data.clear(); }

        // total number of elements in the buffer
        inline size_t size() const { return m_data.size(); }

	    // size of the buffer as if it contains type T
	    template <typename S>
	    inline size_t size() const { sizeInBytes() / sizeof(S); }

        // total size in bytes
        inline size_t sizeInBytes() const { return size() * sizeof(T); }

        // accessing column number
        inline size_t cols() const { return m_c; }
        // accessing row number
        inline size_t rows() const { return m_r; }

        // iterator interface
        T* begin() { return data(); }
        const T* begin() const { return data(); }
        T* end() { return data() + size(); }
        const T* end() const { return data() + size(); }

    private:
        // storage for our data
        std::vector<T> m_data;
        size_t m_c, m_r;
    };

    using BufferInt = Buffer<int>;
    using BufferFloat = Buffer<float>;
    using BufferVec2 = Buffer<glm::vec2>;
    using BufferVec3 = Buffer<glm::vec3>;
    using BufferVec4 = Buffer<glm::vec4>;
}