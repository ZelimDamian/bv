#pragma once

#include "Common.h"
#include "System.h"
#include "RenderContext.h"

namespace bv
{
	class WindowSystem : public SystemSingleton<WindowSystem>
	{
	public:

		WindowSystem();

		int width() { return m_width; }
		int height() { return m_height; }

		void run();
        void exit() { m_shouldExit = true; }

		glm::vec4 viewport() const  { return glm::vec4(0, 0, m_width, m_height); }
		glm::vec2 center() const  { return glm::vec2(m_width / 2.0f, m_height / 2.0f); }

		void beforeRun(Action action) { m_beforeRun += action; }
		void onresize(const Action& action) { m_onreseized += action; }

	private:
		uint32_t m_width { 1280 };
		uint32_t m_height { 720 };

		void state(int width, int height);

		std::string m_title;// { "Birth View" };

		UniqueEvent m_beforeRun;

		Event m_onreseized;

        bool m_shouldExit { false };
	};
}
