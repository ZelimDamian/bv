#pragma once
#include "MathLib.h"
#include <vector>
#include "Buffer.h"

namespace bv
{
	struct Box
	{
        Box() {}

        Box(const glm::vec3& min, const glm::vec3& max): m_min(min), m_max(max) {}

		glm::vec3 center() const { return (m_max + m_min) / 2.0f; }

		void center(const glm::vec3& vec)
		{ 
			glm::vec3 halfsize = this->halfsize();
			m_min = vec - halfsize;
			m_max = vec + halfsize;
		}

        glm::vec3 min() const { return m_min; }
        glm::vec3 max() const { return m_max; }

		inline glm::vec3 size() const { return m_max - m_min; }
		inline glm::vec3 halfsize() const { return size() / 2.0f; }

        void enclose(const std::array<glm::vec3, 3>& verts)
        {
			enclose(verts[0]);
	        enclose(verts[1]);
	        enclose(verts[2]);
        }

		void enclose(const Box& box)
        {
			enclose(box.min());
	        enclose(box.max());
        }

        template <typename TPoint>
		void enclose(const TPoint& point)
        {
            if(m_min.x > point.x) { m_min.x = point.x; }
            if(m_min.y > point.y) { m_min.y = point.y; }
            if(m_min.z > point.z) { m_min.z = point.z; }

            if(m_max.x < point.x) { m_max.x = point.x; }
            if(m_max.y < point.y) { m_max.y = point.y; }
            if(m_max.z < point.z) { m_max.z = point.z; }
        }

		template <typename T>
		void fit(const T& points, const glm::mat4& mtx)
		{
			m_min = glm::vec3(9999999);
			m_max = -m_min;

			for (int i = 0; i < points.size(); ++i)
			{
				enclose(Math::transform(points[i], mtx));
			}

			perturb();
		}

        template <typename T>
		void fit(const T& points)
        {
            m_min = glm::vec3(9999999);
            m_max = -m_min;

            for (int i = 0; i < points.size(); ++i)
            {
                enclose(points[i]);
            }

            perturb();
        }

        // apply slight scaling to avoid flat box
        void perturb()
        {
            glm::vec3 halfsize = this->halfsize();

            const float epsilon = 0.00001f;
            for (int i = 0; i < 3; ++i)
            {
                if(halfsize[i] < epsilon)
                {
                    halfsize[i] += epsilon;
                }
            }

            glm::vec3 center = this->center();

            m_min = center - halfsize;
            m_max = center + halfsize;
        }

		// index into box bound values if(index == 0) return m_min;
		// if(index == 1) return m_max;
		const glm::vec3& operator [](int index) const
		{
			assert(index >= 0 && index < 2);
			return reinterpret_cast<const glm::vec3*>(this)[index];
		}

        int longestAxis();

		static Box transform(const Box& box, const glm::mat4& mat)
		{
			auto center = Math::transform(box.center(), mat);

			auto halfsize = box.halfsize();

			return Box { center - halfsize, center + halfsize };
		}

    private:
		glm::vec3 m_min {  999999999.0f };
        glm::vec3 m_max { -999999999.0f };
    };

	struct Obb
	{
		Box aabb;
		glm::mat4 mtx;
	};
}
