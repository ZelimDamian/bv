#pragma once

#include "Element.h"

namespace bv
{
    namespace gui
    {
        class LabelElement: public IElement
        {
        public:
            LabelElement(PanelElement* parent, const std::string& title) : IElement(parent, title) { }

            virtual std::string text() { return m_title; }

        private:
            void renderImpl() override;
        };

        class DynLabelElement: public LabelElement
        {
        public:
            DynLabelElement(PanelElement* parent, const std::string& title, std::function<std::string()> source):
                m_source(source), LabelElement(parent, title) {}

            std::string text() override { return m_source(); }

        private:
            std::function<std::string()> m_source;
        };
    }
}
