#pragma once

#include "System.h"
#include "Timing.h"
#include "Parallel.h"

namespace bv
{
	class UpdateSystem: public SystemSingleton<UpdateSystem>
	{
	public:

		void onupdate(const Action& action) { m_onupdate += action; }
        void onsync(const Action& action) { m_onsync += action; }

		void update() override
		{
			static const float FIXED_TIMESTEP = Timing::holder().fixed;
			const float dt = Timing::delta();

			const int iterations = std::max(1, (int) (FIXED_TIMESTEP / dt));

            for (int i = 0; i < iterations; ++i)
            {
                auto count = m_onupdate.observers().size();

                auto worker = [&] (int i)
                {
                    m_onupdate.observers()[i]();
                };

                // do updates on parallel
                Parallel::force_foreach(std::vector<int>(count), worker);

	            auto tock = Profiler::instance()->tick("Sync");
                // do a non-paralell update
                m_onsync();
	            tock();
            }
		}

	private:
		Event m_onupdate;
        Event m_onsync;
	};
}