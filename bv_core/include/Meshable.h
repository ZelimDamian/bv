#pragma once

#include "MathLib.h"
#include <Buffer.h>
#include "Tri.h"
#include "Box.h"

namespace bv
{
	struct PointCloud
	{
		// list of all vertex positions
		VectorVec3 positions;
		Box m_box;

		const Box &box() const { return m_box; }
		void box(const Box &box) { m_box = box; }

// translate all by single value
		void translate(const glm::vec3& translation)
		{
			for (size_t i = 0; i < positions.size(); i++)
			{
				positions[i] += translation;
			}
		}

		void translate(const VectorVec3& translations)
		{
			assert(translations.size() == positions.size());

			for (size_t i = 0; i < positions.size(); i++)
			{
				positions[i] += translations[i];
			}
		}

		// move all points so that center of mass matches origin
		glm::vec3 normalize()
		{
			auto center = std::accumulate(positions.begin(), positions.end(), glm::vec3());
			center /= positions.size();

			for (int i = 0; i < positions.size(); ++i)
			{
				positions[i] -= center;
			}

			return center;
		}

		void updateBox()
		{
			m_box.fit(positions);
		}
	};

	template <class VertexType>
	struct Meshable: public PointCloud
	{
		using TVertex = VertexType;

		template < typename ... Args>
		void vertex(Args&& ... args)
		{
			vertices.emplace_back(args...);
		}

		// remove all data from mesh
		void clear()
		{
			vertices.clear();
			indices.clear();
		}

		void reserve(size_t size)
		{
			vertices.reserve(size);
		}

		void resize(size_t size)
		{
			vertices.resize(size);
		}

		void updatePositions()
		{
			positions.resize(vertices.size());

			for (int i = 0; i < vertices.size(); ++i)
			{
				positions.at(i) = vertices[i];
			}
		}

		void updateVertices()
		{
			vertices.resize(positions.size());

			for (int i = 0; i < vertices.size(); ++i)
			{
				auto& vert = vertices[i];
				vert.template get<TVertex::Pos>() = positions.at(i);
			}
		}

		glm::vec3 normalize()
		{
			auto center = PointCloud::normalize();
			updateVertices();
			return center;
		}

        // return an array of vertex positions for the triangle
        std::array<glm::vec3, 3> pos(Tri tri) const
        {
            return {{ positions.at(tri[0]), positions.at(tri[1]), positions.at(tri[2]) }};
        }

        glm::vec3& pos(Tri tri, int index)
        {
            return vertices[tri[index]].template get<TVertex::Pos>();
        }

		// merge two separate meshes into a single mesh
		static void merge(const Meshable& src1, const Meshable& src2, Meshable& dst)
		{
			dst.clear();
			dst.vertices.reserve(src1.vertices.size() + src2.vertices.size());
			dst.vertices.clear();

			for (int i = 0; i < src1.vertices.size(); ++i)
			{
				dst.vertices.push_back(src1.vertices[i]);
			}

			for (int i = 0; i < src2.vertices.size(); ++i)
			{
				dst.vertices.push_back(src2.vertices[i]);
			}

			dst.updatePositions();

			dst.indices.reserve(src1.indices.size() + src2.indices.size());
			dst.indices.clear();

			for (int j = 0; j < src1.indices.size(); ++j)
			{
				dst.indices.push_back(src1.indices[j]);
			}
			for (int k = 0; k < src2.indices.size(); ++k)
			{
				dst.indices.push_back(src2.indices[k] + src1.indices.size());
			}
		}


		std::vector<TVertex> vertices;

//		// list of all vertex normals
//		BufferVec3 normals;
//		// list of all vertex texture coordinates
//		BufferVec2 texcoords;

		// list of indices
		std::vector<uint32_t> indices;
	};
}