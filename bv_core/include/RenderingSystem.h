#pragma once

#include "Common.h"
#include "System.h"
#include "Store.h"

#include "FrameBuffer.h"
#include "RenderMode.h"
#include "Mesh.h"

namespace bv
{
	namespace rendering
	{
		class RenderMesh;
		using RenderMeshId = Handle<RenderMesh>;

		class Uniform;
		using UniformId = Handle<Uniform>;

		enum class TextureType;
		class Texture;
		using TextureId = Handle<Texture>;
	}

    class RenderContext;

	class RenderingSystem : public SystemSingleton<RenderingSystem>
	{
	public:
		// initialize the rendering system
		void init() override;

		// defined in RenderingSystem.[PLATFORM].cpp
		void _render();

		// just render everything
		void update() override { /* render(); */ }

		void destroy() override;

		void setViewSize(int x, int y, int width, int height);
        void setViewSize(Recti rect) { setViewSize(rect.x, rect.y, rect.width, rect.height); }
		void setView(const glm::mat4& view, const glm::mat4& proj, int viewIdx = 0);
		void transform(const glm::mat4 &xform);

		// all below defined in RenderingSystem.[PLATFORM].cpp
//        void render(Mesh::Id meshId);

        void submit(rendering::RenderMeshId meshId);
		void reset(Culling culling = Culling::CW, RenderMode renderMode = RenderMode::TRIANGLES, DepthTest depth = DepthTest::NORMAL);
		void set(rendering::UniformId uniform, const rendering::UniformData& value);

		// immediate rendering
		void render(rendering::RenderMeshId handle, rendering::ShaderId shader);

		void apply(RenderState state);
		void apply(const rendering::UniformSubmission &state);
        void apply(rendering::UniformId uniform, const rendering::UniformData& data);
		void apply(const glm::mat4& xform);

		// apply all render submission components now
		void apply(RenderSubmission &submission);
		void applyPending();

		// creates an empty 2D texture with given size
		rendering::TextureId createTexture2D(rendering::TextureType type, uint16_t width, uint16_t height );
		// creates an empty 3D texture with given size
		rendering::TextureId createTexture3D(rendering::TextureType type, uint16_t width, uint16_t height, uint16_t depth);
		// upload the data into the texture with given id
		void updateTexture2D(rendering::TextureId textureId, const void* data);
		// upload data into the texture with given id
		void updateTexture3D(rendering::TextureId textureId, const void *data);
		// bind a texture to texture unit
		void bindTexture(rendering::TextureId textureId, rendering::UniformId uniformId, uint8_t texUnit);

		void createFrameBuffer(glm::ivec2 size);
		void bindFrameBuffer(rendering::FrameBuffer::Id);

		// query the list of render meshes and return true if one with given id is found
		bool vboExists(rendering::RenderMeshId);
		// create a vbo handle
		void createVbo();
		// upload data to an fbo
		void upload(rendering::RenderMeshId id, const void* data, size_t size, int declId);
        void update(rendering::RenderMeshId handle, const void *data, size_t sizeInBytes);
		void destroy(rendering::RenderMeshId id);

		// create a ibo handle
		void createIbo();
		void uploadIndices(rendering::RenderMeshId id, const void* data, size_t size);

		// subscribe the caller to the render event
		void onrender(Action func);
		// subscribe the caller to the render event that is cleared after every frame
		void onrender_once(Action func);

        void onPreRender(Action func) { m_onPreRender += func; }

    private:
        Event m_onPreRender;

		std::vector<RenderSubmission> m_submissions;
	};
}

