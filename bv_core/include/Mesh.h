#pragma once

#include "MathLib.h"
#include "Tri.h"
#include "Store.h"
#include "Meshable.h"
#include "Vertex.h"

namespace bv
{
    namespace rendering
    {
        class RenderMesh;
    }

    // mesh structure
    template <class VertType>
    class GenMesh:
		    public Storable<GenMesh<VertType>>,
		    public Meshable<VertType>
    {
    public:
	    using TVertex = VertType ;
        using StorableBase = Storable<GenMesh<TVertex>>;
        using Id = typename StorableBase::Id;
        using MeshableBase = Meshable<TVertex>;

		explicit GenMesh(Id id) : StorableBase(id),
                         renderMeshId(Handle<rendering::RenderMesh>::invalid()) { }

        // build a POD version of the triangle at index
        Tri tri(int index) const
        {
            return triangles[index];
        }

        // create a triangle with the given indicies
        Tri tri(uint32_t v1, uint32_t v2, uint32_t v3)
        {
            triangles.emplace_back(triangles.size(), v1, v2, v3);
            return triangles.back();
        }

        // gather up all the indices in this mesh for an IBO
        void updateIndices()
        {
            this->indices.resize(triangles.size() * 3);

            for(int i = 0; i < triangles.size(); i++)
            {
                this->indices[i * 3 + 0] = triangles[i].vi[0];
                this->indices[i * 3 + 1] = triangles[i].vi[1];
                this->indices[i * 3 + 2] = triangles[i].vi[2];
            }
        }

	    void updateWireIndices()
	    {
		    this->indices.resize(triangles.size() * 6);

		    for(int i = 0; i < triangles.size(); i++)
		    {
			    this->indices[i * 6 + 0] = triangles[i].vi[0];
			    this->indices[i * 6 + 1] = triangles[i].vi[1];
			    this->indices[i * 6 + 2] = triangles[i].vi[1];
			    this->indices[i * 6 + 3] = triangles[i].vi[2];
			    this->indices[i * 6 + 4] = triangles[i].vi[2];
			    this->indices[i * 6 + 5] = triangles[i].vi[0];
		    }
	    }

        void assignTriNorToVerts(Tri t)
        {
            const glm::vec3& normal = normals[t.index];

            this->vertices[t.vi[0]].template get<VertType::Nrm>() = normal;
            this->vertices[t.vi[1]].template get<VertType::Nrm>() = normal;
            this->vertices[t.vi[2]].template get<VertType::Nrm>() = normal;
        }

        void updateTriNormals()
        {
            normals.resize(triangles.size());
            this->updatePositions();

            // calculate triangle normals
            for (int i = 0; i < triangles.size(); ++i)
            {
                auto t = tri(i);

                glm::vec3 e1 = this->positions.at(t.vi[2]) - this->positions.at(t.vi[0]);
                glm::vec3 e2 = this->positions.at(t.vi[1]) - this->positions.at(t.vi[0]);

                normals[i] = glm::normalize(glm::cross(e1, e2));
            }
        }

        void removeDoubles(float tolerance)
        {

        }

        void updateAllNormals()
        {
	        updateTriNormals();
	        updateVertNormals();
        }

		void updateVertNormals()
		{
			// only if we have connections setup
			if (!m_connections.empty())
			{
				for (int i = 0; i < this->positions.size(); ++i)
				{
					// reference to the vertex normal
					glm::vec3& normal = this->vertices[i].template get<VertType::Nrm>();


					for (int j = 0; j < m_connections[i].size(); ++j)
					{
						const int triIdx = m_connections[i][j];
						const glm::vec3& triNormal = normals[triIdx];

						normal += triNormal;
					}

					normal = glm::normalize(normal);
				}
			}
        }

	    void setupConnections()
	    {
		    this->m_connections.resize(this->positions.size());
		    this->m_connections.assign(this->positions.size(), {});

		    for (int i = 0; i < triangles.size(); ++i)
            {
                auto t = tri(i);
                this->m_connections[t.vi[0]].push_back(t.index);
                this->m_connections[t.vi[1]].push_back(t.index);
                this->m_connections[t.vi[2]].push_back(t.index);
            }
	    }

	    void clear()
        {
            triangles.clear();
	        normals.clear();
            MeshableBase::clear();
        }

        // list of triangles
        std::vector<Tri> triangles;
        // list of triangle normals
        std::vector<glm::vec3> normals;
        // index of the corresponding mesh
        Handle<rendering::RenderMesh> renderMeshId;

        // vertex indices per triangle
	    std::vector<std::vector<int>> m_connections;
    };

	using Mesh = GenMesh<PosNormColVertex>;
    using ColorMesh = GenMesh<PosNormColVertex>;
}
