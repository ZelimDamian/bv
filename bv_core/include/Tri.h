#pragma once

#include "Common.h"

namespace bv
{
	using TriVerts = std::array<glm::vec3, 3>;

	// a "thin" triangle with indices to data
	struct Tri
	{
		Tri(uint32_t ind, uint32_t v1, uint32_t v2, uint32_t v3) : index(ind)
		{
			vi[0] = v1;
			vi[1] = v2;
			vi[2] = v3;
		}

		// vertex, normal and texcoord indices
		uint32_t vi[3];

		// index of this triangle in the mesh
		uint32_t index;

		inline uint32_t operator[](int index) const { return vi[index]; }

		bool hasVertexWithId(uint32_t index)
		{
			return index == vi[0] || index == vi[1] || index == vi[2];
		}

		inline TriVerts vertices(const VectorVec3 &positions) const
		{
			return TriVerts {positions[vi[0]], positions[vi[1]], positions[vi[2]]};
		}
	};

	inline bool operator == (const Tri& tri1, const Tri& tri2)
	{
		return tri1.vi[0] == tri2.vi[0] && tri1.vi[1] == tri2.vi[1] && tri1.vi[2] == tri2.vi[2];
	}
}
