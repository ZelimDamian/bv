#pragma once

#include "MathLib.h"

namespace bv
{
	struct Ray
	{
        Ray(const glm::vec3& pos, const glm::vec3& dir): m_pos(pos), m_dir(dir)
        {
	        inv_direction = glm::vec3(1/dir.x, 1/dir.y, 1/dir.z);
	        sign[0] = (inv_direction.x < 0);
	        sign[1] = (inv_direction.y < 0);
	        sign[2] = (inv_direction.z < 0);
        }

		glm::vec3 point(float t) const { return m_pos + m_dir * t; }

        glm::vec3 pos() const { return m_pos; }
        glm::vec3 dir() const { return m_dir; }

		static Ray transform(const Ray& ray, const glm::mat4& mat)
		{
			glm::vec3 pos = Math::transform(ray.m_pos, mat);
			glm::vec3 dir = Math::transformNormal(ray.m_dir, mat);
			return Ray(pos, dir);
		}

		glm::vec3 m_pos, m_dir;
		glm::vec3 inv_direction;
		int sign[3];
	};
}
