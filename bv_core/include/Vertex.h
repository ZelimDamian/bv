#pragma once

#include <tuple>
#include <vector>
#include <utility>
#include <TupleReverser.h>

#include "Attribute.h"
#include "AttributeMapping.h"

namespace bv
{

	// a generic vertex declaration structure
	// it is capable of storing a variadic set of types
    template <typename ... Types>
    struct Vertex
    {
    private:

	    // The vertex data type as a tuple
	    using TupleType = std::tuple<Types...>;

#ifdef __APPLE__
		// the tuple is not in reverse order on iOS/Mac
		using ReverseTupleType = TupleType;

		template <int N>
		struct element_index
		{
			static const int value = N;
		};
#else
		// the tuple in the reverse order of variadic types
	    using ReverseTupleType = typename util::tuple_reverse<TupleType>::type;

		template <int N>
		struct element_index
		{
			static const int value = sizeof...(Types) - N - 1;
		};
#endif

	    // allows accessing tuple types at specified index N
	    template <int N>
	    using element_type = typename std::tuple_element<N, ReverseTupleType>::type;

	    // per vertex data
		ReverseTupleType m_data;
		
    public:

	    // accessing attributes per vertex
	    template <int N>
	    auto get() -> element_type<element_index<N>::value>&
	    {
		    return std::get<element_index<N>::value>(m_data);
	    }

		// accessing attributes per vertex
		template <int N>
		auto get() const -> const element_type<element_index<N>::value>&
		{
			return std::get<element_index<N>::value>(m_data);
		}

	    // accessing attributes per vertex
	    template <int N>
	    void set(const element_type<element_index<N>::value>& value)
	    {
		    std::get<element_index<N>::value>(m_data) = value;
	    }

	    operator glm::vec3() { return get<0>(); }

	    static std::vector<Attribute> attributes()
	    {
		    static bool done = false;
		    if(!done)
		    {
				// trick to unroll argument pack and call a template method with each
				(void) std::initializer_list<int>{(add < Types >(), 0)...};
		    }
		    return s_attributes;
	    }

	    using BaseVertex = Vertex<Types...>;


    private:

	    template <typename A>
	    static void add()
	    {
		    using AMap = AttributeMapping<A>;
		    Attribute att = {AMap::size(), AMap::type()};
		    s_attributes.push_back(att);
	    }

	    static std::vector<Attribute> s_attributes;
    };

	template <typename ... TT>
	std::vector<Attribute> Vertex<TT...>::s_attributes;

    // Vertex containing a position and a color vec4
    class PosColorVertex : public Vertex<glm::vec3, glm::vec4>
    {
    public:
	    enum { Pos = 0, Col };

	    PosColorVertex() : PosColorVertex(glm::vec3(), glm::vec4(1.0f)) { }

	    PosColorVertex(const glm::vec3 &pos) : PosColorVertex(pos, glm::vec4(1.0f)) { }

	    PosColorVertex(const glm::vec3 &pos, const glm::vec4 &col)
	    {
		    set<Pos>(pos);
		    set<Col>(col);
	    }

        // semantics to describe vertex attributes
        static SemanticsType semantics;
    };

    // a vertex represented by a positions, normal and texture coord vec2
    class PosNormTexVertex : public Vertex<glm::vec3, glm::vec3, glm::vec2>
    {
    public:

	    enum { Pos = 0, Nrm, Tex };

	    PosNormTexVertex(): PosNormTexVertex(glm::vec3(), glm::vec3(), glm::vec2()) {  }

	    PosNormTexVertex(const glm::vec3& pos): PosNormTexVertex(pos, glm::vec3(), glm::vec2()) {  }

	    PosNormTexVertex(const glm::vec3& pos, const glm::vec3& norm): PosNormTexVertex(pos, norm, glm::vec2()) {  }

	    PosNormTexVertex(const glm::vec3 &pos, const glm::vec3 &nor, const glm::vec2 &tex)
	    {
		    get<Pos>() = pos;
		    get<Nrm>() = nor;
		    get<Tex>() = tex;
	    }

        // semantics to describe vertex attributes
        static SemanticsType semantics;
    };

    class PosNormColVertex: public Vertex<glm::vec3, glm::vec3, glm::vec4>
    {
    public:

        PosNormColVertex(): PosNormColVertex(glm::vec3(), glm::vec3(), glm::vec4(1.0f)) {}
		PosNormColVertex(const glm::vec3& pos): PosNormColVertex(pos, glm::vec3(0.0f), glm::vec4(1.0f)) {}
	    PosNormColVertex(const glm::vec3& pos, const glm::vec3& nrm): PosNormColVertex(pos, nrm, glm::vec4(1.0f)) {}
        PosNormColVertex(const glm::vec3& pos, const glm::vec3& nrm, const glm::vec4& col)
        {
            get<Pos>() = pos;
            get<Nrm>() = nrm;
            get<Col>() = col;
        }

	    const glm::vec3& normal() const { return get<Nrm>(); }
	    void normal(const glm::vec3& normal) { get<Nrm>() = normal; }

        enum { Pos = 0, Nrm, Col };

        // semantics to describe vertex attributes
        static SemanticsType semantics;

    };
}
