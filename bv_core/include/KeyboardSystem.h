#pragma once

#include "Common.h"
#include "System.h"

namespace bv
{
    enum class Key {
        UNKNOWN = 0,
        A = 4,
        B = 5,
        C = 6,
        D = 7,
        E = 8,
        F = 9,
        G = 10,
        H = 11,
        I = 12,
        J = 13,
        K = 14,
        L = 15,
        M = 16,
        N = 17,
        O = 18,
        P = 19,
        Q = 20,
        R = 21,
        S = 22,
        T = 23,
        U = 24,
        V = 25,
        W = 26,
        X = 27,
        Y = 28,
        Z = 29,

        NUM1 = 30,
        NUM2 = 31,
        NUM3 = 32,
        NUM4 = 33,
        NUM5 = 34,
        NUM6 = 35,
        NUM7 = 36,
        NUM8 = 37,
        NUM9 = 38,
        NUM0 = 39,


        RETURN = 40,
        ESCAPE = 41,
        BACKSPACE = 42,
        TAB = 43,
        SPACE = 44,

        MINUS = 45,
        EQUALS = 46,
        LEFTBRACKET = 47,
        RIGHTBRACKET = 48,
        LALT = 128,
        LCTRL = 129,
        LSHIFT = 130,
        LGUI,

        UP,
        DOWN,
        LEFT,
        RIGHT,
        INSERT,
        DEL,
        HOME,
        END,
        PAGEUP,
        PAGEDOWN,
        PRINT,
        PLUS,

        SEMICOLON,
        QUOTE,
        COMMA,
        PERIOD,
        SLASH,
        BACKSLASH,
        TILDE,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        NUMPAD0,
        NUMPAD1,
        NUMPAD2,
        NUMPAD3,
        NUMPAD4,
        NUMPAD5,
        NUMPAD6,
        NUMPAD7,
        NUMPAD8,
        NUMPAD9,
        COUNT
    };

    class KeyboardSystem: public SystemSingleton<KeyboardSystem>
    {
    public:
        // check key state
        bool key(Key key) const { return static_cast<bool>(m_keys[static_cast<size_t>(key)]); }
        // subscribe to keypress
        void onkey(Key key, Action action);

        // key was just pressed
//        bool just(Key key)
//        {
//	        auto just = m_keys[(unsigned int) key] == 0 && m_keys_old[(unsigned int) key] == 1;
//	        return just;
//		}

		void onpressed(Key key, const Action& action) { m_events[(int)key] += action; }

        // update key state
        void key(Key key, bool toggle)
        {
	        if(!toggle && m_keys[(int)key])
	        {
				m_events[(int)key]();
	        }

            m_keys_old[(unsigned int)key] = m_keys[(unsigned int)key];
            m_keys[(unsigned int)key] = (int)toggle;
        }

	    std::string name(Key key)
	    {
		    switch (key)
		    {
			    case Key::A : return "A";
			    case Key::B : return "B";
				case Key::C : return "C";
				case Key::D : return "D";
				case Key::E : return "E";
				case Key::F : return "F";
				case Key::G : return "G";
				case Key::H : return "H";
				case Key::I : return "I";
				case Key::J : return "J";
				case Key::K : return "K";
				case Key::L : return "L";
				case Key::M : return "M";
				case Key::N : return "N";
				case Key::O : return "O";
				case Key::P : return "P";
				case Key::Q : return "Q";
				case Key::R : return "R";
				case Key::S : return "S";
				case Key::T : return "T";
				case Key::U : return "U";
				case Key::V : return "V";
				case Key::W : return "W";
				case Key::X : return "X";
				case Key::Y : return "Y";
				case Key::Z : return "Z";

			    case Key::NUM1 : return "NUM1";
				case Key::NUM2 : return "NUM2";
				case Key::NUM3 : return "NUM3";
				case Key::NUM4 : return "NUM4";
				case Key::NUM5 : return "NUM5";
				case Key::NUM6 : return "NUM6";
				case Key::NUM7 : return "NUM7";
				case Key::NUM8 : return "NUM8";
				case Key::NUM9 : return "NUM9";
				case Key::NUM0 : return "NUM0";


			    case Key::RETURN :      return "RETURN";
				case Key::ESCAPE :      return "ESCAPE";
				case Key::BACKSPACE :   return "BACKSPACE";
				case Key::TAB :         return "TAB";
				case Key::SPACE :       return " ";

			    case Key::MINUS :           return "-";
			    case Key::EQUALS :          return "=";
			    case Key::LEFTBRACKET :     return "[";
				case Key::RIGHTBRACKET :    return "]";
				case Key::LALT :            return "LAlt";
				case Key::LCTRL :           return "LCtrl";
			    case Key::LSHIFT :          return "LShift";
				case Key::LGUI :
				{
#ifdef __APPLE__
					return "Cmd";
#else
					return "Win";
#endif
				}

			    case Key::UP :          return "Up";
				case Key::DOWN :        return "Down";
				case Key::LEFT :        return "Left";
				case Key::RIGHT :       return "Right";
				case Key::INSERT :      return "Insert";
				case Key::DEL :         return "Del";
				case Key::HOME :        return "Home";
				case Key::END :         return "End";
				case Key::PAGEUP :      return "PageUp";
				case Key::PAGEDOWN :    return "PageDown";
				case Key::PRINT :       return "Print";
				case Key::PLUS :        return "+";

				case Key::SEMICOLON :   return ";";
				case Key::QUOTE :       return "'";
				case Key::COMMA :       return ",";
				case Key::PERIOD :      return ".";
			    case Key::SLASH :       return "\\";
				case Key::BACKSLASH :   return "/";
				case Key::TILDE :       return "~";
			    case Key::F1 :          return "F1";
				case Key::F2 :          return "F2";
				case Key::F3 :          return "F3";
				case Key::F4 :          return "F4";
				case Key::F5 :          return "F5";
				case Key::F6 :          return "F6";
				case Key::F7 :          return "F7";
				case Key::F8 :          return "F8";
				case Key::F9 :          return "F9";
			    case Key::F10 :         return "F10";
				case Key::F11 :         return "F11";
				case Key::F12 :         return "F12";
			    case Key::NUMPAD0 : return "NUMPAD0";
				case Key::NUMPAD1 : return "NUMPAD1";
				case Key::NUMPAD2 : return "NUMPAD2";
				case Key::NUMPAD3 : return "NUMPAD3";
				case Key::NUMPAD4 : return "NUMPAD4";
				case Key::NUMPAD5 : return "NUMPAD5";
				case Key::NUMPAD6 : return "NUMPAD6";
				case Key::NUMPAD7 : return "NUMPAD7";
				case Key::NUMPAD8 : return "NUMPAD8";
				case Key::NUMPAD9 : return "NUMPAD9";

			    default: return "?";
		    }
	    }

    private:

        int m_keys[256];
        int m_keys_old[256];

	    Event m_events[256];
    };
}
