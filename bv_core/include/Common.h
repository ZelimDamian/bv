#pragma once

#include <string>
#include <vector>
#include <array>
#include <memory>
#include <unordered_set>
#include <unordered_map>

#ifndef NDEBUG
#include <assert.h>
#endif

#include "Log.h"
#include "MathLib.h"
#include "Timing.h"
#include "Profiler.h"

#include "System.h"

#include "Ray.h"
#include "Plane.h"
#include "Rect.h"


////////////////////////////////////////////////
/// Buffer/Array storage
////////////////////////////////////////////////

//#include "Buffer.h"

////////////////////////////////////////////////
/// PTR stuff
///////////////////////////////////////////////

//#define PTR(__p) std::shared_ptr<__p>
//#define DESTROY(__p)
//#define TO_RAW_PTR(__p) __p.get()
//#define NEW(__T, __args) std::make_shared<__T>(__args)

#define PTR(__p) __p*
#define DESTROY(__p) delete __p
#define TO_RAW_PTR(__p) __p
#define NEW(__T, __args) new __T(__args)

namespace bv
{
    template <typename T>
    size_t areaof(const std::vector<T>& vec)
    {
        return vec.size() * sizeof(T);
    }

	template <class TContainer>
	auto map_find(TContainer& cont, typename TContainer::key_type tofind) -> typename TContainer::value_type
	{
		return std::find(cont.begin(), cont.end(), [tofind] (const typename TContainer::key_type& key)
		{
			return tofind.first == key.first && tofind.second == key.second;
		});
	}
}

// while there is no C++14
#if __cplusplus != 201402L && !defined(_MSC_VER)
namespace std
{
    template<typename T, typename... Args>
    unique_ptr<T> make_unique(Args&&... args)
    {
        return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }
}
#endif

namespace bv
{
	template <class TValue>
	using pair_key = std::pair<TValue, TValue>;

	template<class TKey>
	struct Hasher
	{
		inline size_t operator ()(const TKey& key) const
		{
			std::hash<int> hashInt;
			return hashInt((int)key);
		}
	};

	template <class TValue>
	struct Hasher<pair_key<TValue>>
	{
		using Key = pair_key<TValue>;

		inline size_t operator ()(const Key& key) const
		{
			static std::hash<int> hashInt;
			return hashInt(key.first * 31 + key.second);
		}
	};

	template <class T, class THasher = Hasher<T>>
	static void filterUnique(std::vector<T> &vec)
	{
		// need to sort to make sure unique works
		std::unordered_set<T, THasher> s;
		for (const T& i : vec)
			s.insert(i);
		vec.assign( s.begin(), s.end() );
	}

	template <class T>
	static void append(std::vector<T>& to, const std::vector<T>& from)
	{
		to.reserve(to.size() + from.size());
		to.insert(std::end(to), std::begin(from), std::end(from));
	}

	template <class T, class Predicate>
	static void sort(std::vector<T>& v, const Predicate& p)
	{
		// sort ascending on hit t param
		std::sort(v.begin(), v.end(), p);
	}

    template <typename T, typename It, typename Predicate>
    static void filter(std::vector<T>& vec, const It& beg, const It& end, const Predicate& pred)
	{
		auto notpred = [&] (const T& value) { return !pred(value); };
		vec.erase(std::remove_if(beg, end, notpred), end);
	}

    template <typename T, typename Predicate>
    static void filter(std::vector<T>& vec, const Predicate& pred)
    {
        filter(vec, vec.begin(), vec.end(), pred);
    };
};
