#pragma once
#include "Element.h"

#include "RenderContext.h"

#include "Element.h"

namespace bv
{
    namespace gui
    {
        class ViewportElement: public IElement
        {
        public:
            ViewportElement(PanelElement* parent, const std::string& title):
                    IElement(parent, title)
            {
                m_context = RenderContext::create();
            }

            void renderImpl() override;

            Recti size() const { return m_context->size(); }
            void size(const Recti& rect) { m_context->size(rect); }

            RenderContext::Id context() { return m_context; }

	        void visible(bool visible) override
	        {
		        m_visible = visible;
		        m_context->active(m_visible);
	        }

        private:
            RenderContext::Id m_context;
        };
    }
}