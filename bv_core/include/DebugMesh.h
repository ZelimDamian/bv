#include "System.h"
#include "Store.h"

#include "Mesh.h"
#include "RenderMode.h"
#include "MeshFactory.h"

namespace bv
{
	class DebugMesh: public SystemSingleton<DebugMesh>
	{
	public:

		void point(const glm::vec3& point);
		void triangle(const TriVerts& triangle);

		void render(glm::mat4 xform = glm::mat4(), RenderMode mode = RenderMode::LINES, glm::vec4 color = glm::vec4{ 0.9f, 0.2f, 0.3f, 1.0f });

		void box(const Box& box, const glm::mat4& xform = glm::mat4(), RenderMode mode = RenderMode::LINES, const glm::vec4& color = glm::vec4{ 0.9f, 0.2f, 0.3f, 1.0f });
		void plane(const Plane& box, const glm::mat4& xform = glm::mat4(), RenderMode mode = RenderMode::LINES, const glm::vec4& color = glm::vec4{ 0.9f, 0.2f, 0.3f, 1.0f });

	private:
		
		Mesh::Id m_mesh { Mesh::create() };
		VectorVec3 m_points;
	};
}

