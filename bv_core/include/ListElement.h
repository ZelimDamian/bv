#pragma once
#include "Element.h"

namespace bv
{
	namespace gui
	{
		class ListElement: public Element<int>
		{
		public:
			ListElement(PanelElement* parent, const std::string& title, const std::vector<std::string>& list):
				Element<int>(parent, title, nullptr), m_list(list) { }

			void select(int selectedIndex)
			{
				if(m_selectedIndex != selectedIndex)
				{
					m_selectedIndex = selectedIndex;
					m_onchange.operator()(m_selectedIndex);
				}
			}

			std::vector<std::string> list() const { return m_list; }

			void onchange(const ActionInt& action) { m_onchange += action; }

		private:
			std::vector<std::string> m_list;
			void renderImpl();

			int m_selectedIndex { 0 };

			GenericEvent<int> m_onchange;
		};

		class ComboElement: public Element<int>
		{
		public:
			ComboElement(PanelElement* parent, const std::string& title, const std::vector<std::string>& list, ActionInt action):
					m_onchange(action), Element<int>(parent, title, nullptr)
			{
				m_list = list;
			}

			void select(int selectedIndex) { m_selectedIndex = selectedIndex; }

			std::vector<std::string> list() const { return m_list; }

		private:
			std::vector<std::string> m_list;
			void renderImpl();

			int m_selectedIndex;

			ActionInt m_onchange;
		};

		class MultiListElement : public Element<std::vector<int>>
		{
		public:
			MultiListElement(PanelElement* parent, const std::string& title, const std::vector<std::string>& list, ActionVecInt action):
				m_onchange(action), Element<std::vector<int>>(parent, title, nullptr)
			{
				m_list = list;
			}

		private:
			std::vector<std::string> m_list;
			void renderImpl();

			std::vector<int> m_selectedList;

			ActionVecInt m_onchange;
		};
	}
}