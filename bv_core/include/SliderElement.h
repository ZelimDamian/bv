#pragma once

#include "Element.h"

namespace bv
{
    namespace gui
    {
        class SliderElement: public Element<float>
        {
        public:
            SliderElement(PanelElement* parent, const std::string& title, float* value):
                    Element<float>(parent, title, value), m_min(0.0f), m_max(1.0f), m_step(0.01f) {  }

            SliderElement(PanelElement* parent, const std::string& title, float* value, float min, float max, float step):
                    Element<float>(parent, title, value), m_min(min), m_max(max), m_step(step) {  }

        private:
            float m_min;
            float m_max;
            float m_step;

            void renderImpl() override;
        };

        class SliderElementInt: public Element<int>
        {
        public:
            SliderElementInt(PanelElement* parent, const std::string& title, int* value):
                    Element<int>(parent, title, value), m_min(0), m_max(100) {  }

            SliderElementInt(PanelElement* parent, const std::string& title, int* value, int min, int max, int step):
                    Element<int>(parent, title, value), m_min(min), m_max(max) {  }

	        void onchange(const ActionInt& action) { m_onchange += action; }

        private:
            int m_min;
            int m_max;

            void renderImpl() override;

	        GenericEvent<int> m_onchange;
        };
    }
}