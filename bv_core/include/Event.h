#pragma once

#include <functional>
#include "Log.h"

namespace bv
{
    using Action = std::function<void()>;
	using ActionInt = std::function<void(int)>;
	using ActionBool = std::function<void(bool)>;
	using ActionVecInt = std::function<void(std::vector<int>)>;

	template <class ... Args>
    class GenericEvent
    {
    public:

	    using Action = std::function<void(Args...)>;

		virtual void operator += (Action func)
        {
            m_observers.push_back(func);
        }

        // notify all oberservers
        virtual void operator ()(const Args& ... args)
        {
			for (size_t i = 0; i < m_observers.size(); i++)
            {
				m_observers[i](args...);
            }
        }

        void clear()
        {
            m_observers.clear();
        }

        const std::vector<Action> & observers() const { return m_observers; }

    private:
        std::vector<Action> m_observers;
    };

	using Event = GenericEvent<>;

	// event that clears itself from observers when triggered
	class SingleEvent : public Event
	{
	public:
		virtual void operator  ()() override
		{
			Event::operator()();
			Event::clear();
		}
	};

	// smart event keeps track of a trigger and invokes handlers immediately if it has been triggered
	class UniqueEvent : public Event
	{
	public:

		void operator += (Action func) override
		{
			if (m_triggered)
			{
				func();
			}
			else
			{
				Event::operator+=(func);
			}
		}

		virtual void operator  ()() override
		{
			m_triggered = true;
			Event::operator()();
		}

	private:
		bool m_triggered{ false };
	};
}
