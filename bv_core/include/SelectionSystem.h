#pragma once

#include "Common.h"
#include "System.h"
#include "Entity.h"
#include "Box.h"

namespace bv
{
	enum class SelectionType
	{
		Entity,
		Face,
		Node
	};
	
	class SelectionSystem : public SystemSingleton<SelectionSystem>
	{
	public:

		// initialize the system
		void init() override;

		// update the system
		void update() override;

		// set current selection to the entity
		void set(Entity::Id entity);
		// add an entity to the current selection
		void add(Entity::Id entity);
		// clear out the selection
		void clear();

		// the list of selected entities
		std::vector<Entity::Id> selection();

		glm::vec3 center();
		Box box();

		// subscribe to selection change event
		void onselect(Action action) { m_onselect += action; }

	private:

		//
		void updateEntityList();

		// inform all subscribers
		void onselect();

		// even to detect selection change
		Event m_onselect;
	};
}
