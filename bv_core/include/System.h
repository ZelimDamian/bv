#pragma once

#include "Singleton.h"
#include "Event.h"

namespace bv
{
    class Systems;

    class SystemBase
    {
    public:
        // register all create systems
        SystemBase();

        // returns the priority of this system
        int priority() const { return m_priority; }


        // perfrom an update on all registered systems
        virtual void update() {};

	    void load()
	    {
		    if(!m_loaded)
		    {
			    init();
			    m_loaded = true;
		    }
	    }

        // perform the clean up for this system
        virtual void destroy() { m_ondestroy(); };

        Event& onload() { return m_onload; }
        Event& onload(Action func)
        {
	        // no point waiting if the event has already happened
	        if(m_loaded)
	        {
		        func();
	        }
	        else
	        {
		        // otherwise we will enqueue the func
		        m_onload += func;
	        }

	        return m_onload;
        }

        Event& ondestroy(Action action)
        {
            m_ondestroy += action;
            return m_ondestroy;
        }

    protected:
	    // initialize all registered systems
	    virtual void init() {};

	    friend class Systems;

    private:
        Event m_onload;
        Event m_ondestroy;

        int m_priority { 0 };

	    bool m_loaded { false };
    };

	template <class S>
	class SystemSingleton: public SystemBase, public Singleton<S> { };

    // an aggregator class for all systems in the app
    class Systems: public Singleton<Systems>
    {
	public:

        // register a system (register is a key word)
        void add(SystemBase* system);

        void init();
        void update();
        void destroy();

    private:
        std::vector<SystemBase*> m_systems;

		bool m_loaded { false };
    };
}
