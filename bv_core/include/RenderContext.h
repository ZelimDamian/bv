#pragma once

#include "Common.h"
#include "Store.h"
#include "FrameBuffer.h"
#include "CameraSystem.h"

namespace bv
{
    class RenderContext: public Storable<RenderContext>
    {
    public:

        RenderContext(Id id): StorableBase (id)
        {
            m_renderBuffer = rendering::FrameBuffer::create(glm::ivec2{ 1920, 1080 });
            m_camera = Camera::create();
        }

        Recti size() const { return m_size; }
        void size(const Recti& size)
        {
	        m_camera->viewport({size.x, size.y, size.width, size.height});
	        m_size = size; }

        void render()
        {
			m_onrender_once();
			m_onrender();
        }

        void onrender(const Action& handler) { m_onrender += handler; }
        void onrenderOnce(const Action& handler) { m_onrender_once += handler; }

		rendering::FrameBuffer::Id buffer() const { return m_renderBuffer; }

        Camera::Id camera() const { return m_camera; }

	    bool active() const { return m_active; }
	    void active(bool active) { m_active = active; }

    private:
		Recti m_size { 0, 0, 1, 1 };
        Event m_onrender;
        SingleEvent m_onrender_once;

		rendering::FrameBuffer::Id m_renderBuffer;

        Camera::Id m_camera;

	    bool m_active { true };
    };
}

