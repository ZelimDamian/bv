#pragma once

#include "RenderMesh.h"
#include "Component.h"
#include "RenderingSystem.h"
#include "Shader.h"

namespace bv
{
    class RenderMeshComponent : public Component<RenderMeshComponent>
    {
        using UniformMap = std::unordered_map<std::string, glm::vec4>;

    public:
        RenderMeshComponent(Id id, Entity::Id entity, Mesh::Id mesh, rendering::Shader::Id shader);
        RenderMeshComponent(Id id, Entity::Id entity, Mesh::Id mesh, const std::string& shaderName):
                RenderMeshComponent(id, entity, mesh, rendering::Shader::require(shaderName)) { }

        static void init();

        Mesh::Id mesh() const;
        rendering::RenderMesh::Id renderMesh() const;

        const Culling &culling() const { return m_culling; }
        void culling(const Culling &culling) { m_culling = culling; }

		bool showWireframe() const { return m_showWireframe; }
		void showWireframe(bool showWireframe) { m_showWireframe = showWireframe; }

		void attachGUI();

        void parameter(const std::string& name, const glm::vec4& value) { m_uniforms[name] = value; }

        const UniformMap& uniforms() const { return m_uniforms; }

    private:
        Culling m_culling { Culling::CW };

	    bool m_showNormals { false };
	    bool m_showWireframe { false };
	    float m_normalLenght { 1.0f };

        UniformMap m_uniforms;
    };
}
