#pragma once

#include "Element.h"

namespace bv
{
	namespace gui
	{
		template<class T>
		class GraphElement: public IElement
		{
		public:

			using SourceType = std::function<T()>;

			GraphElement(PanelElement* parent, const std::string& title, const SourceType& source,
			             int showNum, float min, float max ):
					IElement(parent, title), m_source(source), m_showNum(showNum), m_min(min), m_max(max), m_history() {}

		private:

			int m_showNum;
			float m_min, m_max;

			std::vector<T> m_history;

			SourceType m_source;

			void renderImpl() override;
		};

		using GraphElementFloat = GraphElement<float>;
	}
}
