#pragma once

namespace bv
{
	enum class AttribSemantics
	{
		POS = 0,
		NOR,
		COL0,
		COL1,
		TEX0,
		TEX1,
		TEX2
	};

	// semantics type
	using SemanticsType = std::vector<AttribSemantics>;

	enum class AttribType
	{
		FLOAT,
		HALF,
		DOUBLE,
		INT
	};

	struct Attribute
	{
	public:
		size_t size;
		AttribType type;
	};
}