#pragma once

namespace bv
{
    template <class S>
    class Singleton
    {
    public:

	    using SingletonBase = Singleton<S>;

	    inline static S* instance()
	    {
		    static S inst;
		    return &inst;
	    }
//    protected:
//	    Singleton() {}
    };
}
