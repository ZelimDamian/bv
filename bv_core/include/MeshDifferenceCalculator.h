#pragma once
#include "Meshable.h"

namespace bv
{
    class MeshDifferenceCalculator
    {
    public:
        MeshDifferenceCalculator()
        {
        }

	    MeshDifferenceCalculator(const MeshDifferenceCalculator& rhs) = default;
	    MeshDifferenceCalculator&operator=(const MeshDifferenceCalculator& rhs) = default;

	    template <class TMesh1, class TMesh2>
        void calculate(Handle<TMesh1> mesh1, Handle<TMesh2> mesh2)
        {
	        // need to make sure that the size of meshes is the same
	        if(mesh1->positions().size() != mesh2->positions().size())
	        {
		        Log::error("MeshDifferenceCalculator needs classes to have the same number of vertices.");
	        }

	        // allocate the required memory in advance
	        m_differences.resize(mesh1->positions().size());
            for (int i = 0; i < m_differences.size(); ++i)
            {
                m_differences[i] = glm::length(mesh1->positions()[i] - mesh2->positions()[i]);
            }

            m_min = *std::min_element(m_differences.begin(), m_differences.end());
            m_max = *std::max_element(m_differences.begin(), m_differences.end());

            m_error = Math::average(m_differences);

	        m_relError = m_error / std::max(std::abs(m_min), std::abs(m_max));
        }

        const std::vector<float>& differences() { return m_differences; }

        float error() const { return m_error; }
        float min() const { return m_min; }
        float max() const { return m_max; }

    private:

        std::vector<float> m_differences;

        float m_error, m_min, m_max, m_relError;
    };
}