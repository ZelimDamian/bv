#pragma once

#include "MathLib.h"
#include "Attribute.h"

namespace bv
{
	// class that allows mapping our vertex attribute data types to BGFX'es
	template < typename T > struct AttributeMapping;

	template <> struct AttributeMapping<glm::vec3>
	{
		static AttribType type() { return AttribType::FLOAT; }
		static uint8_t size() { return 3; }
	};

	template <> struct AttributeMapping<glm::vec4>
	{
		static AttribType type() { return AttribType::FLOAT; }
		static uint8_t size() { return 4; }
	};

	template <> struct AttributeMapping<glm::vec2>
	{
		static AttribType type() { return AttribType::FLOAT; }
		static uint8_t size() { return 2; }
	};

	template <> struct AttributeMapping<uint16_t>
	{
		static AttribType type() { return AttribType::INT; }
		static uint8_t size() { return 1; }
	};

}
