#pragma once

#include "System.h"
#include "Store.h"

namespace bv
{
	class Light: public Storable<Light>
	{
	public:
		Light(Id id, const glm::vec3& pos) : StorableBase(id), m_pos(pos) {}

		glm::vec3& pos() { return m_pos; }
		void pos(const glm::vec3 &m_pos) { Light::m_pos = m_pos; }

	private:
		glm::vec3 m_pos;
	};

	class LightingSystem: public SystemSingleton<LightingSystem>
	{
	public:
		LightingSystem ()
		{
			Light::create(glm::vec3(-5.0f, 2.0f, -30.0f));
		}

		// TODO: 0?
		Light::Id current() { return Light::Id(0); }
	};
}