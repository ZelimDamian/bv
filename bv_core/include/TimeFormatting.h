#pragma once

#include <ctime>
#include <string.h>

namespace bv
{
	struct TimeFormat
	{
		static std::chrono::system_clock::time_point time(const std::string &str)
		{
			using namespace std;
			using namespace std::chrono;

			int yyyy, mm, dd, HH, MM, SS, fff;

			char scanf_format[] = "%4d.%2d.%2d-%2d.%2d.%2d.%3d";

			sscanf(str.c_str(), scanf_format, &yyyy, &mm, &dd, &HH, &MM, &SS, &fff);

			tm ttm = tm();
			ttm.tm_year = yyyy - 1900; // Year since 1900
			ttm.tm_mon = mm - 1; // Month since January
			ttm.tm_mday = dd; // Day of the month [1-31]
			ttm.tm_hour = HH; // Hour of the day [00-23]
			ttm.tm_min = MM;
			ttm.tm_sec = SS;

			time_t ttime_t = mktime(&ttm);

			system_clock::time_point time_point_result = std::chrono::system_clock::from_time_t(ttime_t);

			time_point_result += std::chrono::milliseconds(fff);
			return time_point_result;
		}

		static std::string string(const std::chrono::system_clock::time_point &tp)
		{
			using namespace std;
			using namespace std::chrono;

			auto ttime_t = system_clock::to_time_t(tp);
			auto tp_sec = system_clock::from_time_t(ttime_t);
			milliseconds ms = duration_cast<milliseconds>(tp - tp_sec);

			std::tm * ttm = localtime(&ttime_t);

			char date_time_format[] = "%Y.%m.%d-%H.%M.%S";

			char time_str[] = "yyyy.mm.dd.HH-MM.SS.fff";

			strftime(time_str, strlen(time_str), date_time_format, ttm);

			std::string result(time_str);
			result.append(".");
			result.append(to_string(ms.count()));

			return result;
		}
	};
}