#pragma once

#include "Mesh.h"
#include "Box.h"
#include "Cylinder.h"

namespace bv
{
    class MeshFactory
    {
    public:
        static Mesh::Id quad(float width, float height);
        static void quad(float width, float height, Mesh::Id);

        static Mesh::Id cube(const glm::vec3& size) { return cube(size.x, size.y, size.z); }
	    static Mesh::Id cube(float width, float height, float depth);
        static void cube(float width, float height, float depth, Mesh::Id);

        static Mesh::Id shape(const Box& box);
        static void shape(const Box& box, Mesh::Id);

	    static Mesh::Id shape(const Plane& plane, float size, int subdiv = 1);
	    static void shape(const Plane& plane, float size, int subdiv, Mesh::Id);

	    static void shape(const TriVerts& tri, Mesh::Id);

	    static Mesh::Id shape(const Cylinder& shape, int subdiv = 32);
	    static void shape(const Cylinder& shape, int subdiv, Mesh::Id);

	    static Mesh::Id extrude(const VectorVec3& contour, const VectorVec3& path);
	    static void extrude(const VectorVec3& contour, const VectorVec3& path, Mesh::Id mesh);
    };
}