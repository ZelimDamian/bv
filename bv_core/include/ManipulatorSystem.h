#pragma once

#include "Common.h"
#include "System.h"

namespace bv
{
	class ManipulatorSystem : public SystemSingleton<ManipulatorSystem>
	{
	public:

		ManipulatorSystem() { }

		void init() override;
        void update() override;
	};
}
