#pragma once

#include <Common.h>
#include "Store.h"

namespace bv
{
    namespace rendering
    {
        enum class TextureType
        {
            R8 = 1,
            R16 = 2,
            RGBA8 = 4
        };

	    class Uniform;
	    using UniformId = Handle<Uniform>;

        class Texture: public NamedStorable<Texture>
        {
        public:

            Texture(Id id, const std::string& name): NamedStorableBase(id, name) {}

            // create a texture from a file with name
            static Texture::Id create2D(const std::string& name);

            // create an empty 2D texture of given size
            static Texture::Id create2D(uint16_t width, uint16_t height, TextureType type);

            // create an empty 3D texture of given size
            static Texture::Id create3D(uint16_t width, uint16_t height, uint16_t dpeth, TextureType type);

            // update the texture contents with the data and determine 2D/3D
            static void update(Texture::Id textureId, const void* data);
            // update the texture contents with the 2D data
            static void update2D(Texture::Id textureId, const void* data);
            // update the texture contents with the 3D data
            static void update3D(Texture::Id textureId, const void* data);

            // bind a texture to texture unit
            static void use(Texture::Id textureId, UniformId, uint8_t texUnit = 0);
            // default binding
            static void use(Texture::Id textureId, const std::string& name, uint8_t texUnit = 0);

            static glm::uvec3 size(Texture::Id textureId);
            static size_t sizeInBytes(Texture::Id textureId);
        };
    }
}