#pragma once

#include "Buffer.h"

namespace bv
{
// Dims class
	template <int NC, int NR = 1>
	struct Dims
	{
		enum { C = NC };
		enum { R = NR };

		static const int SIZE = C * R;
	};

// Array class to store data
// C - columns
// R - rows
	template <typename T, typename Dims>
	class Array
			: public Reinterpretable<Array<T, Dims>>
//          public Indexable<Array<T, Dims>>
	{
	public:
		// default constructor
		Array()// : m_data(Dims::SIZE)
		{

		}

		// construct from a pointer
		Array(const T* in) //: m_data(Dims::SIZE)
		{
//            m_data.resize(Dims::SIZE);
			for (int i = 0; i < Dims::SIZE; ++i)
			{
				m_data[i] = in[i];
			}
		}

		// copy constructor
		Array(const Array& rhs) : m_data(rhs.m_data) { }

		// move constructor
//        Array(Array&& rhs) : m_data(std::move(rhs.m_data)) {}

		// set all elements of the array to value
		void set(const T& value)
		{
			for (int i = 0; i < Dims::SIZE; ++i)
			{
				m_data[i] = value;
			}
		}

		// linear indexing
		inline T& at(int i) { return m_data[i]; }
		// constant linear indexing
		inline const T& at(int i) const { return m_data[i]; }

		// matrix indexing based on the number of rows and columns
		T& at2(int i, int j) { return m_data[j + i * Dims::C]; }
		const T& at2(int i, int j) const { return m_data[j + i * Dims::C]; }

		// linear indexing
		T& operator () (int i) { return at(i); }
		const T& operator () (int i) const { return at(i); }
		T& operator () (int i, int j) { return at2(i, j); }
		const T& operator () (int i, int j) const { return at2(i, j); }

		// access to the underlying data directly
		T* data() { return m_data.data(); }

		// constant access to the underlying data directly
		const T* data() const { return m_data.data(); }

		// accessing column number
		inline int cols() const { return Dims::C; }
		// accessing row number
		inline int rows() const { return Dims::R; }

		// total number of elements
		int size() const { return Dims::SIZE; }

		static void embed(const Array<T, Dims>& array, Buffer<T>& buffer, int offset)
		{
			T* d = buffer.data() + offset;

			for (int i = 0; i < array.size(); ++i)
			{
				d[i] = array.at(i);
			}
		}

	private:
		// storage for our data
		std::array<T, Dims::SIZE> m_data;
	};

	template <int C, int R = 1>
	using ArrayFloat = Array<float, Dims<C, R>>;

	template <int C, int R = 1>
	using ArrayVec3 = Array<glm::vec3, Dims<C, R>>;

}
