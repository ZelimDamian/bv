#pragma once

namespace bv
{
	struct Cylinder
	{
		float radius;
		float height;
	};
}