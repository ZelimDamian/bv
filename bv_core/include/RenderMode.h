#pragma once

#include <set>
#include "Store.h"

namespace bv
{
	enum class Culling
	{
		CW,
		CCW,
		NO
	};

	enum class RenderMode
	{
		TRIANGLES = 0,
		TRISTRIP,
		LINE_STRIP,
		LINES,
		POINTS
	};

	enum class DepthTest
	{
		NORMAL,
		ALWAYS
	};

	struct RenderState
	{
		Culling culling;
		RenderMode type;
		DepthTest test;
	};

	namespace rendering
	{
		class RenderMesh;
		class Shader;
		using ShaderId = Handle<rendering::Shader>;
		class Uniform;
		using UniformId = Handle<rendering::Uniform>;

		using UniformData = std::array<float, 16>;
		using UniformSubmission = std::map<rendering::UniformId, rendering::UniformData>;
	}

	struct RenderSubmission
	{
		using RenderMeshId = Handle<rendering::RenderMesh>;
		using ShaderId = Handle<rendering::Shader>;

		int priority;
		RenderState state;
		RenderMeshId mesh;
		glm::mat4 xform;
		ShaderId shader;
		rendering::UniformSubmission uniforms;
	};
}
