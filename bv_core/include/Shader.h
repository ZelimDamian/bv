#pragma once
#include "Common.h"
#include "Store.h"
#include "RenderingSystem.h"

namespace bv
{
    namespace rendering
    {
        enum class UniformType
        {
	        Int,
	        Float,
	        Vec3,
	        Vec4,
	        Mat4
        };

	    class Uniform;

        class Shader: public NamedStorable<Shader>
        {
		public:
            Shader(Id id, const std::string& name) : NamedStorable(id, name) {}

            /////////////////////////////////////////////////////////////////
			// find existing or create if not found
			static Shader::Id require(const std::string &name);
			// find or create if not found and use shader with name
			static Shader::Id requse(const std::string &name);
			// find and use shader with name
            static Shader::Id use(const std::string& name);
			// use shader with id
            static Shader::Id use(const Shader::Id id);
			// currently attached shader
            static Shader::Id current();

			// get uniform from this shader with name and type
	        Handle<Uniform> uniform(const std::string& name, UniformType type);

        };


        class Uniform: public NamedStorable<Uniform>
        {
        public:

	        template <typename T>
	        void set(const T& value)
	        {
		        Uniform::check(m_id, value);
		        RenderingSystem::instance()->set(m_id, data(value));
	        }

	        template <typename T>
	        UniformData data(const T& value)
	        {
		        UniformData mem;
		        memcpy(&mem, &value, sizeof(value));
		        return mem;
	        }

        private:

	        static void check(Uniform::Id id, const glm::mat4& value);
	        static void check(Uniform::Id id, const glm::vec4& value);
            static void check(Uniform::Id id, const glm::vec3& value);
            static void check(Uniform::Id id, float value);
	        static void check(Uniform::Id id, int value);

        public:

	        static Uniform::Id require(Shader::Id shaderId, const std::string &name, UniformType type);

            Uniform(Uniform::Id id, const std::string& name, Shader::Id shaderId, UniformType type)
		            : NamedStorableBase(id, name), m_shaderId(shaderId), m_type(type) { }

        private:
            Shader::Id m_shaderId;
            UniformType m_type;
        };
    }
}
