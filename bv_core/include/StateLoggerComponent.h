#pragma once

#include "Component.h"

namespace bv
{
	class StateLoggerComponent : public Component<StateLoggerComponent>
	{
	public:
		StateLoggerComponent(Id id, Entity::Id entity) : ComponentBase(id, entity) { }

		static void init()
		{
			subscribeToUpdates();
		}

		void initialize();

		void attachGUI();

		void update();

		struct State
		{
			State(const glm::vec3 &reaction, const glm::vec3 &expulsion, const glm::vec3 &position) :
					reaction(reaction), expulsion(expulsion), position(position) { }

			glm::vec3 reaction;
			glm::vec3 expulsion;
			glm::vec3 position;
			float time;
		};

		void push(State state);

	private:

		bool m_logToFile { false };
		int m_chunkSize { 100 };
		float m_period { 0.0333f };
		float m_lastLog { 0.0f };
		float m_time { 0.0f };

		std::vector<State> m_history;
	};
}