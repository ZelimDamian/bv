#pragma once

#include "Vertex.h"

namespace bv
{
    namespace rendering
    {

        namespace vertexdecl
        {

            void begin(int index);
            void add(int index, AttribSemantics sem, uint8_t size, AttribType attribType);
            void end(int index);
        }
		
		// DONT HIDE!! - causes a bug
		//namespace
		//{
			// we need something to count the number globally
			struct VertexDeclarationCounter
			{
				// number of vertex declarations initialized
				static int& numDeclarations()
				{
					static int s_vertDeclarations = 0;
					return s_vertDeclarations;
				}
			};
		//}
		
        // vertex declaration type for the renderer
        // specialization to extract AttributeTypes
        template <typename TVertex>
        class VertexDeclaration
        {
        public:
//            static void semantics(SemanticsType semantics)
//            {
//                s_semantics = semantics;
//            }

            static int decl()
            {
                
                // only do the init once
                if(declId == -1)
                {
                    // start vertex declaration specification
					vertexdecl::begin(VertexDeclarationCounter::numDeclarations());

                    auto attributes = TVertex::attributes();

                    // add all types
                    // NOTE: Have to use this funny thing to allow for per function pack expansion
                    for (size_t i = 0; i < attributes.size(); ++i)
                    {
						vertexdecl::add(VertexDeclarationCounter::numDeclarations(), TVertex::semantics[i], attributes[i].size, attributes[i].type);
                    }

                    // finish vertex declaration
					vertexdecl::end(VertexDeclarationCounter::numDeclarations());

                    // save current id
					declId = VertexDeclarationCounter::numDeclarations()++;
                }

                return declId;
            }

        private:

//            static auto getNextAtt() ->
//				std::remove_reference<decltype(s_semantics.back())>::type) // force return by value with remove_reference
//            {
//                static int processedAtts = 0;
//                return s_semantics[processedAtts++];
//            }

			// this will be different for each TVertex
			static int declId;

            // storage for the semantics of vertex attributes
//            static SemanticsType s_semantics;
        };

//		template <class TVertex>
//		std::vector<AttribSemantics> VertexDeclaration<TVertex>::s_semantics;
		template <class TVertex>
		int VertexDeclaration<TVertex>::declId = -1;
	}
}

