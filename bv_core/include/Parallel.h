#pragma once

#include <thread>

namespace bv
{
    class Parallel
    {
    public:
		template <class C, class Predicate>
		static void foreach(const C& container, const Predicate& predicate)
		{
			static const int OP_THRESHOLD = 128;

			if (container.size() > OP_THRESHOLD)
			{
				force_foreach(container, predicate);
			}
			else
			{
				for (size_t i = 0; i < container.size(); ++i)
				{
					predicate(i);
				}
			}
		}

		template <class C, class Predicate>
		static void force_foreach(const C& container, const Predicate& predicate)
        {
            using It = typename C::const_iterator;

            std::vector<std::thread> threads;

            int threadNum = std::thread::hardware_concurrency();
            int blockSize = container.size() / threadNum;

			// sub-range looping mechanism
			auto looper = [&container, &predicate](It from, It to) mutable
			{
				for (auto it = from; it != to; ++it)
				{
					predicate((size_t)std::distance(container.begin(), it));
				}
			};

            // sub-range start for a thread
            It from = container.begin();

            // schedule the
            for (int i = 0; i < threadNum - 1; ++i)
            {
                // end of the subrange for a thread
                It to = from + blockSize;

                // create a thread running on current subrange
                threads.push_back(std::thread(looper, from, to));

                // step to the next sub-range
                from = to;
            }

            // do the last elements
            threads.push_back(std::thread(looper, from, container.end()));

            for (int i = 0; i < threads.size(); ++i)
            {
                threads[i].join();
            }
        }
    };
}