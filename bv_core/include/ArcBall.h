#pragma once

#include "Common.h"

namespace bv
{
    class Arcball
    {
    public:
        Arcball(glm::ivec2 center, float radius);
        Arcball(glm::vec2 center, float radius);
        Arcball(glm::vec3 center, float radius);

        void click(glm::ivec2 point) const;
        glm::quat unclick(glm::ivec2 point) const;

    private:
        glm::vec3 mapToSphere(glm::vec2 point) const;

        glm::vec3 m_center;
        float m_radius;

        mutable glm::vec3 m_start, m_end;
        glm::quat m_quaterinon;
    };
}
