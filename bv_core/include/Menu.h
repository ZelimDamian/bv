#pragma once

#include <functional>
#include "Event.h"

#include "HotkeySystem.h"

namespace bv
{
    namespace gui
    {
        class Menu;
        using MenuPtr = std::shared_ptr<Menu>;

        class Menu
        {
        public:

	        Menu(const std::string& title): m_title(title) { }
            Menu(const std::string& title, const Action& action): Menu(title, {}, action) { }
	        Menu(const std::string& title, const std::vector<Key>& keys, const Action& action): m_title(title)
	        {
		        m_hotkey = Hotkey::create(keys, action);
	        }

            Menu* item(const std::string& title, const std::vector<Key>& keys, const Action& action = []{})
            {
	            m_children.emplace_back(MenuPtr(new Menu(title, keys, action)));
	            m_children.back()->m_parent = this;
	            return m_children.back().get();
            }
	        Menu* item(const std::string& title, const Action& action = []{})
            {
	            return item(title, {}, action);
            }

	        void trigger()
	        {
		        m_hotkey->action();
	        }

	        std::string shortcut()
	        {
		        return HotkeySystem::instance()->shortcut(m_hotkey->keys);
	        }

            void renderImpl();

	        Menu* find(const std::string& title)
	        {
		        for (auto &&child : m_children)
		        {
			        if(child->m_title == title)
			        {
				        return child.get();
			        }
		        }

		        return nullptr;
	        }

	        void clear() { m_children.clear(); }

        private:
            std::vector<MenuPtr> m_children;
            Menu* m_parent;

            std::string m_title;
	        Hotkey::Id m_hotkey;
        };
    }
}