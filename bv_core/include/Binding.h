#pragma once

namespace bv
{
    namespace gui
    {
        class IBinding
        {
        public:
            virtual ~IBinding() {}

        };

        template <typename T>
        class Binding : public IBinding
        {
        public:
            Binding(T* value) : m_value(value) {}
            ~Binding() {}

            T& value() { return *m_value; }

        private:
            T* m_value;
        };

        template <typename T>
        class BindingLambda: public IBinding
        {
        public:
            using Lambda = std::function<T*()>;

            BindingLambda(Lambda lambda): m_lambda(lambda) {}

            T& value() { return *(m_lambda()); }

        private:
            Lambda m_lambda;
        };
    }
}