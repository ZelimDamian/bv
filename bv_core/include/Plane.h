#pragma once

#include "MathLib.h"

namespace bv
{
	struct Plane
	{
        Plane(): m_normal(0.0f, 1.0f, 0.0f), m_pos() {}
		Plane(const glm::vec3& pos, const glm::vec3& normal): m_pos(pos), m_normal(glm::normalize(normal)) {}

        glm::vec3 pos() const { return m_pos; }
		void pos(const glm::vec3& pos) { m_pos = pos; }

        glm::vec3 normal() const { return m_normal; }
		void normal(const glm::vec3& normal) { m_normal = glm::normalize(normal); }

		glm::vec3 project(const glm::vec3& point)
		{
			return point - m_normal * glm::dot(m_normal, point - m_pos);
		}

		glm::vec4 equation() const { return glm::vec4(m_normal, glm::dot(m_pos, m_normal)); }

		struct Tangents
		{
			glm::vec3 operator[](int index)
			{
				if(index == 0) { return tangent; }
				else return bitangent;
			}

			glm::vec3 tangent, bitangent;
		};

		Tangents tangents() const
		{
			glm::vec3 candidate = glm::vec3(0.0f, 1.0f, 0.0f);

			// make sure normal doesn't coincide with candidate
			if(glm::abs(glm::dot(candidate, m_normal)) > 0.95f)
			{
				candidate = glm::vec3(1.0f, 0.0f, 0.0f);
			}

			glm::vec3 tangent = glm::cross(m_normal, candidate);
			glm::vec3 bitangent = glm::cross(m_normal, tangent);

			return { tangent, bitangent };
		}

    private:
		glm::vec3 m_pos, m_normal;
	};
}
