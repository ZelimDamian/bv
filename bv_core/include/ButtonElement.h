#pragma once

#include "Element.h"
#include "Event.h"

namespace bv
{
    namespace gui
    {
        class ButtonElement: public IElement
        {
        public:
            ButtonElement(PanelElement* parent, const std::string& title) : IElement(parent, title) {}
            ButtonElement(PanelElement* parent, const std::string& title, Action action, std::function<bool()> active):
                    m_active(active), IElement(parent, title)
            {
                m_onclick += action;
            }

            // add an action handler to the button
            void onclick(Action action) { m_onclick += action; }

        private:
            Event m_onclick;

            std::function<bool()> m_active;

            // render the element using the underlying GUI API
            // find definition in Elements.[PLATFORM].cpp
            void renderImpl() override;
        };

	    class CheckBoxElement: public IElement
	    {
	    public:
		    CheckBoxElement(PanelElement* parent,
		                    const std::string& title,
		                    const Action& action,
		                    std::function<bool()> active): m_active(active), IElement(parent, title)
		    {
			    m_onclick += action;
		    }

	    private:
		    Event m_onclick;

		    std::function<bool()> m_active;

		    // render the element using the underlying GUI API
		    // find definition in Elements.[PLATFORM].cpp
		    void renderImpl() override;
	    };
    }
}