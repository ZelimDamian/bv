#pragma once
#include "SFINAE.h"
#include <array>
#include <string>
#include <map>
#include <unordered_map>
#include <cinttypes>
#include <vector>
#include <assert.h>
#include <algorithm>
#include "Log.h"

namespace bv
{
	// represents a generic handle for type T
	template <class T>
	struct Handle
	{
		// the instance is an invalid handle by default
		Handle(): id(UINT16_MAX) {}

		// constructor from an id
		explicit Handle(uint16_t id): id(id) {}

		Handle(const Handle& other) { id = other.id; }

		// the id
		uint16_t id;

		// allows boolean checks
		//operator const void*() const { return id != UINT16_MAX ? this: nullptr; }
		bool const operator !() const { return id == UINT16_MAX; }
		explicit operator int() const { return id; }
		explicit operator bool() const { return id != UINT16_MAX; }
		bool operator ==(const Handle& other) const { return other.id == id; }

		// allows access to the instance
		T& ref() { return *T::get(*this); }
		const T& ref() const { return *T::get(*this); }

		T* operator -> () { return &ref(); }
		const T* operator -> () const { return &ref(); }

		T& operator *() { return ref(); }
		const T& operator *() const { return ref(); }

		static const Handle invalid() { return Handle(UINT16_MAX); }
	};

	template<class T>
	bool operator < (const Handle<T> lhs, const Handle<T>& rhs) { return lhs.id < rhs.id; }

	// a policy class to represent a Store behaviour
	// by inheriting from this class it becomes a storage for all its instances
	template <class T>
	class Storable
	{
	public:
		using StorableBase = Storable<T>;
		using Id = Handle<T>;

		// create an instance of class with specified arguments that are forwared to the constructor
		template <typename ...Args>
		static Id create(Args&&... args)
        {
	        Id handle ( (uint16_t) s_instances.size());
	        s_instances.emplace_back(handle, std::forward<Args>(args)...);

	        // call allocate() of this class if it is defined
	        sfinae::has_allocate<T>::call();

	        return handle;
        }

		// create an instance of class and return a reference
		template <typename ...Args>
		static T* acquire(Args&&... args)
		{
			return get(create(std::forward<Args>(args)...));
		}

		// get a ref to instance with the specified handle
		static T* get(Id handle)
		{
			assert(!!handle);
			return get(handle.id);
		}

        static T* get(uint16_t idx)
        {
            return &s_instances[idx];
        }

		static void destroy(Id handle)
		{
			// call deallocate() of this if it is defined
			sfinae::has_deallocate<T>::call(handle);
		}

		////////////////////////////////////////////////////////////////////
		// instance methods
		///////////////////////////////////////////////////////////////////

		// should be constructed with a handle specified
		//Storable(): m_id((uint16_t) s_instances.size()) {  }
		Storable(Id handle): m_id(handle)
		{
			// this prevents creating storables through constructors
			// should use Storable::create instead
			assert(handle.id == s_instances.size());
		}

		// default constructor is allowed
		//Storable() {}

		// gets the handle to this instance
		Id id() const { return m_id; }

        // gets the number of of instances created
        static size_t count() { return s_instances.size(); }

		static const std::vector<T>& instances() { return s_instances; }
		static std::vector<Id> handles()
		{
			std::vector<Id> handles(count());
			std::for_each(handles.begin(), handles.end(), [&] (Id& id)
			{
				id.id = &id - &handles[0];;
			});
			return handles;
		}

	protected:
		// id handle of this instance
		Id m_id;

		// list of all class instances created
		static std::vector<T> s_instances;
	};

	template<typename T>
	std::vector<T> Storable<T>::s_instances;

	///////////////////////////////////////////////////////////////////////////////////////

	template <class TKey, class T>
	class KeyedStorable: public Storable<T>
	{
	public:
		using KeyedStorableBase = KeyedStorable<TKey, T>;
		using StorableBase = Storable<T>;
		using Id = Handle<T>;

		KeyedStorable(Id handle, const TKey& key): StorableBase(handle)
		{
			map()[key] = handle;
		}

		// find a handle with name
		static Handle<T> find(const TKey& key)
		{
			// note that this will return an invalid Handle<T> if not found
			return map()[key];
		}

		TKey key() const
		{
			for (auto& i: map())
			{
				if(i.second == this->id())
				{
					return i.first;
				}
			}

			return {};
		}

        static std::vector<TKey> keys()
        {
            // get the map
            auto& m = map();

            std::vector<TKey> keys(m.size());

            // accumulate the keys
            for(const auto& pair: m)
            {
                keys[pair.second.id] = pair.first;
            }

            return keys;
        };

	private:
        struct enum_hash
        {
            template <typename K>
            inline
            typename std::enable_if<std::is_enum<K>::value, std::size_t>::type
            operator ()(K const value) const
            {
                return static_cast<std::size_t>(value);
            }
        };

        template <typename Key>
        using HashType = typename std::conditional<std::is_enum<Key>::value, enum_hash, std::hash<Key>>::type;

		using MapType = std::unordered_map<TKey, Handle<T>, HashType<TKey>>;

		static MapType& map()
		{
			static MapType m;
			return m;
		}
	};

	template <class T>
	class NamedStorable: public KeyedStorable<std::string, T>
	{
	public:
		using KeyedStorableBase = KeyedStorable<std::string, T>;
		using NamedStorableBase = NamedStorable<T>;
		using StorableBase = Storable<T>;
		using Id = Handle<T>;

		NamedStorable(Id handle, const std::string& key): KeyedStorableBase(handle, key) {}

		std::string name() const { return this->key(); }
	};
}
