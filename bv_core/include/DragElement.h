#pragma once

#include "Element.h"

namespace bv
{
    namespace gui
    {
        class DragElement: public Element<float>
        {
        public:
            DragElement(PanelElement* parent, const std::string& title, float* value):
                DragElement(parent, title, value, *value / 100.0f, *value * 100.0f, *value / 100.0f) {}

            DragElement(PanelElement* parent, const std::string& title, float* value, float min, float max, float step):
                Element<float>(parent, title, value), m_min(min), m_max(max), m_step(step) {  }

        private:

            float m_step, m_min, m_max;

            void renderImpl() override;
        };

	    template<class T>
	    class DragElementSourced: public IElement
	    {
	    public:

		    using SourceType = std::function<T()>;
		    using TargetType = std::function<void(const T&)>;

		    DragElementSourced(PanelElement* parent, const std::string& title,
		                       const SourceType& source, const TargetType& target,
		                       float min, float max, float step):
				    IElement(parent, title), m_min(min), m_max(max), m_step(step),
				    m_source(source), m_target(target) {  }

	    private:

		    float m_step, m_min, m_max;

		    SourceType m_source;
		    TargetType m_target;

		    void renderImpl() override;
	    };

	    using DragElementInt = DragElementSourced<int>;
        using DragElementFloat = DragElementSourced<float>;
	    using DragElementVec3 = DragElementSourced<glm::vec3>;
    }
}
