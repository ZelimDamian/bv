#pragma once

#include "Common.h"

#include "Mesh.h"

namespace bv
{
	class ObjLoader
	{
	public:
		static Mesh::Id load(const std::string& name, const std::string ext = ".obj");
		static Mesh::Id load_absolute(const std::string& name, const std::string ext = ".obj");
	};
}