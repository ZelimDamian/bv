#pragma once

#include "Store.h"
#include "System.h"

namespace bv
{
	enum class Key;

	class HotkeySystem: public SystemSingleton<HotkeySystem>
	{
	public:

		void update() override;

		std::string shortcut(const std::vector<Key>& keys);
	};

	class Hotkey: public NamedStorable<Hotkey>
	{
	public:
		Hotkey(Id id, const std::string& name, const std::vector<Key>& keys, const Action& action):
				NamedStorableBase(id, name), keys(keys), action(action), triggered(false) { }


		static Id create(const std::vector<Key>& keys, const Action& action)
		{
			return NamedStorable::create(HotkeySystem::instance()->shortcut(keys), keys, action);
		}

		void trigger();

		std::vector<Key> keys;
		Action action;
		bool triggered;
	};
}