#pragma once
#include "Entity.h"
#include "Box.h"

namespace bv
{
    namespace Bounding
    {
        Box box(Entity::Id entityId);
	    Box box(Entities entities);

        template <class TMesh>
        Box box(Handle<TMesh>& mesh)
        {
			Box b;
			b.fit(mesh->positions);
			return b;
        }

	    float area(const Box& box);

	    Obb obb(const VectorVec3& points, int steps = 16);
    };
}