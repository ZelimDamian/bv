#pragma once

#include "Common.h"

#include <string.h>

#ifdef _MSC_VER
// Dummy context for MS_WIN s_strtok
	static char * context;
	#define c_scanf sscanf_s
	#define c_strtok(str, delim) (strtok_s(str, delim, &context))
	#define c_strcpy(dst, src, count) (strcpy_s(dst, count, src))
#else
#define c_scanf sscanf
#define c_strtok(str, delim) strtok(str, delim)
#define c_strcpy(dst, src, count) strcpy(dst, src)
#endif

namespace bv
{
	struct StringUtils
	{
		static std::string trim(const std::string& str,
		                        const std::string& whitespace = " \t")
		{
			const auto strBegin = str.find_first_not_of(whitespace);
			if (strBegin == std::string::npos)
				return ""; // no content

			const auto strEnd = str.find_last_not_of(whitespace);
			const auto strRange = strEnd - strBegin + 1;

			return str.substr(strBegin, strRange);
		}

		static std::string reduce(const std::string& str,
		                   const std::string& fill = " ",
		                   const std::string& whitespace = " \t")
		{
			// trim first
			auto result = trim(str, whitespace);

			// replace sub ranges
			auto beginSpace = result.find_first_of(whitespace);
			while (beginSpace != std::string::npos)
			{
				const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
				const auto range = endSpace - beginSpace;

				result.replace(beginSpace, range, fill);

				const auto newStart = beginSpace + fill.length();
				beginSpace = result.find_first_of(whitespace, newStart);
			}

			return result;
		}
	};

	class FileReader
	{
	public:
		static std::string read(const std::string& filename);
		static std::string read_absolute(const std::string& filename);

        static std::string pick(const char* filter = NULL /*"png,jpg;pdf"*/);
        static std::string save(const char* name, const char* filter = NULL /*"png,jpg;pdf"*/);
		static std::vector<std::string> multipick(const char* filter = NULL /*"png,jpg;pdf"*/);
        static std::string open(const char* filter = NULL /*"png,jpg;pdf"*/);

		static const std::string ROOT_DIR;
	};
}