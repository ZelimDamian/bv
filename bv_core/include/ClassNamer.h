#pragma once

#include <typeinfo>

#ifndef _MSC_VER
// for name demangling
#include <cxxabi.h>
#endif

namespace bv
{
	class ClassNamer
	{
	public:
		template <class T>
		static std::string name()
		{
			return name(typeid(T));
		}

		template <class T>
		static std::string name(const T* data)
		{
			return name(typeid(*data));
		}


	private:
		static std::string name(const std::type_info& info)
		{
#ifndef _MSC_VER
			static int status;
			return abi::__cxa_demangle(info.name(), 0, 0, &status);
#else
			return info.name();
#endif
		}

	};
}
