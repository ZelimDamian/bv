#pragma once

#include <SliderElement.h>
#include "Component.h"

namespace bv
{
	template<class State>
	class StateHistoryComponent: public Component<StateHistoryComponent<State>>
	{
	public:

		using ComponentBase = Component<StateHistoryComponent<State>>;
		using Id = typename ComponentBase::Id;

		StateHistoryComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
		{
//			this->m_active = false;
		}

		static void init()
		{
			ComponentBase::subscribeToUpdates();
		}

		void update()
		{
			// only record when solver and recorder are both active
			if(this->m_active)
			{
				if(m_time - m_lastLog > m_period)
				{
					State state = m_source();
					m_history.push_back(state);
					m_lastLog = m_time;
				}

				m_time += Timing::delta();

				if(m_history.size() > m_maxSize)
				{
					m_history.erase(m_history.begin(), m_history.end() - m_maxSize);
				}
			}
		}

		const std::vector<State>& history() const { return m_history; }

		float period() const { return m_period; }

		void source(const std::function<State()> &source) { m_source = source; }
		void target(const std::function<void(const State &)> &target) { m_target = target; }

		float duration() const { return m_history.size() * m_period; }

		void apply(float time)
		{
			int index = (int) (time / m_period);
			setTo(index);
		}

		void attachGUI()
		{
			auto gui = this->gui();

			gui->button("Rollback", [this]
			{
				using namespace gui;
				auto dialog = Gui::instance()->window("Rollback step count");
				static int step;
				auto input = dialog->add<gui::SliderElementInt>("Steps", &step, 1, 100, 1);
				input->onchange([=] (int step)
				{
					step = std::min((int)m_history.size(), step);
					setTo(m_history.size() - step);
				});
			});

			gui->bind("Period", m_period);
			gui->bind("Size", m_maxSize, 1, 10000, 10);
			gui->label("Count", [this] { return std::to_string(m_history.size()); });
		}

	private:
		int m_maxSize { 500 };
		float m_period { 0.0333f };
		float m_lastLog { 0.0f };
		float m_time { 0.0f };

		std::vector<State> m_history;

		std::function<State(void)> m_source;
		std::function<void(const State&)> m_target;

		void setTo(int index)
		{
			assert(index >= 0 && index < m_history.size());
			m_target(m_history[index]);
		}
	};

	class HistoryPlayerComponent: public Component<HistoryPlayerComponent>
	{
	public:

		HistoryPlayerComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
		{
			m_active = false;
		}

		static void init()
		{
			subscribeToUpdates();
		}

		void update()
		{
			m_time += Timing::delta() * m_speed;
		}

		void attachGUI()
		{
			auto gui = this->gui();

			gui->button("Restart", [this]
			{
				m_time = 0.0f;
			});

			gui->bind("Speed", m_speed, 0.0f, 5.0f, 0.01f);

		}

	private:
		float m_time { 0.0f };
		float m_speed { 1.0f };
	};
}