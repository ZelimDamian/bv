#pragma once

namespace bv
{
    template <typename T>
    struct Rect
    {
        Rect() {}
        Rect(T x, T y, T width, T height) : x(x), y(y), width(width), height(height) {}

        // check if a point with given coords is inside the rect
        bool isInside(const T& px, const T& py) const
        {
            return px > x && px < x + width && py > y && py < y + height;
        }

	    void scale(const glm::vec2& scale)
	    {
		    x *= scale.x;
		    y *= scale.y;
		    width *= scale.x;
		    height *= scale.y;
	    }

        T x, y, width, height;
    };

    using Recti = Rect<int>;
}