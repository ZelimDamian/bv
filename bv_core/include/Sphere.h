#pragma once

namespace bv
{
    struct Sphere
    {
    public:
        Sphere() {}
        Sphere(const glm::vec3& center, float radius): m_center(center), m_radius(radius) { }

        glm::vec3 center() const { return m_center; }
        float radius() const { return m_radius; }

        void center(const glm::vec3& center) { m_center = center; }
        void radius(float radius) { m_radius = radius; }

	    static Sphere smallest(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& p4)
	    {
		    glm::vec3 center = (p1 + p2 + p3 + p4) / 4.0f;

		    float shortest = glm::max(glm::length2(p1 - center),
							 glm::max(glm::length2(p2 - center),
							 glm::max(glm::length2(p3 - center),
							 glm::length2(p4 - center))));

		    float radius = std::sqrt(shortest);
			return Sphere { center, radius };
	    }

	    template <class C>
	    static Sphere smallest(const C& vertices)
	    {
		    glm::vec3 center;
		    center = (vertices[0] + vertices[1]) / 2.0f;

		    glm::vec3 dd = vertices[1] - center;

		    float maxDistSq = glm::dot(dd, dd);

		    float radiusStep = 0.37f;

		    bool done;
		    do
		    {
			    done = true;
			    for (uint32_t ii = 0, index = rand()%vertices.size(); ii < vertices.size(); ++ii, index = (index + 1)%vertices.size())
			    {
				    dd = vertices[ii] - center;

				    float distSq = glm::dot(dd, dd);

				    if (distSq > maxDistSq)
				    {
					    done = false;

					    center += dd * radiusStep;
					    maxDistSq = glm::mix(maxDistSq, distSq, radiusStep);

					    break;
				    }
			    }

		    } while (!done);

		    return Sphere { center, glm::sqrt(maxDistSq) };
	    }

    private:
        glm::vec3 m_center;
        float m_radius;
    };
}