#pragma once

#include <Component.h>
#include <Meshable.h>

#include <RenderingSystem.h>
#include <RenderMesh.h>
#include <Shader.h>
#include <MeshDifferenceCalculator.h>
#include <ColorLerper.h>

namespace bv
{
    template <class TMesh1, class TMesh2 = TMesh1>
    class MeshDifferenceRendererComponent : public Component<MeshDifferenceRendererComponent<TMesh1, TMesh2>>
    {
        using ComponentBase = Component<MeshDifferenceRendererComponent<TMesh1, TMesh2>>;
        using Id = typename ComponentBase::Id;

    public:
        MeshDifferenceRendererComponent(Id id, Entity::Id entity,
                                        Handle<TMesh1> mesh1, Handle<TMesh2> mesh2):
                ComponentBase(id, entity),
                m_mesh1(mesh1), m_mesh2(mesh2)
        {
//	        mesh1->updateWireIndices();
	        mesh2->updateWireIndices();
        }

        void initialize()
        {
            auto renderer = RenderingSystem::instance();

            auto onrender = [this, renderer]
            {
				if(!this->m_active)
				{
					return;
				}

                // update the difference values
                m_calculator.calculate(m_mesh1, m_mesh2);


                // create a renderable of the mesh
                TMesh1& mesh = TMesh1::get(m_mesh1);

                auto& differences = m_calculator.differences();

                // min and max values for von-Mises stress
                //const auto min_max = std::minmax_element(VMS.begin(), VMS.end());

                const float min = m_calculator.min();
                const float max = m_calculator.max();

                // only do this if there is variation in stresses
                if (min < max)
                {
                    // and use force magnitude as color
                    for (int i = 0; i < mesh.vertices.size(); ++i)
                    {
                        const float magnitude = differences[i];
                        const float factor = (magnitude - min) / (max - min);

                        const glm::vec4 forceColor = m_colorLerper.sample(factor);

                        mesh.vertices[i].template set<TMesh1::TVertex::Col>(forceColor);
                    }
                }

                {
                    using namespace rendering;

                    // update the vertex buffer for the mesh
	                //m_mesh1->updateIndices();
                    RenderMesh::Id renderMesh = RenderMesh::create(m_mesh1);

                    renderer->reset(Culling::CCW);

                    Shader::Id diffuseShader = Shader::use("diffuse_colors");
                    diffuseShader->uniform("lightPos", UniformType::Vec3)->set(glm::vec3(1.0f, 1.0f, 1.0f));

                    // render the mesh
                    renderer->transform(this->m_entity->transform());
					renderer->submit(renderMesh);

	                auto ghostMesh = RenderMesh::create(m_mesh2);
					Shader::use("colors")->uniform("uColor", UniformType::Vec4)->set(glm::vec4(0.2f, 0.2f, 0.0f, 1.0f));

					renderer->reset(Culling::NO, RenderMode::LINES);
					renderer->render(ghostMesh);
                }
            };

            renderer->onrender(onrender);
        }

        void attachGUI()
        {
            auto panel = this->gui();

            panel->label("Average error", [this] { return std::to_string(m_calculator.error()); });
            panel->label("Min error", [this] { return std::to_string(m_calculator.min()); });
            panel->label("Max error", [this] { return std::to_string(m_calculator.max()); });
        }

    private:
        Handle<TMesh1> m_mesh1;
        Handle<TMesh2> m_mesh2;

        MeshDifferenceCalculator m_calculator;

        ColorLerper m_colorLerper { glm::vec4 { 0.0f, 0.0f, 1.0f, 1.0f }, glm::vec4 { 0.0f, 1.0f, 0.0f, 1.0 }, glm::vec4 { 1.0f, 0.0f, 0.0f, 1.0f } };
    };
}