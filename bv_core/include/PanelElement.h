#pragma once

#include "Element.h"
#include "Event.h"

#include "DragElement.h"
#include "GraphElement.h"

namespace bv
{
    namespace gui
    {
        // A panel element representing a container for other elements
        // and other instances of itself
        class PanelElement : public IElement
        {
        public:
            PanelElement(PanelElement* parent, const std::string& name) : IElement(parent, name) {}

            void state(int x, int y, int width, int heght);
	        void state(const Recti& rect);
            Recti rect() const { return m_rect; }

            // trigger rendering of the element
            void render() override;

            // add child element
            void add(const std::shared_ptr<IElement>& child);

            // add child of type T
            template <class T, class ... Args>
            T* add(const std::string& title, Args&& ... args)
            {
                std::shared_ptr<T> el = std::make_shared<T>(this, title, std::forward<Args>(args)...);
                m_children.push_back(el);
                return el.get();
            };

            // remove child element
            void remove(const std::shared_ptr<IElement>& child);

            // remove child element
            void remove(const IElement* child);

            // remove all children
            void removeAll()
            {
                m_children.clear();
            }

	        void close();
            void open();

            // create a child panel with the given name
            PanelElement* panel(const std::string& title);

            // create a button that calls given function when pressed
			IElement* button(const std::string& name, const Action& action, std::function<bool()> active = nullptr);
	        IElement* check(const std::string& name, const Action& action, std::function<bool()> state = nullptr);

            // create a single scalar value binding in the GUI
			IElement* bind(const std::string& name, float& value, float min = 0.0f,
                      float max = 1.0f, float step = 0.0f);

            // create a single integer scalar value binding in the GUI
			IElement* bind(const std::string& name, int& value, int min = 0,
                      int max = 100, int step = 1);

            // create a binding to display and modify a value though the GUI
	        template<class T>
	        IElement* bind(const std::string &name, const typename DragElementSourced<T>::SourceType& source,
	                       const typename DragElementSourced<T>::TargetType& target,
	                       float min = 0, float max = 100.0f, float step = 1.0f)
	        {
		        m_children.push_back(std::make_shared<DragElementSourced<T>>(this, name.c_str(), source, target,
		                                                                     min, max, step));
		        return m_children.back().get();
	        }

			IElement* label(const std::string& name, std::function<std::string()> source);

			IElement* list(const std::string& title, const std::vector<std::string>& list, ActionInt action);
	        IElement* combo(const std::string& title, const std::vector<std::string>& list, ActionInt action);
			IElement* multilist(const std::string& title, const std::vector<std::string>& list, ActionVecInt action);

	        template<class T>
	        IElement* graph(const std::string& title, const typename GraphElement<T>::SourceType & source,
	                        float min = 0.0f, float max = 100.0f, int showNum = 100)
	        {
		        m_children.push_back(std::make_shared<GraphElement<T>>(this, title.c_str(), source, showNum, min, max));
		        return m_children.back().get();
	        }

            static const int MARGIN = 3;

            IElement *renderContext(const std::string &title);

	        bool isOpen() const { return m_open; }

	        void visible(bool visible) override
	        {
		        m_visible = visible;
		        for (auto &&child : m_children)
		        {
			        child->visible(visible);
		        }
	        }

        private:

            // iterate over children and render
            void renderChildren();

            // find impl in Elements.[PLATFORM].cpp
            void renderImpl() override;

            // list of child elements in this panel
            std::vector<IElementPtr> m_children;

            // integer rectangle to show the area of
            Recti m_rect { 0, 0, 120, 120 };

            int m_scroll { 0 };

            int m_lastOccupiedHeight { 0 };
            bool m_open { true };
        };

	    using PanelElementPtr = std::shared_ptr<PanelElement>;
    }
}
