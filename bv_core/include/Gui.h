#pragma once

#include "Common.h"
#include "System.h"
#include "Binding.h"
#include "Element.h"
#include "PanelElement.h"
#include "Menu.h"

namespace bv
{
    namespace gui
    {
        using WindowMap = std::unordered_map<std::string, PanelElementPtr>;

        class Gui : public SystemSingleton<Gui>
        {
        public:
            Gui();

            void init() override;
            void update() override;
	        void destroy() override;

            // returns a reference to the main gui panel
            PanelElement* mainPanel() { return m_windows["main"].get(); }
            PanelElement* sidePanel() { return m_windows["side"].get(); }

            Menu* menu() { return &m_menu; }

            // create a panel
            PanelElement* window(const std::string& name);
	        void remove(PanelElement* window);

            bool isPointInScene(const glm::ivec2& point);

	        void updateWindowsList();

        private:
            WindowMap m_windows;
            Menu m_menu;
        };
    }
}