#pragma once

#include "MathLib.h"
#include <vector>

namespace bv
{
	struct ColorLerper
	{
		template <typename ... Args>
		ColorLerper(Args&& ... args)
		{
			m_colors = { args ...};
		}

		glm::vec4 sample(float point)
		{
			// if a NAN is passed return fallback
			if(std::isnan(point))
			{
				return m_fallback_color;
			}

			point *= 0.9999f;
			float indexFloat = point * (m_colors.size() - 1);
			int index = (int) indexFloat;
			const glm::vec4 minColor = m_colors[index];
			const glm::vec4 maxColor = m_colors[index + 1];

			const float lerpFactor = indexFloat - index;

			return glm::mix(minColor, maxColor, lerpFactor);
		}

		std::vector<glm::vec4> m_colors;
		glm::vec4 m_fallback_color { 0.0f, 0.0f, 0.0f, 1.0f };
	};
}