#pragma once

#include "Mesh.h"

#include <numeric>

namespace bv
{
	struct MeshUtilities
	{
		static std::vector<glm::vec3> uniqueVerts(const std::vector<int> &faceIndices, Mesh::Id meshId)
		{
			const auto vertIndices = uniqueVertIndices(faceIndices, meshId);

			std::vector<glm::vec3> verts(vertIndices.size());

			for (int i = 0; i < verts.size(); ++i)
			{
				verts[i] = meshId->positions[i];
			}

			return verts;
		}

		template <class TMesh>
		static std::vector<int> vertIndices(const std::vector<int> &faceIndices, Handle<TMesh> mesh)
		{
			std::vector<int> verts;
			verts.reserve(faceIndices.size() * 3);

			for (int i = 0; i < faceIndices.size(); ++i)
			{
				const Tri& tri = mesh->triangles[faceIndices[i]];

				verts.push_back(tri.vi[0]);
				verts.push_back(tri.vi[1]);
				verts.push_back(tri.vi[2]);
			}

			return verts;
		}

		template <class TMesh>
		static std::vector<int> uniqueVertIndices(const std::vector<int> &faceIndices, Handle<TMesh> meshId)
		{
			// non unique verts
			auto verts = vertIndices(faceIndices, meshId);

			// remove duplicates
			bv::filterUnique(verts);

			return verts;
		}

		template <class TMesh>
		static std::vector<glm::vec3> collectNormals(const std::vector<int>& faceIndices, Handle<TMesh> mesh)
		{
			// collect the normals of the master surface
			std::vector<glm::vec3> normals;
			normals.reserve(faceIndices.size());
			for (int i = 0; i < faceIndices.size(); ++i)
			{
				normals.push_back(mesh->normals[faceIndices[i]]);
			}

			return normals;
		}


		static std::vector<glm::vec3> collectTriVerts(Mesh::Id mesh,
													  std::vector<int> faceIndices)
		{
			// collect master surface vertices in contact
			std::vector<glm::vec3> masterTriangleVerts(faceIndices.size() * 3);
			for (int i = 0; i < faceIndices.size(); ++i)
			{
				const Tri& tri = mesh->tri(faceIndices[i]);
				const TriVerts& verts = tri.vertices(mesh->positions);

				masterTriangleVerts[i * 3 + 0] = verts[0];
				masterTriangleVerts[i * 3 + 1] = verts[1];
				masterTriangleVerts[i * 3 + 2] = verts[2];
			}

			return masterTriangleVerts;
		}

		// remove vertices not references by triangles
		static void pruneVertices(Mesh::Id mesh)
		{
			std::vector<int> faceIndices(mesh->triangles.size());
			std::iota(faceIndices.begin(), faceIndices.end(), 0);

			auto vertIndices = uniqueVertIndices(faceIndices, mesh);

			VectorVec3 pruned(vertIndices.size());

			for (int i = 0; i < vertIndices.size(); ++i)
			{
				auto vertIndex = vertIndices[i];
				pruned[i] = mesh->positions[vertIndex];

				auto& tris = mesh->triangles;
				for (int j = 0; j < tris.size(); ++j)
				{
					auto& tri = tris[j];
					if(tri.vi[0] == vertIndex)
					{
						tri.vi[0] = i;
					}
					else if(tri.vi[1] == vertIndex)
					{
						tri.vi[1] = i;
					}
					else if(tri.vi[2] == vertIndex)
					{
						tri.vi[2] = i;
					}
				}
			}

			mesh->positions = pruned;
		}

		static void triStripToTriList(Mesh::Id mesh)
		{
			// generate linear indices if none are present
			if(mesh->indices.size() == 0)
			{
				for (int i = 0; i < mesh->vertices.size(); ++i)
				{
					mesh->indices.push_back(i);
				}
			}

			for (int i = 0; i < mesh->indices.size() - 2; i++)
			{
				mesh->tri(i, i+1, i+2);
			}

			mesh->updateIndices();
		}


		static void flipYZ(Mesh::Id mesh)
		{
			for (int i = 0; i < mesh->positions.size(); ++i)
			{
				auto y = mesh->positions[i].z;
				mesh->positions[i].z = -mesh->positions[i].y;
				mesh->positions[i].y = y;
			}
		}
	};
}
