#pragma once

#include "Common.h"
#include "Binding.h"

namespace bv
{
    namespace gui
    {
        // only Panel's can have children
        class PanelElement;

        class IElement
        {
        public:
            IElement(PanelElement* parent, const std::string& title);
            virtual ~IElement() {};

            virtual void update() {}
            virtual void render() { renderImpl(); };

            std::string title() const { return m_title; }
            const std::string& title() { return m_title; }

            void title(const std::string& title) { m_title = title; }

            PanelElement& parent() { return *m_parent; }
	        void parent(PanelElement *parent) { m_parent = parent; }

	        virtual bool visible() const { return m_visible; }
	        virtual void visible(bool visible) { m_visible = visible; }

        protected:
            std::string m_title;

            PanelElement* m_parent;

	        bool m_visible { true };

            // render the element using the underlying GUI API
            // find definition in Elements.[PLATFORM].cpp
            virtual void renderImpl() = 0;
        };

        template <typename T>
        class Element : public IElement
        {
        public:
            Element(PanelElement* parent, const std::string& title, T* value) : IElement(parent, title), m_binding(Binding<T>(value)) {}

            Binding<T>& binding() { return m_binding; }

        protected:
            Binding<T> m_binding;
        };

	    using IElementPtr = std::shared_ptr<IElement>;
    }
}