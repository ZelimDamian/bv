#pragma once

#include "Entity.h"
#include "ClassNamer.h"

namespace bv
{
	class SceneSystem : public SystemSingleton<SceneSystem>
	{
	public:
		void init() override;

		void onscene(const Action& action) { m_onscene += action; }

	private:
		UniqueEvent m_onscene;
	};

    class Scene: public NamedStorable<Scene>
    {
		struct SceneImplBase
		{
			virtual void init() = 0;
		};

	    using SceneImplPtr = std::shared_ptr<SceneImplBase>;

	    template <class T>
	    struct SceneImpl: public SceneImplBase
	    {
		    void init() override { m_scene.init(); }
			T m_scene;
	    };

    public:
        Scene(Id id, const std::string& name, SceneImplPtr&& scene): NamedStorableBase(id, name), m_scene(std::move(scene))
        {
	        SceneSystem::instance();//.add(this);
        }

		template <class T>
		static void create()
		{
			auto scene = NamedStorableBase::create(ClassNamer::name<T>(), std::make_shared<SceneImpl<T>>());
		}

	    void init()
	    {
			m_scene->init();
	    }

	    SceneImplPtr m_scene;
    };
}