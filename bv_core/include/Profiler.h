#pragma once

#include "Timing.h"
#include "System.h"

#include <unordered_map>

namespace bv
{
    class Profiler : public SystemSingleton<Profiler>
    {
        int TICKS { 60 };

        struct Profile
        {
            time_duration total { 0 };

            Profile* tick()
            {
                then = Timer::now();
                return this;
            }

            void tock()
            {
                auto time = Timer::since(then);
                total += time;
                m_average += (time - m_average) / Profiler::instance()->TICKS;
            }

            float average() { return m_average; }

        private:
            time_point then;
            float m_average;
        };

        using Map = std::unordered_map<std::string, Profile>;
        Map m_profiles;

    public:
        Action tick(const std::string& tag)
        {
            auto profile = m_profiles[tag].tick();
            return [profile] { profile->tock(); };
        }

        void tock(const std::string& tag)
        {
            m_profiles[tag].tock();
        }

        void reset(const std::string& tag)
        {
            m_profiles[tag] = Profile();
        }

        const Profile& profile(const std::string& tag)
        {
            return m_profiles[tag];
        }

        float average(const std::string& tag)
        {
            return m_profiles[tag].average();
        }

        //////////////////////////////////////////////////////////////////////////

        void update() override { }

        void init() override;
    };
}