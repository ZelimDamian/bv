#pragma once

#include "MathLib.h"
#include "Store.h"

namespace bv
{
    namespace rendering
    {
        class FrameBuffer: public Storable<FrameBuffer>
        {
        public:
            FrameBuffer(Id id, glm::ivec2 size);

            void bind();


            const glm::ivec2 &size() const { return m_size; }

        private:
            glm::ivec2 m_size;
        };
    }
}