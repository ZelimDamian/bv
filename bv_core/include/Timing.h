#pragma once
#include <chrono>
#include <time.h>
#include <array>
#include <numeric>

namespace bv
{
	using time_point = std::chrono::system_clock::time_point;
    using time_duration = long long;

	template<typename TPrecision = std::chrono::microseconds>
	struct GenTimer
    {
        inline static void tick()
        {
			_then = now();
        }

        inline static time_duration tock()
        {
            return since(_then);
        }

        inline static time_duration since(time_point then)
        {
            return std::chrono::duration_cast<TPrecision>(now() - then).count();
        }

		inline static time_point now()
	    {
		    return std::chrono::system_clock::now();
	    }

    private:
		static time_point _then;
    };

	template<typename TPrecision>
	time_point GenTimer<TPrecision>::_then;// = Timer<TPrecision>::now();

	using Timer = GenTimer<>;

    struct Timing
    {
        static const int BufferSize = 50;

#define CUT_OFF 0.016f

        // hack around Cxx's need for a static member definition in .cpp file
        template <typename T>
        struct TimeHolder
        {
            T time;
            T delta;

            std::array<T, BufferSize> history;
            int counter { 0 };

            float fixed { CUT_OFF };
        };

        static TimeHolder<double>& holder()
        {
            static TimeHolder<double> s_holder;
            return s_holder;
        }

        static float realDelta() { return (float)holder().delta; }
		static float& delta() { return holder().fixed; } // std::min(realDelta(), CUT_OFF); }
        static double total() { return holder().time; }

        static float average()
        {
            float total = std::accumulate(holder().history.begin(), holder().history.end(), 0.0f);
            return total / BufferSize;
        }

        static void update(double totalTime)
        {
            holder().delta = totalTime - holder().time;
            holder().time = totalTime;
            holder().history[holder().counter++] = holder().delta;
            holder().counter = holder().counter % BufferSize;
        }
    };
}