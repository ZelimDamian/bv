#pragma once

#include "Mesh.h"
#include "RenderMesh.h"

namespace bv
{
//	struct Segment
//	{
//		glm::vec3 start, end;
//	};
//
//	struct Circle
//	{
//		glm::vec3 center;
//		glm::vec3 normal;
//		float radius;
//	};

	struct Gizmo
	{
		enum Type {
			Translation,
			Rotation,
			Scaling
		};

		Mesh::Id mesh;
		rendering::RenderMesh::Id renderMesh;
		Type type;

		struct Shape
		{
			glm::vec3 center;
			glm::vec3 normal;
			float radius;
		} shape[3];
	};

	class ManipulatorGizmoMeshFactory
	{
	public:
		static Gizmo translation();
		static Gizmo rotation();
		static Gizmo scaling();
	};
}