#pragma once

#include "Common.h"
#include "Box.h"

namespace bv
{
	namespace Intersection
	{
		glm::vec3 hit(const Ray& ray, const Plane& plane);

		// calculate the distance on the plane from points of intersection with the given rays
		glm::vec3 hitShift(Ray to, Ray from, Plane plane);

		// find distance between a point and a segment with start and end
		float distanceToLine(const glm::vec3& start, const glm::vec3& end, const glm::vec3& point);

		float distanceToSegment(const glm::vec3& start, const glm::vec3& end, const glm::vec3& point);

        bool test(const Box& box1, const Box& box2);
	};
}

