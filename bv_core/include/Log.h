#pragma once

#include <stdio.h>
#include <glm/glm.hpp>
#include <vector>
#include <string>


#ifdef _MSC_VER

#include <sstream>

namespace std
{
    template <typename T>
    std::string to_string(T value)
    {
	    std::ostringstream os ;
	    os << value ;
	    return os.str() ;
    }
}
#endif

namespace bv
{
    struct Log
    {
        template <typename ... Args>
        static void write(const char* fmt, Args... args)
        {
            printf(fmt, args...);
        }

        template <typename ... Args>
        static void writeln(const char* fmt, Args... args)
		{
            write(fmt, args...);
			printf("\n");
        }


        static void write(const char* message)
        {
            write("%s ", message);
        }

        static void writeln(const char* message)
        {
            writeln("%s", message);
        }

        static void write(int num)
        {
            write("%i ", num);
        }

		static void writeln(int num)
		{
			writeln("%i", num);
		}

		static void writeln(long num)
		{
			writeln("%i", num);
		}

		static void writeln(long long num)
		{
			writeln("%i", num);
		}

        static void write(float num)
        {
            write("%f ", num);
        }

        static void writeln(float num)
        {
            writeln("%f", num);
        }

        static void write(const glm::vec3& vec)
        {
            write("(%f, %f, %f) ", vec.x, vec.y, vec.z);
        }

        static void writeln(double num)
        {
            printf("%f\n", num);
        }

        static void write(const std::string& str)
        {
            printf("%s", str.c_str());
        }

        static void writeln(const std::string& str)
        {
            write(str);
            printf("\n");
        }

	    template<typename ...T>
	    static void info(T&& ... error)
	    {
		    writeln(std::forward<T>(error)...);
	    }

        template<typename ...T>
        static void error(T&& ... error)
        {
            writeln(std::forward<T>(error)...);
            assert(false);
        }

        template<typename ...T>
        static void debug(T&& ... error)
        {
		#ifdef _DEBUG
            writeln(std::forward<T>(error)...);
		#endif
        }

		static void writeln(const glm::vec3& vec)
		{
			writeln("(%f, %f, %f)\n", vec.x, vec.y, vec.z);
		}

        template <typename T>
        static void writeln(const std::vector<T>& values)
        {
            for(auto& v: values)
            {
                writeln(v);
            }
        }

        template <typename T>
        static void write(const std::vector<T>& values)
        {
            for(auto& v: values)
            {
                write(v);
            }
        }

        // needs to be defined to avoid ambiguous call to write on clang/osx
        static void write(const std::vector<uint32_t>& values)
        {
            for(auto& v: values)
            {
                write(static_cast<int>( v ));
            }
        }
    };
}
