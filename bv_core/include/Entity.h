#pragma once

#include "Common.h"
#include "System.h"
#include "Store.h"

namespace bv
{
	class Entity: public NamedStorable<Entity>
	{
	public:
		Entity(Id id, const std::string& name);

		glm::vec3& pos() const;
		void pos(const glm::vec3 &position);

		void translate(const glm::vec3 &position);

		glm::vec3 scale() const;
		void scale(const glm::vec3& scale);

		glm::quat orientation() const;
		void orientation(const glm::quat& orientation);

		glm::mat4 transform() const;
		void updateTransform();

		// apply rotation quaternion of this entity
		void rotate(const glm::quat& rotation);

		//////////////////////////////////////////////
		// Component bits
		//////////////////////////////////////////////

		template <class T>
		std::vector<Handle<T>> components() const
		{
			return  T::find_all(m_id);
		}

        // find a component of the given type T
        template <class T>
        Handle<T> component() const
        {
			return T::find(m_id);
        }

		// create a new instance of component of type T
		template <class T, typename ... Args>
		Handle<T> add(Args&&... args)
		{
			return T::create(m_id, std::forward<Args>(args)...);
		};

		// return the existing or create a new instance of component of type T
		template <class T, typename ... Args>
		Handle<T> require(Args&&... args)
		{
			Handle<T> comp = component<T>();

			if(!comp)
			{
				comp = add<T>(std::forward<Args>(args)...);
			}

			return comp;
		}

        // subscribe to the event when the list of entities is changed
        static void onListChange(Action action) { s_onListChange += action; }

        static void allocate();

    protected:

        // list of entities changed event
        static Event s_onListChange;
	};

    class EntitySystem: public SystemSingleton<EntitySystem>
    {
    public:
		// adds the vec translation to the pos of entity with given id
		static void move(Entity::Id entity, const glm::vec3&);

	    void init() override;

		// perform the update for all entities
		void update() override;

    };

	using Entities = std::vector<Entity::Id>;
}

