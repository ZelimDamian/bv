#pragma once
#include <tuple>

namespace bv
{
    namespace util
    {
        template<typename ... Ts> struct reverse;

        template<typename T>
        struct reverse<T> {
            typedef T type;
        };

        template<typename T, typename ... Rest>
        struct reverse<T, Rest...>
        {
            typedef reverse<typename reverse<Rest...>::type, T> type;
        };

        template <typename... Ts>
        struct tuple_reverse;

        template <>
        struct tuple_reverse<std::tuple<>>
        {
            using type = std::tuple<>;
        };

        template <typename T, typename... Ts>
        struct tuple_reverse<std::tuple<T, Ts...>>
        {
            using head = std::tuple<T>;
            using tail = typename tuple_reverse<std::tuple<Ts...>>::type;

            using type = decltype(std::tuple_cat(std::declval<tail>(), std::declval<head>()));
        };
    }
}