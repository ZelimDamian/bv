#pragma once

#include "Buffer.h"
#include "Log.h"

#include <memory>

namespace bv
{
	class FileWriter
	{
	public:
		FileWriter(const std::string& filename, bool append = true);
		~FileWriter();

		void open();

        void write(const std::string& line)
        {
            write(line.c_str());
        }

		void writeln(const std::string& line)
		{
			writeln(line.c_str());
		}

		template<typename T>
		void writeln(const T& value)
		{
			write(value);
			newline();
		}

		template<typename T>
		void write(const T& value)
        {
            write(std::to_string(value));
        }

		template<typename T>
		void writeln(const Buffer<T>& buffer)
		{
			for (int i = 0; i < buffer.size(); ++i)
			{
				writeln(std::to_string(buffer.at(i)));
			}
		}

		void writeln(const BufferVec3& buffer)
		{
			for (int i = 0; i < buffer.size(); ++i)
			{
				const glm::vec3& vec = buffer.at(i);
				write(vec);
				newline();
			}
		}

		void writeln(const glm::vec3& vec)
		{
			write(vec);
			newline();
		}

		void write(const glm::vec3& vec)
		{
			write(vec.x); write(", ");  write(vec.y); write(", "); write(vec.z);
		}

        void write(const glm::vec4& vec)
        {
            write(vec.x); write(", ");  write(vec.y); write(", "); write(vec.z); write(", "); write(vec.w);
        }

        void write(const char* line);

		template <typename ... Args>
		void formatln(const char* fmt, Args ... args )
		{
			format(fmt, args...);
			newline();
		}

		template <typename ... Args>
		void format(const char* format, Args ... args )
		{
//			va_list args;
//			va_start (args, format);
//				vsprintf (buffer,format, args);
//			va_end (args);

			char buffer[512];

#ifdef __APPLE__
            int r = sprintf(buffer, format, args...);
#else
            int r = sprintf_s(buffer, sizeof(buffer), format, args...);
#endif

            Log::write("FileWriter::format r = %d", r);

			write((const char*) buffer);
		}

		void newline()
		{
			write("\n");
		}

	private:
		struct pimpl;

		std::unique_ptr<pimpl> m_pimpl;
        std::string m_filename;
        bool m_append;
	};
}
