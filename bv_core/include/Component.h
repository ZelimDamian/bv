#pragma once

#include "Common.h"
#include "SFINAE.h"
#include "SelectionSystem.h"
#include "Gui.h"
#include "Store.h"
#include "WindowSystem.h"
#include "Entity.h"
#include "RenderingSystem.h"
#include "ClassNamer.h"
#include "Scene.h"
#include "Singleton.h"
#include "UpdateSystem.h"

namespace bv
{
	// the very base class to represent components
    class ComponentRoot
    {
    public:
        ComponentRoot(Entity::Id entity) : m_entity(entity) { }

        Entity::Id entity() const { return m_entity; }

        void attachGUI() {};

	    // this is a noop for components without initialization
        void initialize() { }

	    // per component updates
	    void update() {}

        bool isActive() const { return m_active; }
        void activate() { m_active = true; }
        void deactivate() { m_active = false; }

        void toggleActive() { m_active = !m_active; }

        // specifies if the componet is active
        bool m_active { true };

    protected:
        Entity::Id m_entity;

	    // static panel pointer to represent the component in GUI
	    static gui::PanelElementPtr componentsPanel()
	    {
		    using namespace gui;
		    // panel element representing this component on the gui
		    static auto _panel = std::make_shared<PanelElement>(nullptr, "Components");
		    Gui::instance()->sidePanel()->add(_panel);
		    return _panel;
	    }
    };

    template <class T>
    class Component: public ComponentRoot, public Storable<T>
    {
    protected:
        using Child = T;
    public:
        using ComponentBase = Component<Child>;
	    using StorableBase = Storable<Child>;
	    using Id = Handle<T>;

		Component(Id id, Entity::Id entity) : ComponentRoot (entity), StorableBase(id)
		{
			load();
		}

		template <typename ...Args>
		static Id create(Entity::Id entityId, Args&&... args)
		{
			Id id = StorableBase::create(entityId, args...);

            // will optionally invoke allocate on Child if the method is defined
            bv::sfinae::has_allocate<Child>::call();

			return id;
		}

	    // find the all components connected to the entity with the given id
	    static std::vector<Handle<Child>> find_all(Entity::Id entity)
	    {
		    auto& comps = components();

		    // find a component attached to the entity
		    std::vector<Handle<Child>> found;

		    for (int i = 0; i < comps.size(); ++i)
		    {
			    if(comps[i].entity() == entity)
			    {
				    found.push_back(comps[i].id());
			    }
		    }

		    return found;
	    }

        // find the component instance connected to the entity with the given id
        static Handle<Child> find(Entity::Id entity)
        {
            auto& comps = components();

	        // find a component attached to the entity
        	auto found = std::find_if(comps.begin(), comps.end(), [entity](const Child& comp) { return comp.m_entity == entity; });

	        // return component if its found or invalid handle otherwise
	        if(found != std::end(comps))
	        {
		        return found->m_id;
	        }
	        else
	        {
		        return Id::invalid();
	        }
        }

        static void load()
        {
            // indicates if this component system has been initialized
            static bool s_isLoaded = false;

	        // only do this once
            if(!s_isLoaded)
            {
	            SceneSystem::instance()->onscene([]()
				{
					// loop through all children and call the initialize
					initComponents();

				    // call static init function
				    //Child::init();
				    bv::sfinae::has_init<Child>::call();

//					bv::sfinae::has_update<Child>::call_func([](int i) {
//						Child::subscribeToUpdates();
//					}, 0);

				    // setup gui bindings
				    initOnChange();
				});

	            // mark this as loaded
	            s_isLoaded = true;
            }
        }

	    static Child& components(int index)
	    {
		    return components()[index];
	    }

        // list of all component instances
		static std::vector<Child>& components()
        {
            return StorableBase::s_instances;
        }

        // a default title to display in the GUI
        static std::string title()
        {
	        return ClassNamer::name<Child>();
        }

    protected:

	    template<class Required>
	    Handle<Required> require()
	    {
		    auto required = Required::find(m_entity);

		    if(!required)
		    {
			    Log::error("Missing " + Required::title() + " in " + title());
		    }

		    return required;
	    }

        // static panel pointer to represent the component in GUI
        gui::PanelElementPtr gui()
        {
	        if(m_gui == nullptr)
	        {
		        m_gui = std::make_shared<gui::PanelElement>(nullptr, Child::title());
	        }

	        return m_gui;
        }

	    std::shared_ptr<gui::PanelElement> m_gui;

        static void initOnChange()
        {
            auto selector = SelectionSystem::instance();

            // when selection changes the gui should be updated accordingly
            selector->onselect([&, selector] {

	            auto selection = selector->selection();

	            auto componentsPanel = ComponentBase::componentsPanel();

                // make sure that the selection is not empty
                if(selection.size() != 0)
                {
	                auto entity = selection[0];

	                auto& comps = components();

	                for (int i = 0; i < comps.size(); ++i)
	                {
						// find component of the selected entity
						auto& comp = comps[i];

						// make sure that the component is attached to selected entity
						if(comp.entity() == entity)
						{
							auto pan = comp.gui();
							componentsPanel->add(pan);

							pan->removeAll();

							pan->button("Active",
								[&comp] () mutable { comp.toggleActive(); },
								[&comp] { return comp.isActive(); });

							// call attachGUI on component it is defined
							comp.attachGUI();
						}
						else
						{
							componentsPanel->remove(comp.gui());
						}
	                }
                }
            });
        }

        static void initComponents()
        {
            auto& comps = components();

            for(auto& comp: comps)
            {
                comp.initialize();
            }
        }

	    // setup per component updates
	    static void subscribeToUpdates()
	    {
		    auto onupdate = [&]
		    {
			    for (auto& comp : Child::s_instances)
			    {
				    if(comp.isActive())
				    {
					    // should call static type update for subclass
					    comp.update();
				    }
			    }
		    };

		    UpdateSystem::instance()->onupdate(onupdate);
	    }
    };
}