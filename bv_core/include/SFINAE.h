#pragma once
#include <utility>

namespace bv
{
    namespace sfinae
    {
        #define HAS_MEM_FUNC(func)                                                              \
            template <typename T>                                                               \
            class has_##func                                                                    \
            {                                                                                   \
                typedef char success;                                                           \
                typedef long failure;                                                           \
                                                                                                \
            public:                                                                             \
                template <typename C = T, typename M = decltype(&C::func)>                      \
                static success call_imp(int i)                                                  \
                    { C::func(); return 0; };                                                   \
                                                                                                \
                template <typename C = T, typename M = decltype(&C::func), typename ... Args>   \
                static success call_imp(int i, Args&& ...args)                                  \
                    { C::func(std::forward<Args>(args)...); return 0; };                        \
                                                                                                \
                template <typename C = T, typename M = decltype(&C::func),                      \
							class F, typename ... Args>                                         \
                static success call_imp(int i, const F& f, Args&& ...args)                                  \
                    { f(std::forward<Args>(args)...); return 0; };                              \
                                                                                                \
                template <typename C = T>                                                       \
				static failure call_imp(...) { return 0; };                                     \
                                                                                                \
                template <typename ...Args>                                                     \
				static void call(Args&& ...args) { call_imp(1, std::forward<Args>(args)...); }  \
				static void call() { call_imp(1); }                                             \
																								\
                template <class F, typename ...Args>                                                     \
				static void call_func(const F& func, Args&& ...args) { call_imp(1, func, std::forward<Args>(args)...); }  \
        };                                                                                      \

        HAS_MEM_FUNC(allocate);
        HAS_MEM_FUNC(deallocate);
        HAS_MEM_FUNC(init);
		HAS_MEM_FUNC(update);
    }
}