#pragma once

#include "Common.h"
#include "System.h"
#include "Store.h"

namespace bv
{
    class Camera: public Storable<Camera>
    {

    public:
	    enum class View
	    {
		    Top,
		    Bottom,
		    Left,
		    Right,
		    Front,
		    Back,
		    Perspective
	    };

    public:
        Camera(Id id): Camera(id, glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3()) { }

        Camera(Id id, const glm::vec3& eye, const glm::vec3& target,
                          float distance = 1.0, float near = 0.001f, float far=100.0f);

	    // position of the current camera
	    glm::vec3 eye() const;
	    void eye(glm::vec3 eye);

	    // current camera target position
	    glm::vec3 target() const;
	    void target(glm::vec3 target);

		float fov() const;
        void fov(float fov) const;
		void aspect(float aspect);

	    float distance() const;
	    void distance(float val);

	    glm::vec4 viewport() const;
	    void viewport(const glm::vec4& viewport);

	    const glm::mat4& view();
	    const glm::mat4& proj();

	    glm::quat orientation();
    };

    class CameraSystem : public SystemSingleton<CameraSystem>
    {
    public:
        // update all active cameras
        void update() override;

        // set the camera with id as the current camera
        void current(Camera::Id cameraId) { m_currentId = cameraId; }

	    // camera from hoevered viewport, invalid if no viewport hovered
	    Camera::Id current() { return m_currentId; }

	    // camera from last hovered viewport
	    Camera::Id active() { return m_previousId; }

		// ray emanating from camera passing through the passed screen coords
		Ray ray(const glm::ivec2& screen);

	    void set(Camera::View view);

	    // use lookAt method to enclose bounding box containing the whole selection
	    void lookAtSelection();
	    // set the camera to look at center at distance to enclose halfextent
	    void lookAt(const glm::vec3& center, const glm::vec3& halfextent);

	private:
		// currently active camera
		Camera::Id m_currentId;
	    Camera::Id m_previousId;

		// sends the view and proj matrices to the renderer
		void setup();

    };
}
