#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <numeric>
#include <algorithm>
#include <vector>

#ifndef M_PI
#define M_PI		3.14159265358979323846
#define M_TORAD(__angle) __angle * M_PI / 180.0f
#endif

//#ifndef fabs
//#define fabs(__a) __a >= 0? __a ? -__a
//#endif

struct Math
{
    template <class T>
    static T average(const std::vector<T>& vectors)
    {
        return std::accumulate(vectors.begin(), vectors.end(), T()) / (float)vectors.size();
    }

    static glm::vec3 transform(const glm::vec3 &point, const glm::mat4 &mat)
    {
        return glm::vec3(mat * glm::vec4(point, 1.0f));
    }

    static glm::vec3 transformNormal(const glm::vec3& point, const glm::mat4& mat)
    {
        return glm::vec3(mat * glm::vec4(point, 0.0f));
    }

    static float signedAngleBetween(const glm::vec3& from, const glm::vec3& to, const glm::vec3& ref)
    {
	    if(glm::dot(ref, glm::cross(from, to)) < 0.0f)
		    return -angleBetween(from, to);
	    else
		    return angleBetween(from, to);
    }

	static float angleBetween(const glm::vec3& from, const glm::vec3& to)
    {
	    const float dot = glm::clamp(glm::dot(glm::normalize(from), glm::normalize(to)), -1.0f, 1.0f);
        return glm::acos(dot);
    }

	template <class T>
    static void transformNormals(T& points, const glm::mat4& transform)
    {
        // transform the master vertices in DOD way
        for (auto& point: points)
        {
            point = transformNormal(point, transform);
        }
    }

	template <class T>
	static void transformPoints(T& points, const glm::mat4& xform)
    {
        // transform the master vertices in DOD way
        for (auto& point: points)
        {
            point = transform(point, xform);
        }
    }

	static float distance(const glm::vec3& pos1, const glm::vec3& pos2)
	{
		return glm::length(pos1 - pos2);
	}

	static bool closeEnough(const glm::vec3 &pos1, const glm::vec3 &pos2)
	{
		return  distance(pos1, pos2) <= glm::epsilon<float>() * 10.0f;
	}

	static glm::quat quatBetween(glm::vec3 a, glm::vec3 b)
	{
		a = glm::normalize(a);
		b = glm::normalize(b);
		auto angle = glm::acos(glm::dot(a, b));
		auto axis = glm::cross(a, b);
		return glm::normalize(glm::angleAxis(angle, axis));
	}

    static glm::vec3 right() { return glm::vec3(1.0f, 0.0f, 0.0f); }
    static glm::vec3 up() { return glm::vec3(0.0f, 1.0f, 0.0f); }
    static glm::vec3 forward() { return glm::vec3(0.0f, 0.0f, -1.0f); }
    static glm::vec3 back() { return glm::vec3(0.0f, 0.0f, 1.0f); }
};

using VectorVec3 = std::vector<glm::vec3>;
using VectorVec4 = std::vector<glm::vec4>;
using VectorFloat = std::vector<float>;
using VectorInt = std::vector<int>;
