#pragma once
#include "Common.h"
#include "System.h"

namespace bv
{
	enum class MouseButton 
	{
		Left = 0,
		Right = 1,
		Middle = 2
	};

	class MouseSystem : public SystemSingleton<MouseSystem>
	{
	public:
		int x() { return m_x; }
		int y() { return m_y; }
		int z() { return m_z; }

		void x(int x) { m_oldx = m_x; m_x = x; }
		void y(int y) { m_oldy = m_y; m_y = y; }
		void z(int z) { m_oldz = m_z; m_z = z; }

		int deltaX() { return m_x - m_oldx; }
		int deltaY() { return m_y - m_oldy; }
		int deltaZ() { return m_z - m_oldz; }

		glm::ivec2 pos() { return glm::ivec2(m_x, m_y); }
		glm::ivec2 posOld() { return glm::ivec2(m_oldx, m_oldy); }

		bool button(MouseButton button) const { return m_buttons[(int)button]; }

		void button(MouseButton button, bool state);

		// only return button state if the mouse is inside the screen
		bool buttonInScene(MouseButton button) const;

        // return if the cursor is in scene screen
        bool cursorInScene() const;

		bool left() const { return button(MouseButton::Left); }
		bool middle() const { return button(MouseButton::Middle); }
		bool right() const { return button(MouseButton::Right); }

		void state(int mouseX, int mouseY, int mouseZ, bool leftDown, bool rightDown, bool middleDown);

		// hook the action to the clicked event
		void onleft(Action action) { m_leftClick += action; }
		void onright(Action action) { m_rightClick += action; }

	private:
		int m_x { 0 };
		int m_y { 0 };
		int m_z { 0 };

		int m_oldx { 0 };
		int m_oldy { 0 };
		int m_oldz { 0 };

		bool m_buttons[3];
		bool m_oldbuttons[3];

		Event m_leftClick;
		Event m_rightClick;
		Event m_middleClick;
	};
}

