SET(target bv_core)

INCLUDE(./ext/CMakeLists.txt)

SETUP(${target})

SET(SOURCE_FILES

    include/Common.h
    include/Store.h
    include/Buffer.h
    include/Singleton.h
    include/System.h
    include/Box.h
    include/Camera.h
    include/CameraSystem.h
    include/Component.h
    include/Entity.h
    include/Log.h
    include/Timing.h
    include/MouseSystem.h
    include/WindowSystem.h
    include/RenderingSystem.h
    include/ManipulatorSystem.h
    include/Element.h
    include/Event.h
    include/LabelElement.h
    include/SliderElement.h
    include/Shader.h
    include/RenderMesh.h
    include/Mesh.h
    include/Texture.h
    include/MeshFactory.h
    include/RenderingSystem.h
    include/Scene.h
    include/FileReader.h
    include/RenderMeshComponent.h
    include/Tri.h
    include/SelectionSystem.h
    include/SFINAE.h
    include/Ray.h
    include/Rect.h
    include/Plane.h
    include/PanelElement.h
    include/Meshable.h
    include/MathLib.h
    include/ListElement.h
    include/DragElement.h
    include/KeyboardSystem.h
    include/Intersection.h
    include/ButtonElement.h
    include/Binding.h
    include/TupleReverser.h
    include/TupleIterator.h
    include/FileWriter.h
    include/Attribute.h
    include/AttributeMapping.h
    include/Vertex.h
    include/VertexDecl.h
    include/Intersection.h
    include/Sphere.h

    src/Entity.cpp
    src/MouseSystem.cpp
    src/Box.cpp
    src/CameraSystem.cpp
    src/WindowSystem.cpp
    src/ManipulatorSystem.cpp
    src/System.cpp
    src/SelectionSystem.cpp
    src/Element.cpp
    src/PanelElement.cpp
    src/SliderElement.cpp
    src/RenderMesh.cpp
    src/Mesh.cpp
    src/Tri.cpp
    src/Vertex.cpp
    src/VertexDecl.cpp
    src/Texture.cpp
    src/MeshFactory.cpp
    src/RenderingSystem.cpp
    src/Scene.cpp
    src/FileReader.cpp
    src/RenderMeshComponent.cpp
    src/FileWriter.cpp
    src/Intersection.cpp
)

ADD_CLASS(Gui)
ADD_CLASS(ArcBall)
ADD_CLASS(DebugMesh)
ADD_CLASS(StateLoggerComponent)
ADD_CLASS(FrameBuffer)
ADD_HEADER(StateHistory)
ADD_HEADER(TimeFormatting)
ADD_HEADER(UpdateSystem)
ADD_HEADER(ViewportElement)
ADD_HEADER(RenderMode)
ADD_HEADER(RenderContext)
ADD_CLASS(Profiler)
ADD_CLASS(Bounding)
ADD_CLASS(HotkeySystem)
ADD_CLASS(ManipulatorGizmoMeshFactory)
ADD_CLASS(ObjLoader)
ADD_CLASS(StlLoader)

LINK_TO(./ext/CMake/nativefiledialog nativefiledialog)

BOOTSTRAP(${target})

