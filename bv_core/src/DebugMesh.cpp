#include "DebugMesh.h"

#include "Shader.h"
#include "RenderingSystem.h"

#include "RenderMesh.h"

using namespace bv;
using namespace rendering;

void DebugMesh::point(const glm::vec3 &point)
{
	//m_mesh->vertex(point);
	m_points.push_back(point);
}

void DebugMesh::render(glm::mat4 mat, RenderMode mode, glm::vec4 color)
{
    auto renderer = RenderingSystem::instance();

    auto points = m_points;

    auto onrender = [this, points, mode, color, mat, renderer] () mutable
    {
        if (points.size())
        {
            m_mesh->positions = points;
            m_mesh->updateVertices();
	        m_mesh->indices.clear();

	        auto shader = Shader::requse("colors");
	        auto uniform = shader->uniform("uColor", UniformType::Vec4);
	        renderer->apply(uniform, uniform->data(color));

	        renderer->apply(mat);
	        renderer->apply(RenderState { Culling::NO, mode });

	        auto rendermesh = RenderMesh::create(m_mesh);
	        renderer->render(rendermesh, shader);

	        m_mesh->clear();
        }
    };

    m_points.clear();

    renderer->onrender_once(onrender);
}

void DebugMesh::box(const Box& shape, const glm::mat4& mat, RenderMode mode, const glm::vec4& color)
{
	auto renderer = RenderingSystem::instance();

	auto onrender = [=]
	{
        MeshFactory::shape(shape, m_mesh);
        m_mesh->updateWireIndices();

        auto shader = Shader::requse("colors");
        auto uniform = shader->uniform("uColor", UniformType::Vec4);
        renderer->apply(uniform, uniform->data(color));

        renderer->apply(mat);
        renderer->apply(RenderState { Culling::NO, mode });

        auto rendermesh = RenderMesh::create(m_mesh);
        renderer->render(rendermesh, shader);

		m_mesh->clear();
	};

	renderer->onrender_once(onrender);
}

void DebugMesh::plane(const Plane& shape, const glm::mat4& mat, RenderMode mode, const glm::vec4& color)
{
	auto renderer = RenderingSystem::instance();

	auto onrender = [&, mode, renderer]
	{
		MeshFactory::shape(shape, 1.0f, 50, m_mesh);
		auto rendermesh = RenderMesh::create(m_mesh);
		m_mesh->clear();

		renderer->reset(Culling::NO, mode);
		renderer->transform(mat);

		Shader::requse("colors")->uniform("uColor", UniformType::Vec4)->set(color);

		renderer->submit(rendermesh);
	};

	renderer->onrender_once(onrender);
}

void DebugMesh::triangle(const TriVerts& triangle)
{
//	auto& renderer = RenderingSystem::instance();
//
//	auto onrender = [&, mode]
//	{
//		MeshFactory::shape(triangle, m_mesh);
//		auto rendermesh = RenderMesh::create(m_mesh);
//		m_mesh->clear();
//
//		renderer.reset(Culling::NO, mode);
//		renderer.transform(xform);
//
//		Shader::use("colors")->uniform("uColor", UniformType::Vec4)->set(m_color);
//
//		renderer.render(rendermesh);
//	};
//
//	renderer.onrender_once(onrender);

	m_points.push_back(triangle[0]);
	m_points.push_back(triangle[1]);
	m_points.push_back(triangle[2]);
}
