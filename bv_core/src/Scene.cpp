#include "Scene.h"

#include "WindowSystem.h"

#include "Gui.h"

using namespace bv;
using namespace gui;

static PanelElement* panel;

void SceneSystem::init()
{
	auto sceneNames = Scene::keys();

	if(sceneNames.size() > 1)
	{

		auto gui = Gui::instance();

		gui->onload([=]
		            {
			            panel = gui->sidePanel()->panel("Scenes");


			            auto list = panel->list("Select scene", sceneNames, [=](int selected)
			            {
				            Scene::Id scene = Scene::find(sceneNames[selected]);

				            WindowSystem::instance()->beforeRun([scene, this]() mutable
				                                                {
					                                                scene->init();

					                                                // inform the world that a scene has been selected
					                                                m_onscene();
				                                                });

				            // we don't want to allow future scene selection
				            gui->sidePanel()->remove(panel);
			            });
		            });
	}
	else if(sceneNames.size() == 1)
	{
		auto scene = Scene::find(sceneNames[0]);

		WindowSystem::instance()->beforeRun([scene, this]() mutable
		                                    {
			                                    scene->init();

			                                    // inform the world that a scene has been selected
			                                    m_onscene();
		                                    });
	}
	else
	{
		auto dialog = Gui::instance()->window("No scenes");
	}
}

