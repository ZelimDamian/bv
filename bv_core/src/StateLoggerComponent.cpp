#include "StateLoggerComponent.h"

#include "FileWriter.h"
#include "TimeFormatting.h"

using namespace bv;

static std::vector<std::shared_ptr<FileWriter>> _files;

void StateLoggerComponent::initialize()
{
	std::string filename = "log-";
	filename += TimeFormat::string(std::chrono::system_clock::now());

	_files.push_back(std::make_shared<FileWriter>(filename));
}

void StateLoggerComponent::attachGUI()
{
	auto gui = this->gui();
	gui->bind("Log period", m_period, 0.016f, 10.0f);
}

void StateLoggerComponent::push(State state)
{
	if(m_active)
	{
		if(m_time - m_lastLog > m_period)
		{
			state.time = m_time;
			m_history.push_back(state);
			m_lastLog = m_time;
		}
	}
}

void StateLoggerComponent::update()
{
	m_time += Timing::delta();

	if(m_history.size() > m_chunkSize)
	{
		auto& file = _files[m_id.id];

		for (int i = 0; i < m_history.size(); ++i)
		{
			file->write(m_history[i].time); file->write(", ");
			file->write(glm::length(m_history[i].expulsion)); file->write(", ");
			file->write(glm::length(m_history[i].reaction)); file->write(", ");
			file->writeln(m_history[i].position.y);
		}

		m_history.clear();
	}
}

