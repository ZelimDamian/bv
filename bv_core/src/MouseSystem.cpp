#include "MouseSystem.h"

#include "CameraSystem.h"
#include "Gui.h"

using namespace bv;

void MouseSystem::state(int mouseX, int mouseY, int mouseZ, bool leftState, bool rightState, bool middleState)
{
	m_oldx = m_x;
	m_oldy = m_y;
	m_oldz = m_z;

	m_x = mouseX;
	m_y = mouseY;
	m_z = mouseZ;

	button(MouseButton::Left, leftState);
	button(MouseButton::Right, rightState);
	button(MouseButton::Middle, middleState);
}

bool MouseSystem::cursorInScene() const
{
    return !!CameraSystem::instance()->current();
}

// only return button state if the mouse is inside the screen
bool MouseSystem::buttonInScene(MouseButton button) const
{
	// only return the button state if the cursor is outside the GUI
    // shortcircuiting
    return cursorInScene() && this->button(button);
}

void MouseSystem::button(MouseButton button, bool state)
{
    m_oldbuttons[(int)button] = m_buttons[(int)button];
    m_buttons[(int) button] = state;

    // if this is the first time the button is pressed after a release
    if (!m_oldbuttons[(int) button] && state)
    {
        if (CameraSystem::instance()->current())
        {
            // notify the subscribees of the leftclick event
            switch (button)
            {
                case MouseButton::Left:
                    m_leftClick();
                    break;
                case MouseButton::Right:
                    m_rightClick();
                    break;
                case MouseButton::Middle:
                    m_middleClick();
                    break;
            }
        }
	}
}