#include "Box.h"

using namespace bv;

int Box::longestAxis()
{
    const glm::vec3& hs = halfsize();

    if (hs.x >= hs.y && hs.x >= hs.z)
    {
        return 0;
    }
    if (hs.y >= hs.x && hs.y >= hs.z)
    {
        return 1;
    }
    if (hs.z >= hs.x && hs.z >= hs.y)
    {
        return 2;
    }

    return 0;
}
