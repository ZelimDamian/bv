#include "Bounding.h"
#include "RenderMeshComponent.h"

using namespace bv;

Box Bounding::box(Entities entities)
{
	Box box = Bounding::box(entities[0]);

	for (int i = 1; i < entities.size(); ++i)
	{
		box.enclose(Bounding::box(entities[i]));
	}

	box.perturb();

	return box;
}

Box Bounding::box(Entity::Id entity)
{
    auto renderableId = entity->component<RenderMeshComponent>();

    if(renderableId)
    {
        auto& renderable = renderableId.ref();

        auto mesh = renderable.mesh();

        return Box::transform(box(mesh), entity->transform());
    }
    else
    {
        return Box(glm::vec3(-0.1f), glm::vec3(0.1f));
    }
}

float Bounding::area(const Box& box)
{
	auto size = box.size();
	return 2.0f * (size.x + size.y + size.z);
}

Obb Bounding::obb(const VectorVec3& points, int steps)
{
	Box aabb;
	aabb.fit(points);

	float minArea = area(aabb);

	Obb best;
	best.aabb = aabb;

	float angleStep = 2.0f * glm::pi<float>() / steps;
	float ax = 0.0f;

	glm::mat4 mtx;

	for (uint32_t i = 0; i < steps; ++i)
	{
		float ay = 0.0f;

		for (uint32_t j = 0; j < steps; ++j)
		{
			float az = 0.0f;

			for (uint32_t k = 0; k < steps; ++k)
			{
//						bx::mtxRotateXYZ(mtx, ax, ay, az);
				mtx = glm::mat4_cast(glm::quat(glm::vec3(ax, ay, az)));

				glm::mat4 mtxT = glm::transpose(mtx);

//						calcAabb(aabb, mtxT, _vertices, _numVertices, _stride);
				aabb.fit(points, mtxT);

				float a = area(aabb);
				if (a < minArea)
				{
					minArea = a;

					// normalize aabb
					auto center = aabb.center();
					aabb.center(glm::vec3());

					best.aabb = aabb;

					// apply translation
					glm::mat4 trans;
					auto translation = glm::translate(trans, center);
					best.mtx = mtx * translation;
				}

				az += angleStep;
			}

			ay += angleStep;
		}

		ax += angleStep;
	}

	return best;
}
