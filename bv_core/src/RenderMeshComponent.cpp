#include "Entity.h"
#include "RenderMeshComponent.h"
#include "LightingSystem.h"
#include "DebugMesh.h"

#include <Bounding.h>

using namespace bv;
using namespace bv::rendering;

static std::vector<Mesh::Id> _meshes;
static std::vector<RenderMesh::Id> _renderMeshes;
static std::vector<Shader::Id> _shaders;

RenderMeshComponent::RenderMeshComponent(Id id, Entity::Id entity, Mesh::Id mesh, Shader::Id shader): ComponentBase(id, entity)
{
    _meshes.push_back(mesh);
	_renderMeshes.push_back(RenderMesh::create(mesh));
	_shaders.push_back(shader);
}

void RenderMeshComponent::init()
{
    auto renderer = RenderingSystem::instance();

    renderer->onrender([&, renderer]{

        for (int id = 0; id < components().size(); ++id)
        {
            auto& comp = components()[id];
            if(comp.isActive())
            {
	            renderer->reset(comp.culling(), comp.m_showWireframe ? RenderMode::LINE_STRIP : RenderMode::TRIANGLES);

                const glm::mat4& mat = comp.m_entity->transform();
	            renderer->transform(mat);

	            Shader::Id shader = _shaders[id];
	            Shader::use(shader);

	            const glm::vec3& lightPos = LightingSystem::instance()->current()->pos();
				shader->uniform("u_LightPos", UniformType::Vec3)->set(lightPos);

	            const glm::mat4 normalMatix = glm::transpose(glm::inverse(mat));
	            shader->uniform("u_NormalMatrix", UniformType::Mat4)->set(normalMatix);

				// set all uniforms
				for(auto pair : comp.uniforms())
				{
					auto uniformName = pair.first;
					auto value = pair.second;
					shader->uniform(uniformName, UniformType::Vec4)->set(value);
				}

				// update rendermesh if mesh changed
				// TODO: make a dedicated update method
				_renderMeshes[id] = RenderMesh::create(_meshes[id]);

	            renderer->submit(_renderMeshes[id]);

	            if(comp.m_showNormals)
	            {
					auto debug = DebugMesh::instance();

		            debug->box(Bounding::box(comp.m_entity), comp.m_entity->transform());

		            const auto& verts = comp.mesh()->vertices;

		            for (int i = 0; i < verts.size(); ++i)
		            {
			            const auto& pos = verts[i].get<Mesh::TVertex::Pos>();
			            const auto& nrm = verts[i].get<Mesh::TVertex::Nrm>();

			            debug->point(pos);
			            debug->point(pos + comp.m_normalLenght * nrm);
		            }

		            debug->render(comp.m_entity->transform());
	            }
            }
        }
    });
}

RenderMesh::Id RenderMeshComponent::renderMesh() const
{
    return _renderMeshes[(int)m_id];
}

Mesh::Id RenderMeshComponent::mesh() const
{
    auto id = _renderMeshes[(int) m_id];

    auto& meshes = Mesh::instances();

    for (int i = 0; i < meshes.size(); ++i)
    {
        if(meshes[i].renderMeshId == id)
        {
            return meshes[i].id();
        }
    }

    return Mesh::Id::invalid();
}

void RenderMeshComponent::attachGUI()
{
	auto panel = this->gui();

	panel->check("Draw", [this] { m_showNormals = !m_showNormals; }, [this] { return m_showNormals; });
	panel->bind<float>("Length", [this] { return m_normalLenght; }, [this](float value) { m_normalLenght = value; },
	                      0.001f, 5.0f, 0.1f);

	panel->check("Wireframe", [this] { m_showWireframe = !m_showWireframe; }, [this] { return m_showWireframe; });

	std::vector<std::string> cullings = { "CW", "CCW", "NO" };
	panel->combo("Culling", cullings, [this](int idx) { m_culling = Culling(idx); });

	std::vector<std::string> shaders = Shader::keys();

	panel->combo("Shader", shaders, [this](int idx) { _shaders[(int)m_id] = Shader::Id((uint16_t)idx); });
}
