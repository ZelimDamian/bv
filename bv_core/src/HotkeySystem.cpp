#include "HotkeySystem.h"
#include "KeyboardSystem.h"

using namespace bv;

void HotkeySystem::update()
{
	for (auto &&hotkey : Hotkey::handles())
	{
		bool triggered = std::all_of(hotkey->keys.begin(), hotkey->keys.end(), [] (const Key& key)
		{
			return KeyboardSystem::instance()->key(key);
		});

		if(hotkey->keys.size() > 0 && triggered)
		{
			if(!hotkey->triggered)
			{
				hotkey->trigger();
			}
		}
		else
		{
			hotkey->triggered = false;
		}
	}
}

std::string HotkeySystem::shortcut(const std::vector<Key>& keys)
{
	std::string shortcut;

	for (auto key = keys.begin(); key != keys.end(); ++key)
	{
		shortcut += KeyboardSystem::instance()->name(*key);
		if(*key != keys.back())
		{
			shortcut += " + ";
		}
	}

	return shortcut;
}

void Hotkey::trigger()
{
	action();
	triggered = true;
}