#include "Entity.h"
#include "Gui.h"
#include "SelectionSystem.h"

using namespace bv;

static std::vector<glm::mat4> _xforms;
static std::vector<glm::vec3> _positions;
static std::vector<glm::quat> _orientations;
static std::vector<glm::vec3> _scales;
static std::vector<glm::mat4> _rotations;

Event Entity::s_onListChange;

Entity::Entity(Id id, const std::string &name) : NamedStorableBase(id, name)
{
	_positions.emplace_back();
	_xforms.emplace_back();
	_orientations.emplace_back();
    _scales.emplace_back(1.0f);
	
    // inform the subsribers that the list has changed
    s_onListChange();

    // kick it off
    EntitySystem::instance();
}

glm::vec3& Entity::pos() const
{
    return _positions[(int)m_id];
}

void Entity::pos(const glm::vec3 &trans)
{
	_positions[(int)m_id] = trans;
}

void Entity::translate(const glm::vec3 &trans)
{
	_positions[(int)m_id] += trans;
}

void Entity::rotate(const glm::quat& rotation)
{
//	const auto old = _orientations[(int)m_id];
//	const auto inverse = glm::inverse(old);
//	_orientations[(int)m_id] *= old * rotation * inverse ;
	_orientations[(int)m_id] *= rotation;
}

glm::vec3 Entity::scale() const
{
	return _scales[(int)m_id];
}

glm::mat4 Entity::transform() const
{
	return _xforms[(int)m_id];
}

void Entity::scale(const glm::vec3& scale)
{
	_scales[(int)m_id] = scale;
}

glm::quat Entity::orientation() const
{
	return _orientations[(int)m_id];
}

void Entity::orientation(const glm::quat& orientation)
{
	_orientations[(int)m_id] = orientation;
}

void Entity::allocate()
{

}

void EntitySystem::move(Entity::Id entity, const glm::vec3& translation)
{
    assert(entity);
	_positions[(int)entity] += translation;
}

void EntitySystem::init()
{
	using namespace gui;

	auto panel = Gui::instance()->sidePanel()->panel("Entity pos");

	auto selector = SelectionSystem::instance();
	// when selection changes the gui should be updated accordingly
	selector->onselect([&, selector, panel]
	{
	    panel->removeAll();

		auto selection = selector->selection();

		if (selection.size() != 0)
		{
			Entity::Id entity = selection[0];

			{
				auto source = [entity] { return entity->pos(); };
				auto target = [entity] (const glm::vec3& value) mutable { return entity->pos(value); };
				panel->bind<glm::vec3>("Pos", source, target, -10.0f, 10.0f, 0.01f);
			}
			{
				auto source = [entity] { return glm::degrees(glm::eulerAngles(entity->orientation())); };
				auto target = [entity] (const glm::vec3& value) mutable
				{
					return entity->orientation(glm::quat(glm::radians(value)));
				};

				panel->bind<glm::vec3>("Angles", source, target, -180.0f, 180.0f);
			}

//			panel->label("Pos x", [entity] { return std::to_string(entity->pos().x); });
//			panel->label("Pos y", [entity] { return std::to_string(entity->pos().y); });
//			panel->label("Pos z", [entity] { return std::to_string(entity->pos().z); });
		}
	});
}

void Entity::updateTransform()
{
	static const glm::mat4 identity;

	int i = (int)m_id;

	_xforms[i] = glm::translate(identity, _positions[i]);
	_xforms[i] *= glm::toMat4(_orientations[i]);
}

void EntitySystem::update()
{
    static const glm::mat4 identity;

	for (int i = 0; i < Entity::count(); ++i)
	{
		_xforms[i] = glm::translate(identity, _positions[i]);
	}

	for (int i = 0; i < Entity::count(); ++i)
	{
		_xforms[i] = _xforms[i] * glm::toMat4(_orientations[i]);
	}

    for (int i = 0; i < Entity::count(); ++i)
    {
        _xforms[i] = glm::scale(_xforms[i], _scales[i]);
    }
}
