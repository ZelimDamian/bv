#include "ManipulatorSystem.h"

#include "MouseSystem.h"
#include "KeyboardSystem.h"
#include "WindowSystem.h"
#include "SelectionSystem.h"
#include "Intersection.h"
#include "CameraSystem.h"
#include "RenderMesh.h"
#include "Shader.h"

#include "ArcBall.h"
#include "Gui.h"
#include "DebugMesh.h"

#include "ManipulatorGizmoMeshFactory.h"

using namespace bv;
using namespace rendering;

static enum class ManipulatorType
{
	Translate = 0,
	Rotate,
	Scale
} _manipulatorType;

static int _axisIndex = -1; // which axis

Gizmo gizmos[3];

float gizmoScale(const glm::vec3& gizmoPos)
{
	auto camera = CameraSystem::instance()->active();
	if(!!camera)
	{
		auto distance = Math::distance(camera->eye(), gizmoPos);
		return distance / 3.0f;
	}
	return 1.0f;
}

void ManipulatorSystem::init()
{
	gui::Gui::instance()->onload([&]
	{
		auto menu = gui::Gui::instance()->menu()->item("Manipulators");
		menu->item("Translate", { Key::W }, [this] { _manipulatorType = ManipulatorType::Translate; });
		menu->item("Rotate", { Key::E }, [this] { _manipulatorType = ManipulatorType::Rotate; });
		menu->item("Scale", { Key::R }, [this] { _manipulatorType = ManipulatorType::Scale; });


		gizmos[0] = ManipulatorGizmoMeshFactory::translation();
		gizmos[1] = ManipulatorGizmoMeshFactory::rotation();
		gizmos[2] = ManipulatorGizmoMeshFactory::scaling();

		auto renderer = RenderingSystem::instance();
		renderer->onrender([&, renderer]
	    {
		    renderer->reset(Culling::NO, RenderMode::LINES, DepthTest::ALWAYS);

		    auto selection = SelectionSystem::instance()->selection();

		    if(!selection.empty())
		    {
//			    auto pos = SelectionSystem::instance()->center();
			    auto pos = selection[0]->pos();

			    auto mat = glm::translate(glm::mat4(), pos);

			    auto scale = gizmoScale(pos);

			    mat = glm::scale(mat, glm::vec3(scale));

			    renderer->transform(mat);

			    Shader::requse("diffuse_colors");

			    renderer->submit(gizmos[(int) _manipulatorType].renderMesh);
		    }
	    });
	});
}

void ManipulatorSystem::update()
{
	auto keyboard = KeyboardSystem::instance();
	auto mouse = MouseSystem::instance();
	auto cameraSystem = CameraSystem::instance();

	if (!!cameraSystem->current() && mouse->button(MouseButton::Left))
	{
		Ray now = cameraSystem->ray(mouse->pos());
		Ray before = cameraSystem->ray(mouse->posOld());

		auto camera = cameraSystem->current();

		auto selection = SelectionSystem::instance()->selection();

		for(auto id: selection)
		{
			if(keyboard->key(Key::R))
			{
				const auto viewport = CameraSystem::instance()->current()->viewport();
				auto min = glm::vec2{ viewport.x, viewport.y };
				auto max = min + glm::vec2{ viewport.z, viewport.w };
				const auto center = (max - min) / 2.0f;

				const Arcball ball(center, center.y / 2.0f);
				ball.click(mouse->posOld());
				const glm::quat rotation = ball.unclick(mouse->pos());

//				const glm::quat globalRotation = (camera.orientation()) * rotation * glm::inverse(camera.orientation());

                const glm::quat entityRotation = id->orientation();
                const glm::quat globalRotation = entityRotation *
                        glm::inverse(camera->orientation()) * glm::inverse(entityRotation) *
                        rotation *
                        entityRotation * camera->orientation();

				id->orientation(globalRotation);
			}
			else if(keyboard->key(Key::G))
			{
                const Plane plane { id->pos(), - now.dir() };
                const glm::vec3 translation = Intersection::hitShift(now, before, plane);

                EntitySystem::move(id, translation);
			}
			else if(_manipulatorType == ManipulatorType::Rotate)
			{
				if(_axisIndex < 0) // selecting axis
				{
					for (int i = 0; i < 3; ++i)
					{
						auto pos = id->pos();
						auto center = gizmos[(int)_manipulatorType].shape[i].center + pos;
						auto normal = gizmos[(int)_manipulatorType].shape[i].normal;
						auto radius = gizmos[(int)_manipulatorType].shape[i].radius * gizmoScale(center);

						const Plane plane { center, normal };
						auto hit = Intersection::hit(now, plane);

						float distance = Math::distance(hit, center);
						float margin = glm::abs(distance - radius);
						float tolerance = 0.05f * radius;

						if(margin < tolerance)
						{
							_axisIndex = i;
							break;
						}
					}
				}
				else
				{
					auto pos = id->pos();
					auto center = gizmos[(int)_manipulatorType].shape[_axisIndex].center + pos;
					auto normal = gizmos[(int)_manipulatorType].shape[_axisIndex].normal;
					auto radius = gizmos[(int)_manipulatorType].shape[_axisIndex].radius * gizmoScale(center);

					const Plane plane { center, normal };
					auto hit = Intersection::hit(now, plane);

					auto radiusVector = hit - center;
					float distance = glm::length(radiusVector);
					radiusVector /= distance;

					auto oldHit = Intersection::hit(before, plane);
					auto radiusVectorOld = glm::normalize(oldHit - center);

					if(!Math::closeEnough(radiusVector, radiusVectorOld))
					{
						float angle = glm::acos(glm::dot(radiusVectorOld, radiusVector));
						auto axis = glm::normalize(glm::cross(radiusVectorOld, radiusVector));

						if(!glm::isnan(angle) && glm::length(axis) > 0.0f && !glm::any(glm::isnan(axis)))
						{
							auto rotation = glm::normalize(glm::angleAxis(angle, axis));
							id->orientation(rotation * id->orientation());
						}
					}
				}
			}
			else if(_manipulatorType == ManipulatorType::Translate)
			{
				if(_axisIndex < 0)
				{
					for (int i = 0; i < 3; ++i)
					{
						auto pos = id->pos();
						auto center = gizmos[(int)_manipulatorType].shape[i].center + pos;
						auto normal = gizmos[(int)_manipulatorType].shape[i].normal;
						auto radius = gizmos[(int)_manipulatorType].shape[i].radius * gizmoScale(center);

						int nextI = (i + 1) % 3;
						auto planeNormal = gizmos[(int)_manipulatorType].shape[nextI].normal;

						const Plane plane { center, planeNormal };
						auto hit = Intersection::hit(now, plane);

						auto start = center;
						auto end = center + normal * radius;

						auto distance = Intersection::distanceToSegment(start, end, hit);

						float tolerance = 0.05f * radius;

						if(distance < tolerance)
						{
							_axisIndex = i;
							break;
						}
					}
				}
				else
				{
					auto pos = id->pos();
					auto center = gizmos[(int)_manipulatorType].shape[_axisIndex].center + pos;
					auto normal = gizmos[(int)_manipulatorType].shape[_axisIndex].normal;
					auto radius = gizmos[(int)_manipulatorType].shape[_axisIndex].radius * gizmoScale(center);

					int nextI = (_axisIndex + 1) % 3;
					auto planeNormal = gizmos[(int)_manipulatorType].shape[nextI].normal;
					const Plane plane { center, planeNormal };

					auto hit = Intersection::hit(now, plane);
					auto oldHit = Intersection::hit(before, plane);

					if(!Math::closeEnough(oldHit, hit))
					{
						auto shift = glm::dot(hit - oldHit, normal);

						auto translation = normal * shift;
						id->translate(translation);
					}
				}
			}
			else if(_manipulatorType == ManipulatorType::Scale)
			{
				if(_axisIndex < 0)
				{
					for (int i = 0; i < 3; ++i)
					{
						auto pos = id->pos();
						auto center = gizmos[(int)_manipulatorType].shape[i].center + pos;
						auto normal = gizmos[(int)_manipulatorType].shape[i].normal;
						auto radius = gizmos[(int)_manipulatorType].shape[i].radius * gizmoScale(center);

						int nextI = (i + 1) % 3;
						auto planeNormal = gizmos[(int)_manipulatorType].shape[nextI].normal;

						const Plane plane { center, planeNormal };
						auto hit = Intersection::hit(now, plane);

						auto start = center;
						auto end = center + normal * radius;

						auto distance = Intersection::distanceToSegment(start, end, hit);

						float tolerance = 0.05f * radius;

						if(distance < tolerance)
						{
							_axisIndex = i;
							break;
						}
					}
				}
				else
				{
					auto pos = id->pos();
					auto center = gizmos[(int)_manipulatorType].shape[_axisIndex].center + pos;
					auto normal = gizmos[(int)_manipulatorType].shape[_axisIndex].normal;
					auto radius = gizmos[(int)_manipulatorType].shape[_axisIndex].radius * gizmoScale(center);

					int nextI = (_axisIndex + 1) % 3;
					auto planeNormal = gizmos[(int)_manipulatorType].shape[nextI].normal;
					const Plane plane { center, planeNormal };

					auto hit = Intersection::hit(now, plane);
					auto oldHit = Intersection::hit(before, plane);

					if(!Math::closeEnough(oldHit, hit))
					{
						auto scale = id->scale();

						float d = glm::dot(hit - center, normal);
						float oldD = glm::dot(oldHit - center, normal);

						scale[_axisIndex] += (d - oldD) / radius;

						id->scale(scale);
					}
				}
			}
		}
	}
	else
	{
		_axisIndex = -1;
	}
}

