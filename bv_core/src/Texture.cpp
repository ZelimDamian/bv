#include "Texture.h"
#include <RenderingSystem.h>
#include "Shader.h"

using namespace bv;
using namespace bv::rendering;

//std::vector<Texture> _textures;
std::vector<char*> _textureData;
std::vector<glm::uvec3> _sizes;
std::vector<TextureType> _textureTypes;

auto renderer = RenderingSystem::instance();

void Texture::use(Texture::Id textureId, Uniform::Id uniformId, uint8_t texUnit)
{
    renderer->bindTexture(textureId, uniformId, texUnit);
}

void Texture::use(Texture::Id textureId, const std::string &name, uint8_t texUnit)
{
    Uniform::Id uniformId = Shader::current().ref().uniform(name, UniformType::Int);
    renderer->bindTexture(textureId, uniformId, texUnit);
}

Texture::Id Texture::create2D(uint16_t width, uint16_t height, TextureType type)
{
//    char* data = new char[width * height * (int)type];
//    _textureData.push_back(data);

    Texture::Id id = renderer->createTexture2D(type, width, height);

    _sizes.emplace_back(width, height, 1);
    _textureTypes.emplace_back(type);

    return id;
}

Texture::Id Texture::create3D(uint16_t width, uint16_t height, uint16_t depth, TextureType type)
{
//    char* data = new char[width * height * (int)type];
//    _textureData.push_back(data);

    Texture::Id id = renderer->createTexture3D(type, width, height, depth);

    _sizes.emplace_back(width, height, depth);
    _textureTypes.emplace_back(type);

    return id;
}

void Texture::update(Texture::Id textureId, const void *data)
{
	if (_sizes[(int)textureId].z == 1)
    {
        update2D(textureId, data);
    }
    else
    {
        update3D(textureId, data);
    }
}

void Texture::update2D(Texture::Id id, const void* data)
{
    renderer->updateTexture2D(id, data);
}

void Texture::update3D(Texture::Id id, const void* data)
{
    renderer->updateTexture3D(id, data);
}

glm::uvec3 Texture::size(Texture::Id textureId)
{
    return _sizes[(int)textureId];
}

size_t Texture::sizeInBytes(Texture::Id textureId)
{
    // dimensions of the texture
    glm::uvec3 dims = size(textureId);

    // bits per pixel
    uint16_t bpp = (uint16_t)_textureTypes[(int)textureId];

    return dims.x * dims.y * dims.z * bpp;
}

