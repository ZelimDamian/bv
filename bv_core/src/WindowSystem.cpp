#include "WindowSystem.h"
#include "RenderingSystem.h"

using namespace bv;

void WindowSystem::state(int width, int height)
{
	
	if(m_width != width || m_height != height)
	{
		m_width = width;
		m_height = height;

		RenderingSystem::instance()->setViewSize(0, 0, width, height);
		m_onreseized();
	}
}
