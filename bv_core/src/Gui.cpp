#include "Gui.h"

#include "WindowSystem.h"
#include "CameraSystem.h"
#include "KeyboardSystem.h"

using namespace bv;
using namespace gui;

Gui::Gui() : m_menu("menu")
{
    m_windows["main"] = PanelElementPtr(new PanelElement(nullptr, "Main"));
    m_windows["side"] = PanelElementPtr(new PanelElement(nullptr, "Properties"));

    auto file_menu = m_menu.item("File");
    {
        file_menu->item("Quit", { Key::LCTRL, Key::Q },
                        [] { WindowSystem::instance()->exit(); });
    }

	auto view_menu = m_menu.item("View");
    {
	    view_menu->item("Look at selection", { Key::LSHIFT, Key::F },
	                    [] { CameraSystem::instance()->lookAtSelection(); });

        auto camera_menu = view_menu->item("Camera");
        camera_menu->item("Top", [] { CameraSystem::instance()->set(Camera::View::Top); });
        camera_menu->item("Bottom", [] { CameraSystem::instance()->set(Camera::View::Bottom); });
        camera_menu->item("Left", [] { CameraSystem::instance()->set(Camera::View::Left); });
        camera_menu->item("Right", [] { CameraSystem::instance()->set(Camera::View::Right); });
        camera_menu->item("Front", [] { CameraSystem::instance()->set(Camera::View::Front); });
        camera_menu->item("Back", [] { CameraSystem::instance()->set(Camera::View::Back); });
    }

	updateWindowsList();
}

PanelElement* Gui::window(const std::string &name)
{
    auto window = PanelElementPtr(new PanelElement(nullptr, name));
    m_windows[name] = window;

	updateWindowsList();

    return window.get();
}

void Gui::remove(PanelElement *window)
{
    auto found = std::find_if(m_windows.begin(), m_windows.end(), [window, this] (const WindowMap::value_type& pair)
    {
        return pair.second.get() == window;
    });

	window->visible(false);
    m_windows.erase(found);
}

void Gui::updateWindowsList()
{
	auto window_menu = m_menu.find("Windows");

	if(!window_menu)
	{
		window_menu = m_menu.item("Windows");
	}

	window_menu->clear();

	for (auto pair : m_windows)
	{
		window_menu->item(pair.first.c_str(), [=]
		{
			pair.second->open();
			m_windows[pair.first] = pair.second;
		});
	}
}