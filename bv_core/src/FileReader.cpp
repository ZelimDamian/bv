#include "FileReader.h"

#include <fstream>
#include <sstream>

#include <nfd.h>

using namespace bv;

const std::string FileReader::ROOT_DIR = "../content/";

std::string FileReader::read(const std::string &filename)
{
	return read_absolute(ROOT_DIR + filename);
}

std::string FileReader::read_absolute(const std::string &path)
{
	std::ifstream file(path);

	if(!file.is_open())
	{
		std::string message = "File " + path + " cannot be opened";
		Log::error(message);
		return "";
	}

	std::stringstream sstr;
	sstr << file.rdbuf();
	return sstr.str();
}

std::string FileReader::pick(const char* filter /*"png,jpg;pdf"*/)
{
    nfdchar_t *outPath = NULL;
    nfdresult_t res = NFD_OpenDialog( filter, NULL, &outPath );

    std::string result;

    if (res == NFD_OKAY)
    {
        result = std::string(outPath);
        free(outPath);
    }
    else if ( res == NFD_CANCEL )
    {
        return "";
    }
    else
    {
        Log::error("Error picking file: %s", NFD_GetError());
    }

    return result;
}

std::string FileReader::save(const char* name, const char* filter /*"png,jpg;pdf"*/)
{
    nfdchar_t *outPath = NULL;
    nfdresult_t res = NFD_SaveDialog( filter, name, &outPath );

    std::string result;

    if (res == NFD_OKAY)
    {
        result = std::string(outPath);
        free(outPath);
    }
    else if ( res == NFD_CANCEL )
    {
        return "";
    }
    else
    {
        Log::error("Error picking save file: %s", NFD_GetError());
    }

    return result;
}

std::vector<std::string> FileReader::multipick(const char* filter /*"png,jpg;pdf"*/)
{
	nfdpathset_t outPath;
	nfdresult_t res = NFD_OpenDialogMultiple( filter, NULL, &outPath );

	std::vector<std::string> results;

	if (res == NFD_OKAY)
	{
		for (int i = 0; i < NFD_PathSet_GetCount(&outPath); ++i)
		{
			auto path = NFD_PathSet_GetPath(&outPath, i);

			results.push_back(std::string(path));
		}
	}
	else if ( res == NFD_CANCEL )
	{
		return {};
	}
	else
	{
		Log::error("Error picking file: %s", NFD_GetError());
	}

	return results;
}

std::string FileReader::open(const char* filter /*"png,jpg;pdf"*/)
{
    return read_absolute(pick(filter));
}
