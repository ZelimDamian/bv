#include "RenderingSystem.h"

#include "Gui.h"
#include "RenderContext.h"
#include "RenderMesh.h"
#include "Shader.h"

using namespace bv;
using namespace bv::rendering;
using namespace bv::gui;

static RenderSubmission _pendingSubmission;

// subscribe the caller to the render event
void RenderingSystem::onrender(Action func)
{
	RenderContext::get(0)->onrender(func);
}

// subscribe the caller to the render event that is cleared after every frame
void RenderingSystem::onrender_once(Action func)
{
	RenderContext::get(0)->onrenderOnce(func);
}

//void RenderingSystem::render(Mesh::Id meshId, const RenderState& state)
//{
//	_pendingSubmission.state = state;
//	render(RenderMesh::create(meshId));
//}

// method
void RenderingSystem::submit(RenderMesh::Id handle)
{
	_pendingSubmission.mesh = handle;
	_pendingSubmission.shader = Shader::current();
	m_submissions.push_back(_pendingSubmission);
	_pendingSubmission.uniforms.clear();
	_pendingSubmission.xform = glm::mat4();
}

void RenderingSystem::reset(Culling culling, RenderMode renderMode, DepthTest depthTest)
{
	_pendingSubmission.state = RenderState { culling, renderMode, depthTest };

	if(depthTest == DepthTest::ALWAYS)
	{
		_pendingSubmission.priority = -1;
	}
	else
	{
		_pendingSubmission.priority = 0;
	}
}

void RenderingSystem::transform(const glm::mat4 &xform)
{
	_pendingSubmission.xform = xform;
}

void RenderingSystem::set(rendering::Uniform::Id uniform, const UniformData& value)
{
	_pendingSubmission.uniforms[uniform] = value;
}

void RenderingSystem::applyPending()
{
	apply(_pendingSubmission);
	_pendingSubmission = {};
}
