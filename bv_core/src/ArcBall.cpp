#include "Arcball.h"

using namespace bv;

Arcball::Arcball(glm::ivec2 center, float radius)
{
    m_center = glm::vec3(center.x, center.y, 0.0f);
    m_radius = radius;
}

Arcball::Arcball(glm::vec2 center, float radius)
{
    m_center = glm::vec3(center.x, center.y, 0.0f);
    m_radius = radius;
}

Arcball::Arcball(glm::vec3 center, float radius)
{
    m_center = center;
    m_radius = radius;
}

void Arcball::click(glm::ivec2 point) const
{
    m_start = mapToSphere(point);
}

glm::quat Arcball::unclick(glm::ivec2 point) const
{
    m_end = mapToSphere(point);

    const glm::quat quaternion = glm::angleAxis(glm::dot(m_start, m_end), glm::cross(m_start, m_end));
    return glm::normalize(quaternion);
}

glm::vec3 Arcball::mapToSphere(glm::vec2 point) const
{
    glm::vec3 result;

    result.x = (point.x - m_center.x) / (m_radius * 2.0f);
    result.y = (point.y - m_center.y) / (m_radius * 2.0f);

    float mag = glm::length2(result);

    if (mag > 1.0f)
    {
        result *= 1.0f / glm::sqrt(mag);
        result.z = 0.0f;
    }
    else
    {
        result.z = glm::sqrt(1.0f - mag);
    }

//    result = glm::normalize(result);

    return result;
}