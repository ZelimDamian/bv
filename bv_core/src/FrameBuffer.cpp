#include "FrameBuffer.h"

#include "RenderingSystem.h"

using namespace bv;
using namespace rendering;

FrameBuffer::FrameBuffer(Id id, glm::ivec2 size): StorableBase(id), m_size(size)
{
    RenderingSystem::instance()->onload([this]
    {
        RenderingSystem::instance()->createFrameBuffer(m_size);
    });
}

void FrameBuffer::bind()
{
    RenderingSystem::instance()->bindFrameBuffer(m_id);
}
