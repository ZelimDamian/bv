#include "MeshFactory.h"

#include <Box.h>
#include <MeshUtilities.h>

using namespace bv;

Mesh::Id MeshFactory::quad(float width, float height)
{
    Mesh::Id id = Mesh::create();

    quad(width, height, id);

    return id;
}

void MeshFactory::quad(float width, float height, Mesh::Id id)
{
    const float halfw = width / 2.0f;
    const float halfh = height / 2.0f;

    auto mesh = Mesh::get(id);
//    mesh.vertex (glm::vec3(-halfw, -halfh, 0.0f));
//    mesh.vertex (glm::vec3( halfw, -halfh, 0.0f));
//    mesh.vertex (glm::vec3( halfw,  halfh, 0.0f));

    mesh->vertex(glm::vec3(-halfw, -halfh, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)/*, glm::vec2(0.0f, 0.0f)*/);
    mesh->vertex(glm::vec3( halfw, -halfh, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)/*, glm::vec2(1.0f, 0.0f)*/);
    mesh->vertex(glm::vec3( halfw,  halfh, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)/*, glm::vec2(1.0f, 1.0f)*/);

    mesh->tri(0, 1, 2);

    mesh->vertex(glm::vec3(-halfw, -halfh, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)/*, glm::vec2(0.0f, 0.0f)*/);
    mesh->vertex(glm::vec3( halfw,  halfh, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)/*, glm::vec2(1.0f, 1.0f)*/);
    mesh->vertex(glm::vec3(-halfw,  halfh, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)/*, glm::vec2(0.0f, 1.0f)*/);

    mesh->tri(3, 4, 5);

//    mesh.updateIndices();
}

Mesh::Id MeshFactory::cube(float width, float height, float depth)
{
    Mesh::Id id = Mesh::create();

    cube(width, height, depth, id);

    return id;
}

void MeshFactory::cube(float width, float height, float depth, Mesh::Id id)
{
    auto mesh = Mesh::get(id);

    float halfw = width / 2.0f;
    float halfh = height / 2.0f;
    float halfd = depth / 2.0f;

//        glm::vec3 {-0.5f,  0.5f,  0.5f },
//        glm::vec3 { 0.5f,  0.5f,  0.5f },
//        glm::vec3 {-0.5f, -0.5f,  0.5f },
//        glm::vec3 { 0.5f, -0.5f,  0.5f },
//        glm::vec3 {-0.5f,  0.5f, -0.5f },
//        glm::vec3 { 0.5f,  0.5f, -0.5f },
//        glm::vec3 {-0.5f, -0.5f, -0.5f },
//        glm::vec3 { 0.5f, -0.5f, -0.5f }

        mesh->vertex(glm::vec3 { -halfw,  halfh,  halfd });
        mesh->vertex(glm::vec3 {  halfw,  halfh,  halfd });
        mesh->vertex(glm::vec3 { -halfw, -halfh,  halfd });
        mesh->vertex(glm::vec3 {  halfw, -halfh,  halfd });
        mesh->vertex(glm::vec3 { -halfw,  halfh, -halfd });
        mesh->vertex(glm::vec3 {  halfw,  halfh, -halfd });
        mesh->vertex(glm::vec3 { -halfw, -halfh, -halfd });
        mesh->vertex(glm::vec3 {  halfw, -halfh, -halfd });

//    mesh.indices =
//	{
//			0, 1, 2, // 0
//			1, 3, 2,
//			4, 6, 5, // 2
//			5, 6, 7,
//			0, 2, 4, // 4
//			4, 2, 6,
//			1, 5, 3, // 6
//			5, 7, 3,
//			0, 4, 1, // 8
//			4, 5, 1,
//			2, 3, 6, // 10
//			6, 3, 7
//	};
//    mesh.indices =
    {
        mesh->tri(0, 1, 2);// 0
        mesh->tri(1, 3, 2);
        mesh->tri(4, 6, 5);// 2
        mesh->tri(5, 6, 7);
        mesh->tri(0, 2, 4);// 4
        mesh->tri(4, 2, 6);
        mesh->tri(1, 5, 3);// 6
        mesh->tri(5, 7, 3);
        mesh->tri(0, 4, 1);// 8
        mesh->tri(4, 5, 1);
        mesh->tri(2, 3, 6);// 10
        mesh->tri(6, 3, 7);
    };

    mesh->updateIndices();
}

Mesh::Id MeshFactory::shape(const Box &box)
{
    Mesh::Id id = Mesh::create();
    MeshFactory::shape(box, id);
    return id;
}

void MeshFactory::shape(const Box& box, Mesh::Id id)
{
    auto mesh = Mesh::get(id);
    mesh->clear();

    const glm::vec3 halfsize = box.halfsize();

    float halfw = halfsize.x;
    float halfh = halfsize.y;
    float halfd = halfsize.z;

	const glm::vec3 center = box.center();

    mesh->vertex(glm::vec3 { -halfw,  halfh,  halfd } + center);
    mesh->vertex(glm::vec3 {  halfw,  halfh,  halfd } + center);
    mesh->vertex(glm::vec3 { -halfw, -halfh,  halfd } + center);
    mesh->vertex(glm::vec3 {  halfw, -halfh,  halfd } + center);
    mesh->vertex(glm::vec3 { -halfw,  halfh, -halfd } + center);
    mesh->vertex(glm::vec3 {  halfw,  halfh, -halfd } + center);
    mesh->vertex(glm::vec3 { -halfw, -halfh, -halfd } + center);
    mesh->vertex(glm::vec3 {  halfw, -halfh, -halfd } + center);

    {
        mesh->tri(0, 1, 2);// 0
        mesh->tri(1, 3, 2);
        mesh->tri(4, 6, 5);// 2
        mesh->tri(5, 6, 7);
        mesh->tri(0, 2, 4);// 4
        mesh->tri(4, 2, 6);
        mesh->tri(1, 5, 3);// 6
        mesh->tri(5, 7, 3);
        mesh->tri(0, 4, 1);// 8
        mesh->tri(4, 5, 1);
        mesh->tri(2, 3, 6);// 10
        mesh->tri(6, 3, 7);
    };

    mesh->updateIndices();
}

Mesh::Id MeshFactory::shape(const Plane &plane, float size, int subdiv)
{
	auto mesh = Mesh::create();

	MeshFactory::shape(plane, size, subdiv, mesh);

	return mesh;
}

void MeshFactory::shape(const Plane &plane, float size, int subdiv, Mesh::Id mesh)
{
	mesh->clear();

	auto tangents = plane.tangents();

	auto width = tangents[0] * size;
	auto height = tangents[1] * size;

	float substep = 1.0f / subdiv;

	auto subwidth = width * substep;
	auto subheight = height * substep;

	auto leftbottom = plane.pos() - (width + height) / 2.0f;

	for (float i = 0; i < subdiv + 1; ++i)
	{
		mesh->vertex(leftbottom + width * 0.0f    + subheight * i   );
		mesh->vertex(leftbottom + width * 1.0f    + subheight * i   );
		mesh->vertex(leftbottom + subwidth * i    + height * 0.0f);
		mesh->vertex(leftbottom + subwidth * i    + height * 1.0f);
	}

//	mesh->updateIndices();
}

void MeshFactory::shape(const TriVerts &tri, Mesh::Id mesh)
{
	mesh->clear();

	mesh->vertex(tri[0]);
	mesh->vertex(tri[1]);
	mesh->vertex(tri[2]);
}

void MeshFactory::shape(const Cylinder &shape, int subdiv, Mesh::Id mesh)
{
	for (int i = 0; i < subdiv + 1; ++i)
	{
		float x = glm::cos( 2.0f * (float) i / subdiv * glm::pi<float>()) * shape.radius;
		float z = glm::sin( 2.0f * (float) i / subdiv * glm::pi<float>()) * shape.radius;

		mesh->vertex(glm::vec3{ x, 0, z});
		mesh->vertex(glm::vec3{ x, shape.height, z});
	}
}

Mesh::Id MeshFactory::shape(const Cylinder &shape, int subdiv)
{
	auto mesh = Mesh::create();

	MeshFactory::shape(shape, subdiv, mesh);

	return mesh;
}

Mesh::Id  MeshFactory::extrude(const VectorVec3& contour, const VectorVec3& path)
{
	auto mesh = Mesh::create();

	extrude(contour, path, mesh);

	return mesh;
}

void  MeshFactory::extrude(const VectorVec3& contour, const VectorVec3& path, Mesh::Id mesh)
{
	mesh->clear();

	if(path.size() < 2) return;

	for (int i = 0; i < path.size() - 1; i++)
	{
		auto a = path[i + 0];
		auto b = path[i + 1];

		glm::vec3 dir = glm::normalize(b - a);

		auto orientation = Math::quatBetween(Math::back(), dir);

		for (int j = 0; j < contour.size(); ++j)
		{
			auto point = contour[j];

			mesh->vertex(orientation * point + a);
			mesh->vertex(orientation * point + b);
		}
	}

	MeshUtilities::triStripToTriList(mesh);

	mesh->updatePositions();
	mesh->setupConnections();
	mesh->updateAllNormals();
}
