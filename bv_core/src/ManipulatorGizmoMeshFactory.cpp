#include "ManipulatorGizmoMeshFactory.h"

#include "MeshFactory.h"

using namespace bv;

static glm::vec4 rgb[3]{ glm::vec4(1.0f, 0.0f, 0.0f, 1.0f),
						 glm::vec4 (0.0f, 1.0f, 0.0f, 1.0f),
						 glm::vec4 (0.0f, 0.0f, 1.0f, 1.0f) };

Gizmo ManipulatorGizmoMeshFactory::translation()
{
//	return MeshFactory::shape(Cylinder { 0.001f, 0.1f });

	auto mesh = Mesh::create();

	Gizmo gizmo { mesh, rendering::RenderMesh::Id::invalid(), Gizmo::Type::Translation};

	glm::vec3 normals[3] { glm::vec3(1.0f, 0.0f, 0.0f),
	                       glm::vec3(0.0f, 1.0f, 0.0f),
	                       glm::vec3(0.0f, 0.0f, 1.0f) };

	for (int i = 0; i < 3; ++i)
	{
		gizmo.shape[i].center = glm::vec3();
		gizmo.shape[i].normal = normals[i];
		gizmo.shape[i].radius = 1.0f;

		auto center = gizmo.shape[i].center;
		auto normal = gizmo.shape[i].normal;
		auto radius = gizmo.shape[i].radius;

		mesh->vertex(center, glm::vec3(), rgb[i]);

		auto tip = center + normal * radius;
		mesh->vertex(tip, glm::vec3(), rgb[i]);

		float perpSize = radius * 0.05f;

		{
			int nextI = (i + 1) % 3;
			auto perpNormal = normals[nextI];

			mesh->vertex(tip + perpNormal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip + normal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip + normal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip - perpNormal * perpSize, glm::vec3(), rgb[i]);
		}

		{
			int nextI = ( i + 2 ) % 3;
			auto perpNormal = normals[nextI];

			mesh->vertex(tip + perpNormal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip + normal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip + normal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip - perpNormal * perpSize, glm::vec3(), rgb[i]);
		}
	}

	gizmo.renderMesh = rendering::RenderMesh::create(mesh);

	return gizmo;
}

Gizmo ManipulatorGizmoMeshFactory::rotation()
{
	auto mesh = Mesh::create();

	Gizmo gizmo { mesh, rendering::RenderMesh::Id::invalid(), Gizmo::Type::Rotation };

	gizmo.shape[0].normal = glm::vec3(1.0f, 0.0f, 0.0f);
	gizmo.shape[0].radius = 1.0f;

	gizmo.shape[1].normal = glm::vec3(0.0f, 1.0f, 0.0f);
	gizmo.shape[1].radius = 1.0f;

	gizmo.shape[2].normal = glm::vec3(0.0f, 0.0f, 1.0f);
	gizmo.shape[2].radius = 1.0f;

	for (int i = 0; i < 3; ++i)
	{
		static const int subdiv = 48;
		
		float radius = gizmo.shape[i].radius;
		auto center = gizmo.shape[i].center;
		auto normal = gizmo.shape[i].normal;

		glm::vec3 up (0.0f, 1.0f, 0.0f);

		auto orientation = Math::quatBetween(up, normal);

		for (int j = 0; j < subdiv; ++j)
		{
			float x = glm::cos( 2.0f * (float) j / subdiv * glm::pi<float>()) * radius;
			float z = glm::sin( 2.0f * (float) j / subdiv * glm::pi<float>()) * radius;

			auto point = glm::vec3{ x, 0, z };

			mesh->vertex(orientation * point, glm::vec3(), rgb[i]);
		}
	}

	gizmo.renderMesh = rendering::RenderMesh::create(mesh);

	return gizmo;
}

Gizmo ManipulatorGizmoMeshFactory::scaling()
{
	auto mesh = Mesh::create();

	Gizmo gizmo { mesh, rendering::RenderMesh::Id::invalid(), Gizmo::Type::Translation};

	glm::vec3 normals[3] { glm::vec3(1.0f, 0.0f, 0.0f),
	                       glm::vec3(0.0f, 1.0f, 0.0f),
	                       glm::vec3(0.0f, 0.0f, 1.0f) };

	for (int i = 0; i < 3; ++i)
	{
		gizmo.shape[i].center = glm::vec3();
		gizmo.shape[i].normal = normals[i];
		gizmo.shape[i].radius = 1.0f;

		auto center = gizmo.shape[i].center;
		auto normal = gizmo.shape[i].normal;
		auto radius = gizmo.shape[i].radius;

		mesh->vertex(center, glm::vec3(), rgb[i]);

		auto tip = center + normal * radius;
		mesh->vertex(tip, glm::vec3(), rgb[i]);

		float perpSize = radius * 0.05f;

		{
			int nextI = (i + 1) % 3;
			auto perpNormal = normals[nextI];

			mesh->vertex(tip + perpNormal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip - perpNormal * perpSize, glm::vec3(), rgb[i]);
		}

		{
			int nextI = ( i + 2 ) % 3;
			auto perpNormal = normals[nextI];

			mesh->vertex(tip + perpNormal * perpSize, glm::vec3(), rgb[i]);
			mesh->vertex(tip - perpNormal * perpSize, glm::vec3(), rgb[i]);
		}
	}

	gizmo.renderMesh = rendering::RenderMesh::create(mesh);

	return gizmo;
}
