#include "Profiler.h"

#include "Gui.h"

using namespace bv;

void Profiler::init()
{
    auto show = [this]
    {
        auto dialog = gui::Gui::instance()->window("Profiler");

        for (auto& profile : m_profiles)
        {
            dialog->label(profile.first,
                          [&profile] { return std::to_string(profile.second.average()); });
        }

        auto settings = dialog->panel("Settings");
        settings->bind<int>("Average over",
                             [this] { return TICKS; },
                             [this] (int num) { TICKS = num; },
                             1, 1000);
    };

    auto menu = gui::Gui::instance()->menu()->item("Profiler");
    menu->item("Show", show);
}
