#include "CameraSystem.h"

#include "MouseSystem.h"
#include "KeyboardSystem.h"
#include "RenderingSystem.h"
#include "WindowSystem.h"
#include "Intersection.h"
#include "SelectionSystem.h"

#include "ArcBall.h"
#include "Bounding.h"

using namespace bv;

glm::vec2 pitchBounds { -89.0f * M_PI / 180.0, 89.0f * M_PI / 180.0};
float s_rotSpeed { 0.016f };

// DOD storages
std::vector<float> _distances;
std::vector<float> _nears;
std::vector<float> _fars;
std::vector<float> _aspects;
std::vector<float> _fovs;

std::vector<glm::vec3> _eyes;
std::vector<glm::vec3> _targets;

std::vector<glm::vec4> _viewports;

std::vector<glm::quat> _orientations;
std::vector<glm::vec3> _angles;

std::vector<glm::mat4> _projs;
std::vector<glm::mat4> _views;


Camera::Camera(Id id, const glm::vec3 &eye, const glm::vec3 &target, float distance, float near, float far): StorableBase(id)
{
	// create default values for all types
	_distances.emplace_back(distance);
	_nears.emplace_back(near);
	_fars.emplace_back(far);
	_eyes.emplace_back(eye);
	_angles.emplace_back();
	_targets.emplace_back(target);
	_orientations.emplace_back();

// create storage for the view and projection matrices
	_projs.emplace_back();
	_views.emplace_back();

	// use the values to generate the projection
	int width = WindowSystem::instance()->width();
	int height = WindowSystem::instance()->height();

	_viewports.emplace_back(0, 0, width, height);

	_aspects.push_back((float) width / height);
	_fovs.push_back(45.0f);
}

glm::vec3 Camera::eye() const
{
	return _eyes[(int)m_id];
}

void Camera::eye(glm::vec3 eye)
{
	_eyes[(int)m_id] = eye;
}

glm::vec3 Camera::target() const
{
	return _targets[(int)m_id];
}

void Camera::target(glm::vec3 target)
{
	_targets[(int)m_id] = target;
}

float Camera::distance() const
{
	return _distances[(int)m_id];
}

void Camera::distance(float val)
{
	_distances[(int)m_id] = val;
}

float Camera::fov() const
{
    return _fovs[(int)m_id];
}

void Camera::fov(float fov) const
{
	_fovs[(int)m_id] = fov;
}

void Camera::aspect(float aspect)
{
	_aspects[(int)m_id] = aspect;
}

glm::vec4 Camera::viewport() const
{
	return _viewports[(int)m_id];
}

void Camera::viewport(const glm::vec4& viewport)
{
	_aspects[(int)m_id] = viewport.z / viewport.w;
	_viewports[(int)m_id] = viewport;
}

glm::quat Camera::orientation()
{
    return _orientations[(int)m_id];
}

const glm::mat4& Camera::proj()
{
	return _projs[(int)m_id];
}

const glm::mat4& Camera::view()
{
	return _views[(int)m_id];
}

void CameraSystem::update()
{
	auto mouse = MouseSystem::instance();

    int deltaX = mouse->deltaX();
    int deltaY = mouse->deltaY();
    int deltaZ = mouse->deltaZ();

    for (int i = 0; i < _targets.size(); ++i)
    {
        float distance = _distances[i];
        float near = _nears[i];
        float far = _fars[i];

        // store the Euler angles of the camera
        glm::vec3& angles = _angles[i];

        glm::quat rot = glm::angleAxis(angles.x, glm::vec3(0.0f, 1.0f, 0.0f))
                        * glm::angleAxis(angles.y, glm::vec3(1.0f, 0.0f, 0.0f));

        glm::vec3 target = _targets[i];
        glm::vec3 eye = _eyes[i] = target + rot * glm::vec3(0.0f, 0.0f, distance);

	    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

	    auto targetToEye = glm::normalize(eye - target);

	    if(glm::abs(glm::dot(targetToEye, up)) > 0.99999f)
	    {
		    up = glm::vec3(0.0f, 0.0f, -1.0f);
	    }

        // use the calcualted values to generate the view matrix
        _views[i] = glm::lookAt(eye, target, up);

        _projs[i] = glm::perspective(_fovs[i], _aspects[i], near, far);
    }


	if (!!m_previousId)
	{
        if(!!m_currentId && deltaZ != 0)
		{
			float distance = _distances[(int)m_previousId];
			float near = _nears[(int)m_previousId];
			float far = _fars[(int)m_previousId];
			// zoom into or out of the screen (quadratically)
			distance -= distance * deltaZ * s_rotSpeed;
			// clamp the distance
			_distances[(int)m_previousId] = std::max(std::min(distance, far), near);
		}

        bool shiftPressed = KeyboardSystem::instance()->key(Key::LSHIFT);
        bool mouseDown = mouse->button(MouseButton::Middle);

        if(mouseDown)
        {
            if(shiftPressed)
            {
                Ray now = ray(mouse->pos());

                Ray before = ray(mouse->posOld());

                Plane plane{ _targets[(int)m_previousId], -now.dir() };

                glm::vec3 distance = - Intersection::hitShift(now, before, plane);

                _eyes[(int)m_previousId] += distance;
                _targets[(int)m_previousId] += distance;
            }
            else
            {
                // store the Euler angles of the camera
                glm::vec3& angles = _angles[(int)m_previousId];

                // update the pitch of the current camera
                angles.y += deltaY * s_rotSpeed;

                // the vertical pitch needs to be clamped
                angles.y = std::max(std::min(angles.y, pitchBounds.y), pitchBounds.x);

                // update the yaw
                angles.x -= deltaX * s_rotSpeed;
            }
        }
	}

	if (!!m_currentId)
	{
		m_previousId = m_currentId;
		m_currentId = Camera::Id::invalid();
	}
}

void CameraSystem::setup()
{
	RenderingSystem::instance()->setView(_views[(int)m_currentId], _projs[(int)m_currentId]);
}

Ray CameraSystem::ray(const glm::ivec2& pos)
{
	if (m_currentId)
	{
		const glm::mat4& view = _views[(int)m_currentId];
		const glm::mat4& proj = _projs[(int)m_currentId];

		auto viewport = _viewports[(int)m_currentId];
		viewport.y = WindowSystem::instance()->height() - (viewport.w + viewport.y);

		// local mouse position in viewport
		auto localize = [=](const glm::vec2& pos)
		{
			auto window = glm::vec2{ WindowSystem::instance()->width(),
				WindowSystem::instance()->height() };

			return glm::vec2{ (pos.x - viewport.x) * window.x / viewport.z,
				(pos.y - viewport.y) * window.y / viewport.w };
		};

		auto screen = pos;

		glm::vec3 start = glm::unProject(glm::vec3(screen, 0.0f), view, proj, viewport);
		glm::vec3 end = glm::unProject(glm::vec3(screen, 1.0f), view, proj, viewport);

		glm::vec3 dir = glm::normalize(end - start);

		return { start, dir };
	}
	else
	{
		return Ray { glm::vec3{ 0.0f }, glm::vec3{ 1.0f } };
	}
}

void CameraSystem::lookAtSelection()
{
	auto camera = current();
	glm::vec3& angles = _angles[(int)m_previousId];

	auto selection = SelectionSystem::instance()->selection();

	if(selection.size() > 0)
	{
		auto box = SelectionSystem::instance()->box();
		lookAt(box.center(), box.halfsize());
	}
	else
	{
		auto box = Bounding::box(Entity::handles());
		lookAt(box.center(), box.halfsize());
	}
}

void CameraSystem::lookAt(const glm::vec3 &center, const glm::vec3 &halfextent)
{
	m_previousId->target(center);

	float fov = glm::radians(m_previousId->fov());
	float halfsize = glm::length(halfextent);
	float distance = 2.0f * halfsize / glm::tan(fov);

	float far = distance + halfsize * 15.0f;

	_distances[(int)m_previousId] = distance;
	_fars[(int)m_previousId] = far;
	_nears[(int)m_previousId] = far / 500.0f;
}

void CameraSystem::set(Camera::View view)
{
	glm::vec3& angles = _angles[(int)m_previousId];

	switch (view)
	{
		case Camera::View::Top:
		{
			angles = glm::vec3(0.0f, -glm::pi<float>() / 2.0f, 0.0f);
			break;
		}
		case Camera::View::Bottom:
		{
			angles = glm::vec3(0.0f, glm::pi<float>() / 2.0f, 0.0f);
			break;
		}
		case Camera::View::Left:
		{
			angles = glm::vec3(-glm::pi<float>() / 2.0f, 0.0f, 0.0f);
			break;
		}
		case Camera::View::Right:
		{
			angles = glm::vec3(glm::pi<float>() / 2.0f, 0.0f, 0.0f);
			break;
		}
		case Camera::View::Front:
		{
			angles = glm::vec3(0.0f, 0.0f, 0.0f);
			break;
		}
		case Camera::View::Back:
		{
			angles = glm::vec3(glm::pi<float>(), 0.0f, 0.0f);
			break;
		}
		default: {}
	}
}