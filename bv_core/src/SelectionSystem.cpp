#include "SelectionSystem.h"

#include <KeyboardSystem.h>
#include <Bounding.h>

#include "Gui.h"
#include "Entity.h"

#include "MouseSystem.h"

#include "ListElement.h"

using namespace bv;
using namespace bv::gui;

// data
std::vector<Entity::Id> entities;

static PanelElement* _panel = nullptr;
static ListElement* _list = nullptr;

void SelectionSystem::init()
{
    Entity::onListChange([&]
    {
        updateEntityList();
	});
}

void SelectionSystem::update()
{
}

std::vector<Entity::Id> SelectionSystem::selection()
{
	return entities;
}

void SelectionSystem::set(Entity::Id entity)
{
	entities.clear();
	add(entity);
}

void SelectionSystem::add(Entity::Id entity)
{
	entities.push_back(entity);
	onselect();
}

void SelectionSystem::onselect()
{
//    updateEntityList();
    if(!selection().empty())
    {
        _list->select((int)entities[0]);
    }
    m_onselect();
}

void SelectionSystem::clear()
{
    entities.clear();
	_list->select(-1);
    onselect();
}

void SelectionSystem::updateEntityList()
{
    if(_panel)
    {
        Gui::instance()->sidePanel()->remove(_panel);
    }

    _panel = Gui::instance()->sidePanel()->panel("Entities");

    std::vector<std::string> entityNames = Entity::keys();

    auto onselect = [this](int selected)
    {
		entities.clear();

		if (selected >= 0)
		{
			add(Entity::Id(selected));
		}
    };

    _list = static_cast<ListElement*>(_panel->list("Entities", entityNames, onselect));
}

glm::vec3 SelectionSystem::center()
{
	if(selection().size() > 0)
	{
		return box().center();
	}
	else
	{
		return glm::vec3();
	}
}

Box SelectionSystem::box()
{
	return Bounding::box(selection());
}