#include "System.h"
#include <algorithm> // for std::sort on MINGW

using namespace bv;

// every instance of a system subclass needs to be registered
SystemBase::SystemBase()
{
	Systems::instance()->add(this);
}

// register a system instance
void Systems::add(SystemBase* system)
{
	// initialize immediately if systems are already initalized
	if (m_loaded)
	{
		system->load();
	}

    m_systems.push_back(system);
}

void Systems::init()
{
    // sort systems based on their priority
    std::sort(m_systems.begin(), m_systems.end(), [](const SystemBase* first, const SystemBase* second) -> bool
    {
        return first->priority() > second->priority();
    });

    // initialize all registered systems
	for (int i = 0; i < m_systems.size(); ++i)
	{
		m_systems[i]->load();
	}

	m_loaded = true;

    // notify all the watchers that we are done loading
    for(auto* sys: m_systems)
    {
        sys->onload()();
    }
}

void Systems::update()
{
	for (int i = 0; i < m_systems.size(); ++i)
    {
		m_systems[i]->update();
    }
}

void Systems::destroy()
{
	for (auto* sys : m_systems)
    {
        sys->destroy();
    }
}
