#include "PanelElement.h"

#include "LabelElement.h"
#include "SliderElement.h"
#include "ButtonElement.h"
#include "ListElement.h"
#include "DragElement.h"
#include "ViewportElement.h"
#include "Gui.h"

using namespace bv;
using namespace bv::gui;

void PanelElement::state(const Recti &rect) { m_rect = rect; }

void PanelElement::state(int x, int y, int width, int height)
{
    state(Recti {x, y, width, height });
}

PanelElement* PanelElement::panel(const std::string &title)
{
    // create a new panel
    PanelElement *panel = new PanelElement(this, title);
    // and save it among children
    m_children.push_back(std::shared_ptr<PanelElement>(panel));

    // return the created panel
    return panel;
}

void PanelElement::remove(const IElement* child)
{
    // the predicate to find child iterator
    auto predicate = [child](const std::shared_ptr<IElement>& el)
    {
        return el.get() == child;
    };

    auto it = std::remove_if(m_children.begin(), m_children.end(), predicate);

    // if the child is in this panel
    if(it != m_children.end())
    {
        // remove it from the list of children
        m_children.erase(it, m_children.end());
    }
}

void PanelElement::add(const std::shared_ptr<IElement> &child)
{
	auto it = std::find(m_children.begin(), m_children.end(), child);
	if(it == m_children.end())
	{
		m_children.push_back(child);
		child->parent(this);
	}
}

void PanelElement::remove(const std::shared_ptr<IElement>& child)
{
    auto it = std::remove(m_children.begin(), m_children.end(), child);
    if(it != m_children.end())
    {
        m_children.erase(it, m_children.end());
    }
}

void PanelElement::open()
{
    m_open = true;
	visible(true);
}

void PanelElement::close()
{
	m_open = false;
	visible(false);

	if(m_parent)
	{
		m_parent->remove(this);
	}
}

IElement* PanelElement::bind(const std::string &name, float &value, float min, float max, float step)
{
	m_children.push_back(std::shared_ptr<DragElement>(new DragElement(this, name.c_str(), &value, min, max, step)));
	return m_children.back().get();
}

IElement* PanelElement::bind(const std::string &name, int &value, int min, int max, int step)
{
	m_children.push_back(std::shared_ptr<SliderElementInt>(new SliderElementInt(this, name.c_str(), &value, min, max, step)));
	return m_children.back().get();
}

IElement* PanelElement::button(const std::string &name, const Action& action, std::function<bool()> active)
{
	m_children.push_back(std::make_shared<ButtonElement>(this, name, action, active));
	return m_children.back().get();
}

IElement* PanelElement::check(const std::string &name, const Action& action, std::function<bool()> active)
{
	m_children.push_back(std::make_shared<CheckBoxElement>(this, name, action, active));
	return m_children.back().get();
}

IElement* PanelElement::label(const std::string &name, std::function<std::string()> source)
{
	m_children.push_back(std::make_shared<DynLabelElement>(this, name, source));
	return m_children.back().get();
}

void PanelElement::render()
{
    // resize ourselves to fit the parent panel
//    if (m_parent != nullptr)
//    {
//        // this is the heighest level with no elements
//        int offset = m_parent->m_lastOccupiedHeight;
//
//        // set the dimensions of the panel element
//        m_rect = {
//                m_parent->m_rect.x + MARGIN,
//                m_parent->m_rect.y + MARGIN + offset,
//                m_parent->m_rect.width - 2 * MARGIN,
//                m_rect.height
//        };
//
//        // move the new free level
//        m_parent->m_lastOccupiedHeight += m_rect.height;
//    }

    // kick off the actual element rendering
    IElement::render();
}

void PanelElement::renderChildren()
{
    m_lastOccupiedHeight = 0;

    for (int i = 0; i < m_children.size(); i++)
    {
        m_children[i]->render();
    }
}

IElement* PanelElement::list(const std::string& title, const std::vector<std::string>& list, ActionInt action)
{
	auto element = std::make_shared<ListElement>(this, title, list);
	element->onchange(action);
	m_children.push_back(element);
	return m_children.back().get();
}

IElement* PanelElement::combo(const std::string& title, const std::vector<std::string>& list, ActionInt action)
{
	m_children.push_back(std::make_shared<ComboElement>(this, title, list, action));
	return m_children.back().get();
}

IElement* PanelElement::multilist(const std::string& title, const std::vector<std::string>& list, ActionVecInt action)
{
	m_children.push_back(std::make_shared<MultiListElement>(this, title, list, action));
	return m_children.back().get();
}


IElement *PanelElement::renderContext(const std::string &title)
{
    m_children.push_back(std::make_shared<ViewportElement>(this, title));
    return m_children.back().get();
}

