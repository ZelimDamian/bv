#include "StlLoader.h"

#include "FileReader.h"

#include <stdio.h>
#include <iostream>

using namespace bv;

static const std::string MODELS_ROOT = "models/";

Mesh::Id StlLoader::load(const std::string& filename)
{
	auto filepath = FileReader::ROOT_DIR + MODELS_ROOT + filename + ".stl";

	return load_absolute(filepath);
}

Mesh::Id StlLoader::load_absolute(const std::string& filepath)
{
	auto mesh = Mesh::create();

	load_absolute(filepath, mesh->positions, mesh->normals, mesh->indices);

	for (int i = 0; i < mesh->positions.size(); i+=3)
	{
		mesh->tri(i + 0, i + 1, i + 2);
	}

	mesh->updateIndices();
	mesh->updateVertices();
	mesh->setupConnections();

	return mesh;
}

void StlLoader::load_absolute(const std::string& filepath, std::vector<glm::vec3>& positions,
                     std::vector<glm::vec3>& normals, std::vector<uint32_t> &indices)
{
	// Open file, and check for error
	FILE * stl_file = fopen(filepath.c_str(), "rb");

	if(NULL==stl_file)
	{
		fclose(stl_file);
		Log::error("IOError: %s could not be opened...", filepath.c_str());
	}

	positions.clear();
	indices.clear();
	normals.clear();

	// Specifically 80 character header
	char header[80];
	char solid[80];
	bool is_ascii = true;
	if(fread(header,1,80,stl_file) != 80)
	{
		fclose(stl_file);
		Log::error("IOError: " + filepath + " too short (1).");
	}

	sscanf(header,"%s",solid);

	if(std::string("solid") != solid)
	{
		// definitely **not** ascii
		is_ascii = false;
	}else
	{
		// might still be binary
		char buf[4];
		if(fread(buf,1,4,stl_file) != 4)
		{
			fclose(stl_file);
			Log::error("IOError: " + filepath + " too short (3).");
		}
		size_t num_faces = *reinterpret_cast<unsigned int*>(buf);
		fseek(stl_file,0,SEEK_END);
		int file_size = ftell(stl_file);
		if(file_size == 80 + 4 + (4*12 + 2) * num_faces)
		{
			is_ascii = false;
		}else
		{
			is_ascii = true;
		}
	}
	fclose(stl_file);

	if(is_ascii)
	{
		// Rewind to end of header
		stl_file = fopen(filepath.c_str(),"r");
		// Eat file name
#define LINE_MAX 2048
		char name[LINE_MAX];
		if(NULL==fgets(name,LINE_MAX,stl_file))
		{
			Log::error("IOError: " + filepath + " ascii too short (2).");
			fclose(stl_file);
		}
		// ascii
		while(true)
		{
			int ret;
			char facet[LINE_MAX],normal[LINE_MAX];
			glm::vec3 n;

			double nd[3];
			ret = fscanf(stl_file,"%s %s %lg %lg %lg",facet,normal,nd,nd+1,nd+2);
			if(std::string("endsolid") == facet)
			{
				break;
			}
			if(ret != 5 ||
			   !(std::string("facet") == facet ||
			     std::string("faced") == facet) ||
			     std::string("normal") != normal)
			{
				std::cout<<"facet: "<<facet<<std::endl;
				std::cout<<"normal: "<<normal<<std::endl;
				std::cerr<<"IOError: "<<filepath<<" bad format (1)."<<std::endl;
				fclose(stl_file);
			}
			// copy casts to Type
			n[0] = nd[0]; n[1] = nd[1]; n[2] = nd[2];
			normals.push_back(n);
			char outer[LINE_MAX], loop[LINE_MAX];
			ret = fscanf(stl_file,"%s %s",outer,loop);
			if(ret != 2 || std::string("outer") != outer || std::string("loop") != loop)
			{
				Log::error("IOError: " + filepath + " bad format (2).");
				fclose(stl_file);
			}
			while(true)
			{
				char word[LINE_MAX];
				int ret = fscanf(stl_file,"%s",word);
				if(ret == 1 && std::string("endloop") == word)
				{
					break;
				}else if(ret == 1 && std::string("vertex") == word)
				{
					glm::vec3 v;
					double vd[3];
					int ret = fscanf(stl_file,"%lg %lg %lg",vd,vd+1,vd+2);
					if(ret != 3)
					{
						Log::error("IOError: " + filepath + ". Bad format (3).");
						fclose(stl_file);
					}
					indices.push_back(positions.size());
					// copy casts to Type
					v[0] = vd[0]; v[1] = vd[1]; v[2] = vd[2];
					positions.push_back(v);
				}else
				{
					fclose(stl_file);
					Log::error("IOError: " + filepath + " bad format (4).");
				}
			}
			char endfacet[LINE_MAX];
			ret = fscanf(stl_file,"%s",endfacet);
			if(ret != 1 || std::string("endfacet") != endfacet)
			{
				fclose(stl_file);
				Log::error("IOError: " + filepath + " bad format (5).");
			}
		}
		// read endfacet
		fclose(stl_file);
	}
	else
	{
		// Binary
		stl_file = fopen(filepath.c_str(),"rb");
		if(NULL==stl_file)
		{
			Log::error("Can't open STL file: " + filepath );
		}
		// Read 80 header
		char header[80];
		if(fread(header,sizeof(char),80,stl_file)!=80)
		{
			fclose(stl_file);
			Log::error("Bad STL format.");
		}
		// Read number of triangles
		unsigned int num_tri;
		if(fread(&num_tri,sizeof(unsigned int),1,stl_file)!=1)
		{
			fclose(stl_file);
			Log::error("Bad STL format.");
		}

		positions.resize(num_tri*3);
		normals.resize(num_tri);
		indices.resize(num_tri*3);

		for(int t = 0; t < (int)num_tri; t++)
		{
			// Read normal
			float n[3];
			if(fread(n,sizeof(float),3,stl_file)!=3)
			{
				fclose(stl_file);
				Log::error("Bad STL format.");
			}
			// Read each vertex
			for(int c = 0;c<3;c++)
			{
				indices[3*t+c] = 3*t+c;
				normals[t][c] = n[c];
				float v[3];
				if(fread(v,sizeof(float),3,stl_file)!=3)
				{
					fclose(stl_file);
					Log::error("Bad STL format.");
				}
				positions[3*t+c][0] = v[0];
				positions[3*t+c][1] = v[1];
				positions[3*t+c][2] = v[2];
			}
			// Read attribute size
			unsigned short att_count;
			if(fread(&att_count,sizeof(unsigned short),1,stl_file)!=1)
			{
				fclose(stl_file);
				Log::error("Bad STL format.");
			}
		}
		fclose(stl_file);
	}
}