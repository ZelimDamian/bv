#include "Vertex.h"

using namespace bv;

SemanticsType PosNormColVertex::semantics = { AttribSemantics::POS, AttribSemantics::NOR, AttribSemantics::COL0 };
SemanticsType PosNormTexVertex::semantics = { AttribSemantics::POS, AttribSemantics::NOR, AttribSemantics::COL0 };
SemanticsType PosColorVertex::semantics = { AttribSemantics::POS, AttribSemantics::NOR, AttribSemantics::COL0 };

