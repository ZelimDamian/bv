#include "FileWriter.h"

#include <fstream>

using namespace bv;

struct FileWriter::pimpl
{
	pimpl(const std::string& filename, bool append) : m_file(filename.c_str(), append ? std::ios::app : std::ios::out){}

	std::ofstream m_file;
};

FileWriter::FileWriter(const std::string& filename, bool append):
        m_append(append), m_filename(filename) { }

FileWriter::~FileWriter() // have to keep this for the pimpl
{
}

void FileWriter::write(const char* message)
{
	if(!m_pimpl)
	{
        m_pimpl = std::unique_ptr<pimpl>(new pimpl(m_filename, m_append));
	}

	m_pimpl->m_file << message;
}

