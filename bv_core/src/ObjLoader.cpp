#include "ObjLoader.h"

#include "FileReader.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>
#undef TINYOBJLOADER_IMPLEMENTATION


struct ObjVertex
{
	int p, n, t;
};

struct ObjTri
{
	union
	{
		struct
		{
			ObjVertex v1, v2, v3;
		};

		ObjVertex verts[3];
	};
};

using namespace bv;

void parse(const std::string& content, Mesh& mesh);

static const std::string MODELS_ROOT = "models/";

Mesh::Id ObjLoader::load(const std::string &name, std::string const ext)
{

	auto filename = FileReader::ROOT_DIR + MODELS_ROOT + name + ext;
//    std::string content = FileReader::read(filename);

	return load_absolute(filename);
}


Mesh::Id ObjLoader::load_absolute(const std::string &filename, std::string const ext)
{
	auto mesh = Mesh::acquire();
//	parse(content, mesh);

    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string error;

    tinyobj::LoadObj(shapes, materials, error, filename.c_str(), "", false);

    if(error.find("ERROR") != std::string::npos)
    {
        Log::error(error);
    }

    auto& shape = shapes[0];

    mesh->vertices.reserve(shape.mesh.positions.size() / 3);

    for (int i = 0; i < shape.mesh.positions.size() / 3; ++i)
    {
        glm::vec3 pos(
                shape.mesh.positions[i * 3 + 0],
                shape.mesh.positions[i * 3 + 1],
                shape.mesh.positions[i * 3 + 2]
        );

        mesh->vertex(pos);
    }

	for (int i = 0; i < shape.mesh.normals.size() / 3; ++i)
	{
		glm::vec3 normal(
				shape.mesh.normals[i * 3 + 0],
				shape.mesh.normals[i * 3 + 1],
				shape.mesh.normals[i * 3 + 2]
		);

		mesh->vertices[i].normal(normal);
	}

    for (int i = 0; i < shape.mesh.indices.size() / 3; ++i)
    {
        mesh->tri(
                shape.mesh.indices[i * 3 + 0],
                shape.mesh.indices[i * 3 + 1],
                shape.mesh.indices[i * 3 + 2]
        );
    }

    mesh->updateIndices();
	mesh->updateBox();
	mesh->updatePositions();

	Log::info("Loaded model %s - %d verts, %d faces", filename.c_str(), mesh->vertices.size(), mesh->triangles.size());

	return mesh->id();
}

void allocate(const std::vector<glm::vec3>& positions, const std::vector<glm::vec3>& normals,
              const std::vector<glm::vec2>& texcoords, const std::vector<ObjTri>& tris, Mesh& mesh)
{
	//mesh.resize(tris.size() * 3);

	for (int i = 0; i < tris.size(); ++i)
	{

		const ObjTri& tri = tris[i];

		glm::vec2 texcoord1;
		glm::vec2 texcoord2;
		glm::vec2 texcoord3;

		if (tri.v1.t >= 0)
		{
			texcoord1 = texcoords[tri.v1.t];
		}
		if (tri.v2.t >= 0)
		{
			texcoord2 = texcoords[tri.v2.t];
		}
		if (tri.v3.t >= 0)
		{
			texcoord3 = texcoords[tri.v3.t];
		}

		mesh.vertex(positions[tri.v1.p], normals[tri.v1.n]/*, texcoord1*/);
		mesh.vertex(positions[tri.v2.p], normals[tri.v2.n]/*, texcoord2*/);
		mesh.vertex(positions[tri.v3.p], normals[tri.v3.n]/*, texcoord3*/);

		mesh.tri(i * 3 + 0, i * 3 + 1, i * 3 + 2);
	}
}

void parse(const std::string& content, Mesh& mesh)
{
	std::vector<glm::vec3> positions, normals;
	std::vector<glm::vec2> texcoords;
	std::vector<ObjTri> tris;

	char *lineCharPtr = c_strtok(const_cast<char*>(content.c_str()), "\n");

	while (lineCharPtr != NULL)
	{
		char firstChar = lineCharPtr[0];

		switch (firstChar)
		{
			case 'v':
			case 'V':
			{
				char secondChar = lineCharPtr[1];

				// Process a vertex entry (v, vn or vt)
				switch(secondChar)
				{
					case ' ':
					{
						glm::vec3 vec3;
						// "v " means a vertex in the format of "v x y z"
						c_scanf(lineCharPtr, "%*s %f %f %f", &vec3.x, &vec3.y, &vec3.z);
						positions.push_back(vec3);
					}
				        break;

					case 'n':
					{
						glm::vec3 vec3;
						// "vn " means a normal in the format of "vn x y z"
						c_scanf(lineCharPtr, "%*s %f %f %f", &vec3.x, &vec3.y, &vec3.z);
						normals.push_back(vec3);
					}
				        break;

					case 't':
					{
						glm::vec2 vec2;
						// "vt " means a texcoord in the format of "vt x y z"
						c_scanf(lineCharPtr, "%*s %f %f", &vec2.x, &vec2.y);
						texcoords.push_back(vec2);
					}
				        break;
				}
			}
		        break;

			case 'f':
			case 'F':
			{
				// This is a face entry
				int positionIndex1 = -1, normalIndex1 = -1, texcoordsIndex1 = -1;
				int positionIndex2 = -1, normalIndex2 = -1, texcoordsIndex2 = -1;
				int positionIndex3 = -1, normalIndex3 = -1, texcoordsIndex3 = -1;

				c_scanf(lineCharPtr, "%*s %d/%d/%d %d/%d/%d %d/%d/%d",	&positionIndex1, &texcoordsIndex1, &normalIndex1,
				        &positionIndex2, &texcoordsIndex2, &normalIndex2,
				        &positionIndex3, &texcoordsIndex3, &normalIndex3);

				if(texcoordsIndex1 < 0)
				{
					c_scanf(lineCharPtr, "%*s %d//%d %d//%d %d//%d",	&positionIndex1, &normalIndex1,
					        &positionIndex2, &normalIndex2,
					        &positionIndex3, &normalIndex3);
				}

				// .obj vertex index numbers start from 1, not 0. Ajust for it
				positionIndex1--; positionIndex2--; positionIndex3--;
				normalIndex1--; normalIndex2--; normalIndex3--;
				texcoordsIndex1--; texcoordsIndex2--; texcoordsIndex3--;

				ObjTri tri;

				// Set the face vertex information. Adust for obj numbering
				tri.v1.p = positionIndex1;
				tri.v2.p = positionIndex2;
				tri.v3.p = positionIndex3;

				tri.v1.n = normalIndex1;
				tri.v2.n = normalIndex2;
				tri.v3.n = normalIndex3;

				tri.v1.t = texcoordsIndex1;
				tri.v2.t = texcoordsIndex2;
				tri.v3.t = texcoordsIndex3;

				tris.push_back(tri);
			}
			break;
		}

		lineCharPtr = c_strtok(NULL, "\n");
	}

	allocate(positions, normals, texcoords, tris, mesh);
}

