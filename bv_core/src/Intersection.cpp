#include "Intersection.h"

using namespace bv;

glm::vec3 Intersection::hit(const Ray &ray, const Plane &plane)
{
    const float numerator = glm::dot(plane.normal(), plane.pos() - ray.pos());
    const float denominator = glm::dot(plane.normal(), ray.dir());

    float t = 0;
    if (denominator != 0)
    {
        t = numerator / denominator;
    }

    return ray.point(t);
}

glm::vec3 Intersection::hitShift(Ray to, Ray from, Plane plane)
{
    const glm::vec3 nowhit = hit(to, plane);
    const glm::vec3 beforehit = hit(from, plane);

    return nowhit - beforehit;
}

float Intersection::distanceToLine(const glm::vec3 &start, const glm::vec3 &end, const glm::vec3 &point)
{
    return glm::length(glm::cross(end - start, start - point)) / glm::length(end - start);
}

float Intersection::distanceToSegment(const glm::vec3 &start, const glm::vec3 &end, const glm::vec3 &point)
{
    const glm::vec3 segment = end - start;
    const glm::vec3 dir = glm::normalize(segment);
    // clamp t
    const float t = std::min(std::max(0.0f, glm::dot(dir, point - start)), glm::length(segment));
    const glm::vec3 pointOnLine = start + dir * t;

    return glm::length(point - pointOnLine);
}

bool Intersection::test(const Box &box1, const Box &box2)
{
    const glm::vec3& min1 = box1.min();
    const glm::vec3& max1 = box1.max();
    const glm::vec3& min2 = box2.min();
    const glm::vec3& max2 = box2.max();

    bool disjoint =  min1.x > max2.x || min1.y > max2.y || min1.z > max2.z ||
                     max1.x < min2.x || max1.y < min2.y || max1.z < min2.z;

    return !disjoint;
}

