#pragma once
#include "System.h"
#include "Common.h"

namespace bv
{
	namespace gpu
	{
		class GpuSystem: public SystemSingleton<GpuSystem>
		{
		public:

			// initializes GPU system
			void init();
		};
	}
}
