#include "GpuSystem.h"

#include "KernelCL.h"
#include "ContextCL.h"
#include "DeviceCL.h"

using namespace bv;
using namespace gpu;
using namespace cl;

void printInfo(cl_device_id device)
{
    size_t valueSize = 0;
    char* value;

    // for each device print critical attributes
//    for (int j = 0; j < deviceCount; j++)
    {
//        cl_device_id device = devices[j];

        // print device name
        clGetDeviceInfo(device, CL_DEVICE_NAME, 0, NULL, &valueSize);
        value = (char*) malloc(valueSize);
        clGetDeviceInfo(device, CL_DEVICE_NAME, valueSize, value, NULL);
        printf("Device: %s\n", value);
        free(value);

        // print hardware device version
        clGetDeviceInfo(device, CL_DEVICE_VERSION, 0, NULL, &valueSize);
        value = (char*) malloc(valueSize);
        clGetDeviceInfo(device, CL_DEVICE_VERSION, valueSize, value, NULL);
        printf(" Hardware version: %s\n", value);
        free(value);

        // print software driver version
        clGetDeviceInfo(device, CL_DRIVER_VERSION, 0, NULL, &valueSize);
        value = (char*) malloc(valueSize);
        clGetDeviceInfo(device, CL_DRIVER_VERSION, valueSize, value, NULL);
        printf(" Software version: %s\n", value);
        free(value);

        // print c version supported by compiler for device
        clGetDeviceInfo(device, CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &valueSize);
        value = (char*) malloc(valueSize);
        clGetDeviceInfo(device, CL_DEVICE_OPENCL_C_VERSION, valueSize, value, NULL);
        printf(" OpenCL C version: %s\n", value);
        free(value);

        int maxComputeUnits = 0;

        // print parallel compute units
        clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS,
                        sizeof(maxComputeUnits), &maxComputeUnits, NULL);
        printf(" Parallel compute units: %d\n", maxComputeUnits);

        int frequency = 0;

        // print c version supported by compiler for device
        clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY,
                        sizeof(frequency), &frequency, NULL);
        printf(" Max clock frequency: %d MHz\n", frequency);

        printf("\n");
    }
}

void GpuSystem::init()
{
	/*Step1: Getting platforms and choose an available one.*/
	cl_uint numPlatforms;	//the NO. of platforms
	cl_platform_id platform = NULL;	//the chosen platform
	cl_int	status = clGetPlatformIDs(0, NULL, &numPlatforms);

	if (status != CL_SUCCESS)
	{
		Log::error("Not Getting platforms!");
		return;
	}

	cl_uint platformCount;
	cl_platform_id *platforms;

	// get platform count
	clGetPlatformIDs(5, NULL, &platformCount);

	// get all platforms
	platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id) * platformCount);
	clGetPlatformIDs(platformCount, platforms, NULL);

	// for each platform print all attributes
	for (int i = 0; i < platformCount; i++) {
//		logPlatformInfo(platforms[i]);
	}

	/*For clarity, choose the first available platform. */
	if (numPlatforms > 0)
	{
		platform = platforms[0];
	}
	else
	{
		assert(false);
		exit(0);
	}

#if CL_GL_INTEROP

#ifdef _WIN32
	cl_context_properties properties[] = {
		CL_GL_CONTEXT_KHR, (cl_context_properties)wglGetCurrentContext(),
		CL_WGL_HDC_KHR, (cl_context_properties)wglGetCurrentDC(),
		CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[0]),
		0
	};
#elif defined(__linux__)
	cl_context_properties properties[] = {
		CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
		CL_WGL_HDC_KHR, (cl_context_properties)glXGetCurrentDisplay(),
		CL_CONTEXT_PLATFORM, (cl_context_properties)(mPlatforms[0])(),
		0
	};
#elif defined(__APPLE__)
	CGLContextObj glContext = CGLGetCurrentContext();
	CGLShareGroupObj shareGroup = CGLGetShareGroup(glContext);
	cl_context_properties properties[] = {
		CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE,
		(cl_context_properties)shareGroup,
	};
#endif

	size_t bytes = 0;
	// Notice that extension functions are accessed via pointers
	// initialized with clGetExtensionFunctionAddressForPlatform.

	// queuring how much bytes we need to read
	clGetGLContextInfoKHR(properties, CL_DEVICES_FOR_GL_CONTEXT_KHR, 0, NULL, &bytes);
	// allocating the mem
	size_t devNum = bytes / sizeof(cl_device_id);
	std::vector<cl_device_id> devs(devNum);
	//reading the info
	clGetGLContextInfoKHR(properties, CL_DEVICES_FOR_GL_CONTEXT_KHR, bytes, (void*)&devs, NULL);

#endif

	/*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
	cl_uint				numDevices = 0;
	cl_device_id        *devices;
	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);
	if (numDevices == 0)	//no GPU available.
	{
		Log::writeln("No GPU device available. Using CPU.");
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, &numDevices);
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, numDevices, devices, NULL);
	}
	else
	{
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, devices, NULL);
	}

	/*Step 3: Create context.*/

	cl_int error;

#if CL_GL_INTEROP
	cl_context context = clCreateContext(properties, 1, devices, NULL, NULL, &error);
#else
	cl_context context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &error);
#endif

	if (error != CL_SUCCESS)
	{
		Log::error("Unable to create CL context!");
	}

	// set global context
	Context::context(context);

	for (size_t i = 0; i < numDevices; i++)
	{
		cl_device_id dev_id = devices[i];

        printInfo(dev_id);

		Device::create(dev_id);
	}

//    printInfo(Device::active()->handle());

	Log::debug("Successfully created CL context.");

	free(platforms);
}

