#include "KernelCL.h"
#include "GpuSystem.h"
#include "ContextCL.h"
#include "DeviceCL.h"
#include "BufferCL.h"

using namespace bv;
using namespace gpu;
using namespace cl;

void Kernel::initialize()
{
	size_t srcLen = m_source.length();
	const char* src = m_source.c_str();

	std::string m_name = name();

	cl_context context = Context::instance()->handle();

	cl_int status;

	cl_program program = clCreateProgramWithSource(context, 1, &src, &srcLen, &status);

	if (status != CL_SUCCESS)
	{
		Log::error("Error: creating program " + m_name);
	}

    cl_device_id device = Device::active().ref().handle();

	/* Build program. */
	status = clBuildProgram(program, 1, &device, NULL, NULL, NULL);

    // check compilation errors and report
	if (status != CL_SUCCESS)
	{

		size_t length=0;
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &length);

		char buffer[1024];
		clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, length, buffer, NULL);
		buffer[length+1] = '\0';
		Log::writeln(buffer);
//		delete[] buffer;

		Log::error("Error: building program " + m_name);
	}

	/*Step 8: Create kernel object */
	m_kernel = clCreateKernel(program, m_name.c_str(), &status);

	if (status != CL_SUCCESS)
	{
		Log::debug("Error: creating kernel " + m_name);
		Log::error("%s", Context::error(status));
	}
}

