#pragma once

#include <FileReader.h>
#include "Store.h"
#include "BufferCL.h"

namespace bv
{
	namespace gpu
	{
		namespace cl
		{
			// represents an opencl kernel
			class Kernel: public NamedStorable<Kernel>
			{
			public:
				Kernel(Id id, const std::string &name) : NamedStorableBase(id, name) { }

				void source(const std::string &source) { m_source = source; }

				void initialize();

				template<class ... Args>
				void operator()(const std::vector<size_t>& range, const Args &... args)
				{
					execute(range, args...);
				}

				template<class ... Args>
				void execute(const std::vector<size_t>& range, const Args &... args)
				{
					// set all the arguments
					this->args(args...);

                    const auto& queue = Device::active().ref().queue();
                    clEnqueueNDRangeKernel(queue, m_kernel, range.size(), NULL, range.data(),
                                           NULL, 0, NULL, NULL);
				}

				template<class ... Args>
				void args(Args&& ... args)
				{
					int i = 0;
					// unpack the paramter pack while executing method for each value in the pack
					(void) std::initializer_list<int>{(set(i, args), i++)...};
				}

				template <typename T>
				void set(int index, const T& value)
				{
					cl_int status = 0;
					status = clSetKernelArg(m_kernel, index, sizeof(T), &value);

					if (status != CL_SUCCESS)
					{
						Log::error("Not able to set buffer data to kernel.");
					}
				}

				template<typename T>
				void set(int index, const Handle<BufferCL<T>>& buffer)
				{
					cl_int status = 0;
					cl_mem mem = buffer.ref().mem();
					status = clSetKernelArg(m_kernel, index, sizeof(cl_mem), &mem);

					if (status != CL_SUCCESS)
					{
						Log::error("Not able to set buffer data to kernel.");
					}
				}


				static
				Id require(const std::string& name)
				{
					auto kernel = Kernel::acquire(name);

					std::string source = FileReader::read("kernels/" + name + ".c");
					kernel->source(source);
					kernel->initialize();

					return kernel->id();
				}

			private:
				cl_kernel m_kernel;
				std::string m_source;
			};
		}
	}
}