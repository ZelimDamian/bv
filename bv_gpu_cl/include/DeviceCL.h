#pragma once
#include "ContextCL.h"

namespace bv
{
	namespace gpu
	{
		namespace cl
		{

			class Device : public Storable<Device>
			{
			public:

				Device(Id id, cl_device_id handle): StorableBase(id), m_handle(handle)
				{
					m_queue = clCreateCommandQueue(Context::instance()->handle(), m_handle, 0, NULL);
				}

				cl_device_id handle() const { return m_handle; }
				cl_command_queue queue() const { return m_queue; }

				static Id active() { return s_instances[0].id(); }
//                static Id active() { return s_instances[s_instances.size() - 1].id(); }

			private:
				cl_device_id m_handle;
				cl_command_queue m_queue;
			};
		}
	}
}

