#pragma once

#include "Buffer.h"
#include "Store.h"


#include "DeviceCL.h"
#include "GpuSystem.h"

#ifdef __APPLE__
    #include "OpenCL/cl.h"
#else
    #include "CL/cl.h"
#endif

namespace bv
{
	namespace gpu
	{
		namespace cl
		{
			enum class BufferFormat
			{
				R,
				W,
				RW
			};

		    template <typename T>
		    class BufferCL : public Storable<BufferCL<T>>
		    {
		    public:
			    using StorableBase = Storable<BufferCL<T>>;
			    using Id = typename StorableBase::Id;

			    BufferCL(Id handle, BufferFormat format) : BufferCL(handle, format, 0) {}

			    BufferCL(Id handle, BufferFormat format, size_t size) : m_format(format), m_data(size), StorableBase(handle)
			    {
				    GpuSystem::instance()->onload([&] { init(); });
			    }

			    BufferCL(Id handle, BufferFormat format, const Buffer<T>& data) : m_format(format), m_data(data), StorableBase(handle)
			    {
				    GpuSystem::instance()->onload([&] { init(true); });
			    }

			    BufferCL& operator= (const BufferCL& other)
			    {
				    assert(data().size() == other.data().size());
				    auto queue = Device::active()->queue();
				    clEnqueueCopyBuffer(queue, other.m_mem, m_mem, 0, 0, data().sizeInBytes(), 0, NULL, NULL);
				    return *this;
			    }

			    size_t size() const { return m_data.size(); }

			    void resize(size_t size)
			    {
				    // allocate memory for the new size
				    size_t oldSizeInBytes = m_data.size() * sizeof(T);

				    m_data.resize(size);

				    cl_int status;

				    cl_mem_flags flags = map(m_format);
				    cl_mem new_mem = clCreateBuffer(Context::instance()->handle(), flags, m_data.sizeInBytes(), NULL, &status);

				    if (status != CL_SUCCESS)
				    {
					    Log::writeln(Context::error(status));
					    Log::error("Error creating resized buffer in CL.");
				    }

				    // copy the old data into the new memory
				    auto queue = Device::active()->queue();
				    status = clEnqueueCopyBuffer(queue, m_mem, new_mem, 0, 0, oldSizeInBytes, 0, NULL, NULL);

				    if (status != CL_SUCCESS)
				    {
					    Log::writeln(Context::error(status));
					    Log::error("Error copying resized buffer in CL.");
				    }

				    // clean up the old memory
				    clReleaseMemObject(m_mem);

				    // assign the new memory
				    m_mem = new_mem;
			    }

			    void upload(const Buffer<T>& source)
			    {
				    assert(m_data.size() == source.size());
				    m_data = source;
				    upload();
			    }

                void upload(const T& value)
                {
                    m_data.set(value);
                    upload();
                }

				void upload()
				{
					auto queue = Device::active()->queue();
					cl_int status = clEnqueueWriteBuffer(queue, m_mem, CL_FALSE, 0,
															m_data.sizeInBytes(), m_data.data(), 0, NULL, NULL);

					if (status != CL_SUCCESS)
					{
						Log::writeln(Context::error(status));
						Log::error("Error uploading buffer data from host buffer to CL.");
					}
				}

				void download(Buffer<T>& source)
				{
					download();
					source = m_data;
				}

			    void download()
			    {
                    auto queue = Device::active()->queue();
                    cl_int status = clEnqueueReadBuffer(queue, m_mem, CL_TRUE, 0,
				                                        m_data.sizeInBytes(), m_data.data(), 0, NULL, NULL);

				    if (status != CL_SUCCESS)
				    {
					    Log::error("Error downloading buffer data from CL to host buffer.");
				    }
			    }

			    Buffer<T>& data() { return m_data; }
			    const Buffer<T>& data() const { return m_data; }
			    cl_mem mem() const { return m_mem; }

			    BufferFormat format() const { return m_format; }

		    private:
			    Buffer<T> m_data;
			    cl_mem m_mem;

			    BufferFormat m_format;

			    static cl_mem_flags map(BufferFormat format)
			    {
					switch(format)
					{
						case BufferFormat::R: return CL_MEM_READ_ONLY;
						case BufferFormat::W: return CL_MEM_WRITE_ONLY;
						case BufferFormat::RW: return CL_MEM_READ_WRITE;
						default: return CL_MEM_READ_WRITE;
					}
			    }

			    void init(bool copy = false)
			    {
                    cl_mem_flags flags = map(m_format);
					void* mem = nullptr;

				    if(copy)
				    {
					    flags |= CL_MEM_COPY_HOST_PTR;
						mem = m_data.data();
				    }

					cl_int status;

					m_mem = clCreateBuffer(Context::instance()->handle(), flags, m_data.sizeInBytes(), mem, &status);

					if (status != CL_SUCCESS)
					{
						Log::writeln(Context::error(status));
						Log::error("Error creating buffer in CL.");
					}
			    }
		    };
		}
	}
}