#include <vector>
#include "stdio.h"

template <class T>
class Component;

template <template<typename ...> class TChild, typename ... TArgs>
class Component<TChild<TArgs...>>
{
private:
	using Child = TChild<TArgs...>;
public:

	Component(int data): m_data(data)
	{
	}

	template<typename ... CreateArgs>
	static Child& create(CreateArgs&& ... args)
	{
		children().emplace_back(children().size(), std::forward<CreateArgs...>(args)...);
		return children().back();
	}

	void print() { printf("Things %i", m_data); }

	int m_data;

	static std::vector<Child>& children() { static std::vector<Child> children; return children; }
};

template <class Thing, class Things>
class ChildComponent: public Component<ChildComponent<Thing, Things>>
{
public:
	ChildComponent(int data, Thing thing): m_thing(thing), Component<ChildComponent>(data) {}

	void print() { Component<ChildComponent>::print(); }

	static void doThings(int data) { }

	Thing m_thing;
	Things m_things;
};

int main()
{
	auto& childComponent = ChildComponent<float, int>::create(0.0f);

	return 0;
}