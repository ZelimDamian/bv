#include <stdio.h>
#include <array>

namespace bv
{
    template <int TC, int TR>
    struct Dims
    {
        enum { C = TC };
        enum { R = TR };

        static const int SIZE = C * R;
    };

    template<class T> class Base; // primary template

    template< template<class, typename...> class TT, class T1, typename... Rest>
    class Base<TT<T1, Rest...>>
    {
    public:
        using Type = TT<T1, Rest...>;

        void print(int index)
        {
            printf("Hello %i", static_cast<Type*>(this)->data[index]);
        }

        T1& at(int i, int j)
        {
            return static_cast<Type*>(this)->data[i * j];
        }

    }; // partial specialization of eval

    template <typename T1, typename Dims>
    class A: public Base<A<T1, Dims>>
    {
    public:
        std::array<T1, Dims::SIZE> data;

//        T1& at(int i) { return data[i]; }
    };

    template <typename T1, template<int, int> class TDims, int C, int R>
    float mult(A<T1, TDims<C, R>>& a)
    {
        a.data[0] *= C * R;
    }
}

mc::A<int, mc::Dims<2, 2>> eA;

int main(int argc, char** argv)
{
    eA.data[0] = 111;
    eA.at(0, 1) = 222;

    mc::mult(eA);

    eA.print(0);
}
