#include "Timing.h"

namespace bv
{
    namespace testing
    {
        void multATB(float A[8][3], float B[8][3], float C[3][3])
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    float value = 0;
                    for (int k = 0; k < 8; k++)
                    {
                        value += A[k][i] * B[k][j];
                    }
                    C[i][j] = value;
                }
            }
        }
    }
}
