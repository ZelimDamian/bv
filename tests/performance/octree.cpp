//
// Created by Zelim on 17/01/2016.
//

#include <iostream>
#include "benchmark/benchmark.h"

#include "ObjLoader.h"
//#include "CollisionMethodProvider.h"
#include "CollisionMethod.h"

using namespace bv;
using namespace coldet;

Octree setup(const std::string& name)
{
	// load the mesh representing the skull
	auto mesh = ObjLoader::load(name);
	// make sure that the posititon buffers are populated with vertex data
	mesh->updatePositions();
	mesh->updateTriNormals();

	return Octree(mesh);
}

static void bench(benchmark::State& state)
{
	auto sphere = setup("sphere");
	auto pelvis = setup("pelvisClean");

	while(state.KeepRunning())
	{
		using namespace coldet;
		auto method = CollisionMethodTemplate<Octree, Octree>(sphere, pelvis);
		method.find();
	}
}

BENCHMARK(bench);

BENCHMARK_MAIN();
