//
// Created by pgt09fvu on 5/11/2015.
//

#include "benchmark/benchmark.h"


struct Dummy {
    void do_something() {
        counter+=0.1f;
    }

    float counter;
};
static Dummy dummy;


void do_something()
{
    static float counter = 0.0f;
    counter+=0.1f;
}

//{
//    {
//        std::chrono::duration<double> time = std::chrono::duration_cast<std::chrono::duration<double>>(now - then);
//
//        std::cout << counter << std::endl;
//        std::cout << "Time for global do_somthing: " << time.count() << std::endl;
//    }
//}

static void global_bench(benchmark::State& state)
{
    while(state.KeepRunning())
    {
        do_something();
    }
}

static void instance_bench(benchmark::State& state)
{
    while(state.KeepRunning())
    {
        dummy.do_something();
    }
}

BENCHMARK(global_bench);
BENCHMARK(instance_bench);

BENCHMARK_MAIN();

