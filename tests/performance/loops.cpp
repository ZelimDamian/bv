#include "test_common.h"
#include "MatrixRoutines.h"

using namespace bv;
using namespace bv::testing;

enum Test
{
    Routines = 0,
    Hard,
    NumTests
};


int iterations = 1000000;
int tries = 10;
double times[NumTests];

// the actual tests
std::array<std::function<void()>, NumTests> tests;

void setup()
{
    tests[Hard] = [&]()
    {
        float DhDx[8][3];
        float x[8][3];
        float J[3][3]; memset(DhDx, 0, 9 * sizeof(float));

        for (int j = 0; j < 3; ++j)
        {
            for (int l = 0; l < 8; ++l)
            {
                DhDx[l][j] = 0.125f;
                x[l][j] = 2.0f;
            }
        }

        Timer::tick();

        for (int s = 0; s < iterations; ++s)
        {
            multATB(DhDx, x, J);
        }

        double time = Timer::tock();


        times[Hard] += time;
    };

    tests[Routines] = [&]()
    {
        ArrayFloat<8, 3> DhDx;
        DhDx.set(0.125f);

        ArrayFloat<8, 3> x;
        x.set(2.0f);

        ArrayFloat<3, 3> J;

        Timer::tick();

        for (int i = 0; i < iterations; ++i)
        {
            mc::MatrixRoutines::multABT(DhDx, x, J);
        }

        double time = Timer::tock();


        times[Routines] += time;
    };
}

int main()
{
    setup();

    // run the tests
    for (int i = 0; i < tries; ++i)
    {
        for (int j = 0; j < NumTests; ++j)
        {
            tests[j]();
        }
    }

    // average all time values
    for (int j = 0; j < NumTests; ++j)
    {
        times[j] /= tries;
    }

    Log::writeln("MatrixRoutines gives detJ. And did it in %f", times[Routines]);
    Log::writeln("Hardcoded gives detJ. And did it in %f", times[Hard]);

    Log::writeln("Time ratio Rout/Hard = %f", times[Routines] / times[Hard]);

    return 0;
}