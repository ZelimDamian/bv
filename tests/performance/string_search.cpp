#include <unordered_map>
#include <map>
#include <vector>
#include "benchmark/benchmark.h"

struct Object
{
	Object() {}
	Object(const std::string& name, float value): name(name), some_data(value) {}

	std::string name;
	float some_data;
};

const int MAP_SIZE = 10000;
const std::string to_find = std::to_string(MAP_SIZE / 2);

std::unordered_map<std::string, Object> um;
std::map<std::string, Object> om;
std::vector<Object> vs;

Object obj;

static void bench_umap(benchmark::State& state)
{
    for (int i = 0; i < MAP_SIZE; ++i)
    {
        std::string key = "key" + std::to_string(i);
        um.emplace(key, Object(key, 0.0f));
    }

    while(state.KeepRunning())
    {
        obj = um[to_find];
    }
}

static void bench_omap(benchmark::State& state)
{
    for (int i = 0; i < MAP_SIZE; ++i)
    {
        std::string key = "key" + std::to_string(i);
        om.emplace(key, Object(key, 0.0f));
    }

    while(state.KeepRunning())
    {
        obj = om[to_find];
    }
}

static void bench_vec(benchmark::State& state)
{
    vs.reserve(MAP_SIZE);

    for (int i = 0; i < MAP_SIZE; ++i)
    {
        std::string key = "key" + std::to_string(i);
        vs.emplace_back( key, 0.0f );
    }

    while(state.KeepRunning())
    {
        for (int i = 0; i < MAP_SIZE; ++i)
        {
            if(vs[i].name == to_find)
            {
                obj = vs[i];
                i = MAP_SIZE;
            }
        }
    }
}

BENCHMARK(bench_umap);
BENCHMARK(bench_omap);
BENCHMARK(bench_vec);

BENCHMARK_MAIN();
