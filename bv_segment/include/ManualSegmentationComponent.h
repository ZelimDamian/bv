#pragma once

#include "Component.h"
#include "Volume.h"

namespace bv {
    namespace segment {
        class ManualSegmentationComponent : public Component<ManualSegmentationComponent>
        {
        public:
            ManualSegmentationComponent(Id id, Entity::Id entity, volume::VolumeBase* vol);

            static void init();
            static void allocate();
			static void update();

            // initiate chain creation
            void startChain();
            // clear current chain
            void clearChain();

            void attachGUI();
            static std::string title() { return "Manual segmentation"; }

        private:
            volume::VolumeBase* m_volume;
        };
    }
}
