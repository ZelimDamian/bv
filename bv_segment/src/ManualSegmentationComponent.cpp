#include "ManualSegmentationComponent.h"

#include "CameraSystem.h"
#include "MouseSystem.h"
#include "RenderingSystem.h"
#include "Entity.h"
#include "RenderMesh.h"
#include "MeshFactory.h"
#include "Gui.h"
#include "Intersection.h"
#include "Shader.h"
#include "Texture.h"

using namespace bv;
using namespace bv::segment;
using namespace bv::rendering;
using namespace bv::volume;
using namespace bv::gui;

// collection of collection of points for each slice of the volume
std::vector<std::vector<glm::vec3>> _chains;

static Mesh::Id _quadId;
static Mesh::Id _chainId;
static RenderMesh::Id _quadRenderId;
static RenderMesh::Id _chainRenderId;

// current z level of the volume
static float _slice;
static int _sliceIndex = 1;
static int _oldSliceIndex = 0;

// point dragging
static int _selectedPointIndex = -1;
static std::vector<int> _history;

// shader to draw the preview quad
static Shader::Id _shader;

// uniform of the texture
static Uniform::Id uVolTex;
static Uniform::Id uSlice;
static Uniform::Id uColor;

static std::vector<VolumeBase*> _volumes;
static std::vector<Texture::Id> _textures;

static glm::vec4 _linesColor = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);

ManualSegmentationComponent::ManualSegmentationComponent(Id id, Entity::Id entity, VolumeBase* volume) : m_volume(volume), ComponentBase(id, entity)
{
    _volumes.push_back(volume);

    Texture::Id volTex = Texture::create3D(volume->width(), volume->height(), volume->depth(), TextureType::R8);
    _textures.push_back(volTex);
    Texture::update3D(volTex, volume->data());

    for (int i = 0; i < volume->depth(); ++i)
    {
        _chains.emplace_back();
    }
}

// update the mesh representing the current chain
void updateMesh()
{
    // clear the mesh
    _chainId->clear();

    std::vector<glm::vec3>& chain = _chains[_sliceIndex];

    for (int i = 0; i < chain.size(); ++i)
    {
        _chainId->vertex(chain[i]);
    }

    _chainRenderId = RenderMesh::create(_chainId);
}

// returns index of the clicked point
// if no point is clicked returns -1
int findClickedChainPointIndex(const glm::vec3& pos)
{
    const float threshold = 0.005f;

    std::vector<glm::vec3>& chain = _chains[_sliceIndex];

    // check if the click was nearby an existing point in the chain
    for (int i = 0; i < chain.size(); ++i)
    {
        if(glm::distance(chain[i], pos) < threshold)
        {
            return i;
        }
    }

    return -1;
}

int findClickedChainSegmentIndex(const glm::vec3& pos)
{
    std::vector<glm::vec3>& chain = _chains[_sliceIndex];

    // make sure we have at least a single segment in the chain
    if(chain.size() > 1)
    {
        const float threshold = 0.005f;

        // check if the click was nearby an existing point in the chain
        for (int i = 1; i < chain.size(); ++i)
        {
            float distanceToSegment = Intersection::distanceToSegment(chain[i], chain[i-1], pos);
            if(distanceToSegment < threshold)
            {
                return i - 1;
            }
        }

    }

    return -1;
}

void addPointToChain(const glm::vec3& pos)
{
    // push the point into the current chain
    std::vector<glm::vec3>& chain = _chains[_sliceIndex];
    chain.push_back(pos);

    // update the mesh
    updateMesh();
}

void moveSelectedChainPoint()
{
    auto mouse = MouseSystem::instance();

    auto& chain = _chains[_sliceIndex];

    if(mouse->buttonInScene(MouseButton::Left) &&
            _selectedPointIndex < chain.size() && _selectedPointIndex >= 0 )
    {
        Ray ray = CameraSystem::instance()->ray(mouse->pos());
        Plane plane { glm::vec3(), glm::vec3(0.0f, 0.0f, 1.0f)};

        glm::vec3 pos = Intersection::hit(ray, plane);

        Ray oldRay = CameraSystem::instance()->ray(mouse->posOld());
        glm::vec3 oldPos = Intersection::hit(oldRay, plane);

        glm::vec3 translation = pos - oldPos;
        chain[_selectedPointIndex] += translation;
		updateMesh();
    }
}

void ManualSegmentationComponent::init()
{
    // create a small quad to draw textures on
    _quadRenderId = RenderMesh::create(MeshFactory::quad(1.0f, 1.0f));
    // create a mesh to store the chain
    _chainId = Mesh::create();

    // load a shader to draw the volume slice
    _shader = Shader::find("volumeSlice");
    assert(_shader);

    // link the uniform to teh shader
    uVolTex = Uniform::require(_shader, "uVolTex", UniformType::Int);
    uSlice = Uniform::require(_shader, "uSlice", UniformType::Float);
    uColor = Uniform::require(Shader::find("colors"), "uColor", UniformType::Vec4);

    // mouse system instance
    auto mouse = MouseSystem::instance();

    // subscribe to mouse's left click event
    mouse->onleft([&, mouse]()
    {
        Ray ray = CameraSystem::instance()->ray(mouse->pos());
        Plane plane { glm::vec3(), glm::vec3(0.0f, 0.0f, 1.0f)};

        glm::vec3 pos = Intersection::hit(ray, plane);
        float offset = 0.001f;
        pos.z += offset;

        _selectedPointIndex = findClickedChainPointIndex(pos);
        if(_selectedPointIndex  == -1)
        {
            // check if we clicked on a segment
            int clickedSegmentIndex = findClickedChainSegmentIndex(pos);
            if(clickedSegmentIndex != -1)
            {
                // add the pos into the chain at the clicked index
                auto& chain = _chains[_sliceIndex];
                chain.insert(chain.begin() + clickedSegmentIndex + 1, pos);
                // mark last added index for removal on right click
                _history.push_back(clickedSegmentIndex + 1);
            }
            else
            {
                // we clicked on an empty space
                // add the point to the index of the list
                addPointToChain(pos);
                // mark the added index as the last
                _history.push_back((int) _chains[_sliceIndex].size() - 1);
            }

			updateMesh();
        }
    });

    mouse->onright([&, mouse]
    {
        auto& chain = _chains[_sliceIndex];
        Ray ray = CameraSystem::instance()->ray(mouse->pos());
        Plane plane { glm::vec3(), glm::vec3(0.0f, 0.0f, 1.0f)};

        glm::vec3 pos = Intersection::hit(ray, plane);
        float offset = 0.001f;
        pos.z += offset;
        int clickedIndex = findClickedChainPointIndex(pos);

        if(clickedIndex != -1)
        {
            chain.erase(chain.begin() + clickedIndex);
			updateMesh();
		}

//        if(chain.size())
//        {
//            int index = _history.back();
//            _history.pop_back();
//            {
//                // otherwise remove at the last added index
//                chain.erase(chain.begin() + index);
//            }
//
//            // update mesh for rendering
//            updateMesh();
//        }
    });

    // rendering system instance
    auto renderer = RenderingSystem::instance();

    // hook into the render event
    renderer->onrender([&, renderer]
    {
        // need to sync the sliceIndex with the normalized slice value
        VolumeBase* vol = _volumes[0];
        _slice = (float)_sliceIndex / vol->depth();
        if(_sliceIndex != _oldSliceIndex)
        {
            updateMesh();
			_oldSliceIndex = _sliceIndex;
        }

        // check if we are dragging on a point
        if(_selectedPointIndex != -1)
        {
            moveSelectedChainPoint();
        }

        // attach the slicing shader
        Shader::use(_shader);

        // use an existing texture
        // TODO:: not some assumed first texture
        Texture::Id volTex = _textures[0];

        // use the volume texture
        Texture::use(volTex, uVolTex);

        // the normalized float value of the slice
        uSlice->set(_slice);

        // reset the rendering system
        renderer->reset();

        // perform render call
	    renderer->submit(_quadRenderId);

        // use the colors shader without lighting
        Shader::use("colors");
        uColor->set(_linesColor);
		
        // render the lines
        renderer->reset(Culling::CW, RenderMode::LINE_STRIP);
	    renderer->submit(_chainRenderId);

        // render the points
        renderer->reset(Culling::CW, RenderMode::POINTS);
	    renderer->submit(_chainRenderId);
    });
}

void ManualSegmentationComponent::attachGUI()
{
    auto _panel = gui();

    int id = (int) m_id;

    // hook the value for slice into the gui
    _panel->bind("Slice", _sliceIndex, 1, _volumes[id]->depth() - 1);
    _panel->button("Clear", [&, id]
    {
        _chains[_sliceIndex].clear();
        updateMesh();
    });
    // button to copy the current chain as the next slice's chain
    _panel->button("Copy to next", [&, id]
    {
        if(_sliceIndex < _volumes[id]->depth() - 1)
        {
            _sliceIndex++;
            _chains[_sliceIndex] = _chains[_sliceIndex - 1];
        }
    });
    // button to copy the current chain as the previous slice's chain
    _panel->button("Copy to prev", [&, id]
    {
        if(_sliceIndex > 1)
        {
            _sliceIndex--;
            _chains[_sliceIndex] = _chains[_sliceIndex + 1];
        }
    });
}


void ManualSegmentationComponent::allocate()
{
}

void ManualSegmentationComponent::update()
{
}

void ManualSegmentationComponent::startChain()
{

}

void ManualSegmentationComponent::clearChain()
{
    _chains[_sliceIndex].clear();
    updateMesh();
}
