#include "EhbContactComponent.h"
#include <Timing.h>

#include "CollisionSystem.h"
#include <RigidBodyComponent.h>
#include "UpdateSystem.h"

#include "MeshContact.h"
#include "UtilitiesColDet.h"

using namespace bv;
using namespace physics;
using namespace coldet;

void EhbContactComponent::initialize()
{
    auto collider1 = Collider::find(m_entity);

    if(!!collider1)
    {
        auto collider2 = Collider::find(m_pelvis);

        if(!!collider2)
        {
            auto onupdate = [this]
            {
                if(this->m_active)
                {
                    update();
                }
            };

	        UpdateSystem::instance()->onupdate(onupdate);
        }
        else
        {
            Log::error("There was no collider attached to master (pelvis) in " + title());
        }
    }
    else
    {
        Log::error("There was no collider attached to slave (skull) in " + title());
    }
}


void EhbContactComponent::update()
{
	auto collider1 = Collider::find(m_entity);
	auto collider2 = Collider::find(m_pelvis);
	auto rigidBody = RigidBodyComponent::find(m_entity);

	auto result = CollisionSystem::instance()->contact(collider1, collider2);
	auto contact = result.contact;

	if(contact && contact->isFound())
	{
		if(const MeshContact* meshContact = dynamic_cast<const MeshContact*>(contact))
		{
			auto info = meshContact->info();

			for (int i = 0; i < info->penetrations.size(); ++i)
			{
				float penetration = info->penetrations[i];
				if(penetration > 0.0f && penetration < 0.01f)
				{
					// calculate contact center point
					const glm::vec3 contactPoint = info->slaveVertPositions[i];

					// calculate average skull normal
					const glm::vec3 contactNormal = -info->slaveContactNormals[i];

					const glm::vec3 separation = penetration * contactNormal;

					// push away out of contact
//			rigidBody->applyImpulse(rigidBody->mass() * separation / Timing::delta());
					const glm::vec3 penaltyForce = m_penalty * separation;

					const glm::vec3 pivotPoint = m_entity->pos();

					// find the vector connecting contact point with skull pivot point
					const glm::vec3 shoulder = contactPoint - pivotPoint;

					// the expulsion force acting on the skull
					const glm::vec3 force = rigidBody->forceOld();

					// find the relative velocity at point of contact

					const glm::vec3 normalComponent = contactNormal * glm::dot(contactNormal, penaltyForce);
					const glm::vec3 tangentComponent = penaltyForce - normalComponent;

					const float normalMagnitude = normalComponent.length();
					const glm::vec3 frictionForce = tangentComponent * (normalMagnitude * m_frictionCoeff) / normalMagnitude;
					const glm::vec3 tangentialReaction = -frictionForce;

					rigidBody->applyForce(penaltyForce, contactPoint);

//					const glm::vec3 reaction = normalComponent + tangentialReaction;
//					const glm::vec3 moment = glm::cross(shoulder, reaction);
        //			rigidBody->applyForce(reaction, rigidBody->pos() - contactPoint);
				}
			}

			// apply restitution
			rigidBody->scaleVelocity(m_restitutionCoeff);
		}
		else
		{
			Log::error("Wrong type of contact in EHB Contact");
		}
	}
}

void EhbContactComponent::attachGUI()
{
	auto panel = this->gui();

	panel->bind("Friction", m_frictionCoeff, 0.0f, 1.0f, 0.01f);
	panel->bind("Restitution", m_restitutionCoeff, 0.0f, 1.0f, 0.01f);
	panel->bind("Penalty", m_penalty, 0.0f, 100000.0f, 100.0f);
}

