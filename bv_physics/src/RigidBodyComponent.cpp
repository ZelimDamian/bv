#include "RigidBodyComponent.h"
#include "RenderingSystem.h"
#include <Timing.h>

#include "UtilitiesColDet.h"

using namespace bv;
using namespace physics;

static std::vector<glm::vec3> _origPositions;
static std::vector<glm::quat> _origOrientations;
static std::vector<glm::vec3> _velocitiess;
static std::vector<glm::vec3> _accelerations;
static std::vector<glm::vec3> _rotations;
static std::vector<glm::vec3> _angAccels;

static std::vector<glm::vec3> _forces;
static std::vector<glm::vec3> _forces_old;
static std::vector<float> m_invMasses;
static std::vector<float> m_dampings;
static std::vector<float> m_rotDampings;

glm::vec3 RigidBodyComponent::s_gravity = glm::vec3(0.0f, 0.0f, 0.0f);

RigidBodyComponent::RigidBodyComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
{
	_origPositions.push_back(entity->pos());
	_origOrientations.push_back(entity->orientation());
	_velocitiess.emplace_back();
	_accelerations.emplace_back();
	_rotations.emplace_back();
	_angAccels.emplace_back();
	_forces.emplace_back();
	_forces_old.emplace_back();
	m_invMasses.push_back(1.0f);
	m_dampings.push_back(3650.0f);
	m_rotDampings.push_back(1.0f);
}

void RigidBodyComponent::applyImpulse(const glm::vec3 &impulse)
{
	_velocitiess[(int)m_id] += impulse * m_invMasses[(int)m_id];
}

void RigidBodyComponent::applyForce(const glm::vec3& force)
{
	_forces[m_id.id] += force;
}

void RigidBodyComponent::applyForce(const glm::vec3& force, const glm::vec3& point)
{
	_forces[m_id.id] += force;

	if (glm::length2(point) > FLT_EPSILON)
	{
		glm::vec3 torque = glm::cross(point, force);
		applyTorque(torque);
	}
}

void RigidBodyComponent::applyForceGlobal(const glm::vec3& force, const glm::vec3& point)
{
	// apply the translational component
	applyForce(force);

	// calculate the torque generated
//	const glm::mat4 xform = m_entity->transform();
	const glm::vec3 local = point - this->pos();//Math::transform(point, glm::inverse(xform));

	if (glm::length2(local) > FLT_EPSILON)
	{
		glm::vec3 torque = glm::cross(local, force);
		applyTorque(torque);
	}
}

void RigidBodyComponent::applyTorque(const glm::vec3& torque)
{
	_angAccels[(int)m_id] += torque * m_invMasses[(int)m_id];
}

void RigidBodyComponent::scaleVelocity(float restitution)
{
	_velocitiess[(int)m_id] *= restitution;
}

glm::vec3 RigidBodyComponent::acceleration() const
{
	return _accelerations[(int)m_id];
}

glm::vec3 RigidBodyComponent::velocity() const
{
	return _velocitiess[(int)m_id];
}

glm::vec3 RigidBodyComponent::force() const
{
	return _forces[(int)m_id];
}

glm::vec3 RigidBodyComponent::forceOld() const
{
	return _forces_old[(int)m_id];
}

float RigidBodyComponent::mass() const
{
	return 1.0f / m_invMasses[(int)m_id];
}

void RigidBodyComponent::mass(float mass)
{
	m_invMasses[(int)m_id] = 1.0f / mass;
}

RigidBodyState RigidBodyComponent::state() const
{
	return {
			pos(),
	        velocity(),
	        acceleration(),
	        m_entity->orientation(),
	        _rotations[(int)m_id],
	        _angAccels[(int)m_id]
	};
}

void RigidBodyComponent::state(const RigidBodyState& state)
{
	pos(state.position);
	_velocitiess[(int)m_id] = state.velocity;
	_accelerations[(int)m_id] = state.acceleration;
	m_entity->orientation(state.orientation);
	_rotations[(int)m_id] = state.rotation;
	_angAccels[(int)m_id] = state.angularAcceleration;
}

void RigidBodyComponent::reset()
{
	_velocitiess[(int)m_id] = glm::vec3();
	_forces[(int)m_id] = glm::vec3();
	_forces_old[(int)m_id] = glm::vec3();
	_accelerations[(int)m_id] = glm::vec3();
	_rotations[(int)m_id] = glm::vec3();
	_angAccels[(int)m_id] = glm::vec3();
	m_entity->pos(_origPositions[(int)m_id]);
	m_entity->orientation(_origOrientations[(int)m_id]);
}

float RigidBodyComponent::damping() const
{
	return m_dampings[(int)m_id];
}

void RigidBodyComponent::damping(float damping)
{
	m_dampings[(int)m_id] = damping;
}

float RigidBodyComponent::rotDamping() const
{
	return m_rotDampings[(int)m_id];
}

void RigidBodyComponent::rotDamping(float damping)
{
	m_rotDampings[(int)m_id] = damping;
}

void RigidBodyComponent::init()
{
	using namespace rendering;

	UpdateSystem::instance()->onupdate([&] { update(); } );
}

glm::quat quatIntegrate(const glm::quat& q, const glm::vec3& omega, float deltaT)
{
	glm::quat deltaQ;
	glm::vec3 theta = omega * deltaT * 0.5f;
	float thetaMagSq = glm::length2(theta);
	float s;
	if(thetaMagSq * thetaMagSq / 24.0f < std::numeric_limits<float>::min())
	{
		deltaQ.w = 1.0f - thetaMagSq / 2.0f;
		s = 1.0f - thetaMagSq / 6.0f;
	}
	else
	{
		float thetaMag = glm::sqrt(thetaMagSq);
		deltaQ.w = glm::cos(thetaMag);
		s = glm::sin(thetaMag) / thetaMag;
	}
	deltaQ.x = theta.x * s;
	deltaQ.y = theta.y * s;
	deltaQ.z = theta.z * s;
	return deltaQ * q;
}

void RigidBodyComponent::update()
{
	for (int i = 0; i < count(); ++i)
	{
		if(components(i).isActive())
		{
			glm::vec3  pos = components()[i].pos();
			glm::vec3& vel = _velocitiess[i];
			glm::vec3& acc = _accelerations[i];

			const glm::vec3& F = _forces[i];
			const float invM = m_invMasses[i];

			float dt = Timing::delta();

			// update acceleration
			acc = s_gravity;
			acc += invM * F;

			////////////////////////////////////////////
			// Semi-Implicit Euler
			////////////////////////////////////////////

			////// Position /////////
			{
				// integrate velocity
				vel += acc * dt;

				// apply damping
				vel /= 1 + m_dampings[i] * dt * m_invMasses[i];

				// integrate position
				pos += vel * dt;

				// update entity position
				components(i).pos(pos);
			}

			////// Orientation /////////
			{
				glm::vec3& angAcc = _angAccels[i];
				glm::vec3& rot = _rotations[i];
				glm::quat orientation = components(i).entity()->orientation();

				// integrate angular velocity
				rot += angAcc * dt;
				// apply rotational damping
				rot /= 1 + m_rotDampings[i] * dt * m_invMasses[i];

				// only do the expensive math if there is any rotation
				if(glm::length2(rot) > FLT_EPSILON)
				{
					// perform orientation integration
					orientation = quatIntegrate(orientation, rot, dt);
					// update entity orientation
					components(i).m_entity->orientation(orientation);
				}
			}
		}

		// clear
		_forces_old[i] = _forces[i];
		_forces[i] = glm::vec3();
		_angAccels[i] = glm::vec3();
	}
}

void RigidBodyComponent::attachGUI()
{
	auto panel = ComponentBase::gui();

	panel->button("Reset", [&] { reset(); });

	panel->bind<float>("Timestep",
					   [&] { return Timing::delta(); },
					   [&] (const float& value) { Timing::delta() = value; },
					   0.0001f, 1.0f, 0.0001f);
	panel->bind<glm::vec3>("Gravity",
	                       [&] { return RigidBodyComponent::s_gravity; },
	                       [&] (const glm::vec3& value) { RigidBodyComponent::s_gravity = value; },
	                       -100.0f, 100.0f, 0.1f);
	panel->bind<float>("Mass",
	                   [&] { return m_id->mass(); },
	                   [&] (const float& value) { m_id->mass(value); },
	                   0.001f, 5.0f, 0.001f);
	panel->bind("Damping", m_dampings[(int)m_id], 0.0f, 15.0f, 0.1f);
	panel->bind("RotDamping", m_rotDampings[(int)m_id], 0.0f, 15.0f, 0.1f);

	panel->bind<glm::vec3>("Velocity",
	                       [&] { return velocity(); }, [&] (const glm::vec3& value) {},
	                       -100.0f, 100.0f, 0.1f);
}
