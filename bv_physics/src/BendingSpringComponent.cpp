#include "BendingSpringComponent.h"

#include "RigidBodyComponent.h"
#include <DebugMesh.h>

using namespace bv;
using namespace physics;

void BendingSpringComponent::update()
{
    for (int i = 0; i < m_springs.size(); ++i)
    {
        auto& spring = m_springs[i];

        spring.startXform(m_entity->transform());
        spring.endXform(m_others[i]->transform());

	    // calculate the force
        auto force = spring.force();

	    auto globalStart = spring.globalStart();
	    auto globalEnd = spring.globalEnd();

	    // apply the force to current body
        auto rigidbody = require<RigidBodyComponent>();
        rigidbody->applyForceGlobal(force, globalStart);

	    // apply the inverted force to the other body
        auto otherRigidBody = RigidBodyComponent::find(m_others[i]);
        otherRigidBody->applyForceGlobal(-force, globalEnd);

		auto torque = spring.torsion();

		// apply torque
		rigidbody->applyTorque(torque);
		otherRigidBody->applyTorque(-torque);

	    // draw the spring as a line
	    {
//		    auto debug = DebugMesh::instance();
//		    debug->point(globalStart);
//		    debug->point(globalEnd);
//		    debug->render();
	    }
    }
}

void BendingSpringComponent::link(const glm::vec3 localStart, Entity::Id other, const glm::vec3 localEnd, float k,
								  float kTorsion)
{
    auto spring = BendingSpring(k, localStart, localEnd, m_entity->transform(), other->transform());
	spring.kTorsion(kTorsion);
    m_springs.push_back(spring);
	m_others.push_back(other);
}

void BendingSpringComponent::attachGUI()
{
	auto panel = this->gui();

	auto subpanel = panel->panel("Props");
	auto fillsubpanel = [=](int index)
	{
		subpanel->removeAll();
		subpanel->bind<float>("K", [=] { return m_springs[index].k(); }, [=] (float k) { m_springs[index].k(k); });
		subpanel->bind<float>("KTorsion", [=] { return m_springs[index].kTorsion(); }, [=] (float k) { m_springs[index].kTorsion(k); });
		subpanel->bind<float>("Torsion limit", [=] { return m_springs[index].torsionLimit(); }, [=] (float k) { m_springs[index].torsionLimit(k); });
	};

	if(!m_springs.empty())
	{
		fillsubpanel(0);
	}

	std::vector<std::string> strings(m_springs.size());

	int n = 0;
	std::generate(strings.begin(), strings.end(), [&n]() { return std::to_string(n++); });

	auto list = panel->combo("Springs", strings, fillsubpanel);
}

