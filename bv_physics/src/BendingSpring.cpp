#include "BendingSpring.h"

using namespace bv;
using namespace physics;

BendingSpring::BendingSpring(float k, const glm::vec3 &localStart, const glm::vec3 &localEnd,
                             const glm::mat4 &startXform, const glm::mat4 &endXform, float originalLength) :
		m_k(k), m_localStart(localStart), m_localEnd(localEnd), m_startXform(startXform), m_endXform(endXform),
		m_originalLength(originalLength), m_kBending(0.0f) { }

BendingSpring::BendingSpring() {}

BendingSpring::BendingSpring(float k, const glm::vec3 &localStart, const glm::vec3 &localEnd,
                             const glm::mat4 &startXform, const glm::mat4 &endXform) : BendingSpring(k, localStart, localEnd, startXform, endXform, 0.0f)
{
	auto vector = globalEnd() - globalStart();
	m_originalLength = glm::length(vector);

	if(m_originalLength < FLT_EPSILON)
	{
		m_originalDirectionLocal = glm::normalize(m_localStart);
	}
	else
	{
		m_originalDirectionLocal = Math::transformNormal(vector / m_originalLength, glm::inverse(m_startXform));
	}
}

glm::vec3 BendingSpring::force() const
{
    glm::vec3 start = globalStart();
    glm::vec3 end = globalEnd();

    glm::vec3 springVector = end - start;

    float springLength = glm::length(springVector);

	if (springLength > FLT_EPSILON)
	{
		float x = springLength - m_originalLength;
		return x * m_k * springVector / springLength;
	}
	else
	{
		return glm::vec3();
	}
}

glm::vec3 BendingSpring::vector() const
{
	return globalEnd() - globalStart();
}

glm::vec3 BendingSpring::globalOriginalDirection() const
{
	return Math::transformNormal(m_originalDirectionLocal, m_startXform);
}

glm::vec3 BendingSpring::direction() const
{
	return glm::normalize(vector());
}

glm::vec3 BendingSpring::torsion() const
{
	glm::vec3 x { 1.0f, 0.0f, 0.0f };

	glm::vec3 one = Math::transformNormal(x, m_startXform);
	glm::vec3 two = Math::transformNormal(x, m_endXform);

	float angle = Math::angleBetween(one, two);
	float limit = glm::radians(m_torsionLimitDeg);
	if(std::abs(angle) > limit)
	{
		auto axis = glm::normalize(glm::cross(one, two));
		auto croppedAngle = (glm::abs(angle) - limit);
		auto torque = croppedAngle * m_kTorsion * axis;
		return m_originalDirectionLocal * glm::dot(m_originalDirectionLocal, torque);
	}
	else
	{
		return glm::vec3();
	}
}

glm::vec3 BendingSpring::globalEnd() const
{
    glm::vec3 globalEnd = Math::transform(m_localEnd, m_endXform);
    return globalEnd;
}

glm::vec3 BendingSpring::globalStart() const
{
    glm::vec3 globalStart = Math::transform(m_localStart, m_startXform);
    return globalStart;
}

