#include "ExpulsionComponent.h"

#include <StateLoggerComponent.h>
#include "RigidBodyComponent.h"

#include "UtilitiesColDet.h"

#include "Timing.h"
#include "FileWriter.h"

using namespace bv;
using namespace physics;
using namespace coldet;

void ExpulsionComponent::initialize()
{
    auto onupdate = [this] ()
    {
        if(m_active)
        {
            this->update();
        }
    };

	UpdateSystem::instance()->onupdate(onupdate);
}

float magnitude(float period, float minForce, float maxForce, float time, float duration)
{
	const float progress = std::min(time / duration, 1.0f);
    const float factor = (std::sin(time * (float)M_PI / period) + 1.0f) / 2.0f;
	const float amplitude = maxForce - minForce;
    return minForce +  progress * factor * amplitude;
}

glm::vec3 ExpulsionComponent::expulsion() const
{
	// find the direction of the skull
	const glm::vec3 direction = Math::transformNormal(glm::vec3(0.0f, -1.0f, 0.0f), m_entity->transform());

	return direction * magnitude(m_period, m_minForce, m_maxForce, m_time, m_duration);
}

void ExpulsionComponent::update()
{
    auto rigidBody = require<RigidBodyComponent>();

	// force application point in global coordinate system
	auto globalApplicationPoint = Math::transform(m_applicationPoint, m_entity->transform());

	// calculate the expulsion force
	auto expulsion = this->expulsion();
	m_target->pos(m_entity->pos() + expulsion);

	// apply the expulsion to the rigidbody
	rigidBody->applyForceGlobal(expulsion, globalApplicationPoint);

	auto toVertical = m_entity->pos() + m_applicationPoint - globalApplicationPoint;
	auto toVerticalHor = toVertical - glm::normalize(m_applicationPoint) * glm::dot(toVertical, glm::normalize(m_applicationPoint));
	auto unbendingForce = toVerticalHor * m_bendK;

	// apply force to keep trunk upwards
	rigidBody->applyForceGlobal(unbendingForce, globalApplicationPoint);

	// rendering
	{
//		DebugMesh::instance()->point(globalApplicationPoint);
//		DebugMesh::instance()->point(m_target->pos());
//		DebugMesh::instance()->point(globalApplicationPoint);
//		DebugMesh::instance()->point(globalApplicationPoint + toVerticalHor);
//		auto color = glm::vec4(0.9f, 0.4f, 0.0f, 1.0f);
//		DebugMesh::instance()->render(glm::mat4(), RenderMode::LINES, color);
	}

	m_time += Timing::delta();
}

void ExpulsionComponent::attachGUI()
{
    auto panel = this->gui();

    panel->bind("Period, sec", m_period, 1.0f, 100.0f );
	panel->bind("Duration, sec", m_duration, 1.0f, 1000.0f );
    panel->bind("Min force, N", m_minForce, -100.0f, m_maxForce );
    panel->bind("Max force, N", m_maxForce, m_minForce, 100.0f );

	panel->bind("Bending K, N/m", m_bendK, 0.0f, 10000.0f );
	panel->label("Magnitude", [this] { return std::to_string(glm::length(expulsion())); });
}

void ReactionReportingComponent::attachGUI()
{
	auto gui = this->gui();

	auto rigidbody = RigidBodyComponent::find(m_entity);
	auto expulsion = ExpulsionComponent::find(m_entity);

    if(!rigidbody || !expulsion)
    {
        return;
    }

	gui->bind<glm::vec3>("Applied force", [=]
	{
		glm::vec3 force = rigidbody->force();
		glm::vec3 exp = expulsion->expulsion();

		return force - exp;
	}, [this] (glm::vec3 force) { });

	gui->graph<float>("Reaction", [=]
	{
		glm::vec3 force = rigidbody->force();
		glm::vec3 exp = expulsion->expulsion();

		float magnitude = glm::length(force - exp);
		return magnitude;
	}, 0.0f, expulsion->maxForce());

	gui->graph<float>("Expulsion", [=]
	{
		glm::vec3 exp = expulsion->expulsion();

		float magnitude = glm::length(exp);

		return magnitude;
	}, expulsion->minForce(), expulsion->maxForce());
}

