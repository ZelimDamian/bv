#include "ContactResolverComponent.h"

#include "CollisionSystem.h"
#include "MeshContact.h"
#include "RigidBodyComponent.h"

#include "UpdateSystem.h"

using namespace bv;
using namespace physics;
using namespace coldet;

struct ContactData
{
	Collider::Id collider1, collider2;
	const MeshContact* contact;
	float penalty, friction, restitution;
};

void process(ContactData data)
{
	auto collider1 = data.collider1;
	auto collider2 = data.collider2;
	auto contact = data.contact;

	auto rigidbody1 = RigidBodyComponent::find(collider1->entity());
	auto rigidbody2 = RigidBodyComponent::find(collider2->entity());

	if (!!rigidbody1 && !!rigidbody2)
	{
		auto info = contact->info();

		for (int i = 0; i < info->penetrations.size(); ++i)
		{
			float penetration = info->penetrations[i];

			if(penetration > 0.0f && penetration < 0.01f)
			{
				// calculate contact center point
				const glm::vec3 contactPoint = info->slaveVertPositions[i];

				// calculate average skull normal
				const glm::vec3 contactNormal = -info->slaveContactNormals[i];

				const glm::vec3 separation = penetration * contactNormal;

				// push away out of contact
//                rigidBody->applyImpulse(rigidBody->mass() * separation / Timing::delta());
				const glm::vec3 penaltyForce = data.penalty * separation;

//				const glm::vec3 pivotPoint = collider1->entity()->pos();

				// find the vector connecting contact point with skull pivot point
//				const glm::vec3 shoulder = contactPoint - pivotPoint;

				// the expulsion force acting on the skull
//				const glm::vec3 force = rigidbody1->forceOld();

				// find the relative velocity at point of contact

//				const glm::vec3 normalComponent = contactNormal * glm::dot(contactNormal, penaltyForce);
//				const glm::vec3 tangentComponent = penaltyForce - normalComponent;
//
//				const float normalMagnitude = normalComponent.length();
//				const glm::vec3 frictionForce = tangentComponent * (normalMagnitude * data.friction) / normalMagnitude;
//				const glm::vec3 tangentialReaction = -frictionForce;

				rigidbody1->applyForceGlobal( penaltyForce, contactPoint);
				rigidbody2->applyForceGlobal(-penaltyForce, contactPoint);

//					const glm::vec3 reaction = normalComponent + tangentialReaction;
//					const glm::vec3 moment = glm::cross(shoulder, reaction);
				//			rigidBody->applyForce(reaction, rigidBody->pos() - contactPoint);
			}
		}

		// apply restitution
		rigidbody1->scaleVelocity(data.restitution);
	}
}

void ContactResolverComponent::init()
{
	auto onupdate = [&]
	{
		const size_t size = instances().size();

		for (int i = 0; i < size; ++i)
		{
			auto collider1 = Collider::find(s_instances[i].entity());

			// clear the list of processed pairs
			for (int j = i + 1; j < size; ++j)
			{
				auto collider2 = Collider::find(s_instances[j].entity());

				if(!!collider1 && !!collider2)
				{
					auto result = CollisionSystem::instance()->contact(collider1, collider2);
					auto contact = result.contact;

					if(auto meshContact = dynamic_cast<const MeshContact*>(contact))
					{
						if(meshContact->isFound())
						{
							float penalty = s_instances[i].m_penalty;
							float friction = s_instances[i].m_frictionCoeff;
							float resitution = s_instances[i].m_restitutionCoeff;

							if(result.side == ContactSide::First)
							{
								process({collider1, collider2, meshContact, penalty, friction, resitution});
							}
							else
							{
								process({collider2, collider1, meshContact, penalty, friction, resitution});
							}
						}
					}
				}
			}
		}
	};

	UpdateSystem::instance()->onupdate(onupdate);
}

void ContactResolverComponent::attachGUI()
{
	auto panel = this->gui();

	panel->bind("Penalty", m_penalty, 0.0f, 10000.0f, 1.0f);
}

