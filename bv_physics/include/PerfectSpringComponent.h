#pragma once

#include "Component.h"
#include "RigidBodyComponent.h"
#include "RenderingSystem.h"

namespace bv
{
	namespace physics
	{
		class PerfectSpringComponent: public Component<PerfectSpringComponent>
		{
		public:
			PerfectSpringComponent(Id id, Entity::Id entity, Entity::Id target):
				ComponentBase(id, entity), m_target(target)
			{

			}

			static void init()
			{
                subscribeToUpdates();
			}

			void update()
			{
				auto rigidBody = require<RigidBodyComponent>();

				float dt = Timing::delta();
				float mass = rigidBody->mass();
				float stiffness = mass / (dt * dt);

				glm::vec3 gap = (m_target->pos() - rigidBody->pos());

				glm::vec3 acceleration = rigidBody->acceleration();
				glm::vec3 impulse = mass * -acceleration * dt;
//				rigidBody->applyImpulse(impulse);

				glm::vec3 force = stiffness * gap + impulse / dt;
				rigidBody->applyForce(force);
			}

		private:
			Entity::Id m_target;
		};
	}
}