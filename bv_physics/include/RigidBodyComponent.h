#pragma once

#include "Component.h"
#include "RigidBody.h"

namespace bv
{
	namespace physics
	{
		class RigidBodyComponent: public Component<RigidBodyComponent>
		{
		public:
			RigidBodyComponent(Id id, Entity::Id entity);

			glm::vec3 pos() const { return m_entity->pos(); }
			void pos(const glm::vec3& pos) { m_entity->pos(pos); }

			void reset();

			void attachGUI();

			void applyImpulse(const glm::vec3& impulse);
			void applyForce(const glm::vec3& force);
			void applyForce(const glm::vec3& force, const glm::vec3& point);
			void applyForceGlobal(const glm::vec3& force, const glm::vec3& point);

			void applyTorque(const glm::vec3& torque);
			void scaleVelocity(float restitution);

			float mass() const;
			void mass(float mass);
			glm::vec3 force() const;
			glm::vec3 forceOld() const;
			glm::vec3 acceleration() const;
			glm::vec3 velocity() const;

			RigidBodyState state() const;
			void state(const RigidBodyState& state);

			static void init();

			static glm::vec3& gravity() { return s_gravity; }

			float damping() const;
			void damping(float dapming);

			float rotDamping() const;
			void rotDamping(float rotDamping);

		private:

			static void update();

			static glm::vec3 s_gravity;
		};
	}
}