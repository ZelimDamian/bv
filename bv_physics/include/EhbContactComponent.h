#pragma once

#include <Component.h>

namespace bv
{
    namespace physics
    {
        class EhbContactComponent: public Component<EhbContactComponent>
        {
        public:
            EhbContactComponent(Id id, Entity::Id entity, Entity::Id other):
                    ComponentBase(id, entity), m_pelvis(other)
            {

            }

	        void update();

            void initialize();

	        void attachGUI();

        private:
            Entity::Id m_pelvis;

	        float m_frictionCoeff { 0.5f };
	        float m_restitutionCoeff { 0.8f };
	        float m_penalty { 10000.0f };
        };
    }
}