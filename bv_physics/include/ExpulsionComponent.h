#pragma once

#include "Component.h"
#include "DebugMesh.h"
#include "RigidBodyComponent.h"

namespace bv
{
    namespace physics
    {
        class ExpulsionComponent: public Component<ExpulsionComponent>
        {
        public:
            ExpulsionComponent(Id id, Entity::Id entity, Entity::Id target, const glm::vec3& applicationPoint):
					ComponentBase(id, entity), m_target(target), m_applicationPoint(applicationPoint) { }

			ExpulsionComponent(Id id, Entity::Id entity, Entity::Id target):
					ExpulsionComponent(id, entity, target, glm::vec3{0.0f}) { }

            void initialize();

            void update();

            void attachGUI();

            float minForce() const { return m_minForce; }
            void minForce(float minForce) { m_minForce = minForce; }

            float maxForce() const { return m_maxForce; }
            void maxForce(float maxForce) { m_maxForce = maxForce; }

            float period() const { return m_period; }
            void period(float period) { m_period = period; }

	        glm::vec3 expulsion() const;

        private:
            float m_minForce { 0.0f };
            float m_maxForce { 0.1f };

			glm::vec3 m_applicationPoint;

            Entity::Id m_target;

            float m_period { 10.0f };
	        float m_time { 0.0f };
	        float m_duration { 200.0f };
	        float m_bendK { 1000.0f };
        };

        class ExpulsionTargetComponent: public Component<ExpulsionTargetComponent>
        {
        public:
            ExpulsionTargetComponent(Id id, Entity::Id entity): ComponentBase(id, entity) { }
        };

	    class ReactionReportingComponent: public Component<ReactionReportingComponent>
	    {
	    public:
		    ReactionReportingComponent(Id id, Entity::Id entity): ComponentBase(id, entity) { }

			void attachGUI();
	    };
    }
}