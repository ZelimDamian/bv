#pragma once

#include "Component.h"

namespace bv
{
	namespace physics
	{
		class ContactResolverComponent: public Component<ContactResolverComponent>
		{
		public:
			ContactResolverComponent(Id id, Entity::Id entity): ComponentBase(id, entity) { }

			static void init();

			void attachGUI();

			float penalty() const { return m_penalty; }
			void penalty(float penalty) { m_penalty = penalty; }

		private:
			float m_frictionCoeff { 0.5f };
			float m_restitutionCoeff { 0.8f };
			float m_penalty { 3000.0f };

		};
	}
}

