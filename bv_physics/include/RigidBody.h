#pragma once

#include "Common.h"

namespace bv
{
	namespace physics
	{
		struct RigidBodyState
		{
			glm::vec3 position;
			glm::vec3 velocity;
			glm::vec3 acceleration;

			glm::quat orientation;
			glm::vec3 rotation;
			glm::vec3 angularAcceleration;
		};
	}
}