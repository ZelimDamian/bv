#pragma once

#include "Component.h"
#include "BendingSpring.h"

namespace bv
{
    namespace physics
    {
        class BendingSpringComponent: public Component<BendingSpringComponent>
        {
        public:
            BendingSpringComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
            {

            }

            static void init()
            {
                subscribeToUpdates();
            }

            void link(const glm::vec3 localStart, Entity::Id other, const glm::vec3 localEnd, float k, float kTorsion = 0.0f);

            void update();

	        void attachGUI();

        private:
            std::vector<BendingSpring> m_springs;
            std::vector<Entity::Id> m_others;
        };
    }
}