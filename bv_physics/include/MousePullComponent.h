#pragma once

#include "Component.h"
#include "RigidBodyComponent.h"
#include "MouseSystem.h"
#include "KeyboardSystem.h"
#include "PickingSystem.h"

namespace bv
{
	namespace physics
	{
		class MousePullComponent: public Component<MousePullComponent>
		{

			enum class State
			{
				Pick,
				Drag
			};

			State m_state { State::Pick };
			glm::vec3 m_start;

		public:
			MousePullComponent(Id id, Entity::Id entity): ComponentBase(id, entity) {}

			void initialize()
			{
				auto mouse = MouseSystem::instance();

				mouse->onleft([this, mouse]
				{
					if(m_state == State::Pick)
					{
						using namespace coldet;

						auto hits = PickingSystem::instance()->cache();

						auto it = std::find_if(std::begin(hits), std::end(hits), [this](const PickingSystem::Hit &hit)
						{
							return hit.entity == m_entity;
						});

						if (it != std::end(hits))
						{
							auto ray = it->ray;
							const glm::vec3 hitPos = Ray::transform(ray, glm::inverse(m_entity->transform())).point(it->t);
							m_start = hitPos;
							m_state = State::Drag;
						}
					}
				});
			}

			void update()
			{
				auto keyboard = KeyboardSystem::instance();
				auto mouse = MouseSystem::instance();
				auto cameraSystem = CameraSystem::instance();

				auto now = cameraSystem->ray(mouse->pos());

				if(keyboard->key(Key::F) && m_state == State::Drag)
				{
					if(mouse->button(MouseButton::Left))
					{
						const glm::vec3 start = Math::transform(m_start, m_entity->transform());
						const Plane plane { start, - now.dir() };
						const glm::vec3 end = Intersection::hit(now, plane);
						auto translation = end - start;

						auto rb = require<RigidBodyComponent>();

						if(rb)
						{
							auto force = translation * m_k;
							rb->applyForceGlobal(force, start);

							auto debug = DebugMesh::instance();
							debug->point(start);
							debug->point(end);
							debug->render();
						}
						else
						{
							EntitySystem::move(m_entity, translation);
						}
					}
				}
				else
				{
					m_state = State::Pick;
				}
			}

			static void init()
			{
				subscribeToUpdates();
			}

			float m_k { 1000.0f };
		};
	}
}

