#pragma once

#include "MathLib.h"

namespace bv
{
    namespace physics
    {
        struct BendingSpring
        {
        public:

            BendingSpring();

            BendingSpring(float k, const glm::vec3 &localStart, const glm::vec3 &localEnd, const glm::mat4 &startXform,
                          const glm::mat4 &endXform);

            BendingSpring(float k, const glm::vec3 &localStart, const glm::vec3 &localEnd, const glm::mat4 &startXform,
                          const glm::mat4 &endXform, float originalLength);

            glm::vec3 force() const;
            glm::vec3 torsion() const;

            // normalized direction of the spring in globa coords
            glm::vec3 direction() const;

            // vector connecting start and end in global coords
            glm::vec3 vector() const;

            // original direction in global coords
            glm::vec3 globalOriginalDirection() const;

            float k() const { return m_k; }
            void k(float k) { m_k = k; }

            float kTorsion() const { return m_kTorsion; }
            void kTorsion(float kTorsion) { m_kTorsion = kTorsion; }


            float torsionLimit() const { return m_torsionLimitDeg; }
            void torsionLimit(float torsionLimitDeg) { m_torsionLimitDeg = torsionLimitDeg; }

            const glm::vec3 &localStart() const { return m_localStart; }
            void localStart(const glm::vec3 &localStart) { m_localStart = localStart; }

            const glm::vec3 &localEnd() const { return m_localEnd; }
            void localEnd(const glm::vec3 &localEnd) { m_localEnd = localEnd; }

            const glm::mat4 &endXform() const { return m_endXform; }
            void endXform(const glm::mat4 &endXform) { m_endXform = endXform; }

            const glm::mat4 &startXform() const { return m_startXform; }
            void startXform(const glm::mat4 &startXform) { m_startXform = startXform; }

	        glm::vec3 globalStart() const;
	        glm::vec3 globalEnd() const;

        private:
            float m_k;
            float m_kTorsion;
            float m_kBending;

            float m_torsionLimitDeg { 45.0f };

            glm::vec3 m_localStart;
            glm::vec3 m_localEnd;

            glm::mat4 m_startXform;
            glm::mat4 m_endXform;

            float m_originalLength;
            glm::vec3 m_originalDirectionLocal;
        };
    }
}