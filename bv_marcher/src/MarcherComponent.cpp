#include "MarcherComponent.h"

#include <RenderingSystem.h>
#include <Shader.h>
#include <RenderMesh.h>
#include <Entity.h>
#include <SelectionSystem.h>

using namespace bv;
using namespace bv::rendering;
using namespace bv::marching;

std::vector<Mesh::Id> _meshIds;

void MarcherComponent::init()
{
    auto renderer = RenderingSystem::instance();

    Shader::Id cubesShader = Shader::require("cubes");
    Uniform::Id uLightDir = Uniform::require(cubesShader, "lightDir", UniformType::Vec3);
	uLightDir->set(glm::vec3(1.0f, 1.0f, 0.0f));

    renderer->onrender([cubesShader, uLightDir, renderer]
    {
        renderer->reset(Culling::NO);

        Shader::use(cubesShader);

        for (int i = 0; i < components().size(); i++)
        {
            auto& comp = components()[i];

            const glm::mat4 &xform = comp.entity().ref().transform();
	        renderer->transform(xform);

	        RenderingSystem::instance()->submit(_meshIds[i]->renderMeshId);
        }
    });
}

void MarcherComponent::allocate()
{
    Mesh::Id meshId = Mesh::create();
    _meshIds.push_back(meshId);
}

void MarcherComponent::attachGUI()
{
    auto _panel = gui();

    _panel->bind("Iso level", marcher().thresholdRef(), 1, 255, 1);
    _panel->button("March!", [&] { march(); });
}

void MarcherComponent::update()
{
}

void MarcherComponent::march()
{
	auto mesh = _meshIds[(int)m_id];
    //s_marcher.scale(EntitySystem::scale(m_entity));
    m_marcher.march(mesh.ref());

    mesh->updateAllNormals();

    // create a renderable representation of the mesh
    RenderMesh::create(mesh);

    Log::writeln("Marching: Number of tris and verts: %i and %i", mesh->triangles.size(), mesh->vertices.size());
}

