#pragma once

#include "Common.h"
#include "Component.h"
#include "CubeMarcher.h"

namespace bv
{
    namespace marching
    {
        class MarcherComponent : public Component<MarcherComponent>
        {
        public:
            MarcherComponent(Id id, Entity::Id entity) : m_marcher(32, 32), ComponentBase(id, entity) {}
            MarcherComponent(Id id, Entity::Id entity, int dims, float threshold) : ComponentBase(id, entity), m_marcher(dims, threshold) {}

            CubeMarcher& marcher() { return m_marcher; }

            void march();

            static void init();
            static void allocate();
            static void update();

            void attachGUI();

            static std::string title() { return "Marcher"; }
        private:
            Mesh::Id m_meshId;

            CubeMarcher m_marcher;
        };
    }
}