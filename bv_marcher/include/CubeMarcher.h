#pragma once

#include "Common.h"
#include "Mesh.h"

#include <functional>

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif

namespace bv
{

    namespace marching
    {
        typedef std::function<float (float, float, float)> SamplerFuncPtr;

        class CubeMarcher
        {
        public:
            CubeMarcher(int size, float threshold = 42.0f);
            virtual ~CubeMarcher();

            void march(Mesh&);
            void march(float x, float y, float z, float fScale, Mesh&);

            void sampler(SamplerFuncPtr sampler);

            void threshold(float threshold) { m_threshold = threshold; }
            float threshold() { return m_threshold; }

            float& thresholdRef() { return m_threshold; }

            void scale(const glm::vec3& scale) { m_scale = scale; }

        private:
            int     m_dataSize;
            float   m_stepSize;
            float   m_threshold;
            float   m_time;
            glm::vec3  m_sourcePoint[3];
            glm::vec3 m_scale;

            SamplerFuncPtr m_sampler;
            float sample(float x, float y, float z) const;

            void calcNormal(glm::vec3& normal, float x, float y, float z);
        };

        // sample vector fields
        // a sphere
        float fSample1(float x, float y, float z);
        // three perpendicular cylinders
        float fSample2(float x, float y, float z);
        // a 3D sine-wave
        float fSample3(float x, float y, float z);
    }
}
