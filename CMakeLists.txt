cmake_minimum_required(VERSION 2.8.4)

project(birthview)

######### ############ ############

# include the macros used in the subdirectories
include(CMake.Macros.cmake)

# we want to use the modern C++11 features
if (CMAKE_VERSION VERSION_LESS 3.1.0)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
else()
    set(CMAKE_CXX_STANDARD 11)
endif()

if(MINGW)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wa,-mbig-obj")
endif()

set(CMAKE_C_FLAGS_DEBUG "-D_DEBUG")

add_subdirectory(bv)
add_subdirectory(bendental)
add_subdirectory(bv_planets)
