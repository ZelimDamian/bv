#pragma once

#include <RenderMeshComponent.h>
#include <ObjLoader.h>
#include "Component.h"
#include "BendingSpringComponent.h"
#include "ContactResolverComponent.h"
#include "Shader.h"

namespace bv
{
	class ForcepsComponent: public Component<ForcepsComponent>
	{
	public:
		ForcepsComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
		{
			// load the mesh representing the skull
			auto mesh = ObjLoader::load("forceps");

			auto forcep1 = Entity::create("forcep1");
			auto forcep2 = Entity::create("forcep2");

			// setup self
			setup(forcep1, mesh);
			setup(forcep2, mesh);

			// link the two
			link(forcep1, forcep2);
		}

		void link(Entity::Id forcep1, Entity::Id forcep2)
		{
			// make sure slave follows master forcep with an angle offset
			auto onupdate = [=] () mutable
			{
				if(m_active)
				{
					forcep1->pos(m_entity->pos());
					forcep2->pos(m_entity->pos());

					auto offset1 = glm::angleAxis(  glm::radians(m_angle / 2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
					auto offset2 = glm::angleAxis(- glm::radians(m_angle / 2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
					forcep1->orientation(m_entity->orientation() * offset1);
					forcep2->orientation(m_entity->orientation() * offset2);

					glm::vec3 scale { 1.0f, -1.0f, 1.0f };
					forcep2->scale(scale);
				}
			};

			UpdateSystem::instance()->onupdate(onupdate);
		}

		void setup(Entity::Id forcep, Mesh::Id mesh)
		{
			// make sure that the posititon buffers are populated with vertex data
			mesh->updateTriNormals();

			// create renderable to draw the mesh of the skull with a diffuse shader
			auto shader = rendering::Shader::require("diffuse_color");
			auto rendermesh = forcep->require<RenderMeshComponent>(mesh, shader);
			rendermesh->culling(Culling::NO);
			auto color = glm::vec3(203, 209, 243) / 255.0f;
			rendermesh->parameter("uColor", glm::vec4(color, 1.0f));

			using namespace physics;

			// skull should also be collideable
			using namespace coldet;

			auto collider = forcep->require<Collider>();
			collider->createShape<Octree>(mesh);
			collider->layer(4);

			forcep->require<ContactRendererComponent>();
			forcep->require<ContactResolverComponent>();
			forcep->require<RigidBodyComponent>()->deactivate();
		}

		void attachGUI()
		{
			auto gui = this->gui();
			gui->bind("Angle", m_angle, 0.0f, 45.0f);
		}

	private:
		float m_angle { 0.0f };
	};
}
