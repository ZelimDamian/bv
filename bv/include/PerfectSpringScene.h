#pragma once

#include "Scene.h"

#include "Entity.h"
#include "PerfectSpringComponent.h"
#include "ObjLoader.h"
#include "RenderMeshComponent.h"

namespace bv
{
	class PerfectSpringScene
	{
	private:
		void init()
		{
			using namespace physics;
			using namespace rendering;

			auto target = Entity::create("target");

			target->pos(glm::vec3(1.0f));

			auto bullet = Entity::create("bullet");
			bullet->require<RigidBodyComponent>();
			bullet->require<PerfectSpringComponent>(target)->deactivate();

			auto mesh = ObjLoader::load("sphere");
			auto shader = Shader::require("diffuse");
			bullet->require<RenderMeshComponent>(mesh, shader);
		}
	};
}