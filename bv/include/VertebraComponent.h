#pragma once

#include "Component.h"

namespace bv
{
    class VertebraComponent: public Component<VertebraComponent>
    {
    public:
        VertebraComponent(Id id, Entity::Id entity, int index, Entity::Id parent, Entity::Id child);

        Entity::Id parent() const { return m_parent; }
        void parent(Entity::Id parent) { m_parent = parent; }

        Entity::Id child() const { return m_child; }
        void child(Entity::Id child) { m_child = child; }

        float k() const { return m_k; }
        void k(float k) { m_k = k; }

    private:

        Entity::Id m_parent;
        Entity::Id m_child;

        float m_k { 10000.0f };

        int m_index;
    };
}