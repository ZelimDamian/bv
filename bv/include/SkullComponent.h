#pragma once

#include "Component.h"


namespace bv
{
	class SkullComponent: public Component<SkullComponent>
	{
	public:
		SkullComponent(Id id, Entity::Id entity);

		static void init();

		void update();

		bool isBorn() const;

		void attachGUI();
	};
}
