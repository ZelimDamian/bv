#pragma once

#include <ObjLoader.h>
#include "Component.h"
#include "AssemblyComponent.h"
#include "Collider.h"
#include "Octree.h"

namespace bv
{
	class TopSpringBCComponent: public Component<TopSpringBCComponent>
	{
	public:
		TopSpringBCComponent(Id id, Entity::Id entity): ComponentBase (id, entity) { }

		static void init()
		{
			subscribeToUpdates();
		}

		void initialize()
		{
			using namespace coldet;
			using namespace tled;

			auto box = m_entity->component<Collider>()->concept<Octree>()->shape().box();

			auto assemblyComponent = m_entity->component<AssemblyComponent>();
			auto assembly = &assemblyComponent->assembly();

			for (int i = 0; i < assembly->nodes().size(); ++i)
			{
				auto X = assembly->X(i);

				if(X.y > box.max().y - 0.0001f)
				{
					assembly->spring(i, glm::vec3(0.0f, 0.0f, 0.0f), m_k);
				}
			}
		}

		void attachGUI()
		{
			auto panel = this->gui();

			panel->bind("Stiffness", m_k, 0.0f, 100.0f, 0.01f);
		}

	private:
		float m_k { 10.0f };
	};

	class RimBCComponent: public Component<RimBCComponent>
	{
	public:
		RimBCComponent(Id id, Entity::Id entity, const std::string& name): ComponentBase (id, entity)
		{
			m_rimMesh = ObjLoader::load(name);
		}

		void initialize()
		{
			using namespace coldet;
			using namespace tled;

			auto box = m_entity->component<Collider>()->concept<Octree>()->shape().box();

			auto assemblyComponent = m_entity->component<AssemblyComponent>();
			auto assembly = &assemblyComponent->assembly();

			for (int i = 0; i < m_rimMesh->positions.size(); ++i)
			{
				auto pos = m_rimMesh->positions[i];

				for (int j = 0; j < assembly->X().size(); ++j)
				{
					const glm::vec3& nodePos = assembly->X(j);

					if(Math::closeEnough(nodePos, pos))
					{
						assembly->spring(j, glm::vec3(0.0f, 0.0f, 0.0f), m_k);
					}
				}
			}
		}

	private:
		float m_k { 10.0f };
		Mesh::Id m_rimMesh;
	};

	class SidesBCComponent: public Component<SidesBCComponent>
	{
	public:
		SidesBCComponent(Id id, Entity::Id entity): ComponentBase (id, entity) { }

		void initialize()
		{
			update();
		}

		void update()
		{
			using namespace coldet;
			using namespace tled;

			auto box = m_entity->component<Collider>()->concept<Octree>()->shape().box();

			auto assemblyComponent = m_entity->component<AssemblyComponent>();
			auto assembly = &assemblyComponent->assembly();

			auto min = box.center() - box.halfsize() * (1 - m_cutoff);
			auto max = box.center() + box.halfsize() * (1 - m_cutoff);

			for (int i = 0; i < assembly->X().size(); ++i)
			{
				const glm::vec3& nodePos = assembly->X(i);

				if((nodePos.x <= min.x || nodePos.x >= max.x) ||
				   nodePos.z < min.z || nodePos.z > max.z)
				{
					assembly->spring(i, glm::vec3(0.0f, 0.0f, 0.0f), m_k);
				}
				else
				{
					if(assembly->loads()[i].shape() == LoadShape::SPRING)
					{
						assembly->loads()[i].kill();
					}
				}
			}
		}

		void updateK()
		{
			auto assemblyComponent = m_entity->component<tled::AssemblyComponent>();
			auto assembly = &assemblyComponent->assembly();

			for (int i = 0; i < assembly->X().size(); ++i)
			{
				auto& load = assembly->loads()[i];
				if(load.shape() == tled::LoadShape::SPRING)
				{
					assembly->spring(i, glm::vec3(load.value()), m_k);
				}
			}
		}

		void attachGUI()
		{
			auto panel = this->gui();

			panel->bind<float>("Cutoff",
			    [this] { return m_cutoff; },
				[this] (float value) { m_cutoff = value; },
			    0.0f, 0.5f, 0.01f );

			panel->bind<float>("K",
			    [this] { return m_k; },
			    [this] (float k) { m_k = k; },
			    0.0f, 1000.0f, 0.01f);

			panel->button("Update cutoff", [this] { update(); });
			panel->button("Update k", [this] { updateK(); });
		}

	private:
		float m_cutoff { 0.06f };
		float m_k { 30.0f };
	};
}