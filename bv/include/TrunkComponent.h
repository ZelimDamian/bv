#pragma once

#include "Component.h"

namespace bv
{
    class TrunkComponent: public Component<TrunkComponent>
    {
    public:

	    TrunkComponent(Id id, Entity::Id entity, Entity::Id skull);

        float k() const { return m_k; }
        void k(float k) { m_k = k; }

    private:

        Entity::Id m_skull;

        float m_k { 10000.0f };
        float m_kTorsion { 100.0f };
    };
}
