#include <RenderMeshComponent.h>
#include <MeshFactory.h>
#include "VolumeScene.h"

#include "VolumeLoader.h"
#include "VolumeRendererComponent.h"

using namespace bv;
using namespace volume;

void VolumeScene::init()
{
    auto loader = VolumeLoader();
    auto volume = loader.load<uint8_t>("aneurism", 256, 256, 256);
    auto entity = Entity::create("volume");
    entity->require<VolumeRendererComponent>(volume);
}
