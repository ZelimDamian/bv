#include "ForcepsScene.h"
#include <SkullComponent.h>

#include "LagMultContactComponent.h"
#include "EhbContactComponent.h"
#include "ExporterInpComponent.h"
#include "ContactComponent.h"

//#include "LBIELoader.h"

#include "FemMeshUtils.h"
#include "ContactRendererComponent.h"
#include "ContactResolverComponent.h"

#include "LoadComponent.h"
#include "VertebraComponent.h"
#include "TrunkComponent.h"
#include "ForcepsComponent.h"

using namespace bv;
using namespace tled;

static glm::vec3 size { 1.0f, 1.0f, 1.0f };
static glm::vec3 skullPos {0.0133f, 0.1f, 0.0f};

static Entity::Id setupSkull()
{
	auto entity = Entity::create("skull");
	entity->pos(skullPos);
	entity->updateTransform();

	entity->require<SkullComponent>();

	return entity;
}

static Entity::Id setupPelvis(Entity::Id skull)
{
	// load the mesh representing the skull
	Mesh::Id mesh = ObjLoader::load("pelvisIschinal");
	// make sure that the posititon buffers are populated with vertex data
	mesh->updateTriNormals();

	auto entity = Entity::create("pelvis");

	// create renderable to draw the mesh of the skull with a diffuse shader
	entity->require<RenderMeshComponent>(mesh, rendering::Shader::require("diffuse"))->culling(Culling::NO);

	// skull should also be collideable
	using namespace coldet;

	auto collider = entity->require<Collider>();
	collider->createShape<Octree>(mesh);
	collider->layer(2);

	skull->require<coldet::ContactRendererComponent>();
	entity->require<coldet::ContactRendererComponent>();

	// add a contact resolver
	using namespace physics;
	auto resolver = entity->require<ContactResolverComponent>();
	resolver->penalty(9000.0f);

	entity->require<RigidBodyComponent>()->deactivate();

	return entity;
}

static Entity::Id setupTrunk(Entity::Id skull)
{
	auto entity = Entity::create("trunk");

	entity->require<TrunkComponent>(skull);

	coldet::ColliderLayers::instance()->link(1, 3);
	coldet::ColliderLayers::instance()->link(2, 3);

	return entity;
}

static Entity::Id setupForceps()
{
	auto entity = Entity::create("forceps");

	entity->require<ForcepsComponent>();

	// collide with skull
	coldet::ColliderLayers::instance()->link(1, 4);
	// collide with trunk
//	coldet::ColliderLayers::instance()->link(2, 3);

	// forceps needn't collide with each other
	coldet::ColliderLayers::instance()->unlink(4, 4);

	// position and orientation
	entity->pos({ 0.0f, -0.1f, 0.3f});
	entity->orientation(glm::angleAxis(glm::radians(90.0f), glm::vec3 {0.0f, 1.0f, 0.0f} ));
	entity->rotate(glm::angleAxis(glm::radians(90.0f), glm::vec3 {0.0f, 0.0f, 1.0f} ));

	return entity;
}

void ForcepsScene::init()
{
	auto forceps = setupForceps();
	auto skull = setupSkull();
	auto pelvis = setupPelvis(skull);
	auto trunk = setupTrunk(skull);

	// skull layer and PF layer need to collide
	coldet::ColliderLayers::instance()->link(0, 1);
	// skull layer and pelvis layer need to collide
	coldet::ColliderLayers::instance()->link(1, 2);
	//  need to check beteweinter-pelvic-floor contact
	coldet::ColliderLayers::instance()->unlink(0, 0);
}
