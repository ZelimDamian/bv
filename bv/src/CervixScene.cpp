#include "CervixScene.h"

#include "ContactComponent.h"

#include "FemMeshUtils.h"
#include "SolverCL.h"

#include "ContactRendererComponent.h"
#include "ContactResolverComponent.h"

#include <SkullComponent.h>
#include "VertebraComponent.h"
#include "TrunkComponent.h"
#include "LoadComponent.h"
#include "StateHistory.h"

using namespace bv;
using namespace tled;

static glm::vec3 size { 1.0f, 1.0f, 1.0f };
static glm::vec3 skullPos {0.0133f, 0.1f, 0.0f};

static Entity::Id setupSkull()
{
	auto entity = Entity::create("skull");
	entity->pos(skullPos);
	entity->updateTransform();

	entity->require<SkullComponent>();

	return entity;
}

static Entity::Id setupPelvis(Entity::Id skull)
{
	// load the mesh representing the skull
	Mesh::Id mesh = ObjLoader::load("pelvisIschinal");
	// make sure that the posititon buffers are populated with vertex data
	mesh->updateTriNormals();

	auto entity = Entity::create("pelvis");

	// create renderable to draw the mesh of the skull with a diffuse shader
	entity->require<RenderMeshComponent>(mesh, rendering::Shader::require("diffuse"))->culling(Culling::NO);

	// skull should also be collideable
	using namespace coldet;

	auto collider = entity->require<Collider>();
	collider->createShape<Octree>(mesh);
	collider->layer(2);

	skull->require<coldet::ContactRendererComponent>()->deactivate();
	entity->require<coldet::ContactRendererComponent>()->deactivate();

	// add a contact resolver
	using namespace physics;
	auto resolver = entity->require<ContactResolverComponent>();
	resolver->penalty(9000.0f);

	entity->require<RigidBodyComponent>()->deactivate();

	return entity;
}


static Entity::Id setupCervix()
{
	// load obj to generate fe mesh
	auto mesh = ObjLoader::load("cervix");

	auto entity = FemMeshUtils::setupTledTetSurf<SolverCL>("cervix", glm::vec3(), mesh, 0.1, 2.14, glm::vec3(0.0f), true, 3000.0f);
	entity->require<TopSpringBCComponent>();

	auto collider = entity->require<coldet::Collider>();
	collider->layer(0);

	return entity;
}

static Entity::Id setupPf()
{
	auto mesh2 = ObjLoader::load("full_pf");

	auto entity = FemMeshUtils::setupTledTetSurf<SolverCL>("pf", glm::vec3(), mesh2, 0.1, 1.414, glm::vec3(0.0001f), true, 30000.0f);

	auto history = entity->require<StateHistoryComponent<SolverComponent::State>>();
	auto player = entity->require<HistoryPlayerComponent>();

	history->source([=] () mutable -> SolverComponent::State
	{
		auto solver = entity->component<SolverComponent>();
		if(!solver->isActive())
		{
			history->deactivate();
		}
		return { entity->component<AssemblyComponent>()->assembly().U() };
	});

	history->target([=] (const SolverComponent::State& state)
	{
		entity->component<SolverComponent>()->apply(state);
	});

	auto bc = entity->require<SidesBCComponent>();
	auto rim = entity->require<RimBCComponent>("pf_obt_internus_attach_points");

	auto collider = entity->require<coldet::Collider>();
	collider->layer(0);

	return entity;
}

static void link(Entity::Id entity, Entity::Id skull)
{
	auto solver = entity->component<SolverComponent>();
	solver->activate();
	solver->damping(10650.0f);
	solver->onDiverged += [skull, solver] () mutable
	{
		if(solver->resetOnDiverged())
		{
			skull->component<physics::RigidBodyComponent>()->reset();
		}
		else
		{
			skull->component<physics::RigidBodyComponent>()->deactivate();
		}
	};

	// create contact components to manage contacts between skull and pelvic floor
	auto contactComponent = entity->add<ContactComponent>(skull);

	entity->require<coldet::ContactRendererComponent>()->deactivate();
	skull->require<coldet::ContactRendererComponent>()->deactivate();
}

static Entity::Id setupTrunk(Entity::Id skull)
{
	auto entity = Entity::create("trunk");

	entity->require<TrunkComponent>(skull);

	coldet::ColliderLayers::instance()->link(0, 3);
	coldet::ColliderLayers::instance()->link(1, 3);
	coldet::ColliderLayers::instance()->link(2, 3);

	return entity;
}

void CervixScene::init()
{
	auto cervix = setupCervix();
	auto pf = setupPf();

	// create the skull
	auto skull = setupSkull();
	auto pelvis = setupPelvis(skull);

	link(cervix, skull);
	link(pf, skull);

//	setupNeck(skull);
	auto trunk = setupTrunk(skull);
	link(cervix, trunk);
	link(pf, trunk);

	// skull layer and PF layer need to collide
	coldet::ColliderLayers::instance()->link(0, 1);
	// skull layer and pelvis layer need to collide
	coldet::ColliderLayers::instance()->link(1, 2);
	//  need to check beteweinter-pelvic-floor contact
	coldet::ColliderLayers::instance()->unlink(0, 0);
}
