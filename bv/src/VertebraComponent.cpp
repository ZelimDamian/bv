#include "VertebraComponent.h"

#include <RigidBodyComponent.h>
#include <Collider.h>
#include <Octree.h>
#include <ContactRendererComponent.h>
#include "BendingSpringComponent.h"
#include "ContactResolverComponent.h"
#include "ObjLoader.h"
#include "RenderMeshComponent.h"

using namespace bv;

VertebraComponent::VertebraComponent(Id id, Entity::Id entity, int index, Entity::Id parent, Entity::Id child):
	ComponentBase(id, entity), m_child(child), m_parent(parent), m_index(index)
{
	using namespace physics;

	// height of an average vertebra
	float size = 0.01f;

	auto localStart = glm::vec3(0.0f, size / 2.0f, 0.0f);
	auto localEnd = glm::vec3(0.0f, -size / 2.0f, 0.0f);

	// if this is atlant on skull change the attachment point to further away
	if(m_index == 0)
	{
		localStart = glm::vec3(0.0f, 1.5f * size, 0.0f);
	}

	if(!!m_child)
	{
		auto springComponent = m_entity->require<BendingSpringComponent>();
		springComponent->link(localStart, m_child, localEnd, m_k);
	}

	using namespace rendering;

	if(m_index > 0)
	{
		// construct the name for the current vertebra based on its index
		auto meshname = std::string("C") + std::to_string(m_index);

		// load the geometry for the current vertebra
		auto mesh = ObjLoader::load(meshname);
		mesh->updateAllNormals();

		// make sure we don't keep any translations in the model
		mesh->normalize();

		// create a rendering component to display the mesh
		m_entity->require<RenderMeshComponent>(mesh, Shader::find("diffuse"));

		// create a rigid body for the vertebra
		auto rigidbody = m_entity->require<RigidBodyComponent>();
		rigidbody->mass(0.02f);
		rigidbody->rotDamping(0.2f);

		// enable collision detection for vertebrae as well
		using namespace coldet;
		auto collider = m_entity->require<Collider>();
		collider->createShape<Octree>(mesh);
		collider->layer(3);

		// add a contact resolver
		using namespace physics;
		m_entity->require<ContactResolverComponent>();

		// display contacts
		m_entity->require<ContactRendererComponent>()->deactivate();
	}
}
