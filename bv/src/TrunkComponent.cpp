#include "TrunkComponent.h"

#include "BendingSpringComponent.h"
#include "ContactResolverComponent.h"
#include "Shader.h"
#include "CollisionSystem.h"
#include <ExpulsionComponent.h>
#include <ContactRendererComponent.h>
#include "ObjLoader.h"
#include "RenderMeshComponent.h"
#include <StateHistory.h>

using namespace bv;

TrunkComponent::TrunkComponent(Id id, Entity::Id entity, Entity::Id skull): ComponentBase(id, entity), m_skull(skull)
{
	/////////////////////////////////////////////////////////////////////
	/// Setup mesh and rendering
	/////////////////////////////////////////////////////////////////////

	using namespace rendering;

	// load the geometry for the current vertebra
	auto mesh = ObjLoader::load("trunk");
	mesh->updateAllNormals();
	mesh->updateBox();

	// make sure we don't keep any translations in the model
	mesh->normalize();

	// create a rendering component to display the mesh
	auto shader = rendering::Shader::require("diffuse_color");
	auto rendermesh = entity->require<RenderMeshComponent>(mesh, shader);
	rendermesh->culling(Culling::NO);
//				rendermesh->deactivate();
	auto color = glm::vec3(235, 189, 173) / 255.0f;
	rendermesh->parameter("uColor", glm::vec4(color, 1.0f));

	/////////////////////////////////////////////////////////////////////
	/// Setup relative positions
	/////////////////////////////////////////////////////////////////////

	// height of trunk
	float height = mesh->box().size().y;

	// length of the neck
	float neckLength = 0.012f;
	// direction of the neck
	glm::vec3 direction = glm::vec3(0.0f, 1.0f, 0.0f);
	// offset of the trunk from the skull
	float offset = height / 2.2f;

	glm::vec3 horizontal = glm::vec3(1.0f, 0.0f, 0.0f);
	float horizontalShift = 0.0f;

	m_entity->pos(m_skull->pos() + direction * (neckLength + offset) + horizontal * horizontalShift);
	m_entity->updateTransform();

	/////////////////////////////////////////////////////////////////////
	/// Setup physics related components
	/////////////////////////////////////////////////////////////////////

	using namespace physics;

	float skullLocalOffset = 0.004f;

	auto localEnd = glm::vec3(0.0f, skullLocalOffset, 0.0f);
	auto globalEnd = Math::transform(localEnd, m_skull->transform());
	auto localStart = Math::transform(globalEnd, glm::inverse(m_entity->transform()));

	auto springComponent = m_entity->require<BendingSpringComponent>();
	springComponent->link(localStart, m_skull, localEnd, m_k, m_kTorsion);

	// create a rigid body for the vertebra
	auto rigidbody = m_entity->require<RigidBodyComponent>();
	rigidbody->mass(2.5f);
	rigidbody->rotDamping(3.0f);

	auto history = entity->require<StateHistoryComponent<RigidBodyState>>();
	history->source([=] () mutable
	                {
		                if(!rigidbody->isActive())
		                {
			                history->deactivate();
		                }
		                return rigidbody->state();
	                });
	history->target([=] (const RigidBodyState& state) mutable
	                {
		                rigidbody->state(state);
	                });
	/////////////////////////////////////////////////////////////////////
	/// Setup collision detection
	/////////////////////////////////////////////////////////////////////

	using namespace coldet;

	auto collider = m_entity->require<Collider>();
	collider->createShape<Octree>(mesh);
	collider->layer(3);

	m_entity->require<ContactResolverComponent>();
	m_skull->require<ContactResolverComponent>();

	// display contacts
	m_entity->require<ContactRendererComponent>()->deactivate();

	/////////////////////////////////////////////////////////////////////
	/// Setup simulation logic
	/////////////////////////////////////////////////////////////////////

	auto target = Entity::create("target");
	target->require<physics::ExpulsionTargetComponent>();
	target->pos(glm::vec3(-0.0f, -0.2f, -0.0f));

	// local coordinates of the baby's bottom
	glm::vec3 bottom(0.0f, offset, 0.0f);

	// add expulsion force to the last vertebra
	auto expulsion = m_entity->require<physics::ExpulsionComponent>(target, bottom);//.deactivate();
	expulsion->minForce(30.0f);
	expulsion->maxForce(150.0f);
}
