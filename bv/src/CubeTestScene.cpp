#include "CubeTestScene.h"

#include "RigidBodyComponent.h"
#include "AbqsRprtLoader.h"
#include "SolverCL.h"

#include "FemMeshUtils.h"
#include "MeshFactory.h"

#include <LagMultContactComponent.h>
#include <PenaltyContactComponent.h>
#include <ContactComponent.h>
#include "ContactRendererComponent.h"
#include "CameraSystem.h"

#include <MeshDifferenceRendererComponent.h>
#include <ViewportElement.h>

using namespace bv;
using namespace tethex;
using namespace tled;
using namespace coldet;
using namespace physics;

struct RotatorComponent : public Component<RotatorComponent>
{
	RotatorComponent(Id id, Entity::Id entity) : ComponentBase(id, entity) { }

	static void init()
	{
		subscribeToUpdates();
	}

	void update()
	{
		auto rigidbody = require<RigidBodyComponent>();

		rigidbody->applyForce(force, point);
//		rigidbody->applyForceGlobal(force, this->entity()->pos() + point);
		rigidbody->applyForce(-force);
	}

	void attachGUI()
	{
		auto panel = this->gui();

		panel->bind<glm::vec3>("Force", [this] { return force; }, [this](const glm::vec3& f) { force = f; }, -100.0f, 100.0f);
		panel->bind<glm::vec3>("Point", [this] { return point; }, [this](const glm::vec3& p) { point = p; }, -100.0f, 100.0f);
	}

	glm::vec3 point { 10.0f };
	glm::vec3 force { 1.0f, 0.0f, 0.0f };
};

struct BottomFixer: public Component<BottomFixer>
{
	BottomFixer(Id id, Entity::Id entity) : ComponentBase(id, entity) { }

	static void init()
	{
		subscribeToUpdates();
	}

	void initialize()
	{
		auto assemblyComponent = require<AssemblyComponent>();
		auto& assembly = assemblyComponent->assembly();

		Box box;
		box.fit(assembly.X());

		for (int i = 0; i < assembly.nodes().size(); ++i)
		{
			if(assembly.X(i).y < box.min().y + 0.0001f)
			{
				assembly.fix(i);
			}
		}
	}
};

struct LagMultTestComponent : public Component<LagMultTestComponent>
{
	LagMultTestComponent(Id id, Entity::Id entity, Entity::Id planeEntity):
			ComponentBase(id, entity),
			planeEntity(planeEntity)
	{
		planeMesh = Mesh::create();
	}

	static void init()
	{
		subscribeToUpdates();
	}

	void update()
	{
		// update plane position
		m_plane.pos(planeEntity->pos());

		auto assemblyComponent = require<AssemblyComponent>();
		auto& assembly = assemblyComponent->assembly();

		auto calculatorComponent = require<CalculatorComponent>();
		auto& calculator = calculatorComponent->calculator();

		float dt = Timing::delta();
//		float dt = assembly.cachedDt();
		float dtSqr = dt * dt;

		for (int i = 0; i < assembly.nodes().size(); ++i)
		{
			const glm::vec3 pos = assembly.pos(i);
			const float penetration = glm::dot(m_plane.normal(), pos - m_plane.pos());
			if(m_dirichlet)
			{
                if(penetration < 0.001f)// || assembly.loads()[i].active())
                {
					if(assembly.loads()[i].shape() == LoadShape::FIX ||
					   assembly.loads()[i].shape() == LoadShape::LINEAR )
					{
						continue;
					}

					const glm::vec3 normal = m_plane.normal();
                    const float d = glm::dot(normal, m_plane.pos() - assembly.X(i));
                    const glm::vec4 plane(normal, d);

                    assembly.project(i, plane);

                    continue;
                    ////////// friction

                    const glm::vec3 velocity = calculator.deltaU(i) / dt;
                    const glm::vec3 velocityTangent = velocity - normal * glm::dot(normal, velocity);

                    const glm::vec3 force = -assembly.F(i);
                    const float normalMagnitude = glm::dot(normal, force);
                    const glm::vec3 normalForce = normalMagnitude * normal;
                    const float posNormalMagnitude = std::max(0.0f, normalMagnitude);

                    float coefficient;
                    glm::vec3 direction;

                    if(glm::length2(velocityTangent) > 0.0001f)
                    {
                        // dynamic friction
                        coefficient = m_dynamicCoeff;
                        direction = -glm::normalize(velocityTangent);
                        const glm::vec3 friction = posNormalMagnitude * coefficient * direction;
                        assembly.R(i) = friction;
                    }
                    else
                    {
                        // static friction
                        coefficient = m_staticCoeff;
                        const glm::vec3 forceTangent = force - normalForce;
                        if (glm::length2(forceTangent) > 0.00001f)
                        {
                            direction = -glm::normalize(forceTangent);
                            const glm::vec3 friction = posNormalMagnitude * coefficient * direction;
                            assembly.R(i) = friction;
                        }
                    }
                }
			}
            else
            {
				if(penetration < 0)
				{
					float mass = assembly.M(i);
					float absPenetration = glm::abs(penetration);
					glm::vec3 gap = m_plane.normal() * absPenetration;
					glm::vec3 force = magic * gap * mass / dtSqr;

					assembly.load(i, force);
				}
            }
		}

		// draw plane
		auto render = [this]
		{
			auto renderer = RenderingSystem::instance();
			using namespace rendering;

			// update plane mesh
			MeshFactory::shape(m_plane, 2.0f, 10, planeMesh);
			auto rendermesh = RenderMesh::create(planeMesh);

			renderer->reset(Culling::NO, RenderMode::LINE_STRIP);

			Shader::use("colors")->uniform("uColor", UniformType::Vec4)->set(glm::vec4(0.2f, 0.2f, 0.0f, 1.0f));
			renderer->submit(rendermesh);
		};
		RenderingSystem::instance()->onrender_once(render);

	}

	void attachGUI()
	{
		auto panel = this->gui();

		panel->bind("Magic", magic, 0.01f, 10000.0f);
		panel->bind("Static Friction", m_staticCoeff, 0.01f, 1.0f);
		panel->bind("Dynamic Friction", m_dynamicCoeff, 0.01f, 1.0f);
		panel->check("Dirichlet", [this] { m_dirichlet = !m_dirichlet; }, [this] { return m_dirichlet; });
	}

	Plane m_plane{ glm::vec3(), glm::vec3(0.0f, 1.0f, 0.0f) };
	Mesh::Id planeMesh;
	Entity::Id planeEntity;

	bool m_dirichlet { true };
	float magic { 1.0f };
	float m_dynamicCoeff { 0.5f };
	float m_staticCoeff { 0.5f };
};

class PressurePlateComponent: public Component<PressurePlateComponent>
{
public:
	PressurePlateComponent(Id id, Entity::Id entity, Entity::Id plate): ComponentBase(id, entity), m_plate(plate)
	{
		m_mesh = Mesh::create();
	}

	static void init()
	{
		subscribeToUpdates();
	}

	void update()
	{
		auto plate = m_plate->component<RigidBodyComponent>();

		// update plane position
		m_plane.pos(m_plate->pos());

		auto assemblyComponent = require<AssemblyComponent>();
		auto& assembly = assemblyComponent->assembly();

		Box box;
		box.fit(assembly.X());

		float dt = Timing::delta();
//		float dt = assembly.cachedDt();
		float dtSqr = dt * dt;

		// contact normal
		const glm::vec3 normal = m_plane.normal();

		if(m_useContact)
		{
			if (m_pDN)
			{
				for (int i = 0; i < assembly.nodes().size(); ++i)
				{
					const glm::vec3 pos = assembly.pos(i);
					const glm::vec3 X = assembly.X(i);
					const float penetration = glm::dot(m_plane.normal(), pos - m_plane.pos());
					if (X.y > box.max().y - 0.0001f)
					{
						if (assembly.loads()[i].shape() == LoadShape::FIX ||
							assembly.loads()[i].shape() == LoadShape::LINEAR)
						{
							continue;
						}

                        const float d = glm::dot(normal, m_plane.pos() - assembly.X(i));

                        const glm::vec4 plane(normal, d);
                        assembly.project(i, plane);

                        const glm::vec3 f = -assembly.F(i);
                        const float absFDotN = glm::max(0.0f, glm::dot(f, -normal));
                        plate->applyForce(-normal * absFDotN);
					}
				}
			}
			else
			{
				for (int i = 0; i < assembly.nodes().size(); ++i)
				{
					const glm::vec3 pos = assembly.pos(i);
					const float penetration = glm::dot(m_plane.normal(), pos - m_plane.pos());

					// reference configuration position
					const glm::vec3 X = assembly.X(i);
					if (X.y > box.max().y * 0.999f && penetration < 0.0f)
					{
						const float d = glm::dot(normal, m_plane.pos() - assembly.X(i));

						float g = glm::abs(penetration);

						glm::vec3 force = normal * g * assembly.M(i) / dtSqr;
						assembly.load(i, force);

						plate->applyForce(-force);
					}
				}
			}
		}
		else
		{
			std::vector<int> topNodes;
			float nodesMass = 0.0f;

			for (int i = 0; i < assembly.nodes().size(); ++i)
			{
				// reference configuration position
				const glm::vec3 pos = assembly.X(i);
				if (pos.y > box.max().y - 0.0001f)
				{
					topNodes.push_back(i);
					nodesMass += assembly.M(i);
				}
			}

			const float mass = plate->mass();
			const glm::vec3 gravity = RigidBodyComponent::gravity();
			const glm::vec3 weight = mass * gravity;

			for (int i = 0; i < topNodes.size(); ++i)
			{
				float part = assembly.M(topNodes[i]) / nodesMass;
				assembly.R(topNodes[i], weight * part);
			}
		}

		// draw plane
		auto render = [this]
		{
			auto renderer = RenderingSystem::instance();
			using namespace rendering;

			// update plane mesh
			MeshFactory::shape(m_plane, 2.0f, 10, m_mesh);
			auto rendermesh = RenderMesh::create(m_mesh);

			renderer->reset(Culling::NO, RenderMode::LINE_STRIP);

			Shader::use("colors")->uniform("uColor", UniformType::Vec4)->set(glm::vec4(0.2f, 0.2f, 0.0f, 1.0f));
			renderer->submit(rendermesh);
		};

		RenderingSystem::instance()->onrender_once(render);
	}

	void attachGUI()
	{
		auto panel = this->gui();

		panel->check("Use pDN", [this] { m_pDN = !m_pDN; }, [this] { return m_pDN; });
		panel->check("Use Contact", [this]
		{
			m_useContact = !m_useContact;
			auto rigidbody = m_plate->component<RigidBodyComponent>();
			if(m_useContact)
			{
				rigidbody->activate();
				rigidbody->reset();
			} else {
				rigidbody->deactivate();
			}
		}, [this] {
			return m_useContact;
		});
	}

private:
	Plane m_plane { glm::vec3 { }, glm::vec3 { 0.0f, -1.0f, 0.0f }};
	Entity::Id m_plate;
	Mesh::Id m_mesh;

	bool m_useContact { true };
	bool m_pDN { true };
};

void CubeTestScene::init()
{
	const glm::vec3 size(0.1f);

	physics::RigidBodyComponent::gravity().y = -9.82f;

    //  create a cube to generate fe mesh
	Mesh::Id mesh = MeshFactory::cube(size);

//	auto cube = FemMeshUtils::setupTledTetSurf<SolverCL>("cube", glm::vec3(), mesh, 0.00001, 1.414, glm::vec3(), false);
//
//	cube->require<tled::SolverComponent>()->activate();
//	cube->require<tled::SolverComponent>()->totalTime(0.016f);
//	cube->require<physics::RigidBodyComponent>()->deactivate();

//	TetMesh::Id tetMeshId = cube->component<AssemblyRendererComponent<TetMesh>>()->mesh();

	// create a copy
//	auto original = Mesh::create();
//	*original = *tetMeshId->surface;

//	VectorVec3 abqsU = tled::AbqsRprtLoader::load("cube-x");
//	VectorVec3 abqsU = tled::AbqsRprtLoader::load("cube-xyz");
//	VectorVec3 abqsU = tled::AbqsRprtLoader::load("cube-20kg");

	// apply the deformation to the mesh
//	original->translate(abqsU);
//	original->updateVertices();

//	cube->require<MeshDifferenceRendererComponent<Mesh>>(tetMeshId->surface, original)->deactivate();

	// create ground plane
//	auto ground = Entity::create("Ground");
//	ground->pos(glm::vec3(0.0f, -0.05001f, 0.0f));
//	cube->require<LagMultTestComponent>(ground);//.deactivate();
//	cube->require<BottomFixer>();//.deactivate();

	// create the pressure plate
//	auto plate = Entity::create("Plate");
//	plate->pos(glm::vec3(0.0f, 0.05001f, 0.0f));
//	auto rigidbody = plate->require<physics::RigidBodyComponent>();
//	rigidbody->mass(50.0f);
//	cube->require<PressurePlateComponent>(plate)->deactivate();

	auto sphere = FemMeshUtils::setupCollidableMesh("sphere", glm::vec3{ 0.0f, 0.15f, 0.0f });
	sphere->component<physics::RigidBodyComponent>()->mass(1.0f);
//	cube->require<LagMultContactComponent>(sphere);
//	cube->require<ContactComponent>(sphere);

//	cube->require<ContactRendererComponent>()->deactivate();
//	sphere->require<ContactRendererComponent>().deactivate();

    // thingy shake
    {
        auto window = gui::Gui::instance()->window("test");
        auto viewport = window->add<gui::ViewportElement>("Viewer");
//        auto viewport = new gui::ViewportElement(window, "viewer");
//        auto context = RenderContext::create();

		auto camera = viewport->context()->camera();
		camera->eye(glm::vec3(0.0f, 0.0f, 1.0f));
		camera->target(glm::vec3(0.0f, 0.0f, 0.0f));

        auto onrender = [=] () mutable
        {
            auto renderer = RenderingSystem::instance();
            renderer->reset(Culling::NO, RenderMode::LINE_STRIP);
            rendering::Shader::use("diffuse");
	        renderer->submit(sphere->component<RenderMeshComponent>()->renderMesh());
        };

        viewport->context()->onrender(onrender);
    }
}

