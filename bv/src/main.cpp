#include <CollisionSystem.h>
#include <VolumeScene.h>

#include "ManipulatorSystem.h"
#include "PickingSystem.h"
#include "GpuSystem.h"
#include "Profiler.h"

#include "CubeTestScene.h"
#include "CervixScene.h"
#include "ForcepsScene.h"

using namespace bv;

// for BGFX
extern "C" int _main_(int _argc, char** _argv);
int _main_(int, char**)
{
    //jump start
	EntitySystem::instance();
    ManipulatorSystem::instance();
    SelectionSystem::instance();
    coldet::CollisionSystem::instance();
    coldet::PickingSystem::instance();
	gui::Gui::instance();
    gpu::GpuSystem::instance();
    Profiler::instance();

	Scene::create<CervixScene>();
	Scene::create<CubeTestScene>();
    Scene::create<VolumeScene>();
    Scene::create<ForcepsScene>();

    // run the main simulation loop
    WindowSystem::instance()->run();

    return 0;
}

