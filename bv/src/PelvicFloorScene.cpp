#include "PelvicFloorScene.h"

#include "LagMultContactComponent.h"
#include "EhbContactComponent.h"
#include "SolverCL.h"
#include "ExporterInpComponent.h"
#include "ContactComponent.h"

#include "RigidBodyComponent.h"
#include "ExpulsionComponent.h"
#include "Collider.h"
#include "Octree.h"
#include "Bitree.h"

#include "ObjLoader.h"
#include "InpLoader.h"

#include "FemMeshUtils.h"

#include "CameraSystem.h"
#include "ContactRendererComponent.h"

using namespace bv;
using namespace tled;

static glm::vec3 size { 1.0f, 1.0f, 1.0f };

Entity::Id setupSkull()
{
	// load the mesh representing the skull
	auto mesh = ObjLoader::load("skull_down");
	// make sure that the posititon buffers are populated with vertex data
	mesh->updateTriNormals();

	auto entity = Entity::create("skull");
	entity->pos({0.02515f, 0.09216f, 0.0f});

	// create renderable to draw the mesh of the skull with a diffuse shader
	entity->require<RenderMeshComponent>(mesh, rendering::Shader::require("diffuse"));

	// don't start running immediately
	auto rb = entity->require<physics::RigidBodyComponent>();
	rb->deactivate();
	rb->mass(2.0f);
	rb->damping(3650.0f);
	rb->rotDamping(10.0f);

	// add a component to display the reaction force
	entity->require<physics::ReactionReportingComponent>();

	// add an entity to represent the target of the expulsion force
	auto target = Entity::create("target");
	target->require<physics::ExpulsionTargetComponent>();
	target->pos(glm::vec3(-0.035f, -0.2f, 0.0f));

	entity->require<physics::ExpulsionComponent>(target);//.deactivate();
	entity->component<physics::ExpulsionComponent>()->minForce(0.0f);
	entity->component<physics::ExpulsionComponent>()->maxForce(6.0f);

	// skull should also be collideable
	using namespace coldet;

	auto collider = entity->require<Collider>();
	collider->createShape<Octree>(mesh);
	collider->layer(1);

	return entity;
}

Entity::Id setupPelvis()
{
	// load the mesh representing the skull
	Mesh::Id mesh = ObjLoader::load("pelvisIschinal");
	// make sure that the posititon buffers are populated with vertex data
	mesh->updateTriNormals();

	auto entity = Entity::create("pelvis");
	entity->pos(glm::vec3(0));

	// create renderable to draw the mesh of the skull with a diffuse shader
	entity->require<RenderMeshComponent>(mesh, rendering::Shader::require("diffuse"))->culling(Culling::NO);

	// skull should also be collideable
	using namespace coldet;

	auto collider = entity->require<Collider>();
	collider->createShape<Octree>(mesh);
	collider->layer(2);

	return entity;
}


void PelvicFloorScene::init()
{
	CameraSystem::instance()->current()->distance(0.4f); // 50cm
	CameraSystem::instance()->current()->eye(glm::vec3(0.0f, 0.0f, 0.5f));

	// skull layer and PF layer need to collide
	coldet::ColliderLayers::instance()->link(0, 1);
	// skull layer and pelvis layer need to collide
	coldet::ColliderLayers::instance()->link(1, 2);
	//  need to check beteweinter-pelvic-floor contact
	coldet::ColliderLayers::instance()->unlink(0, 0);
}
