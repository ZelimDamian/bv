#include "SkullComponent.h"

#include <ExpulsionComponent.h>
#include <RenderMeshComponent.h>
#include <ObjLoader.h>
#include <StateLoggerComponent.h>
#include <StateHistory.h>
#include "BendingSpringComponent.h"
#include "ContactResolverComponent.h"
#include "Shader.h"
#include "CollisionSystem.h"
#include "MousePullComponent.h"
#include "ContactComponent.h"

#include "ExpulsionComponent.h"

using namespace bv;
using namespace tled;

SkullComponent::SkullComponent(Id id, Entity::Id entity): ComponentBase(id, entity)
{
	// load the mesh representing the skull
	auto skull = ObjLoader::load("skull_smooth");
	// make sure that the posititon buffers are populated with vertex data
	skull->updateTriNormals();

	// create renderable to draw the mesh of the skull with a diffuse shader
	auto shader = rendering::Shader::require("diffuse_color");
	auto rendermesh = entity->require<RenderMeshComponent>(skull, shader);
	rendermesh->culling(Culling::NO);
	//				rendermesh->deactivate();
	auto color = glm::vec3(235, 189, 173) / 255.0f;
	rendermesh->parameter("uColor", glm::vec4(color, 1.0f));

	using namespace physics;

	// don't start running immediately
	auto rb = entity->require<RigidBodyComponent>();
	rb->mass(1.5f);
	rb->damping(9650.0f);
	rb->rotDamping(3.0f);

	// add a component to display the reaction force
	entity->require<physics::ReactionReportingComponent>()->deactivate();

	auto history = entity->require<StateHistoryComponent<RigidBodyState>>();
	history->source([=] () mutable
	{
		if(!rb->isActive())
		{
			history->deactivate();
		}
		return rb->state();
	});
	history->target([=] (const RigidBodyState& state) mutable
	{
		rb->state(state);
	});

	// skull should also be collideable
	using namespace coldet;

	auto collider = entity->require<Collider>();
	collider->createShape<Octree>(skull);
	collider->layer(1);

	// enabled mouse force application
	entity->require<MousePullComponent>();
}

bool SkullComponent::isBorn() const
{
	return m_entity->pos().y < -0.045f;
}

void SkullComponent::attachGUI()
{
	auto gui = this->gui();

	gui->bind<glm::vec3>("Angles", [this] {
		auto angles = glm::eulerAngles(this->entity()->orientation());
		auto eulerAngles = glm::degrees(angles);
		return eulerAngles;
	}, [this] (const glm::vec3& eulerAngles) {
		this->entity()->orientation(glm::quat(glm::radians(eulerAngles)));
	}, -180.0f, 180.0f);
}

void SkullComponent::init()
{
	subscribeToUpdates();
}

void SkullComponent::update()
{
	using namespace coldet;

	auto collider = entity()->component<Collider>();

	auto pf = Entity::find("pf");
	if(pf)
	{
		auto comps = pf->components<ContactComponent>();
		auto contactComp = *std::find_if(comps.begin(), comps.end(), [this] (const ContactComponent::Id& comp)
		{
			return comp->master() == entity();
		});

		if(isBorn())
		{
			contactComp->forget();
			collider->layer(4); // put skull in separate layer
			ColliderLayers::instance()->link(3, 4); // keep contact with trunk
			ColliderLayers::instance()->link(2, 4); // keep contact with trunk

			auto trunk = Entity::find("trunk");
			auto expulsionComponent = trunk->component<physics::ExpulsionComponent>();

			expulsionComponent->maxForce(60.0f);
		}
	}
}