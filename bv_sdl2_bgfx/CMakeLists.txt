SET(target bv_sdl2_bgfx)

SETUP(${target})
EXPORT(${target})

SET(SOURCE_FILES
        src/WindowSystem.sdl2.bgfx.cpp
)

LINK(core)

set(VIDEO_OPENGLES 0)
set(RENDER_D3D 0)
add_subdirectory(ext/SDL2)
SET(INCLUDE_DIRS ${INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/ext/SDL2/include)
message(WARNING ${CMAKE_CURRENT_SOURCE_DIR}/ext/SDL2/include)
SET(LIBS ${LIBS} SDL2-static)

LINK_TO(../bv_bgfx/ext/CMake bgfx)

BOOTSTRAP(${target})
