#include "WindowSystem.h"

#include "RenderingSystem.h"
#include "MouseSystem.h"
#include "Gui.h"
#include "KeyboardSystem.h"

#include "Timing.h"

#define SDL_STATIC
#include <SDL.h>
#include <SDL_mouse.h>
#include <SDL_syswm.h>

#include <bgfx/bgfxplatform.h>

SDL_Window *handle; /* Our window handle */
SDL_GLContext sdl_context; /* Our opengl context handle */


//#include "../ext/SDL2/include/SDL_events.h"

using namespace bv;
using namespace gui;

/* A simple function that prints a message, the error code returned by SDL,
* and quits the application */
void sdldie(const char *msg)
{
    Log::writeln("%s", msg);
    SDL_Quit();
    //exit(1);
}

void checkSDLError(int line = -1)
{
#ifndef NDEBUG
    const char *error = SDL_GetError();
    if (*error != '\0')
    {
        printf("SDL Error: %s\n", error);
        if (line != -1)
            printf(" + line: %i\n", line);
        SDL_ClearError();
    }
#endif
}

WindowSystem::WindowSystem()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) /* Initialize SDL's Video subsystem */
    {
        sdldie("Unable to initialize SDL"); /* Or die on error */
    }

    /* Create our window centered*/
    handle = SDL_CreateWindow(m_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              m_width, m_height,
                              SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

    if (!handle) /* Die if creation failed */
    {
        sdldie("Unable to create window");
    }

//    checkSDLError(__LINE__);

    /* Create our opengl context and attach it to our window */
//    sdl_context = SDL_GL_CreateContext(handle);
//    checkSDLError(__LINE__);

    /* This makes our buffer swap syncronized with the monitor's vertical refresh */
//    SDL_GL_SetSwapInterval(1);

    bgfx::sdlSetWindow(handle);
//    bgfx::renderFrame();

//    bgfx::init();

    /* Need to initialize the renderer before anything uses rendering context*/
//    m_beforeRun();
}

Key mapKey(SDL_Scancode code)
{
    switch(code)
    {
        case SDL_SCANCODE_A: return Key::A;
        case SDL_SCANCODE_B: return Key::B;
        case SDL_SCANCODE_C: return Key::C;
        case SDL_SCANCODE_D: return Key::D;
        case SDL_SCANCODE_E: return Key::E;
        case SDL_SCANCODE_F: return Key::F;
        case SDL_SCANCODE_G: return Key::G;
        case SDL_SCANCODE_H: return Key::H;
        case SDL_SCANCODE_I: return Key::I;
        case SDL_SCANCODE_J: return Key::J;
        case SDL_SCANCODE_K: return Key::K;
        case SDL_SCANCODE_L: return Key::L;
        case SDL_SCANCODE_M: return Key::M;
        case SDL_SCANCODE_N: return Key::N;
        case SDL_SCANCODE_O: return Key::O;
        case SDL_SCANCODE_P: return Key::P;
        case SDL_SCANCODE_Q: return Key::Q;
        case SDL_SCANCODE_R: return Key::R;
        case SDL_SCANCODE_S: return Key::S;
        case SDL_SCANCODE_T: return Key::T;
        case SDL_SCANCODE_U: return Key::U;
        case SDL_SCANCODE_V: return Key::V;
        case SDL_SCANCODE_W: return Key::W;
        case SDL_SCANCODE_X: return Key::X;
        case SDL_SCANCODE_Y: return Key::Y;
        case SDL_SCANCODE_Z: return Key::Z;
        case SDL_SCANCODE_0: return Key::NUM0;
        case SDL_SCANCODE_1: return Key::NUM1;
        case SDL_SCANCODE_2: return Key::NUM2;
        case SDL_SCANCODE_3: return Key::NUM3;
        case SDL_SCANCODE_4: return Key::NUM4;
        case SDL_SCANCODE_5: return Key::NUM5;
        case SDL_SCANCODE_6: return Key::NUM6;
        case SDL_SCANCODE_7: return Key::NUM7;
        case SDL_SCANCODE_8: return Key::NUM8;
	    case SDL_SCANCODE_9: return Key::NUM9;
        case SDL_SCANCODE_EQUALS: return Key::EQUALS;
	    case SDL_SCANCODE_RIGHTBRACKET: return Key::RIGHTBRACKET;
	    case SDL_SCANCODE_LEFTBRACKET: return Key::LEFTBRACKET;
        case SDL_SCANCODE_MINUS: return Key::MINUS;
        case SDL_SCANCODE_LALT: return Key::LALT;
        case SDL_SCANCODE_LSHIFT: return Key::LSHIFT;
        case SDL_SCANCODE_LCTRL: return Key::LCTRL;
        case SDL_SCANCODE_LGUI: return Key::LGUI;
        case SDL_SCANCODE_BACKSPACE: return Key::BACKSPACE;
	    case SDL_SCANCODE_DELETE: return Key::DEL;
        case SDL_SCANCODE_RETURN: return Key::RETURN;
	    case SDL_SCANCODE_PERIOD: return Key::PERIOD;
	    case SDL_SCANCODE_DECIMALSEPARATOR: return Key::PERIOD;
	    case SDL_SCANCODE_COMMA: return Key::COMMA;
	    case SDL_SCANCODE_KP_PERIOD: return Key::PERIOD;
	    case SDL_SCANCODE_KP_ENTER: return Key::RETURN;
	    case SDL_SCANCODE_KP_PLUS: return Key::PLUS;
	    case SDL_SCANCODE_KP_MINUS: return Key::MINUS;
	    case SDL_SCANCODE_KP_0: return Key::NUMPAD0;
	    case SDL_SCANCODE_KP_1: return Key::NUMPAD1;
	    case SDL_SCANCODE_KP_2: return Key::NUMPAD2;
	    case SDL_SCANCODE_KP_3: return Key::NUMPAD3;
	    case SDL_SCANCODE_KP_4: return Key::NUMPAD4;
	    case SDL_SCANCODE_KP_5: return Key::NUMPAD5;
	    case SDL_SCANCODE_KP_6: return Key::NUMPAD6;
	    case SDL_SCANCODE_KP_7: return Key::NUMPAD7;
	    case SDL_SCANCODE_KP_8: return Key::NUMPAD8;
	    case SDL_SCANCODE_KP_9: return Key::NUMPAD9;
	    case SDL_SCANCODE_ESCAPE: return Key::ESCAPE;
	    case SDL_SCANCODE_RIGHT: return Key::RIGHT;
	    case SDL_SCANCODE_LEFT: return Key::LEFT;

        default: return Key::UNKNOWN;
    }
}

void WindowSystem::run()
{
	Systems::instance()->init();

    // inform that we are about to start running
    m_beforeRun();

    while (!m_shouldExit)
    {

        MouseSystem::instance()->x(MouseSystem::instance()->x());
        MouseSystem::instance()->y(MouseSystem::instance()->y());
        MouseSystem::instance()->z(MouseSystem::instance()->z());

        //// Main event loop

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                {
					m_shouldExit = true;
                }
                case SDL_KEYDOWN: { break; }
                case SDL_KEYUP: { break; }
                case SDL_MOUSEBUTTONDOWN:
                {
//                    if (event.button.button == SDL_BUTTON_LEFT)
//                    {
//                        if(KeyboardSystem::instance()->key(Key::LALT))
//                        {
//                            MouseSystem::instance()->button(MouseButton::Middle, true);
//                        }
//                        else
//                        {
//                            MouseSystem::instance()->button(MouseButton::Left, true);
//                        }
//                    }
//                    else if (event.button.button == SDL_BUTTON_MIDDLE)
//                    {
//                        MouseSystem::instance()->button(MouseButton::Middle, true);
//                    }
//                    else if (event.button.button == SDL_BUTTON_RIGHT)
//                    {
//                        MouseSystem::instance()->button(MouseButton::Right, true);
//                    }
//
                    break;
                }
                case SDL_MOUSEBUTTONUP:
                {
//                    if (event.button.button == SDL_BUTTON_LEFT)
//                    {
//                        MouseSystem::instance()->button(MouseButton::Left, false);
//                        MouseSystem::instance()->button(MouseButton::Middle, false);
//                    }
//                    else if (event.button.button == SDL_BUTTON_MIDDLE)
//                    {
//                        MouseSystem::instance()->button(MouseButton::Middle, false);
//                    }
//                    else if (event.button.button == SDL_BUTTON_RIGHT)
//                    {
//                        MouseSystem::instance()->button(MouseButton::Right, false);
//                    }

                    break;
                }
                case SDL_MOUSEMOTION:
                {
                    MouseSystem::instance()->x(event.motion.x);
                    MouseSystem::instance()->y(m_height - event.motion.y);

                    break;
                }
                case SDL_MOUSEWHEEL:
                {
                    MouseSystem::instance()->z(MouseSystem::instance()->z() + event.wheel.y);

                    break;
                }
	            default:
	            {
	            }
            }
        }

//        int x, y;
//        SDL_GetMouseState(&x, &y);
//        MouseSystem::instance().x(x);
//        MouseSystem::instance().y(m_height - y);

//        if(scroll != MouseSystem::instance().z())
        {
//            MouseSystem::instance()->z(scroll);
        }

        auto sdl_keyboard = SDL_GetKeyboardState(nullptr);

        for (int i = 0; i < SDL_NUM_SCANCODES; ++i)
        {
            auto key = mapKey((SDL_Scancode)i);
	        auto down = sdl_keyboard[i];

            KeyboardSystem::instance()->key(key, down);
        }

        //// Window resize management

        int w, h;
        SDL_GetWindowSize(handle, &w, &h);

		state(w, h);

	    {
		    auto sdl_mouse = SDL_GetMouseState(nullptr, nullptr);

		    bool left = sdl_mouse & SDL_BUTTON_LMASK;
		    bool right = sdl_mouse & SDL_BUTTON_RMASK;
		    bool middle = sdl_mouse & SDL_BUTTON_MMASK;

            MouseSystem::instance()->button(MouseButton::Middle, middle);
            MouseSystem::instance()->button(MouseButton::Right, right);

		    if(KeyboardSystem::instance()->key(Key::LALT))
		    {
			    MouseSystem::instance()->button(MouseButton::Middle, left);
		    }
            else if(KeyboardSystem::instance()->key(Key::LCTRL))
            {
                MouseSystem::instance()->button(MouseButton::Right, left);
            }
		    else
		    {
			    MouseSystem::instance()->button(MouseButton::Left, left);
		    }

	    }

        // Timing management
        Timing::update(SDL_GetTicks() / 1000.0f);

        // Notify all managers about a new frame
        Systems::instance()->update();

        // Render everything
        RenderingSystem::instance()->_render();

//        SDL_Delay(1);

        /* Swap our back buffer to the front */
//        SDL_GL_SwapWindow(handle);
    }

	Systems::instance()->destroy();

	/* Delete our opengl context, destroy our window, and shutdown SDL */
	SDL_GL_DeleteContext(sdl_context);
	SDL_DestroyWindow(handle);
	SDL_Quit();
}

extern "C" int _main_(int _argc, char** _argv);

#undef main

int main(int argc, char** argv)
{
    return _main_(argc, argv);
}