#include "RenderingSystem.h"

#include <bgfx/bgfx.h>
#include <Shader.h>
#include <Texture.h>
#include "AttributeMapping.h"

#include "VertexDecl.h"
#include "RenderMesh.h"
#include "RenderContext.h"

#include "WindowSystem.h"

using namespace bv;
using namespace bv::rendering;

std::vector<bgfx::TextureHandle> _texHandles;

std::vector<bgfx::DynamicVertexBufferHandle> _vbos;
std::vector<bgfx::IndexBufferHandle> _ibos;

std::vector<bgfx::FrameBufferHandle> _frameBuffers;
std::vector<std::vector<bgfx::TextureHandle>> _frameBufferTextures;

// all vertex declarations
extern std::vector<bgfx::VertexDecl> _decls;

// list of uniforms // found in Shader.cpp
extern std::vector<bgfx::UniformHandle> _uniformHandles;
// list of shader programs // found in Shader.cpp
extern std::vector<bgfx::ProgramHandle> _programHandles;

static uint16_t RESET = BGFX_RESET_VSYNC | BGFX_RESET_MSAA_X8;
static uint16_t DEBUG = BGFX_DEBUG_TEXT;

void RenderingSystem::init()
{
	// specify the rendering backend to use
	bgfx::init(bgfx::RendererType::OpenGLES);

	int width = WindowSystem::instance()->width();
	int height = WindowSystem::instance()->height();

	// reset the viewport
	bgfx::reset(width, height, RESET);

	// Enable debug text.
	bgfx::setDebug(DEBUG);

	// Set view 0 default viewport.
	bgfx::setViewRect(0, 0, 0, (uint16_t) width, (uint16_t) height);

	Shader::require("normals");
}

void RenderingSystem::destroy()
{
	SystemBase::destroy();

    // shutdown everything
    bgfx::shutdown();
}

static uint8_t _activeView = 0;

void RenderingSystem::setView(const glm::mat4& view, const glm::mat4& proj, int viewIdx)
{
	bgfx::setViewTransform(viewIdx, &view, &proj);
}

void RenderingSystem::setViewSize(int x, int y, int width, int height)
{
	bgfx::setViewRect(0, x, y, (uint16_t)width, (uint16_t)height);

	// Set view 0 clear state.
	bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.0f, 0);

	uint16_t reset = BGFX_RESET_VSYNC | BGFX_RESET_MSAA_X8;
	bgfx::reset(width, height, reset);
}

void RenderingSystem::render(RenderMesh::Id handle, Shader::Id shader)
{
	if(!handle) return;

	// use the vertex buffer
	auto vbo = _vbos[handle.id];
	bgfx::setVertexBuffer(vbo);

	// check validity and use the valid ibo
	auto ibo = _ibos[handle.id];
	if(bgfx::isValid(ibo))
	{
		bgfx::setIndexBuffer(ibo);
	}

	auto shaderId = _programHandles[(int)shader];

	bgfx::submit(_activeView, shaderId);
}

void RenderingSystem::apply(RenderState state)
{
	uint64_t bit = 0;

	switch(state.culling)
	{
		case Culling::CW: bit |= BGFX_STATE_CULL_CW; break;
		case Culling::CCW: bit |= BGFX_STATE_CULL_CCW; break;
		default: break;
	}

	switch(state.type)
	{
		case RenderMode::LINE_STRIP: bit |= BGFX_STATE_PT_LINESTRIP; break;
		case RenderMode::TRISTRIP: bit |= BGFX_STATE_PT_TRISTRIP; break;
		case RenderMode::LINES: bit |= BGFX_STATE_PT_LINES; break;
		case RenderMode::POINTS: bit |= BGFX_STATE_PT_POINTS | BGFX_STATE_POINT_SIZE(5); break;
		default: break;
	}

	switch(state.test)
	{
		case DepthTest::NORMAL: bit |= BGFX_STATE_DEPTH_TEST_LESS | BGFX_STATE_DEPTH_WRITE; break;
		case DepthTest::ALWAYS: bit |= BGFX_STATE_DEPTH_TEST_ALWAYS | BGFX_STATE_DEPTH_WRITE; break;
	}

	bgfx::setState(0
	               | BGFX_STATE_RGB_WRITE
	               | BGFX_STATE_ALPHA_WRITE
	               | BGFX_STATE_MSAA
	               | bit
	);
}

void RenderingSystem::apply(const glm::mat4& xform)
{
	bgfx::setTransform(&xform);
}

void RenderingSystem::apply(const std::map<Uniform::Id, UniformData>& uniforms)
{
    for (auto &&pair : uniforms)
    {
        apply(pair.first, pair.second);
    }
}

void RenderingSystem::apply(Uniform::Id uniform, const UniformData& data)
{
    bgfx::setUniform(_uniformHandles[(int)uniform.id], &data[0], 1);
}


void RenderingSystem::_render()
{
    // submit an empty call to clean the view anyways
    bgfx::touch(0);

	m_onPreRender();

	for (uint16_t i = 0; i < RenderContext::count(); ++i)
	{
		auto context = RenderContext::get(i);

		if(!context->active()) continue;

        _activeView = context->buffer().id + 1;

        // switch to the appropriate frame buffer
		bindFrameBuffer(context->buffer());

		// setup camera transformation
		setView(context->camera()->view(), context->camera()->proj(), _activeView);

		// inform all subscribers that it is alright to render things
		context->render();

		// sort submissions for better performance
		std::sort(m_submissions.begin(), m_submissions.end(), [] (const RenderSubmission& sub1, const RenderSubmission& sub2)
		{
			return sub1.priority > sub2.priority;
		});

		for (auto &&submission : m_submissions)
		{
			apply(submission);

			render(submission.mesh, submission.shader);
		}

		m_submissions.clear();
	}

	// Advance to next frame. Rendering thread will be kicked to
	// process submitted rendering primitives.
	bgfx::frame();
}

// create an empty vbo placeholder
void RenderingSystem::createVbo()
{
	_vbos.emplace_back();
	_vbos.back().idx = bgfx::invalidHandle;
}

void RenderingSystem::upload(RenderMesh::Id handle, const void *data, size_t sizeInBytes, int declId)
{
	auto decl = _decls[declId];

	auto vbo = bgfx::createDynamicVertexBuffer(bgfx::copy(data, sizeInBytes), decl);

	_vbos[handle.id] = vbo;
}

void RenderingSystem::update(RenderMesh::Id handle, const void *data, size_t sizeInBytes)
{
    assert(handle.id < _vbos.size());
    bgfx::updateDynamicVertexBuffer(_vbos[handle.id], 0, bgfx::copy(data, sizeInBytes));
}

void RenderingSystem::uploadIndices(RenderMesh::Id handle, const void *data, size_t sizeInBytes)
{
	// create an index buffer
	auto ibo = bgfx::createIndexBuffer(bgfx::copy(data, sizeInBytes), BGFX_BUFFER_INDEX32);

	// make sure we have allocated space
	assert(handle.id < _ibos.size());

	// remeber our index buffer object handle
	_ibos[handle.id] = ibo;
}

// create an entpy ibo placeholder
void RenderingSystem::createIbo()
{
	// initially it is invalid
	_ibos.emplace_back();
    _ibos.back().idx = bgfx::invalidHandle;
}

void RenderingSystem::destroy(RenderMesh::Id handle)
{
	assert(handle.id < _ibos.size());

    if(bgfx::isValid(_vbos[handle.id]))
    {
        bgfx::destroyDynamicVertexBuffer(_vbos[handle.id]);
	    _vbos[handle.id].idx = bgfx::invalidHandle;
    }

    if(bgfx::isValid(_ibos[handle.id]))
    {
        bgfx::destroyIndexBuffer(_ibos[handle.id]);
	    _ibos[handle.id].idx = bgfx::invalidHandle;
    }
}


// render a mesh with the specified id
//void RenderingSystem::render(Mesh::Id handle)
//{
//    render(RenderMesh::create(handle));
//}


// check if a vbo for the given render mesh exists
bool RenderingSystem::vboExists(RenderMesh::Id id)
{
	// TODO: should probably run a search through the list
	return id.id < _vbos.size();
}

// a simple mapping between mc and bgfx texture types
bgfx::TextureFormat::Enum convert(TextureType type)
{
	switch(type)
	{
		case TextureType::R8: return bgfx::TextureFormat::R8;
		case TextureType::R16: return bgfx::TextureFormat::R16;
		case TextureType::RGBA8: return bgfx::TextureFormat::RGBA8;
	}
}

Texture::Id RenderingSystem::createTexture2D(TextureType type, uint16_t width, uint16_t height)
{
	bgfx::TextureHandle textureHandle = bgfx::createTexture2D(width, height, 0, convert(type));

	Texture::Id id = Texture::Id(_texHandles.size());
	_texHandles.push_back(textureHandle);

	return id;
}

Texture::Id RenderingSystem::createTexture3D(rendering::TextureType type, uint16_t width, uint16_t height, uint16_t depth)
{
	bgfx::TextureHandle texHandle = bgfx::createTexture3D(width, height, depth, 0, convert(type));

	Texture::Id id = Texture::Id(_texHandles.size());
	_texHandles.push_back(texHandle);

	return id;
}

void RenderingSystem::updateTexture2D(rendering::Texture::Id textureId, const void *data)
{
	auto texDims = Texture::size(textureId);

	size_t size = Texture::sizeInBytes(textureId);

	bgfx::TextureHandle textureHandle = _texHandles[(int) textureId];
	bgfx::updateTexture2D(textureHandle, 0, 0, 0, (uint16_t) texDims.x, (uint16_t) texDims.y,
						  bgfx::copy(data, size));
}

void RenderingSystem::updateTexture3D(rendering::Texture::Id textureId, const void *data)
{
	auto texDims = Texture::size(textureId);

	size_t size = Texture::sizeInBytes(textureId);

	bgfx::TextureHandle textureHandle = _texHandles[(int) textureId];
	bgfx::updateTexture3D(textureHandle, 0, 0, 0, 0, (uint16_t) texDims.x, (uint16_t) texDims.y, (uint16_t) texDims.z,
						  bgfx::copy(data, size));
}

void RenderingSystem::bindTexture(rendering::Texture::Id textureId, Uniform::Id uniformId, uint8_t texUnit)
{
	bgfx::setTexture(texUnit, _uniformHandles[(int)uniformId], _texHandles[(int)textureId]);
}

void RenderingSystem::createFrameBuffer(glm::ivec2 size)
{
	std::vector<bgfx::TextureHandle> textures(2);
	textures[0] = bgfx::createTexture2D(size.x, size.y, 1, bgfx::TextureFormat::RGBA8, BGFX_TEXTURE_RT); // color
	textures[1] = bgfx::createTexture2D(size.x, size.y, 1, bgfx::TextureFormat::D32,   BGFX_TEXTURE_RT); // depth
	_frameBufferTextures.push_back(textures);

	auto fbh = bgfx::createFrameBuffer(textures.size(), &textures[0], true);
	_frameBuffers.push_back(fbh);
}

void RenderingSystem::bindFrameBuffer(rendering::FrameBuffer::Id fb)
{
    bgfx::touch(_activeView);

    bgfx::setViewFrameBuffer(_activeView, _frameBuffers[_activeView - 1]);

    // Set view 0 clear state.
	bgfx::setViewClear(_activeView, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x404040ff, 1.0f, 0);

    bgfx::setViewRect(_activeView, 0, 0, fb->size().x, fb->size().y);

	bgfx::setViewSeq(_activeView, true);
}

void RenderingSystem::apply(RenderSubmission &submission)
{
	apply(submission.xform);
	apply(submission.state);
	apply(submission.uniforms);
}
