#include "Shader.h"

#include <bgfx/bgfx.h>
#include <unordered_map>
#include <FileReader.h>
#include <string.h>
#include <RenderingSystem.h>

using namespace bv;
using namespace bv::rendering;

using ShaderMapType = std::unordered_map<std::string, Shader>;
using ShaderMapValueType = ShaderMapType::value_type;

// shader storage
ShaderMapType _shaders;

std::vector<bgfx::ProgramHandle> _programHandles;

// uniform storage
std::vector<bgfx::UniformHandle> _uniformHandles;

Shader::Id _current;

std::string filename(const char* name)
{
    char filePath[512];

    const char* shaderPath = "shaders/dx9/";

    switch (bgfx::getRendererType() )
    {
        case bgfx::RendererType::Direct3D11:
        case bgfx::RendererType::Direct3D12:
            shaderPath = "shaders/dx11/";
            break;

        case bgfx::RendererType::OpenGL:
            shaderPath = "shaders/glsl/";
            break;

        case bgfx::RendererType::OpenGLES:
            shaderPath = "shaders/gles/";
            break;
        default:
            break;
    }

    strcpy(filePath, shaderPath);
    strcat(filePath, name);
    strcat(filePath, ".bin");

    return std::string(filePath);
}

const bgfx::Memory* memory(const std::string& source)
{
    int size = source.length();

    const bgfx::Memory* mem = bgfx::alloc(size+1);
    memcpy(mem->data, source.c_str(), size);
    mem->data[mem->size-1] = '\0';

    return mem;
}

Handle<Uniform> Shader::uniform(const std::string& name, UniformType type)
{
    return Uniform::require(m_id, name, type);
}

Shader::Id Shader::require(const std::string &name)
{
    auto found = NamedStorableBase::find(name);

    if(!found)
    {
        std::string vs_name = std::string("vs_") + name;
        std::string fs_name = std::string("fs_") + name;

		auto createShader = [=]
        {
            Log::writeln("Building shader %s", name.c_str());

            std::string vs_source = FileReader::read(filename(vs_name.c_str()));
            std::string fs_source = FileReader::read(filename(fs_name.c_str()));

            auto vsh = bgfx::createShader(memory(vs_source));
            auto fsh = bgfx::createShader(memory(fs_source));

            bgfx::ProgramHandle program = bgfx::createProgram(vsh, fsh, true /* destroy shaders when program is destroyed */);

            if(!bgfx::isValid(program))
            {
                Log::writeln("Error building shader!");
            }

            _programHandles.push_back(program);
        };

        RenderingSystem::instance()->onload(createShader);

        found = Shader::create(name);
    }

    return found;
}

Shader::Id Shader::use(const std::string& name)
{
    return Shader::use(Shader::find(name));
}

Shader::Id Shader::requse(const std::string& name)
{
	return Shader::use(Shader::require(name));
}

Shader::Id Shader::use(Shader::Id id)
{
    // set the bound shader as current
    _current = id;

    return id;
}

Shader::Id Shader::current()
{
    return _current;
}

/* ********
 * UNIFORMS
 * ******** */
bgfx::UniformType::Enum convert(UniformType type)
{
    switch(type)
    {
        case UniformType::Int: return bgfx::UniformType::Int1;
        case UniformType::Float: return bgfx::UniformType::Int1;
        case UniformType::Vec3: return bgfx::UniformType::Vec4;
        case UniformType::Vec4: return bgfx::UniformType::Vec4;
        case UniformType::Mat4: return bgfx::UniformType::Mat4;
    }
}

Uniform::Id Uniform::require(Shader::Id shaderId, const std::string &name, UniformType type)
{
    std::string fullname = std::to_string(shaderId.id) + ":" + name;

    auto found = find(fullname);
    if(!found)
    {
        bgfx::UniformHandle uniform = bgfx::createUniform(name.c_str(), convert(type));
        _uniformHandles.push_back(uniform);
        found = Uniform::create( fullname, shaderId, type );
    }
    return found;
}

void Uniform::check(Uniform::Id id, const glm::mat4 &value)
{
	assert(id->m_type == UniformType::Mat4);
//	memcpy(id->m_value, &value, sizeof(value));
}

void Uniform::check(Uniform::Id id, const glm::vec4 &value)
{
	assert(id->m_type == UniformType::Vec4);
//	memcpy(id->m_value, &value, sizeof(value));
}

void Uniform::check(Uniform::Id id, const glm::vec3 &value)
{
	assert(id->m_type == UniformType::Vec3);
//	memcpy(id->m_value, &value, sizeof(value));
}

void Uniform::check(Uniform::Id id, float value)
{
	assert(id->m_type == UniformType::Float);
//	memcpy(id->m_value, &value, sizeof(value));
}

void Uniform::check(Uniform::Id id, int value)
{
	assert(id->m_type == UniformType::Int);
//	memcpy(id->m_value, &value, sizeof(value));
}
