#include "VertexDecl.h"
#include "RenderingSystem.h"
#include "AttributeMapping.h"

#include <bgfx/bgfx.h>

using namespace bv;
using namespace rendering;

std::vector<bgfx::VertexDecl> _decls;

// maps our semantics to BGFX's
static bgfx::Attrib::Enum _semantics(AttribSemantics type);

// maps our types to BGFX's
static bgfx::AttribType::Enum type(AttribType type);

// start vertex declaration at index
void vertexdecl::begin(int index)
{
	if (index == _decls.size())
	{
		_decls.emplace_back();
	}
	_decls[index].begin();
}

// add an attribute to the vertex decalration at index
void vertexdecl::add(int index, AttribSemantics sem, uint8_t size, AttribType attribType)
{
	_decls[index].add(_semantics(sem), size, type(attribType));
}

// ends vertex decalration at index
void vertexdecl::end(int index)
{
	_decls[index].end();
}

// function to map our vertex att types to BGFX's
static bgfx::Attrib::Enum _semantics(AttribSemantics type)
{
	switch (type)
	{
		case AttribSemantics::POS: return bgfx::Attrib::Position;
		case AttribSemantics::NOR: return bgfx::Attrib::Normal;
		case AttribSemantics::TEX0: return bgfx::Attrib::TexCoord0;
		case AttribSemantics::TEX1: return bgfx::Attrib::TexCoord1;
		case AttribSemantics::TEX2: return bgfx::Attrib::TexCoord2;
		case AttribSemantics::COL0: return bgfx::Attrib::Color0;
		case AttribSemantics::COL1: return bgfx::Attrib::Color1;
	}
}

static bgfx::AttribType::Enum type(AttribType type)
{
	switch(type)
	{
		case AttribType::HALF: return bgfx::AttribType::Half;
		case AttribType::INT: return bgfx::AttribType::Int16;
		case AttribType::FLOAT: return bgfx::AttribType::Float;
		default: throw std::runtime_error("Bad attribute type requested!");
	}
}


