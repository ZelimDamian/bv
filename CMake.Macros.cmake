
######### Find platform ############
### Making really sure we are dealing with an Apple
unset(APPLE)

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    # Mac OS X specific code
    set(APPLE TRUE)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")

# setup the target

######### Find platform ############
### Making really sure we are dealing with an Apple
unset(APPLE)

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    # Mac OS X specific code
    set(APPLE TRUE)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")


macro(SETUP target)
    SET(target ${target})
    SET(INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/include ${EXTRA_INCLUDES})
    SET(${target}_INCLUDE_DIR ${INCLUDE_DIRS} PARENT_SCOPE)
    SET(LIB_PATH lib/${OS}/${ARCH})
    UNSET(LIBS)
    UNSET(SOURCE_FILES)
endmacro()

# link to an external library with a CMakeLists.txt file at "path" with name "to"
macro(LINK_TO path to)
    ADD_SUBDIRECTORY(${path} ${to})
    SET(LIBS ${LIBS} ${to})
    SET(LIBS ${LIBS} ${${to}_LIBS})
    SET(INCLUDE_DIRS ${INCLUDE_DIRS} ${${to}_INCLUDE_DIR})
    SET(LIB_PATH ${LIB_PATH} ${${to}_LIB_PATH})
endmacro()

# link to a bv_{whatever} project
macro(LINK to)
    LINK_TO(../bv_${to} bv_${to})
endmacro()

# bootstrap executable target by adding a target with name {target} and specifiying include dirs and libs
macro(BOOTSTRAP_EXE target)
    if(TARGET ${target})
    else()
        MESSAGE(WARNING "Bootstrapping executable - ${target}")
        MESSAGE(WARNING "INCLUDE_DIRS ${INCLUDE_DIRS}")
        MESSAGE(WARNING "LIBS ${LIBS}")
        INCLUDE_DIRECTORIES(${INCLUDE_DIRS})
        ADD_EXECUTABLE(${target} ${SOURCE_FILES})
        LINK_DIRECTORIES(${LIB_PATH})
        TARGET_LINK_LIBRARIES(${target} ${LIBS})
    endif()
endmacro()

# make the target library avaiable for all other linking projects
macro(EXPORT target)
    set(${target}_LIB_PATH ${LIB_PATH} PARENT_SCOPE)
    set(${target}_LIBS ${LIBS} PARENT_SCOPE)
endmacro()

# bootstrap the target by adding a library with name {target} and specifiying include dirs and libs
macro(BOOTSTRAP target)
    if(TARGET ${target})
    else()
        MESSAGE(WARNING "Bootstrapping ${target}")
        MESSAGE(WARNING "INCLUDE_DIRS ${INCLUDE_DIRS}")
        MESSAGE(WARNING "LIBS ${LIBS}")
        INCLUDE_DIRECTORIES(${INCLUDE_DIRS})
        ADD_LIBRARY(${target} STATIC ${SOURCE_FILES})
        TARGET_LINK_LIBRARIES(${target} ${LIBS})
        EXPORT(${target})
    endif()
endmacro()


macro(SETUP_EXE)
    # the application needs to be executed in the runtime directory
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")
    SET(INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/include ${EXTRA_INCLUDES})

    if(APPLE)
        find_library(COCOA_LIB Cocoa)
        find_library(IOKIT_LIB IOKit)
        find_library(QUARTZ_LIB QuartzCore)
        #SET(METAL_LIB Metal.framework)
        set(LIBS ${LIBS} ${COCOA_LIB} ${IOKIT_LIB} ${QUARTZ_LIB})
    elseif(WIN32)
        if(MINGW)
            find_library(PSAPI_LIB Psapi)
            find_library(GDI32_LIB GDI32)
            set(LIBS ${LIBS} ${PSAPI_LIB} ${GDI32_LIB})
            set(CMAKE_EXE_LINKER_FLAGS "-static -static-libgcc -static-libstdc++")
        elseif(MSVC)
            set(LIBS ${LIBS} delayimp.lib gdi32.lib psapi.lib)
        endif()
    endif()
endmacro()

macro(ADD_HEADER name)
    SET(SOURCE_FILES ${SOURCE_FILES} include/${name}.h)
endmacro()

macro(ADD_SOURCE name)
    SET(SOURCE_FILES ${SOURCE_FILES} src/${name}.cpp)
endmacro()

macro(ADD_CLASS name)
    ADD_HEADER(${name})
    ADD_SOURCE(${name})
endmacro()

