SETUP(bv_igl)

ADD_CLASS(MeshSplitter)
ADD_CLASS(Bounding.Eigen)

SET(INCLUDE_DIRS ${INCLUDE_DIRS} ext/eigen)
SET(INCLUDE_DIRS ${INCLUDE_DIRS} ext/libigl/include)

LINK(core)

BOOTSTRAP(${target})
