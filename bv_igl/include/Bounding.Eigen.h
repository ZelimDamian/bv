#pragma once

#include "Box.h"
#include <vector>

namespace bv
{
	namespace Bounding
	{
		Obb obb_cov(const VectorVec3 &points, const std::vector<uint32_t> &indices);
	}
}