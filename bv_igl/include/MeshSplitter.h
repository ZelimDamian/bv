#pragma once

#include "Mesh.h"

namespace bv
{
	namespace geom
	{
		class MeshSplitter
		{
		public:
			static std::vector<Mesh::Id> splitDisjoint(const Mesh::Id mesh);
		};
	}
}