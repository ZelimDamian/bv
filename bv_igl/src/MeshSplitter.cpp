#include <igl/components.h>

#include "MeshSplitter.h"
#include <MeshUtilities.h>

using namespace bv;
using namespace geom;

void convert(Mesh::Id mesh, Eigen::MatrixXi& F, Eigen::MatrixXd& V)
{
	V.resize(mesh->positions.size(), 3);

	for (int i = 0; i < mesh->positions.size(); ++i)
	{
		auto pos = mesh->positions[i];

		V(i, 0) = pos.x;
		V(i, 1) = pos.y;
		V(i, 2) = pos.z;
	}

	F.resize(mesh->triangles.size(), 3);

	for (int i = 0; i < mesh->triangles.size(); ++i)
	{
		auto tri = mesh->tri(i);

		F(i, 0) = tri.vi[0];
		F(i, 1) = tri.vi[1];
		F(i, 2) = tri.vi[2];
	}
}

std::vector<Mesh::Id> MeshSplitter::splitDisjoint(const Mesh::Id mesh)
{
	Eigen::MatrixXi F;
	Eigen::MatrixXd V;

	convert(mesh, F, V);

	Eigen::VectorXi CC;

	igl::components(F, CC);

	std::vector<Mesh::Id> components(CC.maxCoeff() + 1);

	std::vector<std::vector<Tri>> faces(components.size());

	for (int i = 0; i < mesh->positions.size(); i++)
	{
		int idx = CC(i);

		for (int j = 0; j < mesh->triangles.size(); ++j)
		{
			auto tri = mesh->tri(j);
			if(tri.hasVertexWithId(i))
			{
				faces[idx].push_back(tri);
			}
		}
	}

	for (int i = 0; i < components.size(); ++i)
	{
		components[i] = Mesh::create();

		components[i]->positions = mesh->positions;

		struct TriHasher
		{
			inline size_t operator ()(const Tri& key) const
			{
				std::hash<int> hashInt;
				return hashInt(key.index);
			}
		};

		bv::filterUnique<Tri, TriHasher>(faces[i]);

		components[i]->triangles = faces[i];

		// remove vertices not references by triangles
		MeshUtilities::pruneVertices(components[i]);

		components[i]->updateVertices();
		components[i]->updateIndices();
		components[i]->updateBox();
	}

	return components;
}

