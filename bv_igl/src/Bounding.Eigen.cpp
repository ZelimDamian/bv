#include "Bounding.Eigen.h"


#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

using namespace bv;

Obb build_from_points(const std::vector<glm::vec3> &pnts );
Obb build_from_covariance_matrix( const Eigen::Matrix3f &C, const std::vector<glm::vec3> &pnts );

Obb Bounding::obb_cov(const VectorVec3& points, const std::vector<uint32_t>& indices)
{
	return build_from_points(points);

	float Ai, Am=0.0;
	glm::vec3 mu(0.0f, 0.0f, 0.0f), mui;
	Eigen::Matrix3f C;
	float cxx=0.0, cxy=0.0, cxz=0.0, cyy=0.0, cyz=0.0, czz=0.0;

	// loop over the triangles this time to find the
	// mean location
	for( int i=0; i<(int)indices.size(); i+=3 )
	{
		auto &p = points[indices[i+0]];
		auto &q = points[indices[i+1]];
		auto &r = points[indices[i+2]];
		mui = (p+q+r)/3.0f;
		auto c = glm::cross(q-p, r-p);
		Ai = glm::length(c) / 2.0f;
		mu += mui*Ai;
		Am += Ai;

		// these bits set the c terms to Am*E[xx], Am*E[xy], Am*E[xz]....
		cxx += ( 9.0*mui.x*mui.x + p.x*p.x + q.x*q.x + r.x*r.x )*(Ai/12.0);
		cxy += ( 9.0*mui.x*mui.y + p.x*p.y + q.x*q.y + r.x*r.y )*(Ai/12.0);
		cxz += ( 9.0*mui.x*mui.z + p.x*p.z + q.x*q.z + r.x*r.z )*(Ai/12.0);
		cyy += ( 9.0*mui.y*mui.y + p.y*p.y + q.y*q.y + r.y*r.y )*(Ai/12.0);
		cyz += ( 9.0*mui.y*mui.z + p.y*p.z + q.y*q.z + r.y*r.z )*(Ai/12.0);
	}

	// divide out the Am fraction from the average position and
	// covariance terms
	mu /= Am;
	cxx /= Am; cxy /= Am; cxz /= Am; cyy /= Am; cyz /= Am; czz /= Am;

	// now subtract off the E[x]*E[x], E[x]*E[y], ... terms
	cxx -= mu.x*mu.x; cxy -= mu.x*mu.y; cxz -= mu.x*mu.z;
	cyy -= mu.y*mu.y; cyz -= mu.y*mu.z; czz -= mu.z*mu.z;

	// now build the covariance matrix
	C(0,0)=cxx; C(0,1)=cxy; C(0,2)=cxz;
	C(1,0)=cxy; C(1,1)=cyy; C(1,2)=cyz;
	C(2,0)=cxz; C(1,2)=cyz; C(2,2)=czz;

	// set the obb parameters from the covariance matrix
	return build_from_covariance_matrix( C, points );
}

// method to set the OBB parameters which produce a box oriented according to
// the covariance matrix C, which just containts the points pnts
Obb build_from_covariance_matrix( const Eigen::Matrix3f &C, const std::vector<glm::vec3> &pnts )
{
	using namespace Eigen;

	// extract the eigenvalues and eigenvectors from C
	EigenSolver<MatrixXf> es(C, true);

	auto eigvec = es.eigenvectors();
//	auto eigvec = es.pseudoEigenvectors();

	// find the right, up and forward vectors from the eigenvectors
	glm::vec3 r( std::real(eigvec(0,0)), std::real(eigvec(1,0)), std::real(eigvec(2,0)) );
	glm::vec3 u( std::real(eigvec(0,1)), std::real(eigvec(1,1)), std::real(eigvec(2,1)) );
	glm::vec3 f( std::real(eigvec(0,2)), std::real(eigvec(1,2)), std::real(eigvec(2,2)) );

    r = glm::normalize(r);
    u = glm::normalize(u);
    f = glm::normalize(f);

	glm::mat3 rot {r, u, f};

	// now build the bounding box extents in the rotated frame
	glm::vec3 minim(1e10, 1e10, 1e10), maxim(-1e10, -1e10, -1e10);
	for( int i=0; i<(int)pnts.size(); i++ )
    {
		glm::vec3 p_prime = glm::transpose(rot) * pnts[i];
//		glm::vec3 p_prime( glm::dot(r, pnts[i]), glm::dot(u, pnts[i]), glm::dot(f, pnts[i]));
		minim = glm::min(minim, p_prime);
		maxim = glm::max(maxim, p_prime);
	}

	// set the center of the OBB to be the average of the
	// minimum and maximum, and the extents be half of the
	// difference between the minimum and maximum
//	auto center = (maxim+minim)*0.5f;
//	auto pos = glm::vec3( glm::dot(rot[0], center), glm::dot(rot[1], center), glm::dot(rot[2], center));
//	auto pos = rot * center;

//	auto translation = glm::translate(glm::mat4(), pos);

	return Obb { { minim, maxim }, glm::mat4(rot) };
}

//// constructs the corner of the aligned bounding box
//// in world space
//void get_bounding_box( Vec3 *p ){
//	Vec3 r( m_rot[0][0], m_rot[1][0], m_rot[2][0] );
//	Vec3 u( m_rot[0][1], m_rot[1][1], m_rot[2][1] );
//	Vec3 f( m_rot[0][2], m_rot[1][2], m_rot[2][2] );
//	p[0] = m_pos - r*m_ext[0] - u*m_ext[1] - f*m_ext[2];
//	p[1] = m_pos + r*m_ext[0] - u*m_ext[1] - f*m_ext[2];
//	p[2] = m_pos + r*m_ext[0] - u*m_ext[1] + f*m_ext[2];
//	p[3] = m_pos - r*m_ext[0] - u*m_ext[1] + f*m_ext[2];
//	p[4] = m_pos - r*m_ext[0] + u*m_ext[1] - f*m_ext[2];
//	p[5] = m_pos + r*m_ext[0] + u*m_ext[1] - f*m_ext[2];
//	p[6] = m_pos + r*m_ext[0] + u*m_ext[1] + f*m_ext[2];
//	p[7] = m_pos - r*m_ext[0] + u*m_ext[1] + f*m_ext[2];
//}

// build an OBB from a vector of input points.  This
// method just forms the covariance matrix and hands
// it to the build_from_covariance_matrix() method
// which handles fitting the box to the points
Obb build_from_points(const std::vector<glm::vec3> &pnts )
{
	glm::vec3 mu(0.0, 0.0, 0.0);
	Eigen::Matrix3f C;

	// loop over the points to find the mean point
	// location
	for( int i=0; i<(int)pnts.size(); i++ )
    {
		mu += pnts[i]/float(pnts.size());
	}

	// loop over the points again to build the
	// covariance matrix.  Note that we only have
	// to build terms for the upper trianglular
	// portion since the matrix is symmetric
	float cxx=0.0, cxy=0.0, cxz=0.0, cyy=0.0, cyz=0.0, czz=0.0;
	for( int i=0; i<(int)pnts.size(); i++ )
    {
		auto &p = pnts[i];
		cxx += p.x*p.x - mu.x*mu.x;
		cxy += p.x*p.y - mu.x*mu.y;
		cxz += p.x*p.z - mu.x*mu.z;
		cyy += p.y*p.y - mu.y*mu.y;
		cyz += p.y*p.z - mu.y*mu.z;
		czz += p.z*p.z - mu.z*mu.z;
	}

	// now build the covariance matrix
	C(0, 0) = cxx; C(0, 1) = cxy; C(0, 2) = cxz;
	C(1, 0) = cxy; C(1, 1) = cyy; C(1, 2) = cyz;
	C(2, 0) = cxz; C(2, 1) = cyz; C(2, 2) = czz;

	// set the OBB parameters from the covariance matrix
	return build_from_covariance_matrix( C, pnts );
}
